//
//  DetailViewController.h
//  Fitstrap
//
//  Created by Dennis Kung on 4/16/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

