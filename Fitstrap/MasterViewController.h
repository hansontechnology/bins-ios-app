//
//  MasterViewController.h
//  Fitstrap
//
//  Created by Dennis Kung on 4/16/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;

@interface MasterViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;


@end

