//
//  main.m
//  fitstrap
//
//  Created by Dennis Kung on 4/16/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
