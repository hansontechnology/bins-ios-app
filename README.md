# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* iOS app for BiNS wearble device
* Version : Latest up to Jan. 2016
* BiNS is a product and service to combind wristband wearable and mobule app and cloud services to monitor user's health
* This iOS app connects to wearable device through Bluetooth LE, and exchange activity data / settings.
* Product document can be found from [here](/bins-documents/BiNS-Z3-Users-Manual-v2-150831.pdf), and [others](/bins-documents).

![BiNS Z3 Product](/BiNS-Z3-product-photos/BiNS-Z3-photo-1.JPG)

* Other product photos can be found [here](/BiNS-Z3-product-photos)

![BiNS App for iOS](/bins-app-ios-screenshots/bins-app-ios-1.png)

* Screenshots of mobile app for Android is located [here](/bins-app-android-screenshots)
* Screenshots of mobile app for iOS is located [here](/bins-app-ios-screenshots)

### How do I get set up? ###

* Xcode is required to build this app


### Contribution guidelines ###

* Read together with embedded part of the project can help understand the flow of works.

### Who do I talk to? ###

* Please contact denniskung68@hotmail.com for any suggestions.
