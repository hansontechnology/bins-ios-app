//
//  main.m
//  ULB
//
//  Created by Dennis Kung on 11/8/15.
//  Copyright © 2015 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
