//
//  ActivityTableViewController.h
//  bins
//
//  Created by Dennis Kung on 10/25/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeControllerView.h"
#import "CorePlot-CocoaTouch.h"
#import "BinsDataViewController.h"
#import "MIRadioButtonGroup.h"
#import "SWRevealViewController.h"


@interface ActivityTableViewController : UITableViewController <TimeControllerViewDelegate,CPTPlotDataSource, CPTPlotSpaceDelegate,RadioButtonGroupDelegate, SWRevealViewControllerDelegate, BinsDataControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *activityDisplayView;

@property (weak, nonatomic) IBOutlet UITableViewCell *activityGraphView;
@property (weak, nonatomic) IBOutlet UIView *activityGraphContentView;


@property (weak, nonatomic) IBOutlet TimeControllerView *timeControllerView;
@property BinsDataViewController *binsDataController;


- (IBAction)leftButtonClick:(id)sender;
- (IBAction)rightButtonClick:(id)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITableViewCell *dateSelectViewCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *datePickerTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *displayModeTableCell;

- (IBAction)dateSelectPickerValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *dateUnitSelectView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *userSelectButton;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizerRight;
@property (weak, nonatomic) IBOutlet UITableViewCell *timeTableCell;
@property (strong, nonatomic) IBOutlet UITableView *activityTableView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *workingIndicator;

@end
