//
//  ActivityTableViewController.m
//  bins
//
//  Created by Dennis Kung on 10/25/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "ActivityTableViewController.h"

@interface ActivityTableViewController ()

@property (nonatomic, strong) CPTGraphHostingView *hostView;
@property (nonatomic, strong) CPTTheme *selectedTheme;

@end

@implementation ActivityTableViewController

@synthesize hostView = hostView_;
@synthesize selectedTheme = selectedTheme_;
@synthesize timeControllerView;
@synthesize binsDataController;
@synthesize workingIndicator;

#define INTEGER_MAX 0x7fffffff

#define AXIS_COUNT_X 62
#define AXIS_COUNT_Y 10
int axisCountX[] = { 48, 62, 60}; // must follow the order

int mMaxValue = 0;
int mMinValue = INTEGER_MAX;
int mMaxIndex = 0;
int mMinIndex = 0;

int mTotalSteps = 0;


int mHeartRateMaxValue = 0;
int mHeartRateMinValue = INTEGER_MAX;

int mValues[AXIS_COUNT_X];
int mHeartRateValues[AXIS_COUNT_X];
int mHeartRateLowValues[AXIS_COUNT_X];

int mViewMode=VIEW_MODE_UNIT_DAY;
int mPageUnit=0;
int mStartViewHour=0;

NSString *mPulseTitleView=nil;

NSArray *dayLabels;
NSArray *monthLabels;

NSDictionary *data;
NSDictionary *sets;
NSArray *dates;

#define HEIGHT_IPHONE5 568 // 12.22


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    timeControllerView.delegate = self;
    
    binsDataController=[[BinsDataViewController alloc] init];

    // dk for test
    [self generateData];
    
    [self configureHost];
    
    mViewMode=[timeControllerView getBrowseUnit];

        
     // self.datePicker.backgroundColor=COLOR_TIME_PICKER_NEW;

   
    [timeControllerView setUserInteractionEnabled:YES];
    
    self.dateSelectViewCell.hidden=YES;
    
    NSArray *options =[[NSArray alloc]initWithObjects:[NSString localizedStringWithFormat:NSLocalizedString(@"page_day_view",nil) ],[NSString localizedStringWithFormat:NSLocalizedString(@"page_month_view",nil) ],nil];
    MIRadioButtonGroup *group =[[MIRadioButtonGroup alloc]initWithFrame:CGRectMake(0, 0, 200, 44) andOptions:options andColumns:2];
    [self.dateUnitSelectView addSubview:group];
    
    [group setSelected:mViewMode];
    
    group.delegate=self;
    
    
    
    // Change button color
    _sidebarButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    //-- [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // Change button color
    _userSelectButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _userSelectButton.target = self.revealViewController;
    _userSelectButton.action = @selector(rightRevealToggle:);

    
    NSString *leftArrow =@"\U000025C0\U0000FE0E";
    
    NSString *rightArrow =@"\U000025B6\U0000FE0E";
    
    
    [self.leftButton setTitle:leftArrow forState:UIControlStateNormal];
    
    [self.rightButton setTitle:rightArrow forState:UIControlStateNormal];

    self.leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;

    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(revealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer.enabled = NO;
    
    self.tapGestureRecognizerRight = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(rightRevealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
    self.tapGestureRecognizerRight.enabled = NO;
    
    self.revealViewController.delegate=self;
    
}
- (void)radioButtonGroup:(MIRadioButtonGroup*)radioButtonGroup clicked:(int)index {
    
}
- (void)radioButtonGroup:(MIRadioButtonGroup*)radioButtonGroup buttonClicked:(UIButton*)button {
    
    NSString *radioButtonStr=button.titleLabel.text;
    
    if([radioButtonStr isEqual:NSLocalizedString(@"page_day_view",nil)]){
        NSLog(@"Day resolution");
        mViewMode=VIEW_MODE_UNIT_DAY;
    }else if([radioButtonStr isEqual:NSLocalizedString(@"page_month_view",nil)]){
        NSLog(@"Month resolution");
        mViewMode=VIEW_MODE_UNIT_MONTH;
        
    }
    
    [timeControllerView setBrowseUnit:mViewMode];
    
}


-(void)viewWillAppear:(BOOL) animated {
    
    [super viewWillAppear:animated];
    
    self.revealViewController.delegate=self;

    // The plot is initialized here, since the view bounds have not transformed for landscape till now
    [self.timeControllerView refresh];
    
    [self reloadPlot];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - retrive data from database

-(void) queryData: (NSDate*)currentDate {
    
    NSLog(@"queryData ...%@",currentDate);
    
    int numberOfRecords=0;
    
    NSInteger currentTime=[binsDataController timeIntervalInMinute: currentDate withUnitMinute:YES];
    
    
    NSInteger showingStartTime, showingEndTime;
    
    if(mViewMode==VIEW_MODE_UNIT_DAY)
    {
        showingStartTime=currentTime;
        showingEndTime=currentTime+DAY_MINUTES;
        numberOfRecords=48;
    }
    else if(mViewMode==VIEW_MODE_UNIT_MONTH)
    {
        NSDate *startDay, *endDay;
        
        [timeControllerView getMonthPeriod: currentDate startOfMonth:&startDay endOfMonth: &endDay];
         
        showingStartTime=[binsDataController timeIntervalInMinute: startDay withUnitMinute:YES];
        showingEndTime=[binsDataController timeIntervalInMinute: endDay withUnitMinute:YES];
        
        numberOfRecords=(int)((showingEndTime-showingStartTime)/DAY_MINUTES*2);
        
    }
    
    NSLog(@"queryData: show:%ld,%ld", (long)showingStartTime,(long)showingEndTime);
    
    int user=[binsDataController getActiveUserID];
    
    binsDataController.delegate=self;
    int queryResult;
    if([workingIndicator isAnimating]==YES){
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [workingIndicator stopAnimating];
        });

        queryResult=[binsDataController queryActivityRecords: user typeOf: BINS_ACTIVITY_TYPE_STEPS timeFrom: showingStartTime timeTo: showingEndTime];
        
    }else{
        queryResult=[binsDataController queryActivityRecordsWithCloudLoad: user typeOf: BINS_ACTIVITY_TYPE_STEPS timeFrom: showingStartTime timeTo: showingEndTime];
    }
    
    [self resetActivityRecords: numberOfRecords];
    
    if(queryResult==QUERY_COMPLETED){
        
        mTotalSteps=0;
    
        int activityValue;
        NSInteger activityTime;
        while([binsDataController getNextActivityFromQuery:&activityValue atTime:&activityTime]==YES){
       
            mTotalSteps+=activityValue;
            [self setSteps: (activityTime-showingStartTime) withSteps: activityValue];
        
        }
        [self calculateMaxAndMinValue];

    }else if(queryResult==QUERY_WORKING){
        // 20151123 TODO start animation
        dispatch_async(dispatch_get_main_queue(), ^{
            [workingIndicator startAnimating];
        });
    }
    
    //-- Pulse record is stored as unit of second, need to multiply by 60 first
    queryResult=[binsDataController queryActivityRecords: user typeOf: BINS_ACTIVITY_TYPE_PULSE timeFrom: (showingStartTime) timeTo: showingEndTime];
    
    if(queryResult==QUERY_COMPLETED){
        
        int pulseValue;
        NSInteger pulseTimeInMinute;
        while([binsDataController getNextActivityFromQuery:&pulseValue atTime:&pulseTimeInMinute]==YES){
             
            [self setHeartRate: (pulseTimeInMinute-showingStartTime) withPulse: pulseValue];
              
        }
              
        [self calculatePulseRange];
         
    }
    
}




#pragma mark - preparing graph data

int mNumberOfBars=0;

-(void)resetActivityRecords: (int)records {
    
    NSLog(@"resetSteps ...");
    mMaxValue = 0;
    mMinValue = INTEGER_MAX;
    mMaxIndex = 0;
    mMinIndex = 0;
    
    mHeartRateMaxValue = 0; //-- Integer.MIN_VALUE;
    mHeartRateMinValue = INTEGER_MAX;
    
    memset(mValues, 0,sizeof(mValues));
    
    memset(mHeartRateValues, 0, sizeof(mHeartRateLowValues));
    
    for(int i=0; i<AXIS_COUNT_X; i++){
        mHeartRateLowValues[i]=400;
    }
        
    mNumberOfBars=records;
    
    mTotalSteps=0;
    
 }

-(int)barIndexFrom: (NSInteger)timeShift
{
    int timePerBar;
    
    if(mViewMode==VIEW_MODE_UNIT_DAY) {
        
        timePerBar= DAY_MINUTES /mNumberOfBars;
    }else if(mViewMode==VIEW_MODE_UNIT_MONTH){
        timePerBar= DAY_MINUTES/2;
    }else{
        timePerBar= 60/mNumberOfBars;
        // 60 minutes in one hour
    }
    long index = timeShift / timePerBar;
    
    if(index>=mNumberOfBars){
        index=(mNumberOfBars-1);
        NSLog(@"Something wrong with index");
    }
    if(index<0){ //-- 08.14
        index=0;
        NSLog(@"setSteps: wrong time assigned, time was:%ld", (long)timeShift);
    }
    
    return (int)index;
}

-(void)setSteps: (NSInteger)timeShift withSteps: (int)steps {
    
    int index=[self barIndexFrom:timeShift];
    
    mValues[index] += steps;

    //-- NSLog(@"setSteps ... %d = %d ", index, steps);
}

-(void)setHeartRate: (NSInteger)timeShift withPulse: (int)hr {
    
    int index=[self barIndexFrom:timeShift];
    
    // 8.20
    if(hr<40) hr=40;
    if(hr>200) hr=200;
    
    if(hr>mHeartRateValues[index]) mHeartRateValues[index]=hr;
    if(hr<mHeartRateLowValues[index]) mHeartRateLowValues[index]=hr;
    
    //-- NSLog(@"setHeartRate ... %d = %d ", index, hr);
}

-(void)calculateMaxAndMinValue {
    
    NSLog(@"calculateMaxAndMinValue ...");
    
    for (int i = 0; i < axisCountX[mViewMode]; i++) {
        if (mMaxValue < mValues[i]) {
            mMaxValue = mValues[i];
            mMaxIndex = i;
        }
        if (mMinValue > mValues[i] && mValues[i] != 0) {
            mMinValue = mValues[i];
            mMinIndex = i;
        }
    }
    /*
    NSLog(@"mMinIndex = %d",mMinIndex);
    NSLog(@"mMaxIndex = %d",mMaxIndex);
    NSLog(@"mMinValue = %d",mMinValue);
    NSLog(@"mMaxValue = %d", mMaxValue);
     */
    
    mMaxValue += mMaxValue / 10;
    
    if(mViewMode==0) mMaxValue = ((mMaxValue+100)/100)*100;  //-- times of 100
    else mMaxValue = ((mMaxValue+5)/10)*10;
    
    
    if(mMaxValue<100) mMaxValue=100;
  
}

-(void)calculatePulseRange {
    
    //-- for heart rate
    for (int i = 0; i < axisCountX[mViewMode]; i++) {
        if (mHeartRateMaxValue < mHeartRateValues[i]) {
            mHeartRateMaxValue = mHeartRateValues[i];
            if(mHeartRateMaxValue>200) mHeartRateMaxValue=200;
        }
        if (mHeartRateMinValue > mHeartRateValues[i] && mHeartRateValues[i] != 0) {
            mHeartRateMinValue = mHeartRateValues[i];
            
            if(mHeartRateMinValue<40) mHeartRateMinValue=40;
        }
    }
    
    //-- check for single data case
    for (int i = 0; i < axisCountX[mViewMode]; i++) {
        if ((mHeartRateLowValues[i] > mHeartRateValues[i]) && mHeartRateValues[i]!=0) {
            mHeartRateLowValues[i] = mHeartRateValues[i];
        }

    }
    
    mHeartRateMaxValue += mHeartRateMaxValue / 10;
    
    mHeartRateMaxValue = ((mHeartRateMaxValue+10)/10)*10;  //-- times of 10

    if(mHeartRateMaxValue<120) mHeartRateMaxValue=120;
}


-(BOOL) isLoaded {
    return mMinValue != INTEGER_MAX && mMaxIndex != 0;
}


- (void)generateData
{
    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
    
    //Array containing all the dates that will be displayed on the X axis
    dates = [NSArray arrayWithObjects:@"2012-05-01", @"2012-05-02", @"2012-05-03",
             @"2012-05-04", @"2012-05-05", @"2012-05-06", @"2012-05-07", nil];

    //Array containing all the dates that will be displayed on the X axis
    dayLabels = [NSArray arrayWithObjects:@"", @"4", @"8", @"12", @"16", @"20", @"", nil];
    monthLabels = [NSArray arrayWithObjects:@"", @"5", @"10", @"15", @"20", @"25", @"", nil];
    
    
    //Dictionary containing the name of the two sets and their associated color
    //used for the demo
    sets = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blueColor], @"Plot 1",
        //     [UIColor redColor], @"Plot 2",
        //    [UIColor greenColor], @"Plot 3",
            nil];
    
    //Generate random data for each set of data that will be displayed for each day
    //Numbers between 1 and 10
    for (NSString *date in dates) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for (NSString *set in sets) {
            NSNumber *num = [NSNumber numberWithInt:arc4random_uniform(10)+1];
            [dict setObject:num forKey:set];
        }
        [dataTemp setObject:dict forKey:date];
    }
    
    data = [dataTemp copy];
    
    // NSLog(@"%@", data);
    
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 4;
}

-(BOOL)tableView: (UITableView*)tableView shouldHighlightRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section!=0) return 0;
    
    if(indexPath.row==0){
        return 50;
    }
    else if(indexPath.row==1){
        if(self.dateSelectViewCell.hidden==YES){
            return 2;
        }
        else{
            return 160;
        }
    }
    else if(indexPath.row==2){
        return 50;
    }
    else if(indexPath.row==3){
        float h=[self getGraphCellHeight]; // 380;
        return h;
    }
    
    
    return 80; // tableView.rowHeight;
}

-(void)tableView: (UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.section!=0) return;
    
    if(indexPath.row==0){
        
        [cell setBackgroundColor:COLOR_TIME_CONTROLLER_BAR];
        
    }
}



/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)leftButtonClick:(id)sender {
    [self.timeControllerView changeDate: -1];
}

- (IBAction)rightButtonClick:(id)sender {
    [self.timeControllerView changeDate: 1];
}

- (void)timeControllerView:(TimeControllerView *)timeController browseModeChanged:(int)mode {
    
    mViewMode=mode;

    [self queryData: [timeControllerView getCurrentDate]];
    
    // [self reloadPlot];

    
}

- (void)timeControllerView:(TimeControllerView *)timeControllerView timeDidChange:(NSDate*)dateChanged {
    
    NSLog(@"delegate called");

    
/*
    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];

    //Generate random data for each set of data that will be displayed for each day
    //Numbers between 1 and 10
    for (NSString *date in dates) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for (NSString *set in sets) {
            NSNumber *num = [NSNumber numberWithInt:arc4random_uniform(10)+1];
            [dict setObject:num forKey:set];
        }
        [dataTemp setObject:dict forKey:date];
    }
    
    data = [dataTemp copy];
*/
    
    [self queryData: dateChanged];

    [self reloadPlot];
    // [self.hostView.hostedGraph reloadData];

}

- (void)timeControllerView:(TimeControllerView *)timeController touchUpInside:(NSDate*)dateNow {
    
    NSLog(@"delegate touch called");
    
    if(self.dateSelectViewCell.hidden==YES){
        self.dateSelectViewCell.hidden=NO;
        self.datePicker.maximumDate=[NSDate date];
        [self.datePicker setDate:[timeController getCurrentDate]];
        
    }else{
        self.dateSelectViewCell.hidden=YES;
    }
    
    [self.tableView reloadData];
    
}

-(int)getGraphCellHeight {
   //  CGRect parentRect = self.activityDisplayView.bounds;
    CGRect parentRectRef =self.activityTableView.frame;
    
   // return (parentRect.size.height)*(parentRectRef.size.height/parentRect.size.height);
    
    return 344*((parentRectRef.size.height-150)/(HEIGHT_IPHONE5-150));
    
    
}
#pragma mark - Chart behavior
-(void)reloadPlot {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self generateLayout];
    });
}

-(void)configureHost {
    // 1 - Set up view frame
    CGRect parentRect = self.activityDisplayView.bounds;
    CGRect parentRectRef =self.activityTableView.frame;
    
    
    // CGRect parentRect = self.view.bounds;
    parentRect = CGRectMake(parentRect.origin.x,
                            parentRect.origin.y,
                            parentRectRef.size.width,
                            [self getGraphCellHeight]);
    // 2 - Create host view
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:parentRect];
    self.hostView.allowPinchScaling = NO;
    [self.activityDisplayView  addSubview:self.hostView];
    // test self.hostView.backgroundColor= [UIColor greenColor];
}

-(void)configureGraph {
    // 1 - Create and initialise graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    self.hostView.hostedGraph = graph;
    graph.paddingLeft = 0.0f;
    graph.paddingTop = 0.0f;
    graph.paddingRight = 0.0f;
    graph.paddingBottom = 0.0f;
    graph.axisSet = nil;
    // 2 - Set up text style
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color = [CPTColor grayColor];
    textStyle.fontName = @"Helvetica-Bold";
    textStyle.fontSize = 16.0f;
/*
    // 3 - Configure title
    NSString *title = @"Portfolio Prices: May 1, 2012";
    graph.title = title;
    graph.titleTextStyle = textStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -12.0f);
 */
    // 4 - Set theme
    // self.selectedTheme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    // [graph applyTheme:self.selectedTheme];
}


-(void)configureChart {
    // 1 - Get reference to graph
    CPTGraph *graph = self.hostView.hostedGraph;
    // 2 - Create chart
    
    CPTBarPlot *barChart = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
    barChart.identifier = graph.title;
/*
    // 3 - Create gradient
    CPTGradient *overlayGradient = [[CPTGradient alloc] init];
    overlayGradient.gradientType = CPTGradientTypeRadial;
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.0] atPosition:0.9];
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.4] atPosition:1.0];
*/
    barChart.barBasesVary   = NO;
    barChart.barWidth           = CPTDecimalFromFloat(0.8f);
    barChart.barsAreHorizontal  = NO;
    barChart.dataSource         = self;
    barChart.delegate = self;

     // 4 - Add chart to graph
    [graph addPlot:barChart];
}

- (void)generateLayout
{
    
    // 1 - Get graph instance
    // CPTGraph *graph = self.hostView.hostedGraph;

    //Create graph from theme
    CPTGraph *graph                     = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
 
    self.hostView.hostedGraph           = graph;
    graph.plotAreaFrame.masksToBorder   = NO;
    graph.paddingLeft                   = 0.0f;
    graph.paddingTop                    = 0.0f;
    graph.paddingRight                  = 0.0f;
    graph.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor               = [CPTColor whiteColor];
    borderLineStyle.lineWidth               = 2.0f;
    graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
    graph.plotAreaFrame.paddingTop          = 20.0;
    graph.plotAreaFrame.paddingRight        = 25.0;
    graph.plotAreaFrame.paddingBottom       = 40.0; // 80.0;
    graph.plotAreaFrame.paddingLeft         = 25.0; // 70.0;
    
    //Add plot space
    CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    
    plotSpace.identifier=@"step";
    plotSpace.delegate              = self;
    plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                                   length:CPTDecimalFromInt(mMaxValue)];
    
    plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)
                                                                   length:CPTDecimalFromInt(axisCountX[mViewMode])];
    
    //Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth            = 0.75;
    majorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth            = 0.25;
    minorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    
    //Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0-1);
    x.majorIntervalLength           = CPTDecimalFromInt(1);
    x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    

    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //X labels
    int labelLocations = 0;
    NSMutableArray *customXLabels = [NSMutableArray array];
    for (int i=0; i<mNumberOfBars/2; i=i+3) {
        NSString *strLabel=[[NSString alloc] initWithFormat:@"%d",i];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:strLabel textStyle:x.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:i*2] decimalValue]; // [[NSNumber numberWithInt:labelLocations] decimalValue];
        newLabel.offset         = x.labelOffset + x.majorTickLength;
        newLabel.rotation       = M_PI / 4;
        [customXLabels addObject:newLabel];
        labelLocations++;
    }
    x.axisLabels                    = [NSSet setWithArray:customXLabels];
    
    //Y axis
    CPTXYAxis *y            = axisSet.yAxis;
    
    //y.title                 = @"Value";
    //y.titleOffset           = 10.0f; // 50.0f;
    // y.labelingPolicy        = CPTAxisLabelingPolicyEqualDivisions;      // CPTAxisLabelingPolicyAutomatic;
    // y.labelingPolicy        = CPTAxisLabelingPolicyEqualDivisions;
    // y.labelingPolicy     = CPTAxisLabelingPolicyAutomatic;
    
    CPTMutableTextStyle *titleStyleY = [CPTMutableTextStyle textStyle];

    // titleStyleX.color = [CPTColor darkGrayColor];
    // titleStyle.fontName = @"Helvetica";
    
    titleStyleY.fontSize = 10.0f;
    y.labelTextStyle = titleStyleY;
    y.orthogonalCoordinateDecimal = CPTDecimalFromInt(-1);

    y.labelingPolicy                = CPTAxisLabelingPolicyNone;
    
    [customXLabels removeAllObjects];
    
    int step=mMaxValue/5;
    if(step<50) step=50;
    for (int i=0; i<= mMaxValue; i=i+step) {
        NSString *strLabel=[[NSString alloc] initWithFormat:@"%d",i];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:strLabel textStyle:y.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:i] decimalValue]; // [[NSNumber numberWithInt:labelLocations] decimalValue];
        newLabel.offset         = y.labelOffset; // + x.majorTickLength;
        newLabel.rotation       = M_PI / 4;
        [customXLabels addObject:newLabel];
        labelLocations++;
    }
    y.axisLabels                    = [NSSet setWithArray:customXLabels];

    y.majorIntervalLength= CPTDecimalFromInt(step);
    
    // y.majorIntervalLength = [[ NSDecimalNumber decimalNumberWithString : @"50" ] decimalValue ];
    //y.majorGridLineStyle    = majorGridLineStyle;
    //y.minorGridLineStyle    = minorGridLineStyle;
    //y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 1.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
    whiteTextStyle.color                = [CPTColor whiteColor];
    
   
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        [graph addPlot:plot toPlotSpace:plotSpace];
        
    }
    
    
    if(mHeartRateMinValue<INTEGER_MAX)
    {
        
    
      CPTXYPlotSpace *plotSpace2       = [[CPTXYPlotSpace alloc] init];
    
      plotSpace2.identifier=@"pulse";
    
      plotSpace2.xRange = plotSpace.xRange;
      plotSpace2.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                     length:CPTDecimalFromInt(mHeartRateMaxValue)];
    
      plotSpace2.delegate=self;
    
      [graph addPlotSpace:plotSpace2];
    
      //Z axis
      CPTXYAxis *y2 = [(CPTXYAxis *)[CPTXYAxis alloc] init];
    
      y2.plotSpace = plotSpace2;
    
      //-- int self.activityDisplayView.bounds.size.width;
      y2.orthogonalCoordinateDecimal = CPTDecimalFromInt(axisCountX[mViewMode]-1);
      y2.coordinate          =CPTCoordinateY;
    
    
      //Create a bar line style
      CPTMutableLineStyle *y2LineStyle   = [[CPTMutableLineStyle alloc] init];
      y2LineStyle.lineWidth              = 1.0;
      y2LineStyle.lineColor              = [CPTColor redColor];
    
      y2.axisLineStyle=y2LineStyle;
    
      CPTMutableTextStyle *titleStyleZ = [CPTMutableTextStyle textStyle];
      titleStyleZ.fontSize = 10.0f;
      titleStyleZ.textAlignment=CPTAlignmentLeft;
    
      y2.labelTextStyle = titleStyleZ;
    
      y2.labelingPolicy                = CPTAxisLabelingPolicyNone;
      y2.labelOffset=-20; //-25
    
      y2.majorIntervalLength = CPTDecimalFromFloat(mHeartRateMaxValue/3.0f);
    
      int pulseBarWidth=mHeartRateMaxValue/3;
    
      for (int i=0; i<=mHeartRateMaxValue; i=i+pulseBarWidth) {
        NSString *strLabel=[[NSString alloc] initWithFormat:@"%d",i];
        
          
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:strLabel textStyle:y2.labelTextStyle];
          
        newLabel.tickLocation   = [[NSNumber numberWithInt:i] decimalValue];
        newLabel.offset         = y2.labelOffset; // + x.majorTickLength;
        newLabel.rotation       = - (M_PI / 4);
        newLabel.alignment=CPTAlignmentLeft;
        
        [customXLabels addObject:newLabel];
      }
      y2.axisLabels = [NSSet setWithArray:customXLabels];
    
      graph.axisSet.axes = [NSArray arrayWithObjects:x, y, y2, nil];
    
    
      {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        //-- CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        //-- plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        plot.barBasesVary   = YES;
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = @"pulse";
        [graph addPlot:plot toPlotSpace:plotSpace2];
        
      }
    } //-- mHeartRateMaxValue > 0
    
    else{
        graph.plotAreaFrame.paddingRight        = 5.0;
    }
    

    NSString *title = [[NSString alloc] initWithFormat:NSLocalizedString(@"total_steps",nil), mTotalSteps];

    // NSString *title = [[NSString alloc] initWithFormat:@"step %d",mTotalSteps];

    graph.title = title;
    // 3 - Create and set text style
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor darkGrayColor];
    titleStyle.fontName = @"Helvetica";
    titleStyle.fontSize = 13.0f;
    graph.titleTextStyle = titleStyle;
    // graph.titlePlotAreaFrameAnchor+= CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(50.0f, -10.0f);


/*
    //Add legend
    CPTLegend *theLegend      = [CPTLegend legendWithGraph:graph];
    theLegend.numberOfRows	  = 1; //-- sets.count;
    theLegend.fill			  = [CPTFill fillWithColor:[CPTColor colorWithGenericGray:0.15]];
    theLegend.borderLineStyle = barLineStyle;
    theLegend.cornerRadius	  = 10.0;
    theLegend.swatchSize	  = CGSizeMake(15.0, 15.0);
    whiteTextStyle.fontSize	  = 13.0;
    theLegend.textStyle		  = whiteTextStyle;
    theLegend.rowMargin		  = 5.0;
    theLegend.paddingLeft	  = 10.0;
    theLegend.paddingTop	  = 10.0;
    theLegend.paddingRight	  = 10.0;
    theLegend.paddingBottom	  = 10.0;
    graph.legend              = theLegend;
    graph.legendAnchor        = CPTRectAnchorTopRight;
    graph.legendDisplacement  = CGPointMake(-40.0, -10.0);
*/
    

}


-(void)configureLegend {
    // 1 - Get graph instance
    CPTGraph *graph = self.hostView.hostedGraph;
    // 2 - Create legend
    CPTLegend *theLegend = [CPTLegend legendWithGraph:graph];
    // 3 - Configure legen
    theLegend.numberOfColumns = 1;
    theLegend.fill = [CPTFill fillWithColor:[CPTColor whiteColor]];
    theLegend.borderLineStyle = [CPTLineStyle lineStyle];
    theLegend.cornerRadius = 5.0;
    // 4 - Add legend to graph
    graph.legend = theLegend;
    graph.legendAnchor = CPTRectAnchorRight;
    CGFloat legendPadding = -(self.view.bounds.size.width / 8);
    graph.legendDisplacement = CGPointMake(legendPadding, 0.0);
    
  
    //Create graph from theme
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor               = [CPTColor whiteColor];
    borderLineStyle.lineWidth               = 2.0f;
    graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
    graph.plotAreaFrame.paddingTop          = 10.0;
    graph.plotAreaFrame.paddingRight        = 10.0;
    graph.plotAreaFrame.paddingBottom       = 10.0; // 80.0;
    graph.plotAreaFrame.paddingLeft         = 20.0; // 70.0;
    
    //Add plot space
    CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.delegate              = self;
    
    plotSpace.yRange      = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                         length:CPTDecimalFromInt(mMaxValue)];
    
    
    plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(-1)
                                                                   length:CPTDecimalFromInt(mNumberOfBars)];
    
    //Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth            = 0.75;
    majorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth            = 0.25;
    minorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    
    //Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(0);
    x.majorIntervalLength           = CPTDecimalFromInt(1);
    x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //X labels
    int labelLocations = 0;
    NSMutableArray *customXLabels = [NSMutableArray array];
    for (NSString *day in dates) {
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:day textStyle:x.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:labelLocations] decimalValue];
        newLabel.offset         = x.labelOffset + x.majorTickLength;
        newLabel.rotation       = M_PI / 4;
        [customXLabels addObject:newLabel];
        labelLocations++;

    }
    x.axisLabels                    = [NSSet setWithArray:customXLabels];
    
    //Y axis
    CPTXYAxis *y            = axisSet.yAxis;
    y.title                 = @"Value";
    y.titleOffset           = 50.0f;
    y.labelingPolicy        = CPTAxisLabelingPolicyAutomatic;
    y.majorGridLineStyle    = majorGridLineStyle;
    y.minorGridLineStyle    = minorGridLineStyle;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 1.0;
    barLineStyle.lineColor              = [CPTColor blackColor];
    CPTMutableTextStyle *blackTextStyle = [CPTMutableTextStyle textStyle];
    blackTextStyle.color                = [CPTColor blackColor];
    
/*
    //Plot
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = set;
        [graph addPlot:plot toPlotSpace:plotSpace];
    }

  */
    
    //Add legend
    theLegend.numberOfRows	  = sets.count;
    theLegend.fill			  = [CPTFill fillWithColor:[CPTColor colorWithGenericGray:0.15]];
    theLegend.borderLineStyle = barLineStyle;
    theLegend.cornerRadius	  = 10.0;
    theLegend.swatchSize	  = CGSizeMake(15.0, 15.0);
    blackTextStyle.fontSize	  = 13.0;
    theLegend.textStyle		  = blackTextStyle;
    theLegend.rowMargin		  = 5.0;
    theLegend.paddingLeft	  = 10.0;
    theLegend.paddingTop	  = 10.0;
    theLegend.paddingRight	  = 10.0;
    theLegend.paddingBottom	  = 10.0;
    graph.legend              = theLegend;
    graph.legendAnchor        = CPTRectAnchorTopLeft;
    graph.legendDisplacement  = CGPointMake(80.0, -10.0);
}




#pragma mark - CPTPlotDataSource methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    
    return mNumberOfBars;
}

- (double)doubleForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    double num = NAN;
    
    
    
    if([plot.plotSpace.identifier isEqual:@"pulse"]){

        //-- NSLog(@"doubleForPlot:%@, fieldEnum:%d", plot.plotSpace.identifier, fieldEnum);


        if(fieldEnum==0){
            return index;
        }
        
        if(mHeartRateValues[index]==0) return 0;
        
        if(fieldEnum==1){
            //-- NSLog(@"doubleForPlot pulse:%d, %d, %d, %d",index,fieldEnum,mHeartRateLowValues[index],mHeartRateValues[index]);

            int rangeHigh=mHeartRateLowValues[index];
            if(mHeartRateLowValues[index]>=(mHeartRateValues[index]-5)){
                rangeHigh+=5;
            }
            return rangeHigh;
        } else if(fieldEnum==2){
            //-- NSLog(@"doubleForPlot pulse:%d, %d, %d, %d",index,fieldEnum,mHeartRateLowValues[index],mHeartRateValues[index]);

            return mHeartRateValues[index];
        }
        
        return 0;   // unreachable
    }
    
    //X Value
    if (fieldEnum == 0) {
        num = index;
    }
    else {
        double offset = 0;
       
        //Y Value
        if (fieldEnum == 1) {
            // num = [[[data objectForKey:[dates objectAtIndex:index]] objectForKey:plot.identifier] doubleValue] + offset;
            
            num=offset+mValues[index];
        }
        //Offset for stacked bar
        else {
            num = offset;
        }
    }
    
    //NSLog(@"%@ - %d - %d - %f", plot.identifier, index, fieldEnum, num);
    
    return num;
}

-(CPTFill *)barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index {
    CPTColor *fillColor=nil;
    
    
    //-- NSLog(@"fillForBarPlot:%@", barPlot.plotSpace.identifier);

    if([barPlot.plotSpace.identifier isEqual:@"pulse"]){
        CPTColor *RED_COLOR=[CPTColor colorWithComponentRed:0xff/255. green:0 blue:0 alpha:0.5];
        
        fillColor=RED_COLOR;
        
        
    }else if([barPlot.plotSpace.identifier isEqual:@"step"]){
        CPTColor *STEP_COLOR=[CPTColor colorWithComponentRed:0x00/255. green:0x66/255. blue:0xCC/255. alpha:0.9];
        
        fillColor=STEP_COLOR;
    }
    
    if(fillColor==nil){
        NSLog(@"Wrong barPlot from fill function");
    }
    
    return [CPTFill fillWithColor:fillColor];
    
}

- (IBAction)dateSelectPickerValueChanged:(id)sender {
    
    [timeControllerView setDate: self.datePicker.date];
}

-(void) userInteractionEnable: (BOOL)enable {
    self.timeTableCell.userInteractionEnabled=enable;
    self.datePickerTableCell.userInteractionEnabled=enable;
    self.displayModeTableCell.userInteractionEnabled=enable;
    self.tabBarController.tabBar.userInteractionEnabled = enable;

}
- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeftSide) {
        // NSLog(@"FrontViewPositionLeftSide");
        // jump to right side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
        self.tapGestureRecognizerRight.enabled = YES;
        [self userInteractionEnable:NO];
    }
    else if (position == FrontViewPositionLeft){
        // NSLog(@"FrontViewPositionLeft");
        // back to main screen
        self.tapGestureRecognizer.enabled = NO;
        self.tapGestureRecognizerRight.enabled = NO;
        [self userInteractionEnable:YES];

    }
    else if (position == FrontViewPositionRight){
        // NSLog(@"FrontViewPositionRight");
        // jump to left side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.enabled = YES;
        [self userInteractionEnable:NO];

        
    }
}

//-- delegates of BinsDataViewController
//-- 20151119
- (void)binsDataChanged:(BinsDataViewController*)binsDataController {
    
    NSLog(@"activity table, binsDataChanged");
    
    [self queryData: [timeControllerView getCurrentDate]];
    [self reloadPlot];
}


@end
