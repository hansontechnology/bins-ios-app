//
//  AppDelegate.h
//  bins
//
//  Created by Dennis Kung on 10/24/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, CBPeripheralManagerDelegate >

@property (strong, nonatomic) UIWindow *window;

@end

