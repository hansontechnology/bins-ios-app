//
//  AppDelegate.m
//  bins
//
//  Created by Dennis Kung on 10/24/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <AWSCore/AWSCore.h>  // 20151118
#import <ShareSDK/ShareSDK.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import "WXApi.h"
#import "WeiboSDK.h"
#import "AppDelegate.h"
#import "TimeControllerView.h"
#import "DeviceViewController.h"
#import "BinsDataViewController.h"
#import "UnlockingLifeService.h"
#import "BinsCloudService.h"
#import "Constants.h"
#import <AWSMobileAnalytics/AWSMobileAnalytics.h>
#import <AWSSNS/AWSSNS.h> //-- 20151127

@interface AppDelegate () <CLLocationManagerDelegate>

@property (nonatomic, unsafe_unretained) UIBackgroundTaskIdentifier backgroundTaskIdentifier;

@property DeviceViewController *deviceController;
@property BinsDataViewController *binsController;

@property CLLocationManager *locationManager;

@property UnlockingLifeService *unlockingLifeService;

@property BinsCloudService *binsCloudServer;

@property (strong, nonatomic) CBPeripheralManager *peripheralManager;



@end

@implementation AppDelegate

@synthesize deviceController;
@synthesize binsController;
@synthesize backgroundTaskIdentifier;
@synthesize unlockingLifeService;
@synthesize peripheralManager;
@synthesize binsCloudServer;

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

UITabBarController *tabBarController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[UITabBar appearance] setBarTintColor:[UIColor blackColor]];
    // [[UITabBar appearance] setBarTintColor:[UIColor colorWithRed:0x97/255.0 green:0xCB/255. blue:0xFF/255. alpha:1]];

    // [[UINavigationBar appearance] setBarTintColor:UIColorFromRGB(0x067AB5)];
    // [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0x97/255.0 green:0xCB/255. blue:0xFF/255. alpha:1]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0x00/255.0 green:0x4B/255.0 blue:0x97/255.0 alpha:1]];
    // [[UINavigationBar appearance] setBarTintColor:[UIColor blackColor]];
    /*
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           shadow, NSShadowAttributeName,
                                                           [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil]];

    
     */

    
    NSShadow *shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
    shadow.shadowOffset = CGSizeMake(0, 1);
    
    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
                                                           [UIColor colorWithRed:245.0/255.0 green:245.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
                                                           
                                                            nil]];

    
    // This location manager will be used to notify the user of region state transitions.
    self.locationManager = [[CLLocationManager alloc] init];
    
    // This should open an alert that prompts the user, but in iOS 8 it doesn't!
    [self.locationManager startUpdatingLocation];
    
    self.locationManager.delegate = self;
    if([self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [self.locationManager requestAlwaysAuthorization];
    }

    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)]) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeSound|UIUserNotificationTypeBadge categories:nil]];
        
        //-- do it later everytime [[UIApplication sharedApplication] registerForRemoteNotifications];
        

    }
    
    
    

/*
    // reference: UINavigationController* myNavController;  myNavController.navigationBar.tintColor = [UIColor redColor];
    
    UITabBarController *tabBarController = (
 *)self.window.rootViewController;
    UITabBar *tabBar = tabBarController.tabBar;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 7) {
        [tabBar setSelectedImageTintColor:tabBar.tintColor];
        tabBar.tintColor = tabBar.backgroundColor;
    }
*/
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont systemFontOfSize:13.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];

    [UIView appearanceWhenContainedIn:[UITableView class], [UIDatePicker class], nil].backgroundColor = COLOR_TIME_PICKER_NEW2;

    
    
    // side effect cannot UIBarButtonItem *barButtonAppearance = [UIBarButtonItem appearance];
    // [barButtonAppearance setTintColor:[UIColor whiteColor]];

    
    //-- [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    
    
    deviceController=[[DeviceViewController alloc] init];
    binsController=[[BinsDataViewController alloc] init];
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    //-- 150712 to start beacon monitoring 
    [deviceController setDeviceBeaconRegion];
    
#ifdef CONFIG_BINS_HBP
    unlockingLifeService=[[UnlockingLifeService alloc] initWithTableDownload]; //-- 150713
#else
    binsCloudServer=[[BinsCloudService alloc] init];
#endif
    
    //-- for ANCS support
    peripheralManager = [[CBPeripheralManager alloc] initWithDelegate:self queue:nil];

    
    //-- AWS settings
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:CognitoRegionType                                                                                                    identityPoolId:CognitoIdentityPoolId]; //-- 20151118
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:DefaultServiceRegionType
                                                                         credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    [binsCloudServer updateVitalityTables];
    
    //-- [AWSLogger defaultLogger].logLevel = AWSLogLevelVerbose;
    

#if 1

    // Sets up Mobile Push Notification
    UIMutableUserNotificationAction *readAction = [UIMutableUserNotificationAction new];
    readAction.identifier = @"READ_IDENTIFIER";
    readAction.title = @"Read";
    readAction.activationMode = UIUserNotificationActivationModeForeground;
    readAction.destructive = NO;
    readAction.authenticationRequired = YES;
    
    UIMutableUserNotificationAction *deleteAction = [UIMutableUserNotificationAction new];
    deleteAction.identifier = @"DELETE_IDENTIFIER";
    deleteAction.title = @"Delete";
    deleteAction.activationMode = UIUserNotificationActivationModeForeground;
    deleteAction.destructive = YES;
    deleteAction.authenticationRequired = YES;
    
    UIMutableUserNotificationAction *ignoreAction = [UIMutableUserNotificationAction new];
    ignoreAction.identifier = @"IGNORE_IDENTIFIER";
    ignoreAction.title = @"Ignore";
    ignoreAction.activationMode = UIUserNotificationActivationModeForeground;
    ignoreAction.destructive = NO;
    ignoreAction.authenticationRequired = NO;
    
    UIMutableUserNotificationCategory *messageCategory = [UIMutableUserNotificationCategory new];
    messageCategory.identifier = @"MESSAGE_CATEGORY";
    [messageCategory setActions:@[readAction, deleteAction] forContext:UIUserNotificationActionContextMinimal];
    [messageCategory setActions:@[readAction, deleteAction, ignoreAction] forContext:UIUserNotificationActionContextDefault];
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:[NSSet setWithArray:@[messageCategory]]];

    
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];

#else
    

    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
    
#endif
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    
    //-- 20150831 for share screen to other social medias
    
    [ShareSDK registerApp:@"a0af3513c51a"]; // *** is the AppKey that you just got
                            // Check through console site of http://dashboard.mob.com/ShareSDK/#/
    
    
    
    //添加新浪微博应用 注册网址 http://open.weibo.com
    [ShareSDK connectSinaWeiboWithAppKey:@"568898243"
                               appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                             redirectUri:@"http://www.sharesdk.cn"];
    
    //当使用新浪微博客户端分享的时候需要按照下面的方法来初始化新浪的平台
    [ShareSDK  connectSinaWeiboWithAppKey:@"568898243"
                                appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                              redirectUri:@"http://www.sharesdk.cn"
                              weiboSDKCls:[WeiboSDK class]];
    
    //添加腾讯微博应用 注册网址 http://dev.t.qq.com
    [ShareSDK connectTencentWeiboWithAppKey:@"801307650"
                                  appSecret:@"ae36f4ee3946e1cbb98d6965b0b2ff5c"
                                redirectUri:@"http://www.sharesdk.cn"];
    
    /*
    //添加QQ空间应用  注册网址  http://connect.qq.com/intro/login/
    [ShareSDK connectQZoneWithAppKey:@"100371282"
                           appSecret:@"aed9b0303e3ed1e27bae87c33761161d"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
     */

    /*
    //添加QQ应用  注册网址   http://mobile.qq.com/api/
    [ShareSDK connectQQWithQZoneAppKey:@"100371282"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    */
    
    //微信登陆的时候需要初始化
    [ShareSDK connectWeChatWithAppId:@"wxc3c6943167317e3b"   //微信APPID， product@hanson-tech.com BINS
                           appSecret:@"7454bc7a1f269fdb99439f00fe28ea99"  //微信APPSecret
                           wechatCls:[WXApi class]];

    
    //添加Facebook应用  注册网址 https://developers.facebook.com
    [ShareSDK connectFacebookWithAppKey:@"1423185011289553"         // denniskung68@hotmail.com
                              appSecret:@"4982328b1ecd58aec5e1a5a05ad419d1"];
    
    //添加Twitter应用  注册网址  https://dev.twitter.com
    [ShareSDK connectTwitterWithConsumerKey:@"mnTGqtXk0TYMXYTN7qUxg"
                             consumerSecret:@"ROkFqr8c3m1HXqS3rm3TJ0WkAJuwBOSaWhPbZ9Ojuc"
                                redirectUri:@"http://www.sharesdk.cn"];
    
 
    //-- correct ?
    [ShareSDK connectLine];
    
    [ShareSDK connectWhatsApp];

    //连接短信分享
    [ShareSDK connectSMS];
    //连接邮件
    [ShareSDK connectMail];
    //连接打印
    // [ShareSDK connectAirPrint];
    //连接拷贝
    [ShareSDK connectCopy];
    
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
      handleOpenURL:(NSURL *)url
{
    return [ShareSDK handleOpenURL:url
                        wxDelegate:self];
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    return [ShareSDK handleOpenURL:url
                 sourceApplication:sourceApplication
                        annotation:annotation
                        wxDelegate:self];
}


- (void)peripheralManagerDidUpdateState:(CBPeripheralManager *)peripheral
{
    if (peripheral.state == CBPeripheralManagerStatePoweredOn) {
        [self.peripheralManager startAdvertising:@{
                                                   CBAdvertisementDataLocalNameKey: [[UIDevice currentDevice] name]
                                                   }];
    } else {
        [self.peripheralManager stopAdvertising];
    }
}


-(BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];
    return true;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    // We will add content here soon.
    
   /* [deviceController updatePeriodWithCompletionHandler:^(UIBackgroundFetchResult result){
        completionHandler(result);
        }];
    */
    /*
    [deviceController backgroundSyncAndDisconnect: ^() {
        completionHandler(UIBackgroundFetchResultNoData);
    }]; // 12.12
    */
    
    completionHandler(UIBackgroundFetchResultNewData);
    NSLog(@"Fetch completed");
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"confirm_ok",nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}

- (void) displayDeviceData {
    NSString *deviceTokenValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"deviceToken"];
    NSLog(@"device token info: %@",deviceTokenValue?deviceTokenValue:@"N/A");
    
    NSString *endpointArnValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"endpointArn"];
    NSLog(@"device token info: %@", endpointArnValue?endpointArnValue:@"N/A");
}


- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *deviceTokenString = [[[deviceToken description] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSLog(@"deviceTokenString: %@", deviceTokenString);
    [[NSUserDefaults standardUserDefaults] setObject:deviceTokenString forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //-- [self displayDeviceData];
    
    AWSSNS *sns = [AWSSNS defaultSNS];
    AWSSNSCreatePlatformEndpointInput *request = [AWSSNSCreatePlatformEndpointInput new];
    request.token = deviceTokenString;
    request.platformApplicationArn = SNSPlatformApplicationArn;
    
    
    [
     [[sns createPlatformEndpoint:request] continueWithSuccessBlock:^id(AWSTask *task) {
        AWSSNSCreateEndpointResponse *response = task.result;
        NSLog(@"endpointArn: %@",response.endpointArn);
        
        AWSSNSSubscribeInput *subscribeRequest = [AWSSNSSubscribeInput new];
        subscribeRequest.endpoint = response.endpointArn;
        subscribeRequest.protocols = @"application";
        subscribeRequest.topicArn = @"arn:aws:sns:us-east-1:414327512415:BinsSNSTopic";
        return [sns subscribe:subscribeRequest];
    }] continueWithBlock:^id(AWSTask *task) {
        if (task.cancelled) {
            NSLog(@"Task cancelled");
        }
        else if (task.error) {
            NSLog(@"Error occurred: [%@]", task.error);
        }
        else {
            NSLog(@"Success:%@", task.result);
        }
        return nil;
    }
     
     ];
    
#if 0
    [[sns createPlatformEndpoint:request] continueWithBlock:^id(AWSTask *task) {
        if (task.error != nil) {
            NSLog(@"Error: %@",task.error);
        } else {
            AWSSNSCreateEndpointResponse *createEndPointResponse = task.result;
            NSLog(@"endpointArn: %@",createEndPointResponse);
            [[NSUserDefaults standardUserDefaults] setObject:createEndPointResponse.endpointArn forKey:@"endpointArn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //-- [self.window.rootViewController.childViewControllers.firstObject performSelectorOnMainThread:@selector(displayDeviceInfo) withObject:nil waitUntilDone:NO];
            
            
            
            AWSSNSCreateEndpointResponse *response = task.result;
            
            AWSSNSSubscribeInput *subscribeRequest = [AWSSNSSubscribeInput new];
            
            subscribeRequest.endpoint = response.endpointArn;
            subscribeRequest.protocols = @"application";
            subscribeRequest.topicArn = @"arn:aws:sns:us-east-1:414327512415:BinsSNSTopic";
            
            /* @"arn:aws:sns:us-east-1:753780999999:MyAppDevelopingTest"; */
            
            [[sns subscribe:subscribeRequest] continueWithBlock:^id(AWSTask *taskSub) {
                if (taskSub.cancelled) {
                    NSLog(@"Task cancelled");
                }
                else if (taskSub.error) {
                    NSLog(@"Error occurred: [%@]", taskSub.error);
                }
                else {
                    NSLog(@"Success");
                }
                
                return nil;
                
            }];
        }
        
        return nil;
    }];
#endif
    
    
    
    
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Failed to register with error: %@",error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"userInfo: %@",userInfo);
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler {
    
    NSLog(@"notification received:%@", identifier);
    
    NSString *action = @"Undefined";
    if ([identifier isEqualToString:@"READ_IDENTIFIER"]) {
        action = @"read";
        NSLog(@"User selected 'Read'");
    } else if ([identifier isEqualToString:@"DELETE_IDENTIFIER"]) {
        action = @"Deleted";
        NSLog(@"User selected `Delete`");
    } else {
        action = @"Undefined";
    }
    
    
    completionHandler();
}


- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    /*
     
    NSUInteger orientations = UIInterfaceOrientationMaskAllButUpsideDown;


    if(self.window.rootViewController){
        UIViewController *presentedViewController = [[(UINavigationController *)self.window.rootViewController viewControllers] lastObject];
        orientations = [presentedViewController supportedInterfaceOrientations];
    }
     */
    
    return UIInterfaceOrientationMaskPortrait;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
        NSLog(@"Will resign active");
}

NSTimer *backgroundRunTimer;
-(void) backgroundRunTimerRoutine {
    
    //-- float timeLeft=[[UIApplication sharedApplication] backgroundTimeRemaining];

    NSLog(@"background Timer :%f", [[UIApplication sharedApplication] backgroundTimeRemaining] );

    //-- if(timeLeft>60) return;
    
    // [self restartBackgroundTask:[UIApplication sharedApplication]];
    
    
}

-(void)restartBackgroundTask: (UIApplication*)application{
    
    if(backgroundRunTimer==nil){
      backgroundRunTimer= [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(backgroundRunTimerRoutine) userInfo:nil repeats:YES];
    }
    self.backgroundTaskIdentifier =
    [application beginBackgroundTaskWithExpirationHandler:^(void) {
        [self endBackgroundTask];
    }
     ];
    
    
}
- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSLog(@"Did Enter Background");
    
    if([deviceController finderIsRunning]==YES){ //-- otherwise it is not necessary to launch
        [self restartBackgroundTask: application];
    }

}



- (void) endBackgroundTask{
    
    NSLog(@"endBackgroundTask");
    
    
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    
    __weak AppDelegate *weakSelf = self;
    
    dispatch_async(mainQueue, ^(void) {
        
        AppDelegate *strongSelf = weakSelf;
        
        if (strongSelf != nil){
            
            [[UIApplication sharedApplication] endBackgroundTask:self.backgroundTaskIdentifier];
            
            strongSelf.backgroundTaskIdentifier = UIBackgroundTaskInvalid;
        }
        
        if(backgroundRunTimer!=nil){
            [backgroundRunTimer invalidate];
            backgroundRunTimer=nil;
        }
        
    });
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    NSLog(@"Will enter foreground");
    
    
    if (self.backgroundTaskIdentifier != UIBackgroundTaskInvalid){
        
        [self endBackgroundTask]; }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    NSLog(@"Become active");
    
        
    NSInteger syncedTime=[binsController getActiveUserLastSyncTime];
    
    if(syncedTime==0) return;

    NSDate *currentDate=[NSDate date];
    
    NSInteger timeInterval=[currentDate timeIntervalSince1970];

    if((timeInterval-syncedTime)>(60*5))   // after 5 minutes
    {
        //-- NSLog(@"active to start sync:%d",timeInterval);
        [deviceController backgroundSyncAndDisconnect];
    }else{
        [binsController uploadTask];
        
    }
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSLog(@"App going to terminate");
}


- (void)locationManager:(CLLocationManager *)manager didDetermineState:(CLRegionState)state forRegion:(CLRegion *)region
{
    /*
     A user can transition in or out of a region while the application is not running. When this happens CoreLocation will launch the application momentarily, call this delegate method and we will let the user know via a local notification.
     */
    /*
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    
    if(state == CLRegionStateInside)
    {
        notification.alertBody = NSLocalizedString(@"You're inside the region", @"");
    }
    else if(state == CLRegionStateOutside)
    {
        notification.alertBody = NSLocalizedString(@"You're outside the region", @"");
    }
    else
    {
        return;
    }
    */
    /*
     If the application is in the foreground, it will get a callback to application:didReceiveLocalNotification:.
     If it's not, iOS will display the notification to the user.
     */
    //-- [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
}


@end
