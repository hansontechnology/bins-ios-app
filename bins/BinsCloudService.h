//
//  BinsCloudService.h
//  bins
//
//  Created by Dennis Kung on 6/29/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSVParser.h"

#import <Foundation/Foundation.h>
#import <AWSDynamoDB/AWSDynamoDB.h>

@class DDBTableUser;
@class AWSTask;

@class BinsCloudService;

@protocol BinsCloudServiceDelegate

@optional

-(void)binsCloudService: (BinsCloudService*)binsCloudService hasUser: (BOOL)status;

@optional
-(void)binsCloudService: (BinsCloudService*)binsCloudService addedNewUser: (BOOL)status userName: (NSString*)userName userUID: (NSString*)userUID;

-(void)binsCloudService:(BinsCloudService*)binsCloudService addedNewData: (BOOL) isSuccess user: (int)userID ;

-(void)binsCloudService:(BinsCloudService*)binsCloudService loadedUserProfile: (NSString*)userName
                withUID: (NSString*)userUID firstNameIs: (NSString*) firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender
           lastSyncTimeIs: (long)lastSyncTime;

-(void)binsCloudService:(BinsCloudService*)binsCloudService loadUserProfileFailed: (NSString*)userName;

-(void)binsCloudService:(BinsCloudService*)binsCloudService loadActivityCompleted: (NSString*)userUID;

@end


@interface BinsCloudService : NSObject <NSURLConnectionDelegate>


@property (weak) id <BinsCloudServiceDelegate> delegate;

-(BOOL) checkInternet;

-(BOOL) checkUserExist: (NSString*) userName ;

-(BOOL) loadUserProfile: (NSString *)userName  withPassword: (NSString*)password;
    
-(BOOL) insertOrUpdateUserProfile: (NSString*)userUID userNameIs: (NSString *)userName firstNameIs: (NSString*)firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender withPassword: (NSString*)password ;

-(BOOL)loadActivityRecords: (NSString*)userUID timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime;
-(BOOL)loadActivityRecordsSleepPage: (NSString*)userUID timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime ;
-(BOOL)loadActivityRecordsStepPage: (NSString*)userUID timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime ;

-(id)initWithTableDownload;


-(void)createUploadSession: (int)user ofUID: (NSString*)userUID ofType:(int)type;
-(void)addUploadActivity:(int)actValue ofType: (int)actType ofTime:(long)time;
-(BOOL)startUploadSession;


-(void)setLastSyncTime: (NSString*)userUID withName: (NSString*)userName at: (NSInteger)time;


-(void)updateVitalityTables;
+(BOOL) isUpdatingTables;


@end


@interface DDBTableUser : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;   //-- Email
@property (nonatomic, strong) NSString *userFirstName;
@property (nonatomic, strong) NSString *userLastName;
@property (nonatomic, strong) NSString *userDOB;
@property (nonatomic, strong) NSNumber *userWeight;
@property (nonatomic, strong) NSNumber *userHeight;
@property (nonatomic, strong) NSString *userGender;
@property (nonatomic, strong) NSNumber *userGoal;
@property (nonatomic, strong) NSString *userDevice;
@property (nonatomic, strong) NSNumber *alarmTime;
@property (nonatomic, strong) NSString *userPassword;
@property (nonatomic, strong) NSNumber *userLastSyncTime;


//Those properties should be ignored according to ignoreAttributes
@property (nonatomic, strong) NSString *internalName;
@property (nonatomic, strong) NSNumber *internalState;

@end

@interface DDBTableActivity : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSNumber *activityType;
@property (nonatomic, strong) NSNumber *activityValue;
@property (nonatomic, strong) NSNumber *activityTime;

//Those properties should be ignored according to ignoreAttributes
@property (nonatomic, strong) NSString *internalName;
@property (nonatomic, strong) NSNumber *internalState;

@end


@interface DDBTableSleepThreshold : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSNumber *thresholdId;
@property (nonatomic, strong) NSString *thresholdName;
@property (nonatomic, strong) NSNumber *movementCounts;
@property (nonatomic, strong) NSNumber *isStateSleep;


@end

@interface DDBTableTimeEffect : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (nonatomic, strong) NSNumber *timeEffectId;
@property (nonatomic, strong) NSNumber *threshId;
@property (nonatomic, strong) NSNumber *timeEffectNthHour;
@property (nonatomic, strong) NSNumber *recoveryFactor;

@end

@interface DDBTableCircadian : AWSDynamoDBObjectModel <AWSDynamoDBModeling>

@property (nonatomic, strong) NSNumber *circadianId;
@property (nonatomic, strong) NSNumber *circadianHour24InDay;
@property (nonatomic, strong) NSNumber *circadianPercSleepFactor;
@property (nonatomic, strong) NSNumber *circadianPercWakeFactor;

@end

