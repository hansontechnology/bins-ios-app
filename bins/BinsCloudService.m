//
//  BinsCloudService.m
//  bins
//
//  Created by Dennis Kung on 6/29/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BinsCloudService.h"
#import "Reachability.h"
#import "BinsDataViewController.h"

@interface BinsCloudService ()

//-- @property BinsDataViewController *binsDataController=nil;

@end

@implementation BinsCloudService

static BinsDataViewController *binsDataController=nil;

static int mActiveConnectUser=0;   //-- 20151119
static NSString *mActiveConnectUserUID=nil;   //-- 20151119

static BOOL initialized=NO;

static NSMutableArray *arrayActivity=nil;

static BOOL mIsUpdatingVitalityTables=NO;
static BOOL mIsUpdatingTimeEffectTable=NO;
static BOOL mIsUpdatingCircadianTable=NO;
static BOOL mIsUpdatingThresholdTable=NO;

-(id)init
{
    self= [super init];
    
    NSLog(@"BinsCloud init");
    

    if(initialized==NO){
        initialized=YES;
        binsDataController=[[BinsDataViewController alloc] init];
        
        arrayActivity=[NSMutableArray new];
        
    }
    
    return self;
}

//-- 20160121
+(BOOL) isUpdatingTables {
    return mIsUpdatingVitalityTables;
}

-(NSString*)getNewUserId {
    
    NSDate * now = [NSDate date];
    NSTimeInterval uniqueTime = [now timeIntervalSince1970];

    //-- CFAbsoluteTime uniqueTime = CFAbsoluteTimeGetCurrent();
    
    // NSString *strNewUserId=[NSString stringWithFormat:@"%f", uniqueTime];
    
    return [NSString stringWithFormat:@"%f",uniqueTime];
    
}


-(BOOL) checkInternet {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    
    return YES;
}


-(BOOL) checkUserExist: (NSString*) userName {
    
    if([self checkInternet]==NO) return NO;
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    AWSDynamoDBScanExpression *scanExpression = [AWSDynamoDBScanExpression new];
    scanExpression.limit = @10;
    scanExpression.filterExpression = @"userName = :val";
    scanExpression.expressionAttributeValues = @{@":val":userName};
    
    [[dynamoDBObjectMapper scan:[DDBTableUser class]
                     expression:scanExpression]
     continueWithBlock:^id(AWSTask *task) {
         
         BOOL foundExustingUser=NO;
         
         if (task.error) {
             NSLog(@"The user search request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The user search request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableUser *user in paginatedOutput.items) {
                 if(user!=nil){
                     foundExustingUser=YES;
                     break;
                 }
             }
         }
         
         [self.delegate binsCloudService:self hasUser: foundExustingUser];
         
         return nil;
     }];
    
 
    return YES;
    
}

-(BOOL) loadUserProfile: (NSString *)userName  withPassword: (NSString*)password {
    
    
    if([self checkInternet]==NO) return NO;
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];

    AWSDynamoDBScanExpression *scanExpression = [AWSDynamoDBScanExpression new];
    scanExpression.limit = @10;
    scanExpression.filterExpression = @"userName = :usr AND userPassword = :pwd ";
    scanExpression.expressionAttributeValues = @{@":usr":userName, @":pwd":password};
    
    [[dynamoDBObjectMapper scan:[DDBTableUser class]
                     expression:scanExpression]
     continueWithBlock:^id(AWSTask *task) {
         
         BOOL foundExustingUser=NO;
         
         if (task.error) {
             NSLog(@"The user search request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The user search request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableUser *user in paginatedOutput.items) {
                 if(user!=nil){
                     foundExustingUser=YES;
                     
                     int iGender=0;
                     if([user.userGender isEqualToString:@"M"]){
                         iGender=1;
                     }
                     
                     [self.delegate binsCloudService:self loadedUserProfile: user.userName
                                             withUID: user.userId
                                         firstNameIs: user.userFirstName
                                          lastNameIs: user.userLastName
                                               dobIs: user.userDOB
                                            weightIs: [user.userWeight intValue]
                                            heightIs: [user.userHeight intValue]
                                            genderIs: iGender
                                      lastSyncTimeIs: [user.userLastSyncTime longValue]
                      
                      ];

                     break;
                 }
             }
         }
         
         if(foundExustingUser==NO){
             [self.delegate binsCloudService:self loadUserProfileFailed: userName];
         }
         
         return nil;
     }];
    

    return YES;
}

NSString *mLoadingUserName;
NSString *mLoadingUserUID;

//-- both use save and unique hash key from user UID

-(BOOL) insertOrUpdateUserProfile: (NSString*)userUID userNameIs: (NSString *)userName firstNameIs: (NSString*)firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender withPassword:(NSString *)password {
    
    if([self checkInternet]==NO){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_no_internet",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
        
        [alert show];
       
        return NO;
    }

    
    NSString *strGenderName=@"F";
    if(gender==1) strGenderName=@"M";
    
    if(userUID==nil){
        userUID=[self getNewUserId];
    }
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    DDBTableUser *theUser= [DDBTableUser new];
    theUser.userId=mLoadingUserUID=userUID;
    theUser.userFirstName=firstName;
    theUser.userLastName=lastName;
    theUser.userName=mLoadingUserName=userName;
    theUser.userDOB=dob;
    theUser.userWeight=[NSNumber numberWithLong:weight];
    theUser.userHeight=[NSNumber numberWithLong:height];
    theUser.userGender=strGenderName;
    theUser.userPassword=password;
    
    [[dynamoDBObjectMapper save:theUser]
     continueWithBlock:^id(AWSTask *task) {
         
         if (task.error) {
             NSLog(@"The add user request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The add user request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             // TODO 20151119
             
             [self.delegate binsCloudService:self addedNewUser: YES userName: mLoadingUserName userUID: mLoadingUserUID];
             /* 20151121 not work
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableUser *user in paginatedOutput.items) {
                 if(user!=nil){
                     [self.delegate binsCloudService:self addedNewUser: YES userName: user.userName userUID: user.userId];
                     break;
                 }
             }
             */
         }
         return nil;
     }];
    
    return YES;
    
}


-(void)setLastSyncTime: (NSString*)userUID withName: (NSString*)userName at: (NSInteger)time {
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    AWSDynamoDBObjectMapperConfiguration *updateMapperConfig = [AWSDynamoDBObjectMapperConfiguration new];
    
    //-- 2015126 BUG FIX this behavior must be set to only overwrite the sync time value, leave others unchanged
    updateMapperConfig.saveBehavior = AWSDynamoDBObjectMapperSaveBehaviorUpdateSkipNullAttributes;
  
    NSLog(@"binsCloud setLastSyncTime: %@, %@=%ld", userUID, userName, time);
    
    DDBTableUser *theUser= [DDBTableUser new];
    theUser.userId=userUID;
    theUser.userName=userName;
    theUser.userLastSyncTime=[NSNumber numberWithInteger:time];
    
     [[dynamoDBObjectMapper save:theUser configuration:updateMapperConfig]
     continueWithBlock:^id(AWSTask *task) {
         
         if (task.error) {
             NSLog(@"The add user sync time failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The add user sync time failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             // TODO 20151119
             
        }
         return nil;
     }];
    
    return;

}

-(BOOL)loadActivityRecordsStepPage: (NSString*)userUID timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
    NSLog(@"loadActivityRecords step page user: %@ from-to=%ld to %ld:", userUID, fromTime, toTime);
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
    
    //-- queryExpression.indexName = @"Author-Price-index";
    
    queryExpression.hashKeyAttribute = @"userId";
    queryExpression.hashKeyValues = userUID;
    
    queryExpression.rangeKeyConditionExpression = @"activityTime BETWEEN :startTime AND :endTime";
    queryExpression.filterExpression = @"activityType= :stepType OR activityType= :pulseType";
    queryExpression.expressionAttributeValues = @{
        @":endTime":[NSNumber numberWithInteger:toTime],
        @":startTime":[NSNumber numberWithInteger:fromTime],
        @":stepType":@BINS_ACTIVITY_TYPE_STEPS,
        @":pulseType":@BINS_ACTIVITY_TYPE_PULSE
                                                  };
    
    [[dynamoDBObjectMapper query:[DDBTableActivity class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The sleep load activity query request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The sleep load activity query request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableActivity *activity in paginatedOutput.items) {
                 
                 NSLog(@"insert from cloud: %ld=%d:%d",
                       [activity.activityTime longValue],
                       [activity.activityType intValue],
                       [activity.activityValue intValue]
                       );
                 
                 //-- do not need to assign userId, as we already know
                 [binsDataController
                  insertOrUpdateActivitySetUploaded: [activity.activityTime longValue]
                  withType:   [activity.activityType intValue]
                  withValue:  [activity.activityValue intValue]
                  ];
                 
             }
             
             [self.delegate binsCloudService:self loadActivityCompleted:userUID];
         }
         return nil;
     }];
    
    return YES;
}

-(BOOL)loadActivityRecordsSleepPage: (NSString*)userUID timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
    NSLog(@"loadActivityRecords sleep user=%@ from-to=%ld to %ld:", userUID, fromTime, toTime);
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
    
    //-- queryExpression.indexName = @"Author-Price-index";
    
    queryExpression.hashKeyAttribute = @"userId";
    queryExpression.hashKeyValues = userUID;
    
    queryExpression.rangeKeyConditionExpression = @"activityTime BETWEEN :startTime AND :endTime";
    queryExpression.filterExpression = @"activityType= :sleepType OR activityType= :lightType";
    queryExpression.expressionAttributeValues = @{
        @":endTime":[NSNumber numberWithInteger:toTime],
        @":startTime":[NSNumber numberWithInteger:fromTime],
        @":sleepType":@BINS_ACTIVITY_TYPE_SLEEP,
        @":lightType":@BINS_ACTIVITY_TYPE_LIGHT
        };
    
    [[dynamoDBObjectMapper query:[DDBTableActivity class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The sleep load activity query request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The sleep load activity query request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableActivity *activity in paginatedOutput.items) {
                 
                 NSLog(@"insert from cloud: %ld=%d:%d",
                       [activity.activityTime longValue],
                       [activity.activityType intValue],
                       [activity.activityValue intValue]
                       );
                 
                 //-- do not need to assign userId, as we already know
                 [binsDataController
                  insertOrUpdateActivitySetUploaded: [activity.activityTime longValue]
                  withType:   [activity.activityType intValue]
                  withValue:  [activity.activityValue intValue]
                  ];
                 
             }
             
             [self.delegate binsCloudService:self loadActivityCompleted:userUID];
         }
         return nil;
     }];
    
    return YES;
}


-(BOOL)loadActivityRecords: (NSString*)userUID timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
    NSLog(@"loadActivityRecords from-to=%ld to %ld:", fromTime, toTime);
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];

    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
    
    //-- queryExpression.indexName = @"Author-Price-index";
    
    queryExpression.hashKeyAttribute = @"userId";
    queryExpression.hashKeyValues = userUID;
    
    queryExpression.rangeKeyConditionExpression = @"activityTime BETWEEN :startTime AND :endTime";
    queryExpression.expressionAttributeValues = @{@":endTime":[NSNumber numberWithInteger:toTime], @":startTime":[NSNumber numberWithInteger:fromTime]};
     
    
    [[dynamoDBObjectMapper query:[DDBTableActivity class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The load activity query request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The load activity query request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableActivity *activity in paginatedOutput.items) {
                 
                 NSLog(@"insert from cloud: %ld=%d:%d",
                       [activity.activityTime longValue],
                       [activity.activityType intValue],
                       [activity.activityValue intValue]
                       );
                 
                 //-- do not need to assign userId, as we already know
                 [binsDataController
                        insertOrUpdateActivitySetUploaded: [activity.activityTime longValue]
                        withType:   [activity.activityType intValue]
                        withValue:  [activity.activityValue intValue]
                  ];
                 
             }
             
             [self.delegate binsCloudService:self loadActivityCompleted:userUID];
         }
         return nil;
     }];
    
    return YES;
}


-(void)addUploadActivity:(int)actValue ofType: (int)type ofTime:(long)time {

    DDBTableActivity *theActivity= [DDBTableActivity new];
    theActivity.userId=mActiveConnectUserUID;
    theActivity.activityValue=[NSNumber numberWithInt:actValue];
    theActivity.activityType=[NSNumber numberWithInt:type];
    theActivity.activityTime=[NSNumber numberWithLong:time];

    [arrayActivity addObject:theActivity];
}

-(BOOL)sendUploadActivity {
    
    DDBTableActivity *theActivity;
    
    
    if([arrayActivity count]==0){
        return NO;
    }
    
    theActivity=[arrayActivity objectAtIndex:0];   // or lastObject?
    [arrayActivity removeObjectAtIndex:0];      // or [arrayActivity count]-1?
    
    //-- NSLog(@"send Upload : %ld, %d", [theActivity.activityTime longValue], [theActivity.activityType intValue]);
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];


    [[dynamoDBObjectMapper save:theActivity]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The add activity request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The add activity request failed. Exception: [%@]", task.exception);
         }
         if(task.result) {
             //-- 20151122 the locals can be retrieved
             //-- NSLog(@"addUploadActivity successed %@", theActivity.activityTimey);
             
         }
         
         [self continueSendToCloud];

         return nil;
     }];
    
    return YES;
}


-(void)createUploadSession: (int)user ofUID: (NSString*)userUID ofType:(int)type {
    
    mActiveConnectUser=user;
    mActiveConnectUserUID=userUID;

}

-(BOOL)continueSendToCloud {
    BOOL isSendSuccess=[self sendUploadActivity];
    if(isSendSuccess==NO){ // means no more buffer to send
        
        //-- By calling this to continuously trigger uploadTask
        [self.delegate binsCloudService:self addedNewData:YES user:0];
    }
    
    return isSendSuccess;
}

-(BOOL)startUploadSession{
    
    BOOL isSendSuccess=[self sendUploadActivity];
    
    return isSendSuccess;   //-- to inform empty buffer
}

-(void)updateThresholdTable {
    
    NSString *userUID=@"9999";       //-- temporarilly use it for all;
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    AWSDynamoDBQueryExpression *queryExpression = [AWSDynamoDBQueryExpression new];
    
    queryExpression.hashKeyAttribute = @"userId";
    queryExpression.hashKeyValues = userUID;
    
    [[dynamoDBObjectMapper query:[DDBTableSleepThreshold class]
                      expression:queryExpression]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The threshold query request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The threshold query request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableSleepThreshold *threshold in paginatedOutput.items) {
                 
                 [binsDataController updateThresholdItem:
                    [threshold.thresholdId intValue]
                    withName:[threshold.thresholdName UTF8String]
                    withMovements:[threshold.movementCounts intValue]
                    withSleepState:[threshold.isStateSleep intValue]
                  ];
             }
             
             [self.delegate binsCloudService:self loadActivityCompleted:userUID];
         }
         
         //-- 20160121
         mIsUpdatingThresholdTable=NO;
         [self chanceToResetTableUpdateFlag];
         
         return nil;
     }];
    
}


-(void)updateTimeEffectTable {
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    [[dynamoDBObjectMapper scan:[DDBTableTimeEffect class]
                      expression:nil]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The timeEffect scan  request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The timeEffect scan request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             
             //-- 20151126 BUG FIX need to clear the table first
             [binsDataController clearSleepTimeEffectTable];
             
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableTimeEffect *timeEffect in paginatedOutput.items) {
                 
                 //-- NSLog(@"sleep time effect: %d[%d]=%f", [timeEffect.threshId intValue], [timeEffect.timeEffectNthHour intValue], [timeEffect.recoveryFactor floatValue]);
                 
                 [binsDataController updateSleepTimeEffectItem:
                        [timeEffect.threshId intValue]
                        withHour:[timeEffect.timeEffectNthHour intValue]
                        withRecovery:[timeEffect.recoveryFactor floatValue]
                  ];
             }
         }
         //-- 20160121
         mIsUpdatingTimeEffectTable=NO;
         [self chanceToResetTableUpdateFlag];
         
         return nil;
     }];
    
}

-(void)updateCircadianTable {
    
    AWSDynamoDBObjectMapper *dynamoDBObjectMapper = [AWSDynamoDBObjectMapper defaultDynamoDBObjectMapper];
    
    [[dynamoDBObjectMapper scan:[DDBTableCircadian class]
                     expression:nil]
     continueWithBlock:^id(AWSTask *task) {
         if (task.error) {
             NSLog(@"The circadian scan  request failed. Error: [%@]", task.error);
         }
         if (task.exception) {
             NSLog(@"The circadian scan request failed. Exception: [%@]", task.exception);
         }
         if (task.result) {
             AWSDynamoDBPaginatedOutput *paginatedOutput = task.result;
             for (DDBTableCircadian *circadian in paginatedOutput.items) {
                 
                 [binsDataController updateCircadianItem:
                   [circadian.circadianId intValue]
                    withHour: [circadian.circadianHour24InDay intValue]
                    withSleepFactor:[circadian.circadianPercSleepFactor floatValue]
                    withWakeFactor:[circadian.circadianPercWakeFactor floatValue]
                 ];
             }
             
         }
         
         //-- 20160121
         mIsUpdatingCircadianTable=NO;
         [self chanceToResetTableUpdateFlag];
         
         return nil;
     }];
    
}



-(void)updateVitalityTables {
    
    mIsUpdatingVitalityTables=YES;
    mIsUpdatingTimeEffectTable=YES;
    mIsUpdatingCircadianTable=YES;
    mIsUpdatingThresholdTable=YES;
    
    
    [self updateTimeEffectTable];   //-- 20160117

    [self updateCircadianTable];    //-- 20151126 keep this sequence where updateCircadianTable is top
                                    //-- otherwise, multithreaded environemnt will keep sqlite3 crash
    [self updateThresholdTable];

}

//-- 20160121
-(void)chanceToResetTableUpdateFlag {
    
    if(mIsUpdatingTimeEffectTable==NO &&
       mIsUpdatingCircadianTable==NO &&
       mIsUpdatingThresholdTable==NO )
    {
        mIsUpdatingVitalityTables=NO;
        
        [binsDataController uploadTask];
    }
    
}

@end



@implementation DDBTableUser

+ (NSString *)dynamoDBTableName {
    return @"BinsUserTable";
}

+ (NSString *)hashKeyAttribute {
    return @"userId";
}

+ (NSString *)rangeKeyAttribute {
    return @"userName";
}

+ (NSArray *)ignoreAttributes {
    return @[@"internalName",@"internalState"];
}

@end

@implementation DDBTableActivity

+ (NSString *)dynamoDBTableName {
    return @"BinsActivityTable";
}

+ (NSString *)hashKeyAttribute {
    return @"userId";
}

+ (NSString *)rangeKeyAttribute {
    return @"activityTime";
}

+ (NSArray *)ignoreAttributes {
    return @[@"internalName",@"internalState"];
}

@end



@implementation  DDBTableSleepThreshold


+ (NSString *)dynamoDBTableName {
    return @"BinsSleepThresholdTable";
}

+ (NSString *)hashKeyAttribute {
    return @"userId";
}

+ (NSString *)rangeKeyAttribute {
    return @"thresholdId";
}

@end

@implementation DDBTableTimeEffect

+ (NSString *)dynamoDBTableName {
    return @"BinsTimeEffectTable";
}

+ (NSString *)hashKeyAttribute {
    return @"threshId";
}

+ (NSString *)rangeKeyAttribute {
    return @"timeEffectNthHour";
}

@end

@implementation DDBTableCircadian

+ (NSString *)dynamoDBTableName {
    return @"BinsCircadianTable";
}

+ (NSString *)hashKeyAttribute {
    return @"circadianId";
}

+ (NSString *)rangeKeyAttribute {
    return @"circadianHour24InDay";
}


@end
