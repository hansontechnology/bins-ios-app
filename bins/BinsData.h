//
//  binsData.h
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <Foundation/Foundation.h>

#define INVALID_USER_ID  0

@interface BinsData : NSObject

struct ActivityTable {
    char *tableName;
    char *recordID;
    char *user;
    char *device;
    char *type;
    char *value;
    char *time;
    char *uploaded;
} ;

struct DeviceTable {
    char *tableName;
    char *recordID;
    char *deviceName;
    char *deviceModel;
    char *deviceVersion;
    char *deviceBatteryLevel;
    char *deviceLastSyncTime;
    char *deviceAddress;
    char *deviceConfiguration;
    char *finderThreshold;
    char *shakeUiThreshold;
    char *ibeaconUuid;
};

struct UserTable {
    char *tableName;
    char *recordID;
    char *userUID;
    char *userFirstName;
    char *userLastName;
    char *userName;
    char *userDOB;
    char *userWeight;
    char *userHeight;
    char *userGender;
    char *userGoal;
    char *userDevice;
    char *finderEnabled;
    char *sleepEnabled;
    char *alarmTime;
    char *sleepCheckPeriod;
    char *isUserFromNetwork;
    char *userPassword;
    char *userLastSyncTime;
    char *userStartTime;
};

struct ActiveUserTable {
    char *tableName;
    char *userID;
    char *backgroundSync;
    char *filterDummy;
};

@property struct ActivityTable const *activity;
@property struct UserTable const *user;
@property struct DeviceTable const *device;

@property struct ActiveUserTable const *activeUser;



// 20150624


// 150516
struct SleepThresholdTable {
    char *tableName;
    char *sleepThreshId;
    char *sleepThreshLevel;
    char *sleepThreshName;
    char *sleepThreshValue;
    char *sleepThreshState;
};

// 150516
struct SleepTimeEffectTable {
    char *tableName;
    char *sleepTimeEffectThreshId;
    char *sleepTimeEffectHour;
    char *sleepTimeEffectPercSleepFactor;
    char *sleepTimeEffectPercWakeFactor;
};

// 150516"
struct CircadianTable {
    char *tableName;
    char *circId;
    char *cirHour;
    char *circPercSleepFactor;
    char *circPercWakeFactor;
};

// 150516
struct CubeLevelPeriodsTable {
    char *tableName;
    char *userId;
    char *threshId;
    char *dtmStart;
    char *dtmEnd;
    char *deHours;
    char *nightSleepStart;
    char *note;
};

// 150914
struct VitalityTable {
    char *tableName;
    char *userId;
    char *date;
    char *vitality;
    char *isIdleTimeIgnored;
};

@property struct SleepThresholdTable const *sleepThreshold;
@property struct SleepTimeEffectTable const *sleepTimeEffect;
@property struct CircadianTable const *circadian;
@property struct CubeLevelPeriodsTable const *cubeLevelPeriods;
@property struct VitalityTable const *vitality;

@end
