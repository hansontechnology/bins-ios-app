//
//  binsData.m
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "BinsData.h"

@implementation BinsData

@synthesize activity;
@synthesize device;
@synthesize user;
@synthesize activeUser;
@synthesize circadian;
@synthesize sleepThreshold;
@synthesize sleepTimeEffect;
@synthesize cubeLevelPeriods;
@synthesize vitality;


struct ActivityTable activityTable = {
    "activityTable",
    "recordID",
    "user",
    "device",
    "type",
    "value",
    "time",
    "uploaded"
} ;

struct DeviceTable deviceTable = {
    "deviceTable",
    "recordID",
    "deviceName",
    "deviceModel",
    "deviceVersion",
    "deviceBatteryLevel",
    "deviceLastSyncTime",
    "deviceAddress",
    "deviceConfiguration",
    "finderThreshold",
    "shakeUiThreshold",
    "ibeaconUuid"
};

struct UserTable userTable = {
    "userTable",
    "recordID",
    "userUID",
    "userFirstName",
    "userLastName",
    "userName",
    "userDOB",
    "userWeight",
    "userHeight",
    "userGender",
    "userGoal",
    "userDevice",
    "finderEnabled",
    "sleepEnabled",
    "alarmTime",
    "sleepCheckPeriod",
    "isUserFromNetwork",
    "userPassword",
    "userLastSyncTime",
    "userStartTime"
};


struct ActiveUserTable activeUserTable = {
    "activeUserTable",
    "userID",
    "backgroundSync",
    "filterDummy"
};


//20150624


// 150516
struct SleepThresholdTable sleepThresholdTable = {
    "sleepThresholdTable",
    "sleepThreshId",
    "sleepThreshLevel",
    "sleepThreshName",
    "sleepThreshValue",
    "sleepThreshState"
};

// 150516
struct SleepTimeEffectTable sleepTimeEffectTable = {
    "sleepTimeEffectTable",
    "sleepTimeEffectThreshId",
    "sleepTimeEffectHour",
    "sleepTimeEffectPercSleepFactor",
    "sleepTimeEffectPercWakeFactor"
};

// 150516"
struct CircadianTable circadianTable = {
    "circadianTable",
    "circId",
    "cirHour",
    "circPercSleepFactor",
    "circPercWakeFactor",
};

// 150516"
struct CubeLevelPeriodsTable cubeLevelPeriodsTable = {
    "cuveLevelPeriodsTable",
    "userId",
    "threshold",
    "dtmStart",
    "dtmEnd",
    "deHours",
    "nightSleepStart",
    "note",
 };


// 150914
struct VitalityTable vitalityTable = {
    "vitalityTable",
    "userId",
    "date",
    "isIdleTimeIgnored"
};


-(id)init {
    activity=&activityTable;
    device=&deviceTable;
    user=&userTable;
    activeUser=&activeUserTable;
    circadian=&circadianTable;
    sleepTimeEffect=&sleepTimeEffectTable;
    sleepThreshold=&sleepThresholdTable;
    cubeLevelPeriods=&cubeLevelPeriodsTable;
    vitality=&vitalityTable;
    return self;
}

@end
