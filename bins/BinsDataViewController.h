//
//  BinsDataViewController.h
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "BinsData.h"
#import "NotificationControllerView.h"
#import "activity_tracking_service.h"
#import "UnlockingLifeService.h"
#import "BinsCloudService.h"

#define BINS_BATTERY_LOW_LEVEL  35
#define BINS_FINDER_LEVEL_MAX   6
#define BINS_SHAKE_LEVEL_MAX 8

#define BINS_ACTIVITY_TYPE_STEPS 1
#define BINS_ACTIVITY_TYPE_SLEEP 3
#define BINS_ACTIVITY_TYPE_PULSE 2
#define BINS_ACTIVITY_TYPE_LIGHT 4

#define BINS_MODEL_BINSX1   0
#define BINS_MODEL_BINSX2   1

#define TIME_PERIOD_DAY_MINUTES (24*60)
#define TIME_PERIOD_MONTH_MINUTES (31*24*60)

#define DEFAULT_BINS_CONFIGURATION      (BINS_CONFIG_ACTIVITY_NOTIFY_ENABLE_MASK | BINS_CONFIG_HEARTRATE_ENABLE_MASK | BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK |    BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK    )


#define DEFAULT_BINS_USER_GOAL 10000

#define DEFAULT_BINS_FINDER_THRESHOLD (BINS_FINDER_LEVEL_MAX/2)
#define DEFAULT_BINS_SHAKE_UI_THRESHOLD (BINS_SHAKE_LEVEL_MAX/2)

#define DEFAULT_BINS_FINDER_ENABLE  0
#define DEFAULT_BINS_SLEEP_ENABLE   1

@class BinsDataViewController;

@protocol BinsDataControllerDelegate
- (void)binsDataChanged:(BinsDataViewController*)binsDataController;
@end


@interface BinsDataViewController : UIViewController <UnlockingLifeServiceDelegate, BinsCloudServiceDelegate>


@property (assign) id <BinsDataControllerDelegate> delegate;

@property BinsCloudService *binsCloudServer;

-(int)getActiveUserID;
-(void)setActiveUserID: (int)user;
-(void)resetActiveUserID;

-(int)getActiveDeviceType ;
-(void)setActiveDeviceType: (int)type;


-(void)insertOrUpdateActivity: (NSInteger)whenTime withType: (int)type withValue: (int)value;
-(void)insertOrUpdateActivitySetUploaded: (NSInteger)whenTime withType: (int)type withValue: (int)value;

-(void)insertNewActivity: (NSInteger)whenTime withType: (int)type withValue: (int)value;

-(BOOL)getActivityValue: (NSInteger)time withType: (int)type withValue: (int*)value;

-(BOOL)getUserProfile: (int)userID userNameIs: (NSString **)userName firstNameIs: (NSString**) firstName lastNameIs: (NSString**)lastName dobIs: (NSString**)dob weightIs: (int*)weight heightIs: (int*)height genderIs: (int*)gender withPassword: (NSString**)password isRemoteUser: (BOOL*)isRemote uidIs:(NSString**)uid lastSyncTimeIs: (long*)lastSyncTime;

-(BOOL)setUserProfile: (int)userID userNameIs: (NSString *)userName firstNameIs: (NSString*)firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender withPassword: (NSString*)password isRemoteUser: (BOOL)isRemote lastSyncTimeIs: (long)lastSyncTime;

-(void)insertNewUser;

-(BOOL) insertNewUser: (NSString *)userName firstNameIs: (NSString*) firstName lastNameIs: (NSString*) lastName dobIs: (NSString*)dob weightIs: (int) weight heightIs: (int) height genderIs: (int) gender withPassword: (NSString*)password isRemoteUser: (BOOL)isRemote uidIs:(NSString*)uid lastSyncTimeIs: (long)lastSyncTime;


-(BOOL)hasUserExistingName: (NSString*)userName;

-(int)getUserIdByName: (NSString*)userName;

-(BOOL)getNextUserFromQuery: (int*)user withName: (NSString**)name;
-(BOOL)queryUsers;

-(BOOL)deleteUserIfPossible: (int)userID;


-(BOOL)hasSetupAlready;

-(NSString*)getUserName: (int) userID;
-(int)getDeviceID: (int) userID;
-(int)getActiveDeviceID;

-(void)setActiveDeviceID: (int)deviceID;
-(void)addDeviceAddress: (int)userID withDevice: (NSUUID*)deviceAddress;
-(void)setActiveDeviceAddress: (NSUUID*)deviceAddress;
-(NSUUID*)getActiveDeviceAddress;


-(void)setFinderThreshold: (bool)enable withFinderThreshold: (int)threshValue;
-(void)getFinderThreshold: (bool*)enable hasThreshold: (int*) threshValue;

-(void)setFinderThreshold: (int)userID withEnable: (bool)enable withFinderThreshold: (int)threshValue;
-(void)getFinderThreshold: (int)userID isEnabled: (bool*)enable hasThreshold: (int*) threshValue;

-(void)setShakeUiThreshold: (int)userID withThreshold: (int)threshold ;
-(int)getShakeUiThreshold: (int)userID ;

-(void)setShakUiThreshold: (int)threshold;
-(int)getShakeUiThreshold;

-(void)setActiveDeviceLastSyncTime: (NSInteger)time;
-(NSInteger)getActiveDeviceLastSyncTime;

-(long)nextAvailableSleepTime: (long*)sleepRecordTime;
-(long)previousAvailableSleepTime: (long*)sleepRecordTime;

-(void)setActiveUserLastSyncTime: (NSInteger)time;
-(NSInteger)getActiveUserLastSyncTime;
-(NSInteger)getActiveUserCreateTime;

-(void)setActiveDeviceBatteryLevel: (int)batteryLevel;
-(int)getActiveDeviceBatteryLevel;

-(void)setActiveDeviceVersion: (int)major withMinor: (int)minor ;
-(void)getActiveDeviceVersion: (int*)major withMinor: (int*)minor;

-(uint32_t)getActiveDeviceConfiguration;
-(void)setActiveDeviceConfiguration: (uint32_t)configuration;

-(void)setSleepEnable: (bool) enable;
-(BOOL)getSleepEnable;

-(bool)getAlarmEnable;
-(int)getAlarmTime;
-(void)setAlarmEnable: (bool)alarmEnable;
-(void)setAlarmTime: (int)alarm;
-(void)setAlarmTime: (int)alarm withEnable: (bool)alarmEnable;

-(void)insertPulseRecord: (int)pulse;


-(int)getDailyGoal;
-(void)setDailyGoal: (int)goal;


-(int)getBackgroundSync;
-(void)toggleBackgroundSync;
-(void)setBackgroundSync: (int)enable;



-(void)getActivityPeriod: (int)user withType: (int)type onTime: (NSInteger)time forPeriod:(NSInteger)period hasSteps: (int*)steps hasActiveMinutes: (int*)minutes;

-(NSInteger)timeIntervalInMinute: (NSDate*)date withUnitMinute: (BOOL)isMinuteBase;


#define QUERY_COMPLETED 0
#define QUERY_STOPPED 1
#define QUERY_WORKING 2

-(int)queryActivityRecords: (int)user typeOf: (int)type timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime ;

-(BOOL)getNextActivityFromQuery: (int*)value atTime: (NSInteger*)time;

-(int)queryActivityRecordsWithCloudLoad: (int)user typeOf: (int)type timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime ;


@property NotificationControllerView *notificationController;

-(void)setActiveShakeUiThreshold: (int)threshold;
-(int)getActiveShakeUiThreshold;

-(bool)isDeviceFirstTimeSync;

-(void) setIbeaconUuid: (NSString*) ibeaconUuid;
-(NSString*) getIbeaconUuid;

//--- 20151124
-(BOOL)updateThresholdItem: (int)threshId withName: (const char*)name withMovements: (int)movements withSleepState: (int) sleepState;

-(BOOL)updateSleepTimeEffectItem: (int)threshId withHour: (int)hour withRecovery: (float)recoveryFactor;
    
-(BOOL)updateCircadianItem: (int)circadianId withHour: (int)hour withSleepFactor: (float)sleepFactor withWakeFactor: (float)wakeFactor;

-(void)clearSleepTimeEffectTable;
//-----

-(void) updateSleepThresholdTable: (NSArray*) recordList;
-(void) updateCircadianTable: (NSArray*) recordList;
-(void) updateSleepTimeEffectTable: (NSArray*) recordList;


-(BOOL) getLevelPeriodsFrom: (long)startTime to: (long)endTime total: (int*) totalNumber;

-(BOOL) getNextLevelPeriod: (int*)threshId from: (long*)timeStart to: (long*)timeEnd;


-(int) getThresholdId: (int) movements;


-(int) getSleepStateFromThreshold: (int) threshId;

-(BOOL) getCircadian: (int) hour toSleepFactor: (float*) sleepFactor toWakeFactor: (float*)wakeFactor;
-(BOOL) getTimeEffect: (int)threhId timeEffect: (int)hour toSleepFactor: (float*) sleepFactor toWakeFactor: (float*)wakeFactor;

-(BOOL) getThresholds;
-(BOOL) getNextThresholdFromQuery: (int*)threshId movements: (int*)movements;

-(int) getThresholdsCount;

-(void) insertCubeLevelPeriodFrom: (long) startTime to: (long) endTime atThreshold: (int) threshId;

-(int)querySleepRecords: (int)user timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime;
-(BOOL)getNextSleepRecord: (int*)movements ofType: (int*)type atTime: (long*) sleepTime;

-(int)querySleepRecordsWithCloudLoad: (int)user timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime;

-(void) clearCubeLevelPeriods;
    
-(void)uploadTask;

typedef float (^CheckDistanceCallBack)(int userHeight, float stepsPerUnitSampleTime);

-(void)getActivityPeriod: (int)user withType: (int)type onTime: (NSInteger)time forPeriod:(NSInteger)period hasSteps: (int*)steps hasActiveMinutes: (int*)minutes andDistance: (int*)distance usingDistanceChecker:(CheckDistanceCallBack)distanceChecker;

-(void)releaseUserDevice: (int)userID ;

-(void)toggleFilterUnused;
-(int)getFilterUnused ;
-(void)setFilterUnused: (int)enable;


-(BOOL)userIsRemoteUser: (int)user;
-(BOOL)activeUserIsRemote ;

@end
