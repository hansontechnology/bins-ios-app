//
//  BinsDataViewController.m
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "BinsDataViewController.h"
#import "DeviceViewController.h"

@implementation BinsDataViewController


@synthesize binsCloudServer;

static NotificationControllerView *notificationController;

static BOOL             isInitiated=NO;
static char             strSqlStatement[2048];
//-- static sqlite3_stmt     *statement;

static sqlite3_stmt     *activeStatement;
static sqlite3_stmt     *activeUploadStatement;

static sqlite3 *binsDB=nil;

static BinsData *binsData;
UnlockingLifeService *unlockingLifeServer=nil;

static int currentUserID=-1;

static BOOL mIsCheckingUsernameOverServer=NO;

static BOOL mDataIsUploading=NO;

NSString *databasePath;

- (void)baseInit {
    
    if(isInitiated==YES) return;
    
    isInitiated=YES;
 
    
    binsData=[[BinsData alloc] init];
    
#ifdef CONFIG_BINS_HBP
    unlockingLifeServer=[[UnlockingLifeService alloc] init];
    unlockingLifeServer.delegate=self;
#endif
    
    
    NSString *docsDir;
    NSArray *dirPaths;

    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                              initWithString: [docsDir stringByAppendingPathComponent:
                                               @"bins.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &binsDB) == SQLITE_OK)
        {
            [self createActivityTable:binsDB];
            [self createUserTable:binsDB];
            [self createDeviceTable:binsDB];
            [self createCircadianTable:binsDB];
            [self createSleepTimeEffectTable:binsDB];
            [self createSleepThresholdTable:binsDB];
            [self createSleepCubeLevelPeriodsTable:binsDB];
            
            // sqlite3_close(binsDB);
            
        } else {
            NSLog(@"Failed to create database");
        }
    }
    else{
        
        if (sqlite3_open([databasePath UTF8String], &binsDB) != SQLITE_OK)
        {
            NSLog(@"Failed to open database");
        }
    }
  
    
    notificationController=[[NotificationControllerView alloc] init];
    
 }


- (id)init {
    
    self=[super init];
    
    [self baseInit];

    binsCloudServer=[[BinsCloudService alloc] init];
    binsCloudServer.delegate=self;

    [self getActiveUserID];
    [self getActiveDeviceID];
    
    return self;
}


- (void)viewDidLoad {
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the database file
    NSString *databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"bins.db"]];
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &binsDB) == SQLITE_OK)
        {
            [self createActivityTable:binsDB];
            [self createUserTable:binsDB];
            [self createDeviceTable:binsDB];
            
            // sqlite3_close(binsDB);
            
        } else {
            NSLog(@"Failed to open/create database");
        }
    }
    // [filemgr release];
    
    [super viewDidLoad];
}




-(void)createActivityTable: (sqlite3*)db {
    
    char *errMsg;
    sprintf(strSqlStatement,
                                "CREATE TABLE IF NOT EXISTS %s (   \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s                             \
                                ) ;",
                                binsData.activity->tableName,
                                binsData.activity->recordID,  "INTEGER PRIMARY KEY AUTOINCREMENT",
                                binsData.activity->user,      "INTEGER",
                                binsData.activity->device,    "INTEGER",
                                binsData.activity->type,      "INTEGER",
                                binsData.activity->value,     "INTEGER",
                                binsData.activity->time,      "DOUBLE",
                                binsData.activity->uploaded,  "INTEGER DEFAULT 0"
                                );
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        NSLog(@"Failed to create activity table");
        
    }
    
}


-(void)createUserTable: (sqlite3*)db {
    
    char *errMsg;
    
    sprintf(strSqlStatement,
                                "CREATE TABLE IF NOT EXISTS %s (  \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s,                            \
                                %s %s                             \
                                ) ;",
                                binsData.user->tableName,
                                binsData.user->recordID,   "INTEGER PRIMARY KEY AUTOINCREMENT",
                                binsData.user->userUID,   "TEXT",
                                binsData.user->userName,   "TEXT",
                                binsData.user->userFirstName,   "TEXT",
                                binsData.user->userLastName,   "TEXT",
                                binsData.user->userDOB,    "TEXT",
                                binsData.user->userWeight, "INTEGER",
                                binsData.user->userHeight, "INTEGER",
                                binsData.user->userGender, "INTEGER",
                                binsData.user->userGoal,   "INTEGER",
                                binsData.user->userDevice, "INTEGER",
                                binsData.user->finderEnabled, "INTEGER",
                                binsData.user->sleepEnabled, "INTEGER",
                                binsData.user->alarmTime,   "INTEGER",
                                binsData.user->sleepCheckPeriod, "INTEGER",
                                binsData.user->isUserFromNetwork,   "INTEGER",
                                binsData.user->userPassword, "TEXT",
                                binsData.user->userLastSyncTime, "DOUBLE",
                                binsData.user->userStartTime, "DOUBLE"
                                );
    
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        
        NSLog(@"Failed to create user table");
        
    }
    
    
   sprintf(strSqlStatement,
                              "CREATE TABLE IF NOT EXISTS %s (  \
                              %s %s,                            \
                              %s %s,                            \
                              %s %s                             \
                              ) ;",
                              binsData.activeUser->tableName,
                              binsData.activeUser->userID,          "INTEGER",
                              binsData.activeUser->backgroundSync,  "INTEGER",
                              binsData.activeUser->filterDummy,  "INTEGER"
                              );
    
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        NSLog(@"Failed to create active user table");
        
    }
    
    
 
}

-(void)createDeviceTable: (sqlite3*)db {
    
    char *errMsg;

    sprintf(strSqlStatement,
            "CREATE TABLE IF NOT EXISTS %s (  \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s                             \
            ) ;",
            binsData.device->tableName,
            binsData.device->recordID,              "INTEGER PRIMARY KEY AUTOINCREMENT",
            binsData.device->deviceName,            "TEXT",
            binsData.device->deviceModel,           "INTEGER",
            binsData.device->deviceVersion,         "TEXT",
            binsData.device->deviceBatteryLevel,    "INTEGER",
            binsData.device->deviceLastSyncTime,    "DOUBLE",
            binsData.device->deviceAddress,         "INTEGER",
            binsData.device->deviceConfiguration,   "INTEGER",
            binsData.device->finderThreshold,       "INTEGER",
            binsData.device->shakeUiThreshold,      "INTEGER",
            binsData.device->ibeaconUuid,           "TEXT"
            );
    
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        // NSLog(@"SQLITE: Error '%s'", sqlite3_errmsg(binsDB));
        
        NSLog(@"Failed to create device table");
        
    }
    
}


// 20150624
// 150516
-(void) createSleepCubeLevelPeriodsTable: (sqlite3*)db {
    
    char *errMsg;
    
    sprintf(strSqlStatement,
            "CREATE TABLE IF NOT EXISTS %s (  \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s                            \
            ) ;",
               binsData.cubeLevelPeriods->tableName,
               binsData.cubeLevelPeriods->userId, "INTEGER",
               binsData.cubeLevelPeriods->threshId, "INTEGER",
               binsData.cubeLevelPeriods->dtmStart, "INTEGER",
               binsData.cubeLevelPeriods->dtmEnd, "INTEGER",
               binsData.cubeLevelPeriods->deHours, "DOUBLE",
               binsData.cubeLevelPeriods->nightSleepStart, "INTEGER",
               binsData.cubeLevelPeriods->note, "TEXT"
            );
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        // NSLog(@"SQLITE: Error '%s'", sqlite3_errmsg(binsDB));
        
        NSLog(@"Failed to create CubeLevelPeriods table");
        
    }

            
}

// 150516
-(void) createCircadianTable: (sqlite3*)db {
    char *errMsg;
    
    sprintf(strSqlStatement,
            "CREATE TABLE IF NOT EXISTS %s (  \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s                             \
            ) ;",
                binsData.circadian->tableName,
                binsData.circadian->circId , "INTEGER",
                binsData.circadian->cirHour , "INTEGER",
                binsData.circadian->circPercSleepFactor , "DOUBLE",
                binsData.circadian->circPercWakeFactor , "DOUBLE"
            );
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        // NSLog(@"SQLITE: Error '%s'", sqlite3_errmsg(binsDB));
        
        NSLog(@"Failed to create Circadian table");
        
    }
    
}

// 150516
-(void) createSleepTimeEffectTable: (sqlite3*)db {
    char *errMsg;
    
    sprintf(strSqlStatement,
            "CREATE TABLE IF NOT EXISTS %s (  \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s                             \
            ) ;",
                binsData.sleepTimeEffect->tableName,
                binsData.sleepTimeEffect->sleepTimeEffectThreshId, "INTEGER",
                binsData.sleepTimeEffect->sleepTimeEffectHour, "INTEGER",
                binsData.sleepTimeEffect->sleepTimeEffectPercSleepFactor , "DOUBLE",
                binsData.sleepTimeEffect->sleepTimeEffectPercWakeFactor, "DOUBLE"
            );
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        // NSLog(@"SQLITE: Error '%s'", sqlite3_errmsg(binsDB));
        
        NSLog(@"Failed to create SleepTimeEffect table");
        
    }
    
}

// 150516
-(void) createSleepThresholdTable: (sqlite3*)db {
    char *errMsg;
    
    //-- NSLog(@"createSleepThresholdTable");
    
    sprintf(strSqlStatement,
            "CREATE TABLE IF NOT EXISTS %s (  \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s,                            \
            %s %s                            \
            ) ;",
               binsData.sleepThreshold->tableName ,
               binsData.sleepThreshold->sleepThreshId, "INTEGER",
               binsData.sleepThreshold->sleepThreshLevel, "TEXT",
               binsData.sleepThreshold->sleepThreshName, "TEXT",
               binsData.sleepThreshold->sleepThreshValue, "INTEGER",
               binsData.sleepThreshold->sleepThreshState , "INTEGER"
            );
    
    if(sqlite3_exec(binsDB, strSqlStatement, NULL, NULL, &errMsg) != SQLITE_OK)
    {
        //-- NSLog(@"SQLITE: Error '%s'", sqlite3_errmsg(binsDB));
        
        NSLog(@"Failed to create SleepThreshold table");
        
    }
    
}


-(void)insertPulseRecord:(int)pulse {
    // TODO
}

-(void)insertOrUpdateActivitySetUploaded:(NSInteger)whenTime withType: (int)type withValue: (int)value {
    
    if([self updateActivityValueSetUploaded:whenTime withType:type withValue:value]==NO){
        [self insertNewActivitySetUploaded:whenTime withType:type withValue:value];
    }
}
-(void) insertNewActivitySetUploaded: (NSInteger)whenTime withType: (int)type withValue: (int)value {
    
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %d,                         \
            %ld,                         \
            %d,                         \
            %d,                         \
            %d                         \
            ); ",
            binsData.activity->tableName,
            binsData.activity->user,
            binsData.activity->time,
            binsData.activity->type,
            binsData.activity->uploaded,
            binsData.activity->value,
            currentUserID,
            (long)whenTime,
            type,
            1,
            value
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add activity");
        }
    }
    else{
        NSLog(@"New Activity SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}

-(BOOL)updateActivityValueSetUploaded:(NSInteger)time withType: (int)type withValue: (int)value {
    
    BOOL isSuccess=NO;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d,                      \
            %s=%d                           \
            WHERE                           \
            %s=%d AND                     \
            %s=%ld AND                       \
            %s=%d                       \
            ; ",
            binsData.activity->tableName,
            binsData.activity->value, value,
            binsData.activity->uploaded, 1,
            binsData.activity->user,currentUserID,
            binsData.activity->time, (long)time,
            binsData.activity->type, type
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"Update Activity SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        
        NSLog( @"Failed to update user's activity");
    }
    else{
        if(sqlite3_changes(binsDB)>0){
            isSuccess=YES;
        }
    }
    
    sqlite3_finalize(statement);
    
    return isSuccess;
    
}


-(void)insertOrUpdateActivity: (NSInteger)whenTime withType: (int)type withValue: (int)value {
    
    if([self updateActivityValue:whenTime withType:type withValue:value]==NO){
        
        [self insertNewActivity:whenTime withType:type withValue:value];        
    }
}

-(void) insertNewActivity: (NSInteger)whenTime withType: (int)type withValue: (int)value {
    
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %d,                         \
            %ld,                         \
            %d,                         \
            %d,                         \
            %d                         \
            ); ",
            binsData.activity->tableName,
            binsData.activity->user,
            binsData.activity->time,
            binsData.activity->type,
            binsData.activity->uploaded,
            binsData.activity->value,
            currentUserID,
           (long)whenTime,
            type,
            0,
            value
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add activity");
        }
    }
    else{
        NSLog(@"New Activity SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}

-(BOOL)updateActivityValue:(NSInteger)time withType: (int)type withValue: (int)value {
    
    BOOL isSuccess=NO;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d,                      \
            %s=%d                      \
            WHERE                           \
            %s=%d AND                     \
            %s=%ld AND                       \
            %s=%d                       \
            ; ",
            binsData.activity->tableName,
            binsData.activity->value, value,
            binsData.activity->uploaded, 0,
            binsData.activity->user,currentUserID,
            binsData.activity->time, (long)time,
            binsData.activity->type, type
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"Update Activity SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        
        NSLog( @"Failed to update user's activity");
    }
    else{
        if(sqlite3_changes(binsDB)>0){
            isSuccess=YES;
        }
    }
    
    sqlite3_finalize(statement);
    
    return isSuccess;

}

-(BOOL)getActivityValue: (NSInteger)time withType: (int)type withValue: (int*)value {
    
    BOOL isSuccess=NO;
    *value=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d AND %s=%ld AND %s=%d;",
            binsData.activity->value,
            binsData.activity->tableName,
            binsData.activity->user, currentUserID,
            binsData.activity->time, (long)time,
            binsData.activity->type, type
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                *value=sqlite3_column_int(statement, 0);
            
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    return isSuccess;
}


-(void)insertNewUser {
    
    // force to release current active user
    [self setActiveUserID:INVALID_USER_ID];

    /*
    NSString*userName=@"Me@example.com";
    
    int userExisting=[self getUserIdByName:userName];
    if(userExisting!=0){
        [self setActiveUserID:userExisting];
        return; // avoid duplicate the default user
    }
    */
    
    
    /*
    NSString*dob=[NSString stringWithUTF8String:"1968/04/30"];
    int weight=70;
    int height=172;
    int gender=0;

    [self insertNewUser: userName firstNameIs: @"" lastNameIs: @"" dobIs:dob weightIs: weight heightIs: height genderIs: gender];
    */
}

-(BOOL) insertNewUser: (NSString *)userName firstNameIs: (NSString*) firstName lastNameIs: (NSString*) lastName dobIs: (NSString*)dob weightIs: (int) weight heightIs: (int) height genderIs: (int) gender withPassword: (NSString*)password isRemoteUser: (BOOL)isRemote uidIs: (NSString*)uid lastSyncTimeIs:(long)lastSyncTime{

    BOOL isSuccessToCallCloud=NO;
    
    int intIsRemote=0;
    if(isRemote==YES) intIsRemote=1;
    
#ifdef CONFIG_BINS_HBP
    unlockingLifeServer.delegate=self;
    
    //-- NSLog(@"insertNewUser last name=%@", lastName);
    
    mIsCheckingUsernameOverServer=YES;

    [unlockingLifeServer addNewUser: userName firstNameIs: firstName lastNameIs: lastName dobIs: dob weightIs: weight heightIs: height genderIs: gender];
#else
    
    //-- 20151119
    
    binsCloudServer.delegate=self;
    
    NSLog(@"calling binsCloud addNewUser  email=%@", userName);
    
    mIsCheckingUsernameOverServer=YES;
    
    isSuccessToCallCloud=[binsCloudServer insertOrUpdateUserProfile: uid userNameIs: userName firstNameIs: firstName lastNameIs: lastName dobIs: dob weightIs: weight heightIs: height genderIs: gender withPassword: password];
    
#endif
    
    //-- 20151126 used to note user profile creation time
    NSInteger currentTime=[self timeIntervalInMinute:[NSDate date] withUnitMinute:YES];

    
    sprintf(strSqlStatement,
            "INSERT INTO %s (               \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                              \
            ) VALUES (                      \
            \"%s\",                         \
            \"%s\",                         \
            \"%s\",                         \
            \"%s\",                         \
            %d,                             \
            %d,                             \
            %d,                              \
            %d,                             \
            %d,                             \
            %d,                              \
            \"%s\",                         \
            \"%s\",                         \
            %ld,                             \
            %ld                             \
            ); ",
            
            binsData.user->tableName,
            
            binsData.user->userName,
            binsData.user->userFirstName,
            binsData.user->userLastName,
            binsData.user->userDOB,
            binsData.user->userGoal,
            binsData.user->userWeight,
            binsData.user->userHeight,
            binsData.user->userGender,
            binsData.user->sleepEnabled,
            binsData.user->isUserFromNetwork,
            binsData.user->userUID,
            binsData.user->userPassword,
            binsData.user->userLastSyncTime,
            binsData.user->userStartTime,
            
            [userName UTF8String],
            [firstName UTF8String],
            [lastName UTF8String],
            [dob UTF8String],
            DEFAULT_BINS_USER_GOAL,
            weight,
            height,
            gender,
            DEFAULT_BINS_SLEEP_ENABLE,
            intIsRemote,  // from network
            [uid UTF8String],
            [password UTF8String],
            lastSyncTime,
            (long)(currentTime)
            );
  sqlite3_stmt     *statement;
  if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add user");
        }
        else{
            currentUserID=(int)sqlite3_last_insert_rowid(binsDB);
            
        }
    }
    else{
        NSLog(@"insert New User SQLITE: Failed to prepare statement! Error: '%s', '%s' ", sqlite3_errmsg(binsDB), strSqlStatement);
    }
    sqlite3_finalize(statement);
    
    [self setActiveUserID:currentUserID];
    
    
    return isSuccessToCallCloud;
}


-(BOOL)activeUserIsRemote {
    return [self userIsRemoteUser:currentUserID];
}

-(BOOL)userIsRemoteUser: (int)user{
    BOOL    isSuccess=NO;
    int     isFromNetwork=-1;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            binsData.user->isUserFromNetwork,
            binsData.user->tableName,
            binsData.user->recordID, user
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                isFromNetwork=sqlite3_column_int(statement, 0);
            
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    if(isFromNetwork==1) return YES;
    
    return NO;
}


-(BOOL)hasUserExistingName: (NSString*)userName{
    BOOL    isSuccess=NO;
    int     userID=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=\"%s\"",
            binsData.user->recordID,
            binsData.user->tableName,
            binsData.user->userName, [userName UTF8String]
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                userID=sqlite3_column_int(statement, 0);
            
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    return isSuccess;
}
-(int)getUserIdByName: (NSString*)userName {
  
    BOOL    isSuccess=NO;
    int     userID=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=\"%s\"",
            binsData.user->recordID,
            binsData.user->tableName,
            binsData.user->userName, [userName UTF8String]
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                userID=sqlite3_column_int(statement, 0);
            
            isSuccess=YES;
        }
    } else {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
    
    return userID;

}

-(NSString*)getUserName: (int) userID {
    
    BOOL isSuccess=NO;
    NSString *userName=@"";
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.user->userName,
            binsData.user->tableName,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                userName=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];

            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    return userName;
}

-(NSString*)getUserUID: (int) userID {
    
    BOOL isSuccess=NO;
    NSString *userUID=nil;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.user->userUID,
            binsData.user->tableName,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            userUID=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    return userUID;
}


-(BOOL)findUser: (int)userID {
    
    BOOL isSuccess=NO;
    
    sprintf(strSqlStatement,
            "SELECT * FROM %s WHERE       \
            %s=%d ",
            binsData.user->tableName,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    return isSuccess;
 
}

-(BOOL)setUserProfile: (int)userID userNameIs: (NSString *)userName firstNameIs: (NSString*)firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender withPassword: (NSString*)password isRemoteUser: (BOOL)isRemote lastSyncTimeIs: (long)lastSyncTime {
    
    BOOL isSuccessToCallCloud=NO;
    
    int intIsRemote=0;
    if(isRemote==YES) intIsRemote=1;

    BOOL isSuccess=NO;
    
    NSLog(@"setUserProfile last name=%@", lastName);

    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=\"%s\",                      \
            %s=\"%s\",                      \
            %s=\"%s\",                      \
            %s=\"%s\",                      \
            %s=%d,                      \
            %s=%d,                       \
            %s=%d,                       \
            %s=\"%s\" ,                      \
            %s=%ld                      \
            WHERE                           \
            %s=%d                      \
            ; ",
            
            binsData.user->tableName,
            binsData.user->userName,    [userName UTF8String],
            binsData.user->userFirstName,    [firstName UTF8String],
            binsData.user->userLastName,    [lastName UTF8String],            
            binsData.user->userDOB,     [dob UTF8String],
            binsData.user->userWeight,  weight,
            binsData.user->userHeight,  height,
            binsData.user->userGender,  gender,
            binsData.user->userPassword,  [password UTF8String],
            binsData.user->userLastSyncTime,  lastSyncTime,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        
        NSLog( @"Failed to update user's profile");
    }
    else{
        isSuccess=YES;
    }
    
    sqlite3_finalize(statement);
    
    
#ifndef CONFIG_BINS_HBP
    
    //-- 20151122
    if(isRemote==NO){   // only local user needs to update profile
        
        NSString *userUID=[self getUserUID: userID];

        binsCloudServer.delegate=self;
    
        isSuccessToCallCloud=[binsCloudServer insertOrUpdateUserProfile: userUID userNameIs: userName firstNameIs: firstName lastNameIs: lastName dobIs: dob weightIs: weight heightIs: height genderIs: gender withPassword: password];
    }
    
#endif

    return isSuccessToCallCloud;

}

-(void)setActiveUserLastSyncTime: (NSInteger)time {

    int user=[self getActiveUserID];
    
    sprintf(strSqlStatement,
                "UPDATE %s  SET                \
                %s=%ld                          \
                WHERE                           \
                %s=%d                       \
                ; ",
                
                binsData.user->tableName,
                binsData.user->userLastSyncTime, (long)time,
                binsData.user->recordID, user
                );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
    int sqlResult=sqlite3_step(statement);
    sqlite3_finalize(statement);
        
    if( sqlResult!= SQLITE_DONE)
    {
            NSLog( @"Failed to update user's last sync time");
    }
#ifndef CONFIG_BINS_HBP
    else{
            int user=[self getActiveUserID];
            NSString *userUID=[self getUserUID:user];
            
            NSString *userName=[self getUserName:user];
            [binsCloudServer setLastSyncTime: userUID withName: userName at: time];
    }
#endif
   
}

-(NSInteger)getActiveUserLastSyncTime {
    NSInteger time=0;
    
    int user=[self getActiveUserID];
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.user->userLastSyncTime,
            binsData.user->tableName,
            binsData.user->recordID, user
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            time=sqlite3_column_double(statement, 0);
        }
    }
    sqlite3_finalize(statement);
    return time;
    
}


-(BOOL)getUserProfile: (int)userID userNameIs: (NSString **)userName firstNameIs:(NSString **)firstName lastNameIs:(NSString **)lastName dobIs:(NSString *__autoreleasing *)dob weightIs:(int *)weight heightIs:(int *)height genderIs:(int *)gender withPassword: (NSString**)password isRemoteUser: (BOOL*)isRemote uidIs:(NSString**)uid lastSyncTimeIs: (long*)lastSyncTime {
    
    BOOL isSuccess=NO;
    
    *userName=@"";
    *dob=[NSString stringWithUTF8String:"1968/04/30"];
    *weight=70;
    *height=172;
    *gender=0;
    
    *lastName=@"";
    *firstName=@"";
    
    *lastSyncTime=0;
    
    sprintf(strSqlStatement,
            "SELECT %s, %s, %s, %s,%s,%s, %s, %s, %s,%s, %s FROM %s WHERE       \
            %s=%d ",
            binsData.user->userFirstName,
            binsData.user->userLastName,
            binsData.user->userName,
            binsData.user->userDOB,
            binsData.user->userWeight,
            binsData.user->userHeight,
            binsData.user->userGender,
            binsData.user->userPassword,
            binsData.user->isUserFromNetwork,
            binsData.user->userUID,
            binsData.user->userLastSyncTime,
            
            binsData.user->tableName,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            
            if(sqlite3_column_text(statement, 0)!=nil)
                *firstName=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];
            if(sqlite3_column_text(statement, 1)!=nil)
                *lastName=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 1)];
            if(sqlite3_column_text(statement, 2)!=nil)
                *userName=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 2)];
            if(sqlite3_column_text(statement, 3)!=nil)
                *dob=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 3)];
            *weight=sqlite3_column_int(statement, 4);
            *height=sqlite3_column_int(statement, 5);
            *gender=sqlite3_column_int(statement, 6);
            if(sqlite3_column_text(statement, 7)!=nil)  //-- 20151119
                *password=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 7)];
            int isFromNetwork=sqlite3_column_int(statement, 8);
            if(isFromNetwork==0) *isRemote=NO;
            else *isRemote=YES;
            *uid=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 9)];
            *lastSyncTime=sqlite3_column_int(statement, 10);
            
            
            isSuccess=YES;
        }
        
    }
    sqlite3_finalize(statement);
    
    NSLog(@"getUserProfile last name=%@", *lastName);

    return isSuccess;
}

-(BOOL)deleteUserIfPossible: (int)userID {
    //-- if([self numberOfUsers]<1) return NO;
    
    [self deleteUser: userID];
    
    if([self getActiveUserID]==userID){
        BOOL isSuccessQuery=[self queryUsers];
        int newUser;
        NSString *newUserName;
        if(isSuccessQuery==YES){
            if([self getNextUserFromQuery: (&newUser) withName:(&newUserName)]==YES){
                [self setActiveUserID:newUser];
                sqlite3_finalize(activeStatement);
            }else{
                [self resetActiveUserID];
            }
        }else{
            //-- no more availabele user, reflect it
            [self resetActiveUserID];
        }
    }
    return YES;
}

-(void) deleteUser: (int) userID {
    sqlite3_stmt    *statement;
 
    sprintf(strSqlStatement,
                                "DELETE FROM %s  WHERE         \
                                %s = %d                         \
                                 ",
                                
                                binsData.user->tableName,
                                binsData.user->recordID, userID
                                );
    
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)==SQLITE_OK){
        if (sqlite3_step(statement) != SQLITE_DONE) {
            NSLog(@"Failed to delete user %s", sqlite3_errmsg(binsDB));
        }
    }
    else{
        NSLog(@"Failed to delete user too %s", sqlite3_errmsg(binsDB));        
    }
   
    sqlite3_finalize(statement);
    
    
    sprintf(strSqlStatement,
            "DELETE FROM %s  WHERE         \
            %s = %d                         \
            ",
            
            binsData.activity->tableName,
            binsData.activity->user, userID
            );
    
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)==SQLITE_OK){
        if (sqlite3_step(statement) != SQLITE_DONE) {
            NSLog(@"Failed to delete user activities %s", sqlite3_errmsg(binsDB));
        }
    }
    else{
        NSLog(@"Failed to delete user activities too %s", sqlite3_errmsg(binsDB));
    }
    
    sqlite3_finalize(statement);
}


-(void)updateUserDevice {
    
}

-(void)setAlarmEnable: (bool)alarmEnable {
    int alarm=([self getAlarmTime] & 0x7FFF);
    
    if(alarmEnable==YES) alarm= alarm | 0x8000;
    
    [self setAlarmTime:alarm];
}
-(bool)getAlarmEnable {
    
    bool alarmEnable=NO;
    int alarm=[self getAlarmTime];
    if((alarm&0x8000)!=0) alarmEnable=YES;
    
    return alarmEnable;
    
}

-(int)getAlarmTime {
    
    int value=0;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            
            binsData.user->alarmTime,
            binsData.user->tableName,
            binsData.user->recordID, currentUserID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            value=sqlite3_column_int(statement, 0);
        }
        
    }
    sqlite3_finalize(statement);
    
    return value;
}

-(void)setAlarmTime: (int)alarm {
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                       \
            WHERE                           \
            %s=%d                      \
            ; ",
            
            binsData.user->tableName,
            binsData.user->alarmTime, alarm,
            binsData.user->recordID, currentUserID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update user's alarm");
    }
    
    sqlite3_finalize(statement);
    
}

-(void)setAlarmTime: (int)alarmTime withEnable: (bool)alarmEnable{
    
    int alarm= (alarmTime & 0x7FFF);
    if(alarmEnable==YES) alarm|=0x8000;
    
    [self setAlarmTime:alarm];
}

-(int)getDailyGoal {
    
    int goalValue=0;
    sprintf(strSqlStatement,
                                "SELECT %s FROM %s WHERE       \
                                %s=%d",
            
                                binsData.user->userGoal,
                                binsData.user->tableName,
                                binsData.user->recordID, currentUserID
                                );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            goalValue=sqlite3_column_int(statement, 0);
        }
        
    }
    sqlite3_finalize(statement);
    
    return goalValue;
}

-(void)setDailyGoal: (int)goal {
    
    sprintf(strSqlStatement,
                                "UPDATE %s  SET                \
                                %s=%d                       \
                                WHERE                           \
                                %s=%d                      \
                                ; ",
                                
                                binsData.user->tableName,
                                binsData.user->userGoal, goal,
                                binsData.user->recordID, currentUserID
                                );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update user's goal");
    }
    
    sqlite3_finalize(statement);
    
}
-(int)getActiveDeviceID {
    return [self getDeviceID:currentUserID];
}

-(int)getDeviceID: (int)userID {
    
    BOOL isSuccess=NO;
    int deviceID=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.user->userDevice,
            binsData.user->tableName,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            deviceID=sqlite3_column_int(statement, 0);
            
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    
    return deviceID;
}

-(void)setActiveDeviceID: (int)deviceID {
    
[self setDeviceID: currentUserID withDevice: deviceID];
    
}

-(NSUUID*)getActiveDeviceAddress {
    
    NSUUID* deviceAddress=nil;
    
    bool isSuccess=NO;
    int deviceID=[self getActiveDeviceID];
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            binsData.device->deviceAddress,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            // if(sqlite3_changes(binsDB)>0)
            {
                NSString *strDeviceAddress=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(statement, 0)];
                deviceAddress=[[NSUUID alloc] initWithUUIDString:strDeviceAddress];
                
                // deviceAddress=[[NSUUID alloc] initWithUUIDBytes:sqlite3_column_text(statement, 0)];
                isSuccess=YES;
            }
        }
    }
    sqlite3_finalize(statement);
    
    return deviceAddress;
}

-(bool) getDeviceID: (int*)deviceID fromAddress: (NSUUID*)deviceAddress {
    bool isSuccess=NO;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=\"%s\"",
            binsData.device->recordID,
            binsData.device->tableName,
            binsData.device->deviceAddress, [[deviceAddress UUIDString] UTF8String]
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            //if(sqlite3_changes(binsDB)>0)
            {
                *deviceID=sqlite3_column_int(statement, 0);
                isSuccess=YES;
            }
        }
    }
    sqlite3_finalize(statement);
    
    return isSuccess;
}

-(void)setActiveDeviceAddress: (NSUUID*)deviceAddress {
    [self addDeviceAddress:currentUserID withDevice:deviceAddress];
}
-(void)addDeviceAddress: (int)userID withDevice: (NSUUID*)deviceAddress {
    
    int deviceID;
    bool isSuccess=NO;

    if([self getDeviceID: &deviceID fromAddress: deviceAddress]==NO){
        
        int defaultConfiguration=DEFAULT_BINS_CONFIGURATION;
        sprintf(strSqlStatement,
                    "INSERT INTO %s (                   \
                    %s,                                 \
                    %s,                                  \
                    %s,                                 \
                    %s,                                 \
                    %s                                  \
                    ) VALUES (                          \
                    \"%s\",                             \
                    %d,                                  \
                    %d,                                  \
                    %d,                                  \
                    %ld                                  \
                    ); ",
                    binsData.device->tableName,
                
                    binsData.device->deviceAddress,
                    binsData.device->deviceConfiguration,
                    binsData.device->finderThreshold,
                    binsData.device->shakeUiThreshold,
                    binsData.device->deviceLastSyncTime,
                
                    [[deviceAddress  UUIDString] UTF8String],
                    defaultConfiguration,
                    DEFAULT_BINS_FINDER_THRESHOLD,
                    DEFAULT_BINS_SHAKE_UI_THRESHOLD,
                    0L
                    );
        sqlite3_stmt     *statement;
        if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK){
            if (sqlite3_step(statement) == SQLITE_DONE){
                isSuccess=YES;
            }
        }
        if(isSuccess==NO){
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        }
        sqlite3_finalize(statement);
        
        //-- 20151114
        if((defaultConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0){
            [self setSleepEnable:YES];
        }else{
            [self setSleepEnable:NO];
        }
        
        [self getDeviceID: &deviceID fromAddress: deviceAddress];
    }
    else{
        isSuccess=YES;
    }
    
    if(isSuccess==NO){
        NSLog(@"Failed to add device address for user");
    }
    
    int currentDeviceId=[self getActiveDeviceID];   //-- 20151129
    if(currentDeviceId!=deviceID){
        [self setDeviceID:userID withDevice:deviceID];
        [self setActiveDeviceLastSyncTime:0];       //-- 20151129 reset the field to handle special case for new bands paired
    }
}

-(void)setDeviceID: (int)userID withDevice: (int)deviceID{
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                      \
            WHERE                           \
            %s=%d                      \
            ; ",
            
            binsData.user->tableName,
            binsData.user->userDevice, deviceID,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));

        NSLog( @"Failed to update device ID");
    }
    
    sqlite3_finalize(statement);
    
}

-(int)getActiveDeviceType {
    
    BOOL isSuccess=NO;
    int type=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.device->deviceModel,
            binsData.device->tableName,
            binsData.device->recordID, [self getActiveDeviceID]
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            type=sqlite3_column_int(statement, 0);
            isSuccess=YES;
        }
    }
    sqlite3_finalize(statement);
    
    if(isSuccess==NO){
        NSLog(@"Failed to get device model");
        return -1;
    }
    
    return type;
}

-(void)setActiveDeviceType: (int)type {
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                       \
            WHERE                           \
            %s=%d                      \
            ; ",
            
            binsData.device->tableName,
            binsData.device->deviceModel,type,
            binsData.device->recordID, [self getActiveDeviceID]
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        NSLog( @"Failed to update device type");
    }
    
    sqlite3_finalize(statement);
    
}

-(void)setFinderThreshold: (bool) enable withFinderThreshold: (int)threshold
{
    [self setFinderThreshold:currentUserID withEnable:enable withFinderThreshold:threshold];
}

-(void)setFinderThreshold: (int)userID withEnable: (bool) enable withFinderThreshold: (int)threshold
 {
    
     int deviceID=[self getDeviceID:userID];
    
     [self setDeviceFinderThreshold: deviceID withFinderThreshold:threshold];
     
     int enableInt;
     
     if(enable==YES) enableInt=1;
     else enableInt=0;
     
     sprintf(strSqlStatement,
             "UPDATE %s  SET                \
             %s=%d                          \
             WHERE                           \
             %s=%d                       \
             ; ",
             
             binsData.user->tableName,
             binsData.user->finderEnabled, enableInt,
             binsData.user->recordID, userID
             );
     sqlite3_stmt     *statement;
     sqlite3_prepare_v2(binsDB, strSqlStatement,
                        -1, &statement, NULL);
     
     if (sqlite3_step(statement) != SQLITE_DONE)
     {
         NSLog( @"Failed to update device's finder enable");
     }
     
     sqlite3_finalize(statement);
     
}

-(void)getFinderThreshold: (bool*)enable hasThreshold: (int*)threshValue {
    
    [self getFinderThreshold:currentUserID isEnabled:enable hasThreshold:threshValue];
}

-(void)getFinderThreshold: (int)userID isEnabled: (bool*)enable hasThreshold: (int*)threshValue {
    
    int deviceID=[self getDeviceID: userID];
    

    *threshValue=[self getFinderThreshold: deviceID];
    
    
    int enabled=0;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            
            binsData.user->finderEnabled,
            binsData.user->tableName,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            enabled=sqlite3_column_int(statement, 0);
        }
        
    }
    sqlite3_finalize(statement);
    
    if(enabled==0) *enable=NO;
    else *enable=YES;
    
}

-(void)setSleepEnable: (bool) enable
{
    int enableInt;
    
    if(enable==YES) enableInt=1;
    else enableInt=0;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.user->tableName,
            binsData.user->sleepEnabled, enableInt,
            binsData.user->recordID, currentUserID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's finder enable");
    }
    
    sqlite3_finalize(statement);
    
}

-(BOOL)getSleepEnable {
    
    BOOL result=YES;
    
    int enabled=-1;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            
            binsData.user->sleepEnabled,
            binsData.user->tableName,
            binsData.user->recordID, currentUserID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            enabled=sqlite3_column_int(statement, 0);
        }
        
    }
    sqlite3_finalize(statement);
    
    if(enabled==0) result=NO;
    
    return result;
}


-(void)setDevice: (int)deviceID lastSyncTimeIs: (NSInteger)time {
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%ld                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.device->tableName,
            binsData.device->deviceLastSyncTime, (long)time,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    int sqlResult=sqlite3_step(statement);
    sqlite3_finalize(statement);

    if( sqlResult!= SQLITE_DONE)
    {
        NSLog( @"Failed to update device's finder threshold");
    }
}

-(void)setActiveDeviceLastSyncTime: (NSInteger)time {
    [self setDevice: [self getActiveDeviceID] lastSyncTimeIs: time];
}

-(NSInteger)getActiveUserCreateTime {
    NSInteger time=0;
    
    int user=[self getActiveUserID];
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.user->userStartTime,
            binsData.user->tableName,
            binsData.user->recordID, user
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            time=sqlite3_column_double(statement, 0);
        }
    }
    sqlite3_finalize(statement);
    return time;
}

-(NSInteger)getDeviceLastSyncTime: (int)deviceID {
    NSInteger time=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.device->deviceLastSyncTime,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            time=sqlite3_column_double(statement, 0);
        }
    }
    sqlite3_finalize(statement);
    return time;
}

-(NSInteger)getActiveDeviceLastSyncTime{
    return [self getDeviceLastSyncTime: [self getActiveDeviceID]];
}

-(uint32_t)getActiveDeviceConfiguration {
    
    uint32_t configuration=0;
    
    int deviceID=[self getActiveDeviceID];
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.device->deviceConfiguration,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            configuration=sqlite3_column_int(statement, 0);
            
        }
    }
    sqlite3_finalize(statement);
    
    
    BOOL enable=[self getSleepEnable];
    
    if(enable==YES){
        configuration |= (BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK);
    }else{
        configuration &= (~(BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK));
    }

    return configuration;
}

-(void)setActiveDeviceConfiguration: (uint32_t)configuration {

    int deviceID=[self getActiveDeviceID];
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.device->tableName,
            binsData.device->deviceConfiguration, configuration,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's configuration");
    }
    
    sqlite3_finalize(statement);
    
    if((configuration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK)!=0){
        [self setSleepEnable:YES];
    }else{
        [self setSleepEnable:NO];
    }
    
}

//-- 150502

-(void) setIbeaconUuid: (NSString*) ibeaconUuid
{    
    int deviceID=[self getActiveDeviceID];
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=\"%s\"                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.device->tableName,
            binsData.device->ibeaconUuid, [ibeaconUuid UTF8String],
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's beacon UUID");
    }
    
    sqlite3_finalize(statement);
}

-(NSString*) getIbeaconUuid {
    
    NSString *ibeaconUuid=@"";
    
    int deviceID=[self getActiveDeviceID];
    
    if(deviceID==0) return ibeaconUuid;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.device->ibeaconUuid,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            const unsigned char* charUuid=sqlite3_column_text(statement, 0);
            if(charUuid!=nil){
                ibeaconUuid=[[NSString alloc] initWithUTF8String:(const char*)charUuid];
            }
        }
    }
    sqlite3_finalize(statement);

    return ibeaconUuid;
}

-(int)getFinderThreshold: (int) deviceID {
    
    int threshold=-1;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            binsData.device->finderThreshold,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                threshold=sqlite3_column_int(statement, 0);
            
        }
    }
    sqlite3_finalize(statement);
    return threshold;
}


-(void)setDeviceFinderThreshold: (int)deviceID withFinderThreshold: (int)threshValue{

    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.device->tableName,
            binsData.device->finderThreshold, threshValue,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's finder threshold");
    }
    
    sqlite3_finalize(statement);
}

-(int)getActiveShakeUiThreshold {
    
    return [self getShakeUiThreshold:currentUserID];
    
}

-(int)getShakeUiThreshold: (int)userID  {
    
    int deviceID=[self getDeviceID:userID];
    
    int threshold=-1;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            binsData.device->shakeUiThreshold,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            threshold=sqlite3_column_int(statement, 0);
            
        }
    }
    sqlite3_finalize(statement);
    
    return threshold;
    
}

-(void)releaseUserDevice: (int)userID {
    
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                      \
            ; ",
            
            binsData.user->tableName,
            binsData.user->userDevice, 0,
            binsData.user->recordID, userID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to release user device");
    }
    
    sqlite3_finalize(statement);
    
}

-(void)setActiveShakeUiThreshold: (int)threshold{
    
    [self setShakeUiThreshold: currentUserID withThreshold: threshold];
    
}
-(void)setShakeUiThreshold: (int)userID withThreshold: (int)threshold{
    
   int deviceID=[self getDeviceID:userID];
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                      \
            ; ",
            
            binsData.device->tableName,
            binsData.device->shakeUiThreshold, threshold,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's shake UI threshold");
    }
    
    sqlite3_finalize(statement);
    
}

-(void)setShakUiThreshold: (int)threshold {
    [self setShakeUiThreshold: currentUserID withThreshold: threshold];
}

-(int)getShakeUiThreshold {
    return [self getShakeUiThreshold: currentUserID];
}

-(void)setActiveDeviceBatteryLevel: (int)batteryLevel {
    
    int previousLevel=[self getActiveDeviceBatteryLevel];
    
    int deviceID=[self getDeviceID:currentUserID];
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.device->tableName,
            binsData.device->deviceBatteryLevel, batteryLevel,
            binsData.user->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's battery level");
    }
    
    sqlite3_finalize(statement);
    
    
    if(batteryLevel<BINS_BATTERY_LOW_LEVEL && previousLevel>batteryLevel){
        
        [notificationController addNotification:
         [NSString localizedStringWithFormat:NSLocalizedString(@"battery_low",nil), batteryLevel ] withAction: [NSString localizedStringWithFormat:NSLocalizedString(@"charge_device",nil) ]];
        
    }
    
}


-(int)getActiveDeviceBatteryLevel {

    int deviceID=[self getDeviceID:currentUserID];
    
    int batteryLevel=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.device->deviceBatteryLevel,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil)
                batteryLevel=sqlite3_column_int(statement, 0);
            
        }
    }
    sqlite3_finalize(statement);
    
    return batteryLevel;

}


-(BOOL)hasSetupAlready {
    if([self getActiveUserID]!=INVALID_USER_ID)
    {
        return YES;
    }
    
    return NO;
}


-(int)getActiveUserID {
    
    sprintf(strSqlStatement,
                            "SELECT %s FROM %s",
                            binsData.activeUser->userID,
                            binsData.activeUser->tableName );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            currentUserID=sqlite3_column_int(statement, 0);
        }
        else{
            currentUserID=INVALID_USER_ID;
        }
        
    }
    sqlite3_finalize(statement);
    
    return currentUserID;
    
}


-(int)getUserHeight: (int) userId{
    
    int userHeight=0;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d",
            binsData.user->userHeight,
            binsData.user->tableName,
            binsData.user->recordID, userId
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            userHeight=sqlite3_column_int(statement, 0);
        }
    }

    sqlite3_finalize(statement);
    
    return userHeight;
    
}


-(void)setActiveDeviceVersion: (int)major withMinor: (int)minor {
    
    int deviceID=[self getDeviceID:currentUserID];
    
    int revision=((major & 0x00FF)<<8) + (minor & 0x00FF);
    bool isSuccess=NO;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d                          \
            WHERE                           \
            %s=%d                       \
            ; ",
            
            binsData.device->tableName,
            binsData.device->deviceVersion, revision,
            binsData.user->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog( @"Failed to update device's version");
    }
    
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
            if(sqlite3_column_text(statement, 0)!=nil){
                revision=sqlite3_column_int(statement, 0);
                isSuccess=NO;
            }
            
        }
    }
    sqlite3_finalize(statement);
    
}

-(void)getActiveDeviceVersion: (int*)major withMinor: (int*)minor {

    int deviceID=[self getDeviceID:currentUserID];
    
    int revision=0;
    bool isSuccess=NO;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d ",
            binsData.device->deviceVersion,
            binsData.device->tableName,
            binsData.device->recordID, deviceID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        if (sqlite3_step(statement) == SQLITE_ROW) {
                revision=sqlite3_column_int(statement, 0);
                isSuccess=NO;
        }
    }
    sqlite3_finalize(statement);
    
    *major=(revision & 0xFF00) >> 8;
    *minor=(revision & 0x00FF);
    
}

-(bool)isDeviceFirstTimeSync {
    
    NSInteger time=[self getActiveDeviceLastSyncTime];

    if(time==0){
        return YES;
    }
    
    return NO;
}

-(int)getBackgroundSync {
    BOOL isExisting=NO;
    int currentStatus=0;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s",
            binsData.activeUser->backgroundSync,
            binsData.activeUser->tableName );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            currentStatus=sqlite3_column_int(statement, 0);
            isExisting=YES;
        }
    }
    
    return currentStatus;
    
}

-(void)toggleFilterUnused {
    int status=[self getFilterUnused ];
    if(status==1) status=0;
    else status=1;
    
    [self setFilterUnused: status];
}

-(int)getFilterUnused {
    BOOL isExisting=NO;
    int currentStatus=0;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s",
            binsData.activeUser->filterDummy,
            binsData.activeUser->tableName );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            currentStatus=sqlite3_column_int(statement, 0);
            isExisting=YES;
        }
    }
    
    return currentStatus;
    
}
-(void)setFilterUnused: (int)enable {
    
    BOOL isExisting=NO;
    int currentStatus=0;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s",
            binsData.activeUser->filterDummy,
            binsData.activeUser->tableName );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            currentStatus=sqlite3_column_int(statement, 0);
            isExisting=YES;
        }
    } else {
        NSLog(@"SQLITE: filter dummy '%s'", sqlite3_errmsg(binsDB));
    }
    
    if(isExisting==YES)
    {
        sprintf(strSqlStatement,
                "UPDATE %s  SET                \
                %s=%d                       \
                ; ",
                binsData.activeUser->tableName,
                binsData.activeUser->filterDummy, enable
                );
        
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"Failed to update background sync");
        }
        sqlite3_finalize(statement);
    }
    
}


-(void)toggleBackgroundSync {
    int status=[self getBackgroundSync ];
    if(status==1) status=0;
    else status=1;
    
    [self setBackgroundSync: status];
}
-(void)setBackgroundSync: (int)enable {
    
    BOOL isExisting=NO;
    int currentStatus=0;
    sprintf(strSqlStatement,
            "SELECT %s FROM %s",
            binsData.activeUser->backgroundSync,
            binsData.activeUser->tableName );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            currentStatus=sqlite3_column_int(statement, 0);
            isExisting=YES;
        }
    } else {
        NSLog(@"SQLITE: background sync Error '%s'", sqlite3_errmsg(binsDB));
    }
    
    if(isExisting==YES)
    {
        sprintf(strSqlStatement,
                "UPDATE %s  SET                \
                %s=%d                       \
                ; ",
                binsData.activeUser->tableName,
                binsData.activeUser->backgroundSync, enable
                );
        
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"Failed to update background sync");
        }
        sqlite3_finalize(statement);
    }
    
}

-(void)clearSleepTimeEffectTable {
    sprintf(strSqlStatement,
            "DELETE FROM %s\
            ",
            binsData.sleepTimeEffect->tableName
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)==SQLITE_OK){
        if (sqlite3_step(statement) != SQLITE_DONE) {
            NSLog(@"Failed to clear sleep time effect table: %s", sqlite3_errmsg(binsDB));
        }
    }
    else{
        NSLog(@"Failed to clear sleep time effect table too %s", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}


-(void)resetActiveUserID {
    sprintf(strSqlStatement,
            "DELETE FROM %s\
            ",
            binsData.activeUser->tableName
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)==SQLITE_OK){
        if (sqlite3_step(statement) != SQLITE_DONE) {
            NSLog(@"Failed to clear active user table %s", sqlite3_errmsg(binsDB));
        }
    }
    else{
        NSLog(@"Failed to clear active user table too %s", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}

-(void)setActiveUserID: (int)userID {
 
    BOOL isExisting=NO;
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s",
            binsData.activeUser->userID,
            binsData.activeUser->tableName );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)== SQLITE_OK) {
        
        if (sqlite3_step(statement) == SQLITE_ROW) {
            currentUserID=sqlite3_column_int(statement, 0);
            isExisting=YES;
        }
    }
    
    if(isExisting==YES)
    {
        sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d,                       \
            %s=%d                       \
            ; ",
            
            binsData.activeUser->tableName,
            binsData.activeUser->userID, userID,
            binsData.activeUser->filterDummy, 1
            );
    
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"Failed to update active user");
        }
        sqlite3_finalize(statement);
    }
    else{
        sprintf(strSqlStatement,
                        "INSERT INTO %s (            \
                        %s, %s                           \
                        ) VALUES (                      \
                        %d, %d                         \
                        ); ",
                        
                binsData.activeUser->tableName,
                binsData.activeUser->userID,
                binsData.activeUser->filterDummy,
                userID, 1
                );
        
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"Failed to insert active user");
        }
        sqlite3_finalize(statement);
        
        [self setBackgroundSync:1]; // 12.07 enable background sync
    }
   
    currentUserID=userID;
    
    
    
}

static int MAX_STEP_SAMPLE_WINDOW_MINUTES=5;


-(void)getActivityPeriod: (int)user withType: (int)type onTime: (NSInteger)time forPeriod:(NSInteger)period hasSteps: (int*)steps hasActiveMinutes: (int*)minutes andDistance: (int*)distance usingDistanceChecker:(CheckDistanceCallBack)distanceChecker {
    
    int userHeight=[self getUserHeight: user];
    
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s WHERE       \
            %s=%d       AND     \
            %s=%d       AND     \
            %s>=%ld     AND     \
            %s<=%ld             \
            ; ",
            binsData.activity->value,
            binsData.activity->time,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, type,
            binsData.activity->time, (long)time,
            binsData.activity->time, (long)(time+period)
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return;
    }
    
    *steps =0;
    *minutes=0;
    *distance=0;
    
    int stepMinute;
    long nowActivityTime=0;
    long lastActivityTime=0;
    long samplePeriod=0;
    
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        stepMinute=sqlite3_column_int(statement, 0);
        nowActivityTime=sqlite3_column_int(statement, 1);
        
        //-- NSLog(@"minute-step=%ld:%d",nowActivityTime,stepMinute);    //-- 20151102
        
        if((nowActivityTime-lastActivityTime)>MAX_STEP_SAMPLE_WINDOW_MINUTES){
            samplePeriod=MAX_STEP_SAMPLE_WINDOW_MINUTES;
        }else{
            samplePeriod=(nowActivityTime-lastActivityTime);
        }
        if(samplePeriod<=0) samplePeriod=1;
        lastActivityTime=nowActivityTime;
        
        
        float avgStepsPerMinute=((float)stepMinute/samplePeriod);
        
        //-- NSLog(@"step minute=%d,%ld",stepMinute, samplePeriod);

        if(avgStepsPerMinute > 10 ) (*minutes)+= samplePeriod;	//-- 151002 20 to 10, 150908
        
        (*steps)+=stepMinute;
        float distanceThisTime=distanceChecker(userHeight, (float)stepMinute/samplePeriod)*samplePeriod;
        
        //-- NSLog(@"distanceThisTime steps/distance= %f, %f", (float)(stepMinute), distanceThisTime);
        (*distance)+=distanceThisTime;
        
    }
    sqlite3_finalize(statement);

}

-(void)getActivityPeriod: (int)user withType: (int)type onTime: (NSInteger)time forPeriod:(NSInteger)period hasSteps: (int*)steps hasActiveMinutes: (int*)minutes {
    
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE       \
            %s=%d       AND     \
            %s=%d       AND     \
            %s>=%ld     AND     \
            %s<=%ld             \
            ; ",
            binsData.activity->value,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, type,
            binsData.activity->time, (long)time,
            binsData.activity->time, (long)(time+period)
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return;
    }
    
    *steps =0;
    *minutes=0;
    
    int stepMinute;
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        stepMinute=sqlite3_column_int(statement, 0);
        if(stepMinute>20) (*minutes)++;
        (*steps)+=stepMinute;
    }
    sqlite3_finalize(statement);
    
}

-(NSInteger)timeIntervalInMinute: (NSDate*)date withUnitMinute: (BOOL)isMinuteBase
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate* newDate=nil;
    
    if(isMinuteBase==YES){
        NSDateComponents *dayComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:date];
        dayComponents.second=0;
        
        newDate=[calendar dateFromComponents:dayComponents];
    }
    else{
        newDate=date;
    }
    
    NSInteger interval = [newDate timeIntervalSince1970];
    
    return (interval / 60);
}


-(long)nextAvailableSleepTime: (long*)pSleepRecordTime {
    
    if((*pSleepRecordTime)==0) return 0;  // special case to just return 0
    
    long sleepRecordThisTime=(*pSleepRecordTime);
    
    int user=[self getActiveUserID];
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE        \
            %s=%d       AND                 \
            %s=%d       AND                 \
            %s>%ld                          \
            ORDER BY %s ASC                 \
            ; ",
            binsData.activity->time,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, BINS_ACTIVITY_TYPE_SLEEP,
            binsData.activity->time, *pSleepRecordTime,
            binsData.activity->time
            );
    
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                      -1, &statement, NULL)!= SQLITE_OK) {
        
        NSLog(@"Failed to query from next available sleep time");
        return 0;
    }

    long nextOccupiedTime=0;
    
    (*pSleepRecordTime)++;  // unit of minute
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        nextOccupiedTime=sqlite3_column_double(statement, 0);
        
        if((*pSleepRecordTime)<nextOccupiedTime){
            break;
        }else if((*pSleepRecordTime)==nextOccupiedTime){
            *pSleepRecordTime=(nextOccupiedTime+1);
        }
    }
    sqlite3_finalize(statement);
    return sleepRecordThisTime;
}

-(long)previousAvailableSleepTime: (long*)pSleepRecordTime {
    int user=[self getActiveUserID];

    if((*pSleepRecordTime)==0) return 0;  // special case to just return 0

    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE        \
            %s=%d       AND                 \
            %s=%d       AND                 \
            %s<%ld                          \
            ORDER BY %s DESC                \
            ; ",
            binsData.activity->time,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, BINS_ACTIVITY_TYPE_SLEEP,
            binsData.activity->time, *pSleepRecordTime,
            binsData.activity->time
            );
    
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        
        NSLog(@"Failed to query from previous available sleep time");
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        return 0;
    }
    
    long nextOccupiedTime=0;
    
    (*pSleepRecordTime)--;  // unit of minute
    
    while(sqlite3_step(statement) == SQLITE_ROW) {
        nextOccupiedTime=sqlite3_column_double(statement, 0);
        
        if((*pSleepRecordTime)>nextOccupiedTime){
            break;
        }else if((*pSleepRecordTime)==nextOccupiedTime){
            *pSleepRecordTime=(nextOccupiedTime-1);
        }
    }
    sqlite3_finalize(statement);
    return (*pSleepRecordTime);
    
}


-(BOOL)queryActivitiesForUpload: (int)user typeOf: (int)type ofLimit: (int)limit {
    
    if(type>=0){
        
        sprintf(strSqlStatement,
            "SELECT %s, %s, %s, %s FROM %s WHERE       \
            %s=%d       AND     \
            %s=%d       AND     \
            %s=%d                \
            ORDER BY %s ASC  \
            LIMIT %d            \
            ; ",
            binsData.activity->time,
            binsData.activity->type,
            binsData.activity->value,
            binsData.activity->recordID,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, type,
            binsData.activity->uploaded, 0,
            binsData.activity->recordID,
            limit
            );
    }else{
        sprintf(strSqlStatement,
                "SELECT %s, %s, %s, %s FROM %s WHERE       \
                %s=%d       AND     \
                %s=%d                \
                ORDER BY %s ASC     \
                LIMIT %d            \
                ; ",
                binsData.activity->time,
                binsData.activity->type,
                binsData.activity->value,
                binsData.activity->recordID,
                binsData.activity->tableName,
                binsData.activity->user, user,
                binsData.activity->uploaded, 0,
                binsData.activity->recordID,
                limit
                );
    }
    
    //-- TODO
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    activeUploadStatement=statement;
    
    return YES;
}

-(BOOL)getNextActivityForUploadFromQuery: (int*)value ofType: (int*)type atTime: (NSInteger*)time ofRecordNo: (int*)recordID {
    
    if(sqlite3_step(activeUploadStatement) == SQLITE_ROW) {
        (*time)=sqlite3_column_double(activeUploadStatement, 0);
        (*type)=sqlite3_column_int(activeUploadStatement, 1);
        (*value)=sqlite3_column_int(activeUploadStatement, 2);
        (*recordID)=sqlite3_column_int(activeUploadStatement, 3);
        
    }else{
        sqlite3_finalize(activeUploadStatement);
        return NO;
    }
    
    return YES;
}

-(BOOL)setActivitiesUploaded: (int)user ofType: (int)type until: (int)recordSerialNo {
    
    BOOL isSuccess;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET             \
            %s=%d                       \
            WHERE                       \
            %s=%d   AND                 \
            %s=%d   AND                 \
            %s=%d   AND                 \
            %s<=%d                      \
            ; ",
            binsData.activity->tableName,
            binsData.activity->uploaded, 1,
            binsData.activity->user, user,
            binsData.activity->type, type,
            binsData.activity->uploaded, 0,
            binsData.activity->recordID, recordSerialNo
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        NSLog( @"Failed to update uploaded");
    }
    else{
        int changeNum;
        if((changeNum=sqlite3_changes(binsDB))>0){
            NSLog(@"set upload changed number=%d",changeNum);
            isSuccess=YES;
        }
    }
    
    sqlite3_finalize(statement);
    
    return isSuccess;
}
-(int)queryActivityRecords: (int)user typeOf: (int)type timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
    
        sprintf(strSqlStatement,
                "SELECT %s, %s, %s FROM %s WHERE       \
                %s=%d       AND     \
                %s=%d       AND     \
                %s>=%ld     AND     \
                %s<=%ld             \
                ORDER BY %s ASC     \
                ; ",
                binsData.activity->time,
                binsData.activity->value,
                binsData.activity->type,
                binsData.activity->tableName,
                binsData.activity->user, user,
                binsData.activity->type, type,
                binsData.activity->time, (long)fromTime,
                binsData.activity->time, (long)toTime,
                binsData.activity->time
                );
        //-- 20151129 must follow the ORDER sequence to check sample period
        sqlite3_stmt     *statement;
        if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                              -1, &statement, NULL)!= SQLITE_OK) {
            
            NSLog(@"queryActivityRecords failed, something wrong");
            return QUERY_STOPPED;
        }else{
            activeStatement=statement;
            
            return QUERY_COMPLETED;
        }
    
    return QUERY_STOPPED; //-- won't reach here
}


-(int)queryActivityRecordsWithCloudLoad: (int)user typeOf: (int)type timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
    int totalQueriedNumber=0;
    
    sprintf(strSqlStatement,
            "SELECT COUNT(*) FROM %s WHERE       \
            %s=%d       AND     \
            ( %s=%d OR %s=%d )  AND     \
            %s>=%ld     AND     \
            %s<=%ld             \
            ; ",
            binsData.activity->tableName,
            binsData.activity->user, user,
            //-- 20151125 don't care about type
            //-- 20151129 should care about type,
            // step and sleep query period have half day difference
            binsData.activity->type, BINS_ACTIVITY_TYPE_STEPS,
            binsData.activity->type, BINS_ACTIVITY_TYPE_PULSE,
            binsData.activity->time, (long)fromTime,
            binsData.activity->time, (long)toTime
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        
        NSLog(@"queryActivityRecordsWithCloudLoad failed, something wrong");
        return QUERY_STOPPED;
        
    }else{
        if(sqlite3_step(statement) == SQLITE_ROW) {
            totalQueriedNumber = sqlite3_column_int(statement, 0);
        }
    }

#ifndef CONFIG_BINS_HBP
    NSInteger currentTime=[self timeIntervalInMinute:[NSDate date] withUnitMinute:YES];
#endif
    
    BOOL needLoadFromCloud=NO;
    NSInteger userCreateTime=[self getActiveUserCreateTime];
    if(userCreateTime>0 && toTime<userCreateTime){
        needLoadFromCloud=YES;
    }
    
    //-- following is no need to load from cloud
    if(needLoadFromCloud==NO ||
       (
       totalQueriedNumber>0
#ifndef CONFIG_BINS_HBP
       //-- if for today, always reload
       //-- this can be optimized by checking lastSyncTime
       && (currentTime>=fromTime && currentTime<=toTime)==NO
#endif
       )
       ){

        sprintf(strSqlStatement,
            "SELECT %s, %s, %s FROM %s WHERE       \
            %s=%d       AND     \
            %s=%d       AND     \
            %s>=%ld     AND     \
            %s<=%ld             \
            ; ",
            binsData.activity->time,
            binsData.activity->value,
            binsData.activity->type,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, type,
            binsData.activity->time, (long)fromTime,
            binsData.activity->time, (long)toTime
            );
    
        if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
            
            NSLog(@"queryActivityRecordsWithCloudLoad failed, something wrong");
            return QUERY_STOPPED;
        }else{
            activeStatement=statement;
            
            return QUERY_COMPLETED;
        }
    }else{
            
#ifndef CONFIG_BINS_HBP //-- 20151119
        
        //-- if([self userIsRemoteUser: user]==YES)
        {

            binsCloudServer.delegate=self;
            //-- 20151129 separate download page by types per page view
            [binsCloudServer loadActivityRecordsStepPage:[self getUserUID:user] timeFrom:fromTime timeTo:toTime ];
            
            //-- after finish load from network, it will return by delegate to inform us
            
        }
        return QUERY_WORKING;
#else
        return QUERY_STOPPED;
#endif

    }
    
    
    return QUERY_STOPPED; //-- won't reach here
}

-(int)querySleepRecords: (int)user timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
        sprintf(strSqlStatement,
                "SELECT %s, %s, %s FROM %s WHERE       \
                %s=%d               AND     \
                ( %s=%d OR %s=%d )  AND     \
                %s>=%ld             AND     \
                %s<=%ld                     \
                ; ",
                binsData.activity->time,
                binsData.activity->value,
                binsData.activity->type,
                binsData.activity->tableName,
                binsData.activity->user, user,
                binsData.activity->type, BINS_ACTIVITY_TYPE_SLEEP,
                binsData.activity->type, BINS_ACTIVITY_TYPE_LIGHT,
                binsData.activity->time, (long)fromTime,
                binsData.activity->time, (long)toTime
                );
        sqlite3_stmt     *statement;
        if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                              -1, &statement, NULL)!= SQLITE_OK) {
            
            NSLog(@"querySleepRecords failed, something wrong");
            return QUERY_STOPPED;
        }else{
            activeStatement=statement;
            
            return QUERY_COMPLETED;
        }
    
    return QUERY_STOPPED; //-- won't reach here
    
}

-(int)querySleepRecordsWithCloudLoad: (int)user timeFrom: (NSInteger)fromTime timeTo: (NSInteger)toTime {
    
    int totalQueriedNumber=0;
    
    sprintf(strSqlStatement,
            "SELECT COUNT(*) FROM %s WHERE       \
            %s=%d               AND     \
            ( %s=%d OR %s=%d )  AND     \
            %s>=%ld             AND     \
            %s<=%ld                     \
            ; ",
            binsData.activity->tableName,
            binsData.activity->user, user,
            //-- 20151125 don't care about type
            //-- 20151129 should care about type,
            // step and sleep query period have half day difference
            binsData.activity->type, BINS_ACTIVITY_TYPE_SLEEP,
            binsData.activity->type, BINS_ACTIVITY_TYPE_LIGHT,
            binsData.activity->time, (long)fromTime,
            binsData.activity->time, (long)toTime
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        
        NSLog(@"querySleepRecordsWithCloudLoad failed, something wrong");
        return QUERY_STOPPED;
        
    }else{
        if(sqlite3_step(statement) == SQLITE_ROW) {
            totalQueriedNumber = sqlite3_column_int(statement, 0);
        }
    }

#ifndef CONFIG_BINS_HBP
    NSInteger currentTime=[self timeIntervalInMinute:[NSDate date] withUnitMinute:YES];
#endif
    
    BOOL needLoadFromCloud=NO;
    NSInteger userCreateTime=[self getActiveUserCreateTime];
    if(userCreateTime>0 && toTime<userCreateTime){
        needLoadFromCloud=YES;
    }
    
    //-- following is no need to load from cloud
    if(needLoadFromCloud==NO ||
       (
       totalQueriedNumber>0
#ifndef CONFIG_BINS_HBP
       //-- if for today, always reload
       //-- this can be optimized by checking lastSyncTime
       && (currentTime>=fromTime && currentTime<=toTime)==NO
#endif
        )
       ){
    
        sprintf(strSqlStatement,
            "SELECT %s, %s, %s FROM %s WHERE       \
            %s=%d               AND     \
            ( %s=%d OR %s=%d )  AND     \
            %s>=%ld             AND     \
            %s<=%ld                     \
            ; ",
            binsData.activity->time,
            binsData.activity->value,
            binsData.activity->type,
            binsData.activity->tableName,
            binsData.activity->user, user,
            binsData.activity->type, BINS_ACTIVITY_TYPE_SLEEP,
            binsData.activity->type, BINS_ACTIVITY_TYPE_LIGHT,
            binsData.activity->time, (long)fromTime,
            binsData.activity->time, (long)toTime
            );
    
        if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                              -1, &statement, NULL)!= SQLITE_OK) {
            
            NSLog(@"querySleepRecordsWithCloudLoad failed, something wrong");
            return QUERY_STOPPED;
        }else{
            activeStatement=statement;
            
            return QUERY_COMPLETED;
        }
    }else{
        
#ifndef CONFIG_BINS_HBP //-- 20151119
        
        //-- if([self userIsRemoteUser: user]==YES)
        {
            
            binsCloudServer.delegate=self;
            //-- 20151129 separate download page by types per page view
            [binsCloudServer loadActivityRecordsSleepPage:[self getUserUID:user] timeFrom:fromTime timeTo:toTime ];
            
            //-- after finish load from network, it will return by delegate to inform us
            return QUERY_WORKING;
        }
        
#else
        return QUERY_STOPPED;
#endif
        
    }    
    
    return QUERY_STOPPED; //-- won't reach here

}

-(BOOL)getNextActivityFromQuery: (int*)value atTime: (NSInteger*)time {
    
    if(sqlite3_step(activeStatement) == SQLITE_ROW) {
        (*time)=sqlite3_column_double(activeStatement, 0);
        (*value)=sqlite3_column_int(activeStatement, 1);
        
    }else{
        sqlite3_finalize(activeStatement);
        return NO;
    }

    return YES;
}

-(BOOL)getNextActivityFromQuery: (int*)value ofType: (int*)type atTime: (NSInteger*)time {
    if(sqlite3_step(activeStatement) == SQLITE_ROW) {
        (*time)=sqlite3_column_double(activeStatement, 0);
        (*value)=sqlite3_column_int(activeStatement, 1);
        (*type)=sqlite3_column_int(activeStatement, 2);
        
    }else{
        sqlite3_finalize(activeStatement);
        return NO;
    }
    
    return YES;
}

-(int)numberOfUsers {
    int count=0;
    if([self queryUsers]){
        count=[self getNumberOfUsersFromQuery];
    }
    return count;
}

-(BOOL)queryUsers {
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s \
            ; ",
            binsData.user->recordID,
            binsData.user->userName,
            binsData.user->tableName
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    activeStatement=statement;
    
    return YES;
}

-(int)getNumberOfUsersFromQuery {
    
    int count=0;
    int user;
    NSString *name;
    
    while([self getNextUserFromQuery: &user withName: &name]){
        count++;
    }
    
    return count;
}

-(BOOL)getNextUserFromQuery: (int*)user withName: (NSString**)name {
    
    if(sqlite3_step(activeStatement) == SQLITE_ROW) {
        (*user)=sqlite3_column_int(activeStatement, 0);
        (*name)=[[NSString alloc] initWithUTF8String:(const char*)sqlite3_column_text(activeStatement, 1)];
    }else{
        sqlite3_finalize(activeStatement);
        return NO;
    }
    
    return YES;
}


-(void) clearCubeLevelPeriods {
    sprintf(strSqlStatement,
            "DELETE FROM %s  WHERE  \
                %s = %d \
            ",
            binsData.cubeLevelPeriods->tableName,
            binsData.cubeLevelPeriods->userId,
            currentUserID
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)==SQLITE_OK){
        if (sqlite3_step(statement) != SQLITE_DONE) {
            NSLog(@"Failed to delete cube level periods %s", sqlite3_errmsg(binsDB));
        }
    }
    else{
        NSLog(@"Failed to delete cube level periods too %s", sqlite3_errmsg(binsDB));
    }
    
    sqlite3_finalize(statement);

}


-(void) insertSleepTimeEffectItem:(int)threshId withHour: (int)hour withRecovery: (float)recoveryFactor {

    //-- NSLog(@"insertSleepTimeEffectItem: %d[%d]=%f", threshId, hour, recoveryFactor);

    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %d,                         \
            %d,                         \
            %f,                         \
            %f                         \
            ); ",
            binsData.sleepTimeEffect->tableName,
            binsData.sleepTimeEffect->sleepTimeEffectThreshId,
            binsData.sleepTimeEffect->sleepTimeEffectHour,
            binsData.sleepTimeEffect->sleepTimeEffectPercSleepFactor,
            binsData.sleepTimeEffect->sleepTimeEffectPercWakeFactor,
            threshId,
            hour,
            recoveryFactor,
            0.0
            );
    
    
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add sleep time effect");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    
    sqlite3_finalize(statement);

}


-(BOOL)updateSleepTimeEffectItem: (int)threshId withHour: (int)hour withRecovery: (float)recoveryFactor {
    
    BOOL isSuccess=NO;
    
    //-- NSLog(@"updateSleepTimeEffectItem: %d[%d]=%f", threshId, hour, recoveryFactor);

    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%f,                      \
            %s=%f                   \
            WHERE                           \
            %s=%d AND                       \
            %s=%d                       \
            ; ",
            binsData.sleepTimeEffect->tableName,
            binsData.sleepTimeEffect->sleepTimeEffectPercSleepFactor, recoveryFactor,
            binsData.sleepTimeEffect->sleepTimeEffectPercWakeFactor, 0.0,
            binsData.sleepTimeEffect->sleepTimeEffectThreshId, threshId,
            binsData.sleepTimeEffect->sleepTimeEffectHour, hour
            );
    
    
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
            NSLog( @"Failed to update sleep time effect table");
        }
        else{
            if(sqlite3_changes(binsDB)>0){
                isSuccess=YES;
            }
        }
    }
    sqlite3_finalize(statement);
    
    
    if(isSuccess==NO){
        [self insertSleepTimeEffectItem:threshId withHour:hour withRecovery: (float)recoveryFactor];
    }
    
    return isSuccess;
    
}

-(void) insertSleepTimeEffect:(const char*)threshId withHour: (const char*)hour withSleepFactor: (const char*)sleepFactor withWakeFactor: (const char*)wakeFactor {


    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %s,                         \
            %s,                         \
            %s,                         \
            %s                         \
            ); ",
            binsData.sleepTimeEffect->tableName,
            binsData.sleepTimeEffect->sleepTimeEffectThreshId,
            binsData.sleepTimeEffect->sleepTimeEffectHour,
            binsData.sleepTimeEffect->sleepTimeEffectPercSleepFactor,
            binsData.sleepTimeEffect->sleepTimeEffectPercWakeFactor,
            threshId,
            hour,
            sleepFactor,
            wakeFactor
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add sleep time effect");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}


-(BOOL)updateSleepTimeEffect: (const char*)threshId withHour: (const char*)hour withSleepFactor: (const char*)sleepFactor withWakeFactor: (const char*)wakeFactor {
    
    BOOL isSuccess=NO;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=\"%s\",                      \
            %s=\"%s\"                      \
            WHERE                           \
            %s=\"%s\" AND                       \
            %s=\"%s\"                       \
            ; ",
            binsData.sleepTimeEffect->tableName,
            binsData.sleepTimeEffect->sleepTimeEffectPercSleepFactor, sleepFactor,
            binsData.sleepTimeEffect->sleepTimeEffectPercWakeFactor, wakeFactor,
            binsData.sleepTimeEffect->sleepTimeEffectThreshId, threshId,
            binsData.sleepTimeEffect->sleepTimeEffectHour, hour
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        NSLog( @"Failed to update sleep time effect table");
    }
    else{
        if(sqlite3_changes(binsDB)>0){
            isSuccess=YES;
        }
    }
    
    sqlite3_finalize(statement);
    
    return isSuccess;
    
}

-(void) updateSleepTimeEffectTable: (NSArray*) recordList
{
    //-- the first row is for names
    //-- the last row is out of boundary, don't know why
    int i;
    for(i=0; i<recordList.count; i++){
        
        NSDictionary *record=[recordList objectAtIndex:i];
        
        NSEnumerator *enumRec=[[record allValues] objectEnumerator];
        NSString *tokenHour=[enumRec nextObject];
        NSString *tokenPercWake=[enumRec nextObject];;
        NSString *tokenThreshId=[enumRec nextObject];;
        NSString *tokenPercSleep=[enumRec nextObject];;

        const char *charThreshId=[tokenThreshId UTF8String];
        const char *charHour=[tokenHour UTF8String];
        const char *charPercSleep=[tokenPercSleep UTF8String];
        const char *charPercWake=[tokenPercWake UTF8String];
        
        if([self updateSleepTimeEffect:charThreshId withHour:charHour withSleepFactor:charPercSleep withWakeFactor:charPercWake]==NO){

            [self insertSleepTimeEffect: charThreshId withHour:charHour withSleepFactor:charPercSleep withWakeFactor:charPercWake];
            
        }
    }
    
}

-(void) insertCircadianItem: (int)circadianId withHour: (int)hour withRecoveryFactor: (float)sleepFactor withWakeFactor: (float)wakeFactor {
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %d,                         \
            %d,                         \
            %f,                         \
            %f                         \
            ); ",
            binsData.circadian->tableName,
            binsData.circadian->circId,
            binsData.circadian->cirHour,
            binsData.circadian->circPercSleepFactor,
            binsData.circadian->circPercWakeFactor,
            circadianId,
            hour,
            sleepFactor,
            wakeFactor
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add sleep time effect");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}

-(BOOL)updateCircadianItem: (int)circadianId withHour: (int)hour withSleepFactor: (float)sleepFactor withWakeFactor: (float)wakeFactor {
    
    BOOL isSuccess=NO;
    
    //-- NSLog(@"updateCircadianItem: %d[%d]=%f", circadianId, hour, sleepFactor);
    

    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%f,                      \
            %s=%f,                      \
            %s=%d                       \
            WHERE                           \
            %s=%d; ",
            binsData.circadian->tableName,
            binsData.circadian->circPercSleepFactor, sleepFactor,
            binsData.circadian->circPercWakeFactor, wakeFactor,
            binsData.circadian->cirHour, hour,
            binsData.circadian->circId, circadianId
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
            
            NSLog( @"Failed to update circadian table");
        }
        else{
            if(sqlite3_changes(binsDB)>0){
                isSuccess=YES;
            }
        }
    
        sqlite3_finalize(statement);
    }else{
        NSLog(@"updateCircadianItem sqlite3_prepare_v2 error- %s", sqlite3_errmsg(binsDB));
    }
    
    
    if(isSuccess==NO){
        [self insertCircadianItem: circadianId withHour: hour withRecoveryFactor:sleepFactor withWakeFactor:wakeFactor];
    }
    
    return isSuccess;
    
}


-(void) insertCircadian:(const char*)hour withSleepFactor: (const char*)sleepFactor withWakeFactor: (const char*)wakeFactor {
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %s,                         \
            %s,                         \
            %s                         \
            ); ",
            binsData.circadian->tableName,
            binsData.circadian->cirHour,
            binsData.circadian->circPercSleepFactor,
            binsData.circadian->circPercWakeFactor,
            hour,
            sleepFactor,
            wakeFactor
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add sleep time effect");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}

-(BOOL)updateCircadian: (const char*)hour withSleepFactor: (const char*)sleepFactor withWakeFactor: (const char*)wakeFactor {
        
    BOOL isSuccess=NO;
    
    sprintf(strSqlStatement,
                "UPDATE %s  SET                \
                %s=%s,                      \
                %s=%s                      \
                WHERE                           \
                %s=%s                       \
                ; ",
                binsData.circadian->tableName,
                binsData.circadian->circPercSleepFactor, sleepFactor,
                binsData.circadian->circPercWakeFactor, wakeFactor,
                binsData.circadian->cirHour, hour
                );
        sqlite3_stmt     *statement;
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
            
            NSLog( @"Failed to update circadian table");
        }
        else{
            if(sqlite3_changes(binsDB)>0){
                isSuccess=YES;
            }
        }
        
        sqlite3_finalize(statement);
        
        return isSuccess;
        
}

-(void) updateCircadianTable: (NSArray*) recordList
{
    //-- the first row is for names
    //-- the last row is out of boundary, don't know why
    int i;
    for(i=0; i<recordList.count; i++){
        
        NSDictionary *record=[recordList objectAtIndex:i];
        
        // NSString *tokenHour=[record objectForKey:@""];
        // NSString *tokenPercSleep=[record objectForKey:@""];
        // NSString *tokenPercWake=[record objectForKey:@""];

        NSEnumerator *enumRec=[[record allValues] objectEnumerator];
        NSString *tokenHour=[enumRec nextObject];
        NSString *tokenPercWake=[enumRec nextObject];
        NSString *tokenPercSleep=[enumRec nextObject];
        

        
        const char *charHour=[tokenHour UTF8String];
        const char *charPercSleep=[tokenPercSleep UTF8String];
        const char *charPercWake=[tokenPercWake UTF8String];
        
        if([self updateCircadian: charHour withSleepFactor:charPercSleep withWakeFactor:charPercWake]==NO){
            
            [self insertCircadian: charHour withSleepFactor:charPercSleep withWakeFactor:charPercWake];
            
        }
        
        
    }
}

-(BOOL)updateThresholdItem: (int)threshId withName: (const char*)name withMovements: (int)movements withSleepState: (int) sleepState {

    /*
    sprintf(strSqlStatement,
                "DELETE FROM %s\
                ",
                binsData.sleepThreshold->tableName
                );
        sqlite3_stmt     *statement1;
        if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                              -1, &statement1, NULL)==SQLITE_OK){
            if (sqlite3_step(statement1) != SQLITE_DONE) {
                NSLog(@"Failed to clear sleep threshold table %s", sqlite3_errmsg(binsDB));
            }
        }
        else{
            NSLog(@"Failed to clear sleep threshold table too %s", sqlite3_errmsg(binsDB));
        }
    sqlite3_finalize(statement1);
    */
    
    BOOL isSuccess=NO;
    
    //-- NSLog(@"updateThresholdItem: %d=%d,%d", threshId, movements, sleepState);
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%d,                      \
            %s=%d,                      \
            %s=\"%s\"                     \
            WHERE                           \
            %s=%d                       \
            ; ",
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshValue, movements,
            binsData.sleepThreshold->sleepThreshState, sleepState,
            binsData.sleepThreshold->sleepThreshName,  name,
            binsData.sleepThreshold->sleepThreshId, threshId
            );
    
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
            NSLog( @"Failed to update threshold table");
        }
        else{
            // int numThresh=sqlite3_changes(binsDB);
            // NSLog(@"updateThresholdItem count=%d", numThresh);
            
            if(sqlite3_changes(binsDB)>0){
                isSuccess=YES;
            }
        }
    }
    sqlite3_finalize(statement);
    
    if(isSuccess==NO){
        [self insertThresholdItem:threshId withName:name withMovements:movements withSleepState:sleepState];
    }
    
    
    //-- int  stateSleepNew=[self getSleepStateFromThreshold:threshId ];

    //-- NSLog(@"updateThresholdItem %d=%d", threshId, stateSleepNew);
    
    return isSuccess;
    
}


-(void) insertThresholdItem:(int)threshId withName:(const char*)name withMovements:(int)movements withSleepState: (int) sleepState {
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %d,                         \
            \"%s\",                         \
            %d,                         \
            %d                         \
            ); ",
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshValue,
            binsData.sleepThreshold->sleepThreshName,
            binsData.sleepThreshold->sleepThreshState,
            binsData.sleepThreshold->sleepThreshId,
            movements,
            name,
            sleepState,
            threshId
            );
    
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add threshold");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}


-(BOOL)updateThreshold: (const char*) charThreshId withName:(const char*) charName withMovements:(const char*) charMovements withSleepState: (const char*) charSleepState {
    
    BOOL isSuccess=NO;
    
    sprintf(strSqlStatement,
            "UPDATE %s  SET                \
            %s=%s,                      \
            %s=%s,                      \
            %s=%s                      \
            WHERE                           \
            %s=%s                       \
            ; ",
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshValue, charMovements,
            binsData.sleepThreshold->sleepThreshName, charName,
            binsData.sleepThreshold->sleepThreshState, charSleepState,
            binsData.sleepThreshold->sleepThreshId, charThreshId
            );
    sqlite3_stmt     *statement;
    sqlite3_prepare_v2(binsDB, strSqlStatement,
                       -1, &statement, NULL);
    
    if (sqlite3_step(statement) != SQLITE_DONE)
    {
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
        
        NSLog( @"Failed to update threshold table");
    }
    else{
        if(sqlite3_changes(binsDB)>0){
            isSuccess=YES;
        }
    }
    
    sqlite3_finalize(statement);
    
    return isSuccess;
    
}

-(void) insertThreshold:(const char*) charThreshId withName:(const char*) charName withMovements:(const char*) charMovements withSleepState: (const char*) charSleepState {
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %s,                         \
            %s,                         \
            %s,                         \
            %s                         \
            ); ",
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshValue,
            binsData.sleepThreshold->sleepThreshName,
            binsData.sleepThreshold->sleepThreshState,
            binsData.sleepThreshold->sleepThreshId,
            charMovements,
            charName,
            charSleepState,
            charThreshId
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add threshold");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);
}


-(void) updateSleepThresholdTable: (NSArray*) recordList
{
    //-- the first row is for names
    //-- the last row is out of boundary, don't know why
    int i;
    for(i=0; i<recordList.count; i++){
        
        NSDictionary *record=[recordList objectAtIndex:i];
        
        NSEnumerator *enumRec=[[record allValues] objectEnumerator];
        NSString *tokenName=[enumRec nextObject];
        NSString *tokenMovements=[enumRec nextObject];
        NSString *tokenThreshId=[enumRec nextObject];
        NSString *tokenSleepState=[enumRec nextObject];
        
        const char *charThreshId=[tokenThreshId UTF8String];
        const char *charName=[tokenName UTF8String];
        const char *charMovements=[tokenMovements UTF8String];
        const char *charSleepState=[tokenSleepState UTF8String];
        
        if([self updateThreshold: charThreshId withName:charName withMovements:charMovements withSleepState: charSleepState]==NO){
            
            [self insertThreshold: charThreshId withName:charName withMovements:charMovements withSleepState: charSleepState];
            
        }
        
    }
}


-(BOOL) getLevelPeriodsFrom: (long) startTime to: (long) endTime total: (int*) totalNumber
{
    //-- int userID=currentUserID;
    
    //-- Log.i(TAG, "getLevelPeriods:"+startTime+":"+endTime);

    *totalNumber=0;

    sprintf(strSqlStatement,
            "SELECT COUNT(*) FROM %s WHERE %s>=%ld AND %s<=%ld\
            ;",
            binsData.cubeLevelPeriods->tableName,
            binsData.cubeLevelPeriods->dtmStart, startTime,
            binsData.cubeLevelPeriods->dtmEnd, endTime
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }else{
        if(sqlite3_step(statement) == SQLITE_ROW) {
            *totalNumber = sqlite3_column_int(statement, 0);
        }
    }
    
    if(*totalNumber==0) return NO;
    
    
    sprintf(strSqlStatement,
            "SELECT %s, %s, %s FROM %s WHERE %s>=%ld AND %s<=%ld\
            ;",
            binsData.cubeLevelPeriods->threshId,
            binsData.cubeLevelPeriods->dtmStart,
            binsData.cubeLevelPeriods->dtmEnd,
            binsData.cubeLevelPeriods->tableName,
            binsData.cubeLevelPeriods->dtmStart, startTime,
            binsData.cubeLevelPeriods->dtmEnd, endTime
            );
    
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    activeStatement=statement;
    
    
    return YES;
    
}


-(BOOL) getNextLevelPeriod: (int*)threshId from: (long*)timeStart to: (long*)timeEnd
{
    if(sqlite3_step(activeStatement) == SQLITE_ROW) {
        (*threshId)=sqlite3_column_int(activeStatement, 0);
        (*timeStart)=sqlite3_column_double(activeStatement, 1);
        (*timeEnd)=sqlite3_column_double(activeStatement, 1);
    }else{
        sqlite3_finalize(activeStatement);
        return NO;
    }
    
    return YES;
}


-(BOOL)getThresholds {
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s ORDER BY %s ASC \
            ;",
            binsData.sleepThreshold->sleepThreshId,
            binsData.sleepThreshold->sleepThreshValue,
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshId
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    activeStatement=statement;
    
    return YES;
    
}
-(BOOL)getNextThresholdFromQuery: (int*)threshId movements: (int*)movements {
    
    if(sqlite3_step(activeStatement) == SQLITE_ROW) {
        (*threshId)=sqlite3_column_int(activeStatement, 0);
        (*movements)=sqlite3_column_int(activeStatement, 1);
        
    }else{
        sqlite3_finalize(activeStatement);
        return NO;
    }
    
    return YES;
}
-(int)getThresholdsCount {
    
    return sqlite3_column_count( activeStatement );
}

-(int) getThresholdId: (int) movements
{
    int threshId=0;
 
    //-- Log.i(TAG, "sleep getThreshId :"+movements);
    
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s ORDER BY %s ASC \
            ;",
            binsData.sleepThreshold->sleepThreshId,
            binsData.sleepThreshold->sleepThreshValue,
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshValue
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return 0;
    }

    if(sqlite3_step(statement) == SQLITE_ROW) {
        threshId = sqlite3_column_int(statement, 0);
    }
    
    return threshId;
}

-(int) getSleepStateFromThreshold: (int) threshId {

    int sleepState=0;
    
    if(threshId<=0) return sleepState;  //-- 20160117
    
    sprintf(strSqlStatement,
            "SELECT %s FROM %s WHERE %s=%d \
            ;",
            binsData.sleepThreshold->sleepThreshState,
            binsData.sleepThreshold->tableName,
            binsData.sleepThreshold->sleepThreshId,
            threshId
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        
        NSLog(@"sleepState read error");
        
        return sleepState;
    }
    
    if(sqlite3_step(statement) == SQLITE_ROW) {
        sleepState = sqlite3_column_int(statement, 0);
    } else {
        NSLog(@"sleepState, cannot find for threshold=%d",threshId);
        
    }
    sqlite3_finalize(statement);


    //-- NSLog(@"getSleepStateFromThreshold %d=%d", threshId, sleepState);   //-- 20160117
    
    return sleepState;
}


-(BOOL) getCircadian: (int) hour toSleepFactor: (float*) sleepFactor toWakeFactor: (float*)wakeFactor {
    
    
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s WHERE %s=%d \
            ;",
            binsData.circadian->circPercSleepFactor,
            binsData.circadian->circPercWakeFactor,
            binsData.circadian->tableName,
            binsData.circadian->cirHour,
            hour
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    if(sqlite3_step(statement) == SQLITE_ROW) {
        *sleepFactor = sqlite3_column_double(statement, 0);
        *wakeFactor = sqlite3_column_double(statement, 1);
     
        sqlite3_finalize(statement);

        return YES;
    }
    
    sqlite3_finalize(statement);

    return NO;
}

-(BOOL) getTimeEffect: (int)threhId timeEffect: (int)hour toSleepFactor: (float*) sleepFactor toWakeFactor: (float*)wakeFactor {
    
    //-- 20151126 BUG FIX
    
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s WHERE %s=%d AND %s=%d \
            ;",
            binsData.sleepTimeEffect->sleepTimeEffectPercSleepFactor,
            binsData.sleepTimeEffect->sleepTimeEffectPercWakeFactor,
            binsData.sleepTimeEffect->tableName,
            binsData.sleepTimeEffect->sleepTimeEffectThreshId, threhId,
            binsData.sleepTimeEffect->sleepTimeEffectHour, hour
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    if(sqlite3_step(statement) == SQLITE_ROW) {
        *sleepFactor = sqlite3_column_double(statement, 0);
        *wakeFactor = sqlite3_column_double(statement, 1);
        sqlite3_finalize(statement);

        return YES;
    }
    sqlite3_finalize(statement);

    return NO;
}

-(void) insertCubeLevelPeriodFrom: (long) startTime to: (long) endTime atThreshold: (int) threshId {
    
    
    //-- Log.i(TAG, "sleep insertCubeLevelPeriod:"+startTime+":"+endTime+":"+threshId);
    
    
    sprintf(strSqlStatement,
            "INSERT INTO %s (              \
            %s,                             \
            %s,                             \
            %s,                             \
            %s                           \
            ) VALUES (                      \
            %d,                         \
            %d,                         \
            %ld,                         \
            %ld                         \
            ); ",
            binsData.cubeLevelPeriods->tableName,
            binsData.cubeLevelPeriods->userId,
            binsData.cubeLevelPeriods->threshId,
            binsData.cubeLevelPeriods->dtmStart,
            binsData.cubeLevelPeriods->dtmEnd,
            currentUserID,
            threshId,
            startTime,
            endTime
            );
    sqlite3_stmt     *statement;
    if(sqlite3_prepare_v2(binsDB, strSqlStatement, -1, &statement, NULL)==SQLITE_OK)
    {
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog( @"Failed to add cube Level Period");
        }
    }
    else{
        NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
    }
    sqlite3_finalize(statement);

}

-(BOOL)getNextSleepRecord: (int*)movements atTime: (long*)sleepTime {
    return [self getNextActivityFromQuery:movements atTime:sleepTime];
}

-(BOOL)getNextSleepRecord: (int*)movements ofType: (int*)type atTime: (long*)sleepTime {
    return [self getNextActivityFromQuery:movements ofType:type atTime:sleepTime];
}


//-- delegates of BinsCloudService

-(void)binsCloudService:(BinsCloudService*)binsCloudService loadActivityCompleted: (NSString*)userUID {
    NSLog(@"activity load from network completed - %@", userUID);
    
    [self.delegate binsDataChanged:self];
}

- (void)binsCloudService:(BinsCloudService*)binsCloudService addedNewData:(BOOL)isSuccess user:(int)userID {
    NSLog(@"addedNewData");
    
    //-- mDataIsUploading=NO; this will force multiple instances, avoid it
    
    if(isSuccess==YES){
        [self uploadTask];
    }
    
}

-(void)binsCloudService: (BinsCloudService*)binsCloudService addedNewUser: (BOOL)result userName: (NSString*)userName userUID: (NSString*)userUID {

    NSLog(@"user '%@' added returned: %d", userName, result);
    
    mIsCheckingUsernameOverServer=NO;
    
    if(result==YES){
        
        BOOL isSuccess=NO;
        
        sprintf(strSqlStatement,
                "UPDATE %s  SET                \
                %s=\"%s\"                       \
                WHERE                           \
                %s=\"%s\"                      \
                ; ",
                binsData.user->tableName,
                binsData.user->userUID,  [userUID UTF8String],
                binsData.user->userName, [userName UTF8String]
                );
        sqlite3_stmt     *statement;
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
            
            
            NSLog( @"Failed to update user's user ID");
        } else {
            isSuccess=YES;
        }
        sqlite3_finalize(statement);
        
        /* not required, as userId wont changed, only UID 150820
         if(isSuccess==YES){
         if(currentUserId==activeUserId){
         [self setActiveUserID:userId];
         }
         }
         */
    }
    
    [self.delegate binsDataChanged:self ];  //-- to inform completion of upload
    
}

//-- delegates of UnlockingLifeService


- (void)unlockingLifeService:(UnlockingLifeService*) ulockingService addedNewData:(BOOL)isSuccess user:(int)userID {
    NSLog(@"addedNewData of user %d",userID);
    
    mDataIsUploading=NO;
    
    if(isSuccess==YES){
        
        [self uploadTask];
    }
    
}
                              
- (void)unlockingLifeService:(UnlockingLifeService *)ulockingService hasUser: (BOOL)isUserExist {
 
}

- (void)unlockingLifeService:(UnlockingLifeService *)ulockingService addedNewUser: (BOOL)result userName: (NSString*)userName userId: (int)userUID {
    
    NSLog(@"user '%@' added returned: %d", userName, result);
    
    mIsCheckingUsernameOverServer=NO;
    
    if(result==YES){
        
        BOOL isSuccess=NO;
        
        NSString *strUserUID=[[NSString alloc] initWithFormat:@"%d", userUID];

        sprintf(strSqlStatement,
                "UPDATE %s  SET                \
                %s=\"%s\"                       \
                WHERE                           \
                %s=\"%s\"                      \
                ; ",
                binsData.user->tableName,
                binsData.user->userUID,  [strUserUID UTF8String],
                binsData.user->userName, [userName UTF8String]
                );
        sqlite3_stmt     *statement;
        sqlite3_prepare_v2(binsDB, strSqlStatement,
                           -1, &statement, NULL);
        
        if (sqlite3_step(statement) != SQLITE_DONE)
        {
            NSLog(@"SQLITE: Failed to prepare statement! Error: '%s'", sqlite3_errmsg(binsDB));
            
            
            NSLog( @"Failed to update user's user ID");
        } else {
            isSuccess=YES;
        }
        sqlite3_finalize(statement);
        
        /* not required, as userId wont changed, only UID 150820
        if(isSuccess==YES){
            if(currentUserId==activeUserId){
                [self setActiveUserID:userId];
            }
        }
         */
    }
}

#define MAX_UPLOAD_ACTIVITIES_SESSION 100

-(BOOL) uploadActivitiesOfType: (int) uploadType {
    BOOL isSuccess;
    
    
    NSString *userUID=[self getUserUID: currentUserID];
    
    if(userUID==nil){
        NSLog(@"uploadActivities user UID not assigned yet.");
        
        return NO;
    }
    
    isSuccess=[self queryActivitiesForUpload:currentUserID typeOf:uploadType ofLimit:MAX_UPLOAD_ACTIVITIES_SESSION];
    
    NSLog(@"upload session uploadType of: %d, success? %d", uploadType, isSuccess);

    if(isSuccess==YES){
        
#ifdef CONFIG_BINS_HBP
        
        [unlockingLifeServer createUploadSession: userUID ofType:uploadType];
        
        int actValue=0;
        int actType=0;
        long actTime=0;
        int  actSerialNo=0;
        long firstActivityTime=0;
        int numUploading=0;
        
        while([self getNextActivityForUploadFromQuery:&actValue ofType:&actType atTime:&actTime ofRecordNo: &actSerialNo]){
            [unlockingLifeServer addUploadActivity:actValue ofType: actType ofTime: actTime];
            numUploading++;
            if(firstActivityTime==0) firstActivityTime=actTime;
            //-- NSLog(@"uploading data:%d=%d",numUploading, actSerialNo);
            
        }
        
        if(numUploading==0) return NO;
        //-- 20150823
        //-- it has potential risk to lose track of status
        //-- if the data upload is not successful
        
        [self setActivitiesUploaded:currentUserID ofType:uploadType until:actSerialNo];
        
#else
        //-- TODO 20151119

        binsCloudServer.delegate=self;
        
        [binsCloudServer createUploadSession: currentUserID ofUID: userUID ofType:uploadType];

        int actValue=0;
        int actType=0;
        long actTime=0;
        int  actSerialNo=0;
        long firstActivityTime=0;
        int numUploading=0;

        while([self getNextActivityForUploadFromQuery:&actValue ofType:&actType atTime:&actTime ofRecordNo: &actSerialNo]){
            binsCloudServer.delegate=self;
            [binsCloudServer addUploadActivity:actValue ofType: actType ofTime: actTime];
            numUploading++;
            if(firstActivityTime==0) firstActivityTime=actTime;
            
            //-- NSLog(@"uploading data:%d=%d,%d",numUploading, actSerialNo, actType);
        }
        
        if(numUploading==0) return NO;
        //-- 20150823
        //-- it has potential risk to lose track of status
        //-- if the data upload is not successful
        
        [self setActivitiesUploaded:currentUserID ofType:uploadType until:actSerialNo];

#endif

    }
    
    return isSuccess;
}

#ifdef CONFIG_BINS_HBP  //-- 20151112

-(void)uploadTask {
    
    //-- 20160121  to avoid possible resource conflict
    // only start upload if not connected to band device
    if([deviceController isActiveDeviceConnected]==NO){
        NSLog(@"uploadTask - device connected, stop");
        return;
    }

    NSLog(@"uploadTask - loading? %d", mDataIsUploading);
    
    if(mDataIsUploading==YES){
        return;
    }
    
    mDataIsUploading=YES;
    
    //-- this function will create record session data of the specific activity type
    [self uploadActivitiesOfType:BINS_ACTIVITY_TYPE_SLEEP];
    [self uploadActivitiesOfType:BINS_ACTIVITY_TYPE_STEPS];
    
    //-- merge the session data to the command header and send upload request
    
    unlockingLifeServer.delegate=self;
    
    BOOL isSuccess=[unlockingLifeServer startUploadSession];

    if(isSuccess==NO) mDataIsUploading=NO;
        
    //-- later delegate addedNewData is called back from the server
    //-- and uploadTask can be called again if the delegate returns success, otherwise, this uploadTask will be called after every time finish of data synchronizing
}

#else
    //-- 20151119

-(void)uploadTask {
    
    //-- 20160121  to avoid possible resource conflict
    // only start upload if not connected to band device
    if([DeviceViewController isActiveDeviceConnectedStatic]==YES){
        NSLog(@"uploadTask - device connected, stop");
        return;
    }

    NSLog(@"BinsCloud uploadTask -is doing Loading? %d", mDataIsUploading);
    
    if(mDataIsUploading==YES){
        return;
    }
    
    mDataIsUploading=YES;   //-- a critical session for single access
    
    //-- this function will create record session data of the specific activity type
    [self uploadActivitiesOfType:BINS_ACTIVITY_TYPE_SLEEP];
    [self uploadActivitiesOfType:BINS_ACTIVITY_TYPE_STEPS];
    
    [self uploadActivitiesOfType:BINS_ACTIVITY_TYPE_PULSE];
    [self uploadActivitiesOfType:BINS_ACTIVITY_TYPE_LIGHT];
    
    //-- merge the session data to the command header and send upload request
    
    binsCloudServer.delegate=self;
    
    //-- BOOL isSuccess=
    
    [binsCloudServer startUploadSession];
    
    //-- if(isSuccess==YES)  //-- always YES
    
    mDataIsUploading=NO;
    
}

#endif

/*
-(float)getVitality: (NSInteger)dateInMinute  ofUser: (int)user{
    sprintf(strSqlStatement,
            "SELECT %s, %s FROM %s WHERE %s=%d AND %s=%d \
            ;",
            binsData.vitality->
            binsData.vitality->tableName,
            binsData.vitality->userId, user,
            binsData.vitality->date, [dateInMinute intValue]
            );
    
    if(sqlite3_prepare_v2(binsDB, strSqlStatement,
                          -1, &statement, NULL)!= SQLITE_OK) {
        return NO;
    }
    
    if(sqlite3_step(statement) == SQLITE_ROW) {
        *sleepFactor = sqlite3_column_double(statement, 0);
        *wakeFactor = sqlite3_column_double(statement, 1);
        sqlite3_finalize(statement);
        
        return YES;
    }
    sqlite3_finalize(statement);
    
}

-(NSInteger)getTodayStartTimeInMinute {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *dayComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:now];
    
    dayComponents.hour=0;
    dayComponents.minute=0;
    dayComponents.second=0;
    
    
    NSDate *today= [calendar dateFromComponents:dayComponents];
    
    
    NSInteger interval = [today timeIntervalSince1970];
    
    NSInteger intervalMinute = interval / 60;
    
    return intervalMinute;

}

-(float) getVitality: (int)userId ofPreviousDay: (int)numDays {

    NSInteger todayTime=[self getTodayStartTimeInMinute];
    
    NSInteger dateToCheck=todayTime+numDays*(60*24);
    
    
    
    return 0;
}
 */
/* TODO 20150915 */
/*
-(BOOL)vitalityDecreasedFrom: (float)vitDay1 to: (float)vitDay2 {
    if((vitDay2-vitDay1)<-30){
        return YES;
    }
    return NO;
}

-(BOOL)vitalityTooLow: (float)vitDay {
    if(vitDay<30) return YES;
    return NO;
}


#define MAX_VITALITY_TRACK_DAY 3
-(void) checkVitalityState: (int)userId {
    float vitalityValues[MAX_VITALITY_TRACK_DAY];
    
    for(int i=0; i<MAX_VITALITY_TRACK_DAY; i++){
        vitalityValues[i]=[self getVitality: userId ofPreviousDay: -(i)];
    }
 
    BOOL isDecreased=YES;
    BOOL isTooLow=YES;
    for(int i=0; i<(MAX_VITALITY_TRACK_DAY-1); i++){
        if([self vitalityDecreasedFrom: vitalityValues[i] to: vitalityValues[i+1]]==NO){
            isDecreased=NO;
        }
    }
    for(int i=0; i<(MAX_VITALITY_TRACK_DAY); i++){
        if([self vitalityTooLow: vitalityValues[i]]==NO){
            isTooLow=NO;
        }
    }
    
    if(isDecreased){
        [notificationController addNotification:
         NSLocalizedString(@"vitality_decrease",nil) withAction: NSLocalizedString(@"vitality_action",nil)];
        
    }else{
        if(isTooLow){
            [notificationController addNotification:
             NSLocalizedString(@"vitality_low",nil) withAction: NSLocalizedString(@"vitality_action",nil)];

        }
    }

}
*/
@end

