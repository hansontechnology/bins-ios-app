//
//  BinsTableViewController.h
//  bins
//
//  Created by Dennis Kung on 10/25/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TimeControllerView.h"
#import "DeviceViewController.h"
#import "BinsDataViewController.h"
#import "UserTableViewController.h"
#import "DevicePairView.h"
#import "PulseControllerView.h"
#import "FinderStatusView.h"
#import "SWRevealViewController.h"

@interface BinsTableViewController : UITableViewController <TimeControllerViewDelegate,DeviceControllerDelegate, DevicePairDelegate, PulseControllerDelegate, SWRevealViewControllerDelegate, BinsDataControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;
- (IBAction)rightButtonClick:(id)sender;

- (IBAction)leftButtonClick:(id)sender;

@property (weak, nonatomic) IBOutlet TimeControllerView *timeControllerView;

@property (weak, nonatomic) IBOutlet UIProgressView *stepsProgressView;

@property (weak, nonatomic) IBOutlet UIProgressView *distanceProgressView;

@property (weak, nonatomic) IBOutlet UIProgressView *activeMinutesProgressView;


@property (weak, nonatomic) IBOutlet UIProgressView *caloriesProgressView;

@property (weak, nonatomic) IBOutlet UILabel *pulseMessageLabel;

@property (weak, nonatomic) IBOutlet UILabel *pulseResultLabel;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *pulseRunningIndicator;
@property (weak, nonatomic) IBOutlet UILabel *binsSyncMessageLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *binsSyncIndicator;

@property DeviceViewController *deviceController;
@property BinsDataViewController *binsDataController;

@property DevicePairView *devicePair;

@property PulseControllerView *pulseController;

@property (weak, nonatomic) IBOutlet UILabel *stepCurrentLabel;

@property (weak, nonatomic) IBOutlet UILabel *stepMaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceCurrentLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceMaxLabel;
@property (weak, nonatomic) IBOutlet UILabel *minuteCurrentLabel;

@property (weak, nonatomic) IBOutlet UILabel *minuteMaxLabel;


@property (weak, nonatomic) IBOutlet UILabel *caloriesMaxLabel;


@property (weak, nonatomic) IBOutlet UILabel *caloriesCurrentLabel;


@property (weak, nonatomic) IBOutlet UILabel *userLabel;

@property (weak, nonatomic) IBOutlet UILabel *syncStatusLabel;
@property (weak, nonatomic) IBOutlet UITableViewCell *pulseViewCell;

@property (strong, nonatomic) IBOutlet UserTableViewController *userController;
@property (weak, nonatomic) IBOutlet FinderStatusView *finderStatusView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *timeSetViewCell;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)dateSelectPicker:(id)sender;
- (IBAction)dateSelectPickerValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *ppmLabel;

- (IBAction)userSelectButton:(UIBarButtonItem *)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *rightBarButton;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizerRight;

@property (weak, nonatomic) IBOutlet UITableViewCell *binsSyncTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *pulseTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *timeTableCell;

@property (weak, nonatomic) IBOutlet UIButton *buttonSleepRecBypass;

- (IBAction)buttonBypassTouchUpInside:(id)sender;
@property (weak, nonatomic) IBOutlet UITableViewCell *mentalTableCell;
@property (weak, nonatomic) IBOutlet UILabel *mentalValueLabel;


@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *mentalRunningIndicator;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;

@property (weak, nonatomic) IBOutlet UIProgressView *syncProgressView;

@end
