//
//  BinsTableViewController.m
//  bins
//
//  Created by Dennis Kung on 10/25/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "BinsTableViewController.h"
#import "LevelPeriodBuilder.h"
#import "UIButton+Glossy.h"
#import "binsHelper.h"
#import "UIView+Toast.h"
#import "BinsCloudService.h"

@interface BinsTableViewController ()

@end


@implementation BinsTableViewController

@synthesize deviceController;
@synthesize binsDataController;
@synthesize userController;
@synthesize devicePair;
@synthesize timeControllerView;
@synthesize pulseController;
@synthesize finderStatusView;
@synthesize buttonSleepRecBypass;
@synthesize mentalRunningIndicator;
@synthesize loadIndicator;
@synthesize syncProgressView;

static int currentUserID=INVALID_USER_ID;

static bool isFirstTimeSetup=NO;

static UIColor *originColor=nil;

#define HEIGHT_IPHONE5 568 // 12.22

static int heightOfPhone;

static int gMentalEffectValue=0;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    /*
    [UIView appearanceWhenContainedIn:[UITableView class], [UIDatePicker class], nil].backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    */
    //self.datePicker.backgroundColor=COLOR_TIME_PICKER_NEW;

    self.timeControllerView.delegate = self;
    
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 3.0f);
    
    //-- light blue
    UIColor *progressColor=[UIColor colorWithRed:((0x66) / 255.0) green:((0xB2) / 255.0) blue:((0xFF) / 255.0) alpha:(1)];
    
    self.stepsProgressView.transform=transform;
    self.distanceProgressView.transform=transform;
    self.activeMinutesProgressView.transform=transform;
    self.caloriesProgressView.transform=transform;
    
    self.stepsProgressView.progressTintColor=progressColor;
    self.distanceProgressView.progressTintColor=progressColor;
    self.activeMinutesProgressView.progressTintColor=progressColor;
    self.caloriesProgressView.progressTintColor=progressColor;
    
   
    
    deviceController=[[DeviceViewController alloc] init];
    deviceController.delegate=self;
    
    if([deviceController detectBluetooth]==NO){
        
    }
    
    binsDataController=[[BinsDataViewController alloc] init];
    
    
    if([binsDataController hasSetupAlready]==NO)
    {
        // [binsDataController insertNewUser];
     
        
        // [self setupFirstTime];
    }
    
    self.timeSetViewCell.hidden=YES;
    
    
    devicePair=[[DevicePairView alloc] init];
    devicePair.delegate=self;
    
    self.pulseViewCell.hidden=YES;
    
    pulseController=[[PulseControllerView alloc] init:self];
    pulseController.delegate=self;
    
    syncProgressView.hidden=YES;
    
    finderStatusView.backgroundColor=[UIColor clearColor];
    
    
    [timeControllerView setUserInteractionEnabled:YES];

    
    
    // Change button color
    _sidebarButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
   
    
    // Change button color
    _rightBarButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    _rightBarButton.target = self.revealViewController;
    _rightBarButton.action = @selector(rightRevealToggle:);
    
    
    // Set the gesture
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    // [self.navigationItem.titleView setBackgroundColor:[UIColor redColor]];
     
    
    
    NSString *leftArrow =@"\U000025C0\U0000FE0E";
    
    NSString *rightArrow =@"\U000025B6\U0000FE0E";

    
    [self.leftButton setTitle:leftArrow forState:UIControlStateNormal];
    
    [self.rightButton setTitle:rightArrow forState:UIControlStateNormal];
    
    [self.ppmLabel setHidden:YES];
    [self.pulseResultLabel setHidden:YES];
    [self.pulseMessageLabel setHidden:NO];
    self.pulseMessageLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"message_press_check_pulse",nil) ];
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(revealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer.enabled = NO;
    
    self.tapGestureRecognizerRight = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(rightRevealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
    self.tapGestureRecognizerRight.enabled = NO;
    
    self.revealViewController.delegate=self;
   
    heightOfPhone=self.view.frame.size.height;
    
    self.buttonSleepRecBypass.hidden=YES;
    
    //-- [self.buttonSleepRecBypass makeGlossy];
    
    buttonSleepRecBypass.layer.cornerRadius = 10.0;
    
    buttonSleepRecBypass.layer.borderWidth = 3.0;
    
    buttonSleepRecBypass.layer.borderColor = [UIColor grayColor].CGColor;
    buttonSleepRecBypass.clipsToBounds = YES;
    buttonSleepRecBypass.backgroundColor=Rgb2UIColor(240, 255, 255, 0.7 ); // 12.23
    
    self.mentalValueLabel.text=@"---";
    
}

-(void)setupFirstTime {
    
 
   userController=[[UserTableViewController alloc] init];
    
   // [self.navigationController pushViewController:userController animated:YES];
    
    [self performSegueWithIdentifier:@"setupUserSegue" sender:self];
    
    isFirstTimeSetup=YES;
}

-(void)viewWillAppear:(BOOL) animated {
    
    [super viewWillAppear:animated];
    
    [self.timeControllerView refresh];
    
    currentUserID=[binsDataController getActiveUserID];
    
    
    [self updateSyncMessageView];
    
    [self updateActivityView: YES];
    
    
    deviceController.delegate=self;

    
    NSLog(@"binsTable will appear");
    
    //if(isFirstTimeSetup==YES){
    //    [devicePair  autoPair];
    //}
    
    self.revealViewController.delegate=self;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 9;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 0) return;
    
    
    if (indexPath.row == 2) {
        
        [tableView deselectRowAtIndexPath:indexPath animated: YES];
        
        if([binsDataController getActiveDeviceID]==0){
            int userId=[binsDataController getActiveUserID];

            if(userId==0){
                [binsDataController insertNewUser];

                [self performSegueWithIdentifier:@"setupUserSegue" sender:self];
                
            }
            else{
                BOOL isRemoteUser=[binsDataController userIsRemoteUser: userId];

                if(isRemoteUser==NO){
                    [self.binsSyncIndicator startAnimating];
                
                    self.syncStatusLabel.text=NSLocalizedString(@"Connect to device..",nil);
                
                    [devicePair autoPair];
                }
            }
        } else {
            int userId=[binsDataController getActiveUserID];
            BOOL isRemoteUser=[binsDataController userIsRemoteUser: userId];
            if(isRemoteUser==NO){
                // to sync or stop sync
                [self syncToggle];
            }
        }
    } else if(indexPath.row==3){

        [self startUpdateMental];
        
    } else if(indexPath.row==4){
        pulseController.delegate=self;

        [deviceController startHeartrateDetection];
    }
    
   [tableView deselectRowAtIndexPath:indexPath animated: YES];
    
   // UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
   //[cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //[cell setBackgroundColor:[UIColor yellowColor]];
    
    
}

- (void)startUpdateMental {
    
    if(self.mentalRunningIndicator.isAnimating){
        [self.mentalRunningIndicator stopAnimating];
    }else{
        if([binsDataController hasSetupAlready]){
            [self.mentalRunningIndicator startAnimating];
            
            [self performSelector:@selector(continueUpdateMental) withObject:nil afterDelay:0.1f];
        }
    }
    
}

- (void)continueUpdateMental {
    
    [self updateMentalEffect];
    [self.mentalRunningIndicator stopAnimating];

}

-(BOOL)tableView: (UITableView*)tableView shouldHighlightRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.row==2) return YES;
    else if(indexPath.row==3) return YES;
    else if(indexPath.row==4) return YES;

    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section!=0) return 0;
    
    int activeHeightTableCell=(int)(80.0*(heightOfPhone-100)/(HEIGHT_IPHONE5-100));
    
    if(activeHeightTableCell<80) activeHeightTableCell=80;
    
    
    if(indexPath.row==0){
        return 50;
    }
    else if(indexPath.row==1){
        if(self.timeSetViewCell.hidden==YES){
            return 2;
        }
        else{
            return 160;
        }
    }else if(indexPath.row==2){
        return activeHeightTableCell;
    }else if(indexPath.row==3){
        return activeHeightTableCell;
    }else if(indexPath.row==4){
        if(self.pulseViewCell.hidden==YES){
            return 2;
        }
    }
        
    return activeHeightTableCell; // tableView.rowHeight;
}

-(void)tableView: (UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.section!=0) return;
    
    if(indexPath.row==0){
        [cell setBackgroundColor:COLOR_TIME_CONTROLLER_BAR];
    }
    
    if(indexPath.row==2){
        if(originColor==nil) originColor=cell.backgroundColor;

        if([deviceController isDeviceConnected:[binsDataController getActiveDeviceID]]){
        
            [cell setBackgroundColor:[UIColor colorWithRed:255/255 green:255/255 blue:254/255 alpha:0.05]];
            
        }
        else{
            [cell setBackgroundColor:originColor];
        }
        
        
    }
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"binsSyncTableCellIdentifier"];
    
    //  forIndexPath:indexPath
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:@"binsSyncTableCellIdentifier"];
    }

    // Configure the cell...
    
    //[cell setSelectionStyle: UITableViewCellSelectionStyleNone];

    
    //[cell setBackgroundColor:[UIColor yellowColor]];
    //Change the background image of a cell:
    
   // UIImage *theImage = [UIImage imageNamed:@"ximage.png"];
    //[[cell imageView] setImage:theImage];
  
    return cell;
}
*/


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)rightButtonClick:(id)sender {
    
    NSLog(@"right button");
    
    [self.timeControllerView changeDate: 1];
}

- (IBAction)leftButtonClick:(id)sender {
    
    NSLog(@"left button");
    
    [self.timeControllerView changeDate: -1];

}

-(void)timeControllerView:(TimeControllerView *)timeController browseModeChanged:(int)mode {
    
    [self updateActivityView: YES];
    
}
- (void)timeControllerView:(TimeControllerView *)timeControllerView timeDidChange:(NSDate*)dateChanged {

    NSLog(@"delegate called bins");
    
    [self updateActivityView: YES];
}


- (void)timeControllerView:(TimeControllerView *)timeController touchUpInside:(NSDate*)dateNow {
    
    
    if(self.timeSetViewCell.hidden==YES){
        self.timeSetViewCell.hidden=NO;
        self.datePicker.maximumDate=[NSDate date];
        [self.datePicker setDate:[timeController getCurrentDate]];
        
    }else{
        self.timeSetViewCell.hidden=YES;
    }
    
    [self.tableView reloadData];

}

-(void)deviceController:(DeviceViewController *)deviceViewController tryConnect:(int)option {
    
    int deviceID=[binsDataController getActiveDeviceID];
    
    NSLog(@"tryConnect");
    
    if([deviceController isDeviceConnected:deviceID]==true) return;
    
    if(option==TRY_CONNECT_WITH_SYNC){
        [self syncToggle];
    }else if(option==TRY_CONNECT_ONLY){
        [deviceController connectOnly];
    }
  
}


- (void)deviceController:(DeviceViewController*)deviceViewController sleepRecordRead: (long)savedNo of: (long)total {
    //-- 150610
    
    //-- NSLog(@"sleep rec read already:%ld",savedNo);
        
    [self.binsSyncIndicator stopAnimating];
        
    NSString *dispMessage=[[NSString alloc] initWithFormat:@"%ld of %ld", savedNo, total];
        
    self.binsSyncMessageLabel.text=dispMessage;
    
    syncProgressView.hidden=NO;
    float progressPercentage=fmod(((float)savedNo)/total,1);
    syncProgressView.progress=progressPercentage;
        
    //-- 20151129 disable this bypass function, self.buttonSleepRecBypass.hidden=NO;
}

- (void)deviceController:(DeviceViewController*)deviceViewController pulseNotified:(NSUUID*)deviceIdentifier withPulse: (int) pulse {
/*
    [self.ppmLabel setHidden:YES];
    [self.pulseResultLabel setHidden:YES];
    [self.pulseMessageLabel setHidden:NO];
*/
    self.pulseResultLabel.text=[NSString stringWithFormat:@"%d", pulse];
    self.pulseMessageLabel.text=NSLocalizedString(@"Reading Pulse", nil);
    
}

- (void)deviceController:(DeviceViewController*)deviceViewController stepsNotified:(NSInteger)time  withNewSteps: (int) steps {
    
    [self updateActivityView: NO];
    
}

-(void)deviceController:(DeviceViewController *)deviceViewController stateChanged: (int)state {
    
    NSLog(@"bins view deviceController delegate:%d",state);
    
    if(state==BINS_STATE_SYNC_FINISHED || state==BINS_STATE_CONNECTED){
    
        
        [self updateActivityView: YES ];

        [self.binsSyncIndicator stopAnimating];
    
        [self updateSyncMessageView];
        
        if([binsDataController getActiveDeviceType]==BINS_MODEL_BINSX1) {
            self.pulseViewCell.hidden=YES;
        }
        else{
            self.pulseViewCell.hidden=NO;
        }
        
        if(state==BINS_STATE_SYNC_FINISHED){
            [self.timeControllerView moveToToday];
        }
        
        
    } else if(state==BINS_STATE_DISCONNECTED ){
        
        [self.binsSyncIndicator stopAnimating];
        
        [self updateSyncMessageView];
        
        self.pulseViewCell.hidden=YES;
    }

    self.buttonSleepRecBypass.hidden=YES;
    syncProgressView.hidden=YES;

    [self.tableView reloadData];
   
}

-(void)deviceController:(DeviceViewController *)deviceViewController newActivityRead:(int)numberSync {
    
    NSLog(@"new data Sync seq:%d",numberSync);
    
    [self.binsSyncIndicator stopAnimating];
    
    NSString *dispMessage=[[NSString alloc] initWithFormat:@" %d", numberSync];
    
    self.binsSyncMessageLabel.text=dispMessage;
    
    syncProgressView.hidden=NO;
    float progressNumber=fmod(0.02*numberSync,1);
    syncProgressView.progress=progressNumber;

    
}

- (void)deviceController:(DeviceViewController*)deviceViewController finderStatusChanged:(int)status {
    
    NSLog(@"finder state changed:%d", status);
    
}

- (void)deviceController:(DeviceViewController*)deviceViewController errorFound:(NSString *)errMsg {
    
    NSLog(@"device error:%@", errMsg);
    
    [self.binsSyncIndicator stopAnimating];
    
    [self updateSyncMessageView];
    
    //-- self.pulseViewCell.hidden=YES; // unless disconnect otherwise no need
    
}

#define DISPLAY_NAME_LENGTH_MAX 25

-(void)updateSyncMessageView {
    
    syncProgressView.hidden=YES;
    
    if([binsDataController getActiveUserID]==INVALID_USER_ID){
        self.userLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"device_not_assigned", nil)];
    } else {
        
        NSString *userName=[binsDataController getUserName: currentUserID];
        
        if([userName length]>DISPLAY_NAME_LENGTH_MAX){
            userName= [userName substringToIndex:(DISPLAY_NAME_LENGTH_MAX-2)];
            userName= [userName stringByAppendingString:@".."];
        }
        self.userLabel.text=userName;

    }
    
    
    if([binsDataController getActiveDeviceID]==0 && [binsDataController activeUserIsRemote]==NO){
        self.syncStatusLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"press_to_setup", nil)];
        return;
    }
    
    NSInteger syncedTime=[binsDataController getActiveUserLastSyncTime];
    
    if(syncedTime==0){
        // first time
        self.binsSyncMessageLabel.text=@"---";
        
        return;
    }
    
    NSDate *syncedDate=[[NSDate alloc] initWithTimeIntervalSince1970:syncedTime*60]; // to second unit by *60
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd, HH:mm"];
    
    //NSString *strDate=[NSDateFormatter localizedStringFromDate:syncedDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle];
 
    NSString *strDate=[formatter stringFromDate:syncedDate];


    NSString *dispMessage=[[NSString alloc] initWithFormat:@"%@ - %@", strDate,NSLocalizedString(@"last_sync_time",nil)];
    
    
    self.binsSyncMessageLabel.text=dispMessage;
    
    
}


-(void)updateActivityView: (BOOL) isWithMentalUpdate {
    
    int user=[binsDataController getActiveUserID];
    
    int type=BINS_ACTIVITY_TYPE_STEPS;
    
    int steps;
    int minutes;
    
    NSDate* currentDate=[timeControllerView  getCurrentDate];
    
    NSInteger time=[binsDataController timeIntervalInMinute: currentDate withUnitMinute:YES];
    
    //-- [binsDataController getActivityPeriod: user withType: type onTime: time forPeriod:TIME_PERIOD_DAY_MINUTES hasSteps: &steps hasActiveMinutes: &minutes];
    
    int distance;
    
    
    NSLog(@"getActivityPeriod range: %ld to %ld", time, time+TIME_PERIOD_DAY_MINUTES-1);
    binsDataController.delegate=self;
    
    [binsDataController getActivityPeriod: user withType: type onTime: time forPeriod:TIME_PERIOD_DAY_MINUTES hasSteps: &steps hasActiveMinutes: &minutes andDistance: &distance usingDistanceChecker: ^(int userHeight, float steps){
        return [self distanceOfStepsPerMinute: userHeight ofSteps: steps];
    }];

    int goal=[binsDataController getDailyGoal];
    
    //-- [self updateDataView:steps withActiveMinutes:minutes withGoal:goal];
    
    [self updateDataView:steps withActiveMinutes:minutes withDistance:distance withGoal:goal];
    
    if(isWithMentalUpdate==YES){
        [self startUpdateMental];
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });

 
}


const int WALK_STEPS_PER_MINUTE_HEIGHT172CM = 96;
const int RUN_STEPS_PER_MINUTE_HEIGHT172CM = 163;
const float WALK_SPEED_KM_PER_HOUR_HEIGHT172CM = 4.8f;
const float RUN_SPEED_KM_PER_HOUR_HEIGHT172CM = 12.0f;
const float COMPENSATE_FOR_BINS_WALK_STEPS_PER_MINUTE = 0.13f;
const int WALK_DISTANCE_PER_WALK_HEIGHT172CM = 83;
const int WALK_DISTANCE_PER_WALK_HEIGHT155CM = 60;
const float BINS_WALK_STEPS_PER_MINUTE_HEIGHT172CM = WALK_STEPS_PER_MINUTE_HEIGHT172CM*(1-COMPENSATE_FOR_BINS_WALK_STEPS_PER_MINUTE);


// 01.09
-(float) distanceOfStepsPerMinute: (int)userHeight ofSteps: (float) stepsOfMinute {
    float stepsPerMinute=70; // steps in minute for lowest speed
    if(userHeight<140) userHeight=140;	// height in CM
    if(stepsOfMinute>stepsPerMinute) stepsPerMinute=stepsOfMinute;
    
    float factor_x= (RUN_SPEED_KM_PER_HOUR_HEIGHT172CM-WALK_SPEED_KM_PER_HOUR_HEIGHT172CM)/
    (RUN_STEPS_PER_MINUTE_HEIGHT172CM-BINS_WALK_STEPS_PER_MINUTE_HEIGHT172CM);
    float factor_y= WALK_SPEED_KM_PER_HOUR_HEIGHT172CM-(BINS_WALK_STEPS_PER_MINUTE_HEIGHT172CM*factor_x);
    
    float speedInDefaultHeight=(stepsPerMinute*factor_x+factor_y); //-- (stepsPerMinute*0.134f-9.895f);
    float ratioToDefaultHeight=(userHeight*1.15f-118.25f)/ (172*1.15f-118.25f);
    
    float speedOfHeight=speedInDefaultHeight*ratioToDefaultHeight;
    
    float distancePerMinute=speedOfHeight*1000/60; // in meter
    
    float distanceForSteps=distancePerMinute*stepsOfMinute/96;
    //-- Log.i(TAG, "Distance Minute:"+stepsOfMinute+":"+distanceForSteps+":"+mCurrentDistanceSum);
    
    return distanceForSteps;
}



#define DAY_MINUTES (24*60)

-(void)updateDataView: (int) steps withActiveMinutes: (int) minutes withDistance: (int)distance withGoal: (int)gDailyGoal {
    
    NSLog(@"updateDataView");
    
    if(gDailyGoal==0) gDailyGoal=10000;
    
    int maxSteps = steps + steps / 5;
    if(maxSteps<gDailyGoal) maxSteps=gDailyGoal;
    [self.stepsProgressView setProgress: ((float)steps/maxSteps)];
    self.stepMaxLabel.text=[NSString stringWithFormat:@"%d", maxSteps];
    self.stepCurrentLabel.text=[NSString stringWithFormat:@"%d", steps];
    
    //-- int distanceCurrent=(mCurrentStepsValue)*6/10;
    int maxDistance=distance*12/10;
    if(maxDistance<3000) maxDistance=3000;
    
    [self.distanceProgressView setProgress: ((float)distance/maxDistance) ];
    
    self.distanceMaxLabel.text=[NSString stringWithFormat:@"%d", maxDistance];
    self.distanceCurrentLabel.text=[NSString stringWithFormat:@"%d", distance];
 
    int idealMaxActivePerDay=(DAY_MINUTES/3);
    [self.activeMinutesProgressView setProgress:((float)minutes/idealMaxActivePerDay)];
    self.minuteMaxLabel.text=[NSString stringWithFormat:@"%d", idealMaxActivePerDay];
    self.minuteCurrentLabel.text=[NSString stringWithFormat:@"%d", minutes];
    
    
    double fCaloryCurrent=(steps)*0.03;
    
    int caloryCurrent=(int)fCaloryCurrent;
    //-- int maxCalory=caloryCurrent*13/10;
    int maxCalory=513; //-- 513 Kcal per day, if want to lose 1 kg in 15 days
    
    [self.caloriesProgressView setProgress:((float)caloryCurrent)/maxCalory ];
    
    self.caloriesMaxLabel.text=[NSString stringWithFormat:@"%d", maxCalory];

    self.caloriesCurrentLabel.text=[NSString stringWithFormat:@"%d", caloryCurrent];
    
    
}

-(void)devicePair:(DevicePairView *)devicePair foundDevice: (NSUUID*)deviceIdentifier withStrength:(int)rssi {
}

-(void)devicePair:(DevicePairView *)devicePair selectingDevice: (NSString*)ibeaconUuid withId: (int)deviceId {
    
}

-(void)devicePair:(DevicePairView *)devicePair pairToDevice:(NSUUID *)deviceIdentifier {
    
    NSLog(@"paired to device:%@",deviceIdentifier);
    
    [self.binsSyncIndicator stopAnimating];
    
    
    if(deviceIdentifier==nil){
        
        [self updateSyncMessageView];
        return;
    }
    
    [binsDataController setActiveDeviceAddress: deviceIdentifier];
    
    [deviceController startSync:[binsDataController getActiveDeviceID]];
}

-(void)syncToggle {
    
    NSLog(@"syncToggle");
    
    int deviceID=[binsDataController getActiveDeviceID];
    
    if([deviceController isDeviceConnected:deviceID]){
        [deviceController stopSync:deviceID];
        
    }else{
        
        if([deviceController startSync:deviceID]){
            dispatch_async(dispatch_get_main_queue(), ^{
                self.binsSyncMessageLabel.text=NSLocalizedString(@"Syncing..",nil);
            });        
        
            [self.binsSyncIndicator startAnimating];
        } else{
            
            [deviceController stopSync:deviceID];
        }

    }
    

}

#pragma mark - Pulse Controller 

-(void)PulseControllerView: (PulseControllerView *)pulseController hasStatus: (int)status withMessage: (NSString*)stateMessage {

    NSLog(@"pulseStatus");

    
    [self.ppmLabel setHidden:YES];
    [self.pulseResultLabel setHidden:YES];
    [self.pulseMessageLabel setHidden:NO];

    self.pulseMessageLabel.text=stateMessage;
    
    if(status==PULSE_STATUS_READING_STREAM)
        [self.pulseRunningIndicator startAnimating];
    else if(status==PULSE_STATUS_PREPARING_STREAM){
        
        self.pulseResultLabel.text=[[NSString alloc] initWithFormat:@"%d",0];
    }
    else if(status==PULSE_STATUS_STOP_STREAMING){
        
        [self.pulseRunningIndicator stopAnimating];
        
        self.pulseMessageLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"message_press_check_pulse",nil) ];
 
    }
    
}

-(void)PulseControllerView: (PulseControllerView *)pulseController pulseResult: (int)pulse isReady: (BOOL)ready{
    
    NSLog(@"pulseResult:%d", ready);
    
    [self.ppmLabel setHidden:NO];
    [self.pulseResultLabel setHidden:NO];
    [self.pulseMessageLabel setHidden:YES];

    self.pulseResultLabel.text=[[NSString alloc] initWithFormat:@"%d",pulse];
    
    if(ready==YES){
        [self.pulseResultLabel setTextColor:[UIColor blackColor]];
    } else{
        [self.pulseResultLabel setTextColor:[UIColor orangeColor]];
    }
    
}


/*
- (BOOL)revealControllerPanGestureShouldBegin:(SWRevealViewController *)revealController;
{
    return NO;
}

- (BOOL)revealControllerTapGestureShouldBegin:(SWRevealViewController *)revealController;
{
    return NO;
}

*/

- (IBAction)dateSelectPicker:(id)sender {
}

- (IBAction)dateSelectPickerValueChanged:(id)sender {
    
    [timeControllerView setDate: self.datePicker.date];
}
- (IBAction)userSelectButton:(UIBarButtonItem *)sender {
    
    
}

-(void) userInteractionEnable: (BOOL)enable {
    self.timeTableCell.userInteractionEnabled = enable;
    self.binsSyncTableCell.userInteractionEnabled = enable;
    self.pulseTableCell.userInteractionEnabled = enable;
    
    self.tabBarController.tabBar.userInteractionEnabled = enable;

}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeftSide) {
        // NSLog(@"FrontViewPositionLeftSide");
        // jump to right side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
        self.tapGestureRecognizerRight.enabled = YES;
        [self userInteractionEnable:NO];
    }
    else if (position == FrontViewPositionLeft){
        // NSLog(@"FrontViewPositionLeft");
        // back to main screen
        self.tapGestureRecognizer.enabled = NO;
        self.tapGestureRecognizerRight.enabled = NO;
        [self userInteractionEnable:YES];
    }
    else if (position == FrontViewPositionRight){
        // NSLog(@"FrontViewPositionRight");
        // jump to left side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.enabled = YES;
        [self userInteractionEnable:NO];

    }
}

- (IBAction)buttonBypassTouchUpInside:(id)sender {
    
    self.buttonSleepRecBypass.hidden=YES;
    syncProgressView.hidden=YES;
    [deviceController stopSleepRecordUpload];
    
}

typedef struct LevelPeriod{
    int threshId;
    long startTime;
    long endTime;
} LevelPeriod;


//-- 150615
-(BOOL) updateMentalEffect {
    

#ifndef CONFIG_BINS_HBP //-- 20160121
    if([BinsCloudService isUpdatingTables]){
        return NO;
    }
#endif
    
    float mentalEffectTotal=0;
    
    self.mentalValueLabel.text=@"---";

    [binsDataController clearCubeLevelPeriods];

    
    NSDate* currentDate=[timeControllerView  getCurrentDate];
    
    //-- 20160117
    long endTime=[binsDataController timeIntervalInMinute: currentDate withUnitMinute:YES];
    long startTime=endTime-(24*60*2);
    
    if(currentDate==[timeControllerView getToday]){ // if include today
        NSDate* currentTime=[NSDate date];  //-- up to now
        endTime=[binsDataController timeIntervalInMinute: currentTime withUnitMinute:YES];
    }else{
        endTime=endTime+(24*60);    //-- by end of the day
    }

    int numLevelPeriods;
    
    {
        binsDataController.delegate=self;
        
        NSLog(@"query sleep range:%ld to %ld", startTime, endTime);
        
        int user=[binsDataController getActiveUserID];

        int queryResult=[binsDataController queryActivityRecords: user typeOf: BINS_ACTIVITY_TYPE_SLEEP timeFrom: startTime timeTo: endTime];
        
        if(queryResult==QUERY_STOPPED) return NO;
        
        if(queryResult==QUERY_WORKING){
            NSLog(@"should running animation now");

            dispatch_async(dispatch_get_main_queue(), ^{
                [loadIndicator startAnimating];
            });

            return NO;
        }


        LevelPeriodBuilder *levelPeriod=[[LevelPeriodBuilder alloc] initWithSleepPeriodFrom:startTime to:endTime];

        
        float availPercent=[levelPeriod getDataAvailability]; //-- 20151023
        NSLog(@"movement avail=%f",availPercent);
        if(availPercent<0.7){
            if(availPercent==0) return NO;
            
            return NO;
        }
        
        
        [levelPeriod storeToDB];
        
        //-- return NO;
    }
    
    
    BOOL queryResult=[binsDataController getLevelPeriodsFrom: startTime to: endTime total: (&numLevelPeriods)];

    
    //-- Log.i(TAG, "mentaleffect cursor:"+cursor+":"+startTime+":"+endTime);
    
    if (queryResult==NO || numLevelPeriods==0){
        return queryResult; 	// nothing to do
        
    }
    
    LevelPeriod *levelPeriodList=calloc(numLevelPeriods, sizeof(LevelPeriod));
    
    int threshId;
    long timeStart;
    long timeEnd;

    int periodIndex=0;
    while ([binsDataController getNextLevelPeriod: &threshId from: &timeStart to: &timeEnd])
    {
        //-- Log.i(TAG, "mentaleffect list:"+threshId+":"+timeStart+":"+timeEnd);
        
        levelPeriodList[periodIndex].threshId=threshId;
        levelPeriodList[periodIndex].startTime=timeStart;
        levelPeriodList[periodIndex].endTime=timeEnd;
        
        periodIndex++;
    }
    
    if(periodIndex==0) return NO;
    
    //-- the level period is listed in order of time,
    //-- for calculation of circadian effect, it is necessary to know the reletive time
    //-- in 24 hour format.
    //-- the timeStart and timeEnd is a minute numbers starting from 1970
    //-- to use this as a start point to calculate the 00:00 time

    int i;
    for(i=0; i<numLevelPeriods; i++){
        float periodDelta=0;
        int threshId=levelPeriodList[i].threshId;
        
        long timeStart=levelPeriodList[i].startTime;
        long timeEnd=levelPeriodList[i].endTime;
        int  stateSleep=[binsDataController getSleepStateFromThreshold:threshId ];
        
        long timePeriod=(timeEnd-timeStart+1);
        float hourFraction;
        
        for(int theHour=0; theHour*60<=timePeriod; theHour++){
            if((theHour+1)*60<timePeriod){
                hourFraction=1;
            }else{
                hourFraction=((float)(timePeriod-theHour*60)/60);
            }

#if 1
            //-- int cirHour=((timeStart+theHour*60)/60+18)%24;
            
            
            NSDate *cirDate=[[NSDate alloc] initWithTimeIntervalSince1970:(timeStart+theHour*60)*60]; // transfrom from minute to second as the unit by *60
            
            NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

            NSDateComponents *cirDateComponents = [calendar
                    components:NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitHour |NSCalendarUnitMinute |NSCalendarUnitSecond
                                                   fromDate:cirDate];
            
            int cirHour = (int)(cirDateComponents.hour);
            
            //-- NSLog(@"mental circadian:%d, %d, %d",cirHour, threshId, stateSleep);

            //-- cirHour+=18; //-- 0-23 to 18-41 ????
            cirHour+=1; //-- Nth hour 0-23 changes to 1-24

            if(cirHour>24){ cirHour-=24; } //-- 20151125 >23 to 24,
            
            float cirPercSleepFactor;
            float cirPercWakeFactor;

            BOOL cirQueryResult=
                    [binsDataController getCircadian: cirHour
                    toSleepFactor: &cirPercSleepFactor
                    toWakeFactor: &cirPercWakeFactor ];
            
            if(cirQueryResult==YES){
                if(stateSleep!=0){
                    periodDelta+=hourFraction*cirPercSleepFactor;
                } else {
                    periodDelta+=hourFraction*cirPercWakeFactor;
                }
            }
            //-- NSLog(@"mental circadian:%d, %d, %d, %f= %f, %f=%f,%f",cirHour, threshId, stateSleep, hourFraction, cirPercSleepFactor, cirPercWakeFactor, mentalEffectTotal, periodDelta);
#endif
            
#if 1
            int timeEffectHour=theHour+1; //-- nth hour, start from 1 instead of 0
            
            float timePercSleepFactor;
            float timePercWakeFactor;

            BOOL timeEffectQueryResult=
                [binsDataController getTimeEffect: threshId
                    timeEffect: timeEffectHour
                    toSleepFactor: &timePercSleepFactor
                    toWakeFactor: &timePercWakeFactor ];
            
            if(timeEffectQueryResult==YES){ //-- 20151125 no related to sleep state,
                                            //-- it is recoveryFactor
                periodDelta+=hourFraction*timePercSleepFactor;
                
            }
            //-- NSLog(@"mental timeeffect:%d, %d, %d, %f= %f, %f=%f,%f", timeEffectQueryResult, timeEffectHour, threshId, hourFraction, timePercSleepFactor, timePercWakeFactor, mentalEffectTotal, periodDelta);
#endif
            
        } // loop for every hour of the cube level period
        
        
        //-- NSLog(@"mentaleffect periodDelta:%f, %f", mentalEffectTotal,periodDelta);
        
        mentalEffectTotal+=periodDelta;
        
        
        if(mentalEffectTotal>100) mentalEffectTotal=100;
        if(mentalEffectTotal<0) mentalEffectTotal=0;
        
        
        
    }	// loop for every cube level period of the period of time.
    
    //-- this mentalEffectTotal should be output to UI
    
    gMentalEffectValue=(int)mentalEffectTotal;
    
    
    self.mentalValueLabel.text=[NSString stringWithFormat:@"%d%%",gMentalEffectValue];
    
    return queryResult;
    
}

-(void)uiCheck {
    dispatch_async(dispatch_get_main_queue(), ^{
        [loadIndicator stopAnimating];
    });
}
//-- delegates of BinsDataViewController
//-- 20151119
- (void)binsDataChanged:(BinsDataViewController*)binsDataController {
    NSLog(@"binsDataChanged from BinsTableView");
    
    [self uiCheck];
    
    [self updateActivityView: YES];
}

@end
