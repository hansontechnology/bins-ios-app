/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#import "Constants.h"

#warning To run this sample correctly, you must set the following constants.
AWSRegionType const CognitoRegionType = AWSRegionUSEast1; // e.g. AWSRegionUSEast1
AWSRegionType const DefaultServiceRegionType = AWSRegionUSEast1; // e.g. AWSRegionUSEast1
NSString *const CognitoIdentityPoolId = @"us-east-1:c9792aff-127a-48c5-908f-ab5e137c02ae"; // identity name is 'bins'
NSString *const AWSSampleDynamoDBTableName = @"BinsUserTable";
NSString *const SNSPlatformApplicationArn = @"arn:aws:sns:us-east-1:414327512415:app/APNS_SANDBOX/BinsSNSApplicationDev";
NSString *const MobileAnalyticsAppId = @"e7f757745d9f40d08c2f9e0cfd0abb07";