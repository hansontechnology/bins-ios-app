//
//  DevicePairView.h
//  bins
//
//  Created by Dennis Kung on 11/1/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
@import CoreBluetooth;
@import QuartzCore;

#include "DeviceViewController.h"



@class DevicePairView;

@protocol DevicePairDelegate
- (void)devicePair:(DevicePairView*)devicePairView pairToDevice: (NSUUID*)deviceIdentifier;
- (void)devicePair:(DevicePairView*)devicePairView selectingDevice: (NSString*)ibeaconUuid withId: (int)deviceId;
- (void)devicePair:(DevicePairView *)devicePair foundDevice: (NSUUID*)deviceIdentifier withStrength:(int)rssi;

@end


@interface DevicePairView : UIView <CBCentralManagerDelegate, CBPeripheralDelegate>

@property (assign) id <DevicePairDelegate> delegate;

@property (nonatomic, strong) CBCentralManager *bluetoothManager;
@property (nonatomic, strong) CBPeripheral     *binsPeripheral;

-(void)autoPair;

@end
