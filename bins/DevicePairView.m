//
//  DevicePairView.m
//  bins
//
//  Created by Dennis Kung on 11/1/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "DevicePairView.h"

@implementation DevicePairView

@synthesize bluetoothManager;
@synthesize binsPeripheral;

CBCentralManager *mCentralManager=nil;
CBPeripheral    *selectedPeripheral=nil;

NSUUID *activeDeviceUUID;
NSString *binsDeviceName;

NSTimer *autoPairCheckTimer;
bool hasNewDeviceFound=NO;
NSMutableDictionary *binsDevicesFound;

NSMutableDictionary *deviceToIdTable;
NSMutableDictionary *deviceToRssiTable;

bool isInitiated=NO;
static int deviceCheckCounter=0;

NSInteger mRssiStrongest=-200;


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(DevicePairView*)init {
    
    CBCentralManager *centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    bluetoothManager = centralManager;
    
    binsDevicesFound=[[NSMutableDictionary alloc] init];
    
    deviceToIdTable=[[NSMutableDictionary alloc] init];
    deviceToRssiTable=[[NSMutableDictionary alloc] init];
    
    isInitiated=YES;
    
    return [super init];
}


-(void)autoPair {
    
    if(autoPairCheckTimer!=nil){
        [self stopPairing: nil];
        return;
    }
    
    mRssiStrongest=-200;
    if(selectedPeripheral!=nil){
        [bluetoothManager cancelPeripheralConnection:selectedPeripheral];
        selectedPeripheral=nil;
    }
    [deviceToRssiTable removeAllObjects];
    
    NSArray *services = @[[CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING]];

    binsDeviceName=BINS_DEVICE_NAME_STRING;
    
    if([bluetoothManager state]!=CBCentralManagerStatePoweredOn){
        
        return;
    }
    
    [binsDevicesFound removeAllObjects];
    
    [bluetoothManager scanForPeripheralsWithServices:nil options:nil];
    
    deviceCheckCounter=0;
    if(autoPairCheckTimer!=nil){
        [autoPairCheckTimer invalidate];
        autoPairCheckTimer=nil;
    }
    //-- 20151028 3
    autoPairCheckTimer = [NSTimer scheduledTimerWithTimeInterval:6 target:self selector:@selector(autoPairCheckTimerRoutine) userInfo:services repeats:YES];
    
}

-(BOOL) addDevice: (CBPeripheral*)peripheral withRSSI: (NSNumber*)rssi
{

    BOOL isNewPeripheralSelected=NO;
    
    BOOL isNeedFindDeviceId=NO;
    
    NSNumber *existRssi=(NSNumber*)[deviceToRssiTable objectForKey:peripheral];
    if((existRssi==nil) ||
       (existRssi!=nil && [existRssi intValue]!=[rssi intValue]))
    {
        NSLog(@"new or updated peripheral: old / new=%d / %d ", [existRssi intValue], [rssi intValue]);
        

        [deviceToRssiTable setObject:
            [[NSNumber alloc] initWithInteger:[rssi intValue]]
            forKey:peripheral];
        
        int newRssi=[rssi intValue];

        if((newRssi<0) && newRssi>mRssiStrongest){
            mRssiStrongest=[rssi intValue];
            if(selectedPeripheral!=peripheral){
                selectedPeripheral=peripheral;
                isNewPeripheralSelected=YES;
            }
            
            [self.delegate devicePair: self foundDevice: peripheral.identifier withStrength:(int)mRssiStrongest];

        }
    }else{
        NSLog(@"rssi equals to before: %d", [rssi intValue]);
    }

    if(isNewPeripheralSelected==YES){
     /*
        NSString *ibeaconUuid=[deviceToIdTable objectForKey:peripheral];

        if(ibeaconUuid!=nil){
            int deviceId=[self getDeviceIdFromBeacon: ibeaconUuid];
            [self.delegate devicePair: self selectingDevice:deviceId];
        }else{
            isNeedFindDeviceId=YES;
        }
     */
    }
    
    if(existRssi==nil) hasNewDeviceFound=YES;
    
    [binsDevicesFound setObject:rssi forKey: peripheral];
    
    return isNeedFindDeviceId;
}

-(void)stopPairing: (CBPeripheral*)peripheral {
    
    [bluetoothManager stopScan];
    [autoPairCheckTimer invalidate];
    autoPairCheckTimer=nil;

    if(peripheral==nil){
        [self.delegate devicePair: self pairToDevice: nil];
        return;
    }
    
    NSUUID *device=peripheral.identifier;
    
    NSString *ibeaconUuid=[deviceToIdTable objectForKey:peripheral];
    int deviceId=0;
    if(ibeaconUuid!=nil){
        deviceId=[self getDeviceIdFromBeacon: ibeaconUuid];
    }
    else{
        [mCentralManager connectPeripheral:peripheral options:nil];
    }

    [self.delegate devicePair: self pairToDevice: device];
    
}

-(void)autoPairCheckTimerRoutine {
    
    NSLog(@"pair timeout:%d", deviceCheckCounter);
    
    if(hasNewDeviceFound==NO){
        [self stopPairing:selectedPeripheral];

    }
    hasNewDeviceFound=NO;

}

#pragma mark - CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"device pair failed to connect");
    
    //-- report failure
    [self.delegate devicePair: self selectingDevice:nil withId: 0];
    
}

// method called whenever you have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    if(peripheral.state == CBPeripheralStateConnected){
        
        NSLog(@"didConnectPeripheral %@", peripheral);
        [peripheral setDelegate:self];
        
        [peripheral discoverServices:nil];

    }
}

// CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
    NSString *localName = [advertisementData objectForKey:CBAdvertisementDataLocalNameKey];

    //-- 20151028 no valid data for iOS, always return null
    // NSString *manufName = [advertisementData objectForKey:CBAdvertisementDataManufacturerDataKey];

    if ([localName length]>0) {
         if([localName isEqualToString:binsDeviceName]){
             NSLog(@"Found wearable: %@, %@, rssi=%d", localName, [peripheral identifier], [RSSI intValue]);
             
             mCentralManager=central;

             if([self addDevice: peripheral withRSSI: RSSI]==YES){
                 
                 binsPeripheral=peripheral;

                 //-- [mCentralManager stopScan];
                 //-- [mCentralManager connectPeripheral:peripheral options:nil];
                  
             }
             
             
          }
     }
}

- (BOOL)detectBluetooth
{
    if([bluetoothManager state]==CBCentralManagerStatePoweredOn){
        return YES;
    }else{
        [self centralManagerDidUpdateState:bluetoothManager]; // Show initial state
    }
    
    return NO;
}

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSString *stateString;

    if(isInitiated==NO) return;

    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        stateString=(@"CoreBluetooth BLE hardware is powered off 123");
    }
    else if ([central state] == CBCentralManagerStatePoweredOn){
        stateString=(@"CoreBluetooth BLE hardware is powered on and ready");
    }
    else if ([central state] == CBCentralManagerStateUnauthorized) {
    stateString=(@"CoreBluetooth BLE state is unauthorized");
    }
    else if ([central state] == CBCentralManagerStateUnknown) {
        stateString=(@"CoreBluetooth BLE state is unknown");
        }
    else if ([central state] == CBCentralManagerStateUnsupported) {
        stateString=(@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
   
    /*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Bluetooth is not ready", nil) message:stateString delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
    
    if([central state]!=CBCentralManagerStatePoweredOn) [alert show];
     */
    
}


#pragma mark - CBPeripheralDelegate

// CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    
    for (CBService *service in peripheral.services) {
        
        NSLog(@"Discovered service: %@", service.UUID);
        //-- if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING]])
        {
            
            [peripheral discoverCharacteristics:nil forService:service];
        }

    }

}

// Invoked when you discover the characteristics of a specified service.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING]]){
        
        NSLog(@"Found activity characteristics");
        
        for (CBCharacteristic *characteristic in service.characteristics)
        {
            //-- NSLog(@"character.. %@", characteristic.UUID);
            if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_IBEACON_UUID]]){
                
                [peripheral readValueForCharacteristic:characteristic];
            }
            
        }
    }
}

-(int)getDeviceIdFromBeacon: (NSString*) ibeaconUuid {
    
    // Create a NSUUID with the same UUID as the broadcasting beacon
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:ibeaconUuid];
    unsigned char uuidBytes[16];
    
    [uuid getUUIDBytes:uuidBytes];
    
    
    /*
     int systemId=0;

    for(int i=0; i<15; i++){
        //-- NSLog(@"uuid %d=%d", i, uuidBytes[i]);
        if(i==0) systemId+= (uuidBytes[i]);
        if(i==1) systemId+= (uuidBytes[i]<<8);
        if(i==2) systemId+=  (uuidBytes[i]<<16);
     
    }
     systemId %=1000000;
     return systemId;

     */
    
    int deviceId=0;
    unsigned char *bd=uuidBytes;
    
    /*
    for(int i=0; i<16; i++){
        NSLog(@"beacon id=%d=%x", i, bd[i]);
    }
    */
    
    deviceId+=(bd[2] & 0xFF);
    deviceId<<=8;
    deviceId+=(bd[1] & 0xFF);
    deviceId<<=8;
    deviceId+=(bd[0] & 0xFF);
    
    deviceId%=100000;

    
    NSLog(@"deviceId=%d for %@", deviceId, ibeaconUuid );
    return deviceId;

}
// Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_IBEACON_UUID]]){
        NSLog(@"Ibeacon uuid: %lu", (unsigned long)[[characteristic value] length]);
        
        const uint8_t *bytes=[[characteristic value] bytes];
        
        NSMutableString *strProxiID = [NSMutableString stringWithCapacity:16];
        
        int i;
        for(i=0; i<16; i++){
            //-- Log.i(TAG, "Ibeacon:"+i+":"+bytes[i]);
            switch (i)
            {
                case 3:
                case 5:
                case 7:
                case 9:[strProxiID appendFormat:@"%02x-", bytes[i]]; break;
                default:[strProxiID appendFormat:@"%02x", bytes[i]];
            }
            
        }
        NSLog(@"Ibeacon uuid string:%@",strProxiID); // 150502s
        
        
        [deviceToIdTable setObject: strProxiID forKey: peripheral];
        
        NSLog(@"Now and Selected peripheral:%@, %@",peripheral.identifier, selectedPeripheral.identifier); // 150502s
        
        if(selectedPeripheral==peripheral){
            NSString *ibeaconUuid=strProxiID;
            int deviceId=[self getDeviceIdFromBeacon: ibeaconUuid];
            [self.delegate devicePair: self selectingDevice:ibeaconUuid withId: deviceId];
            
        }
        [bluetoothManager cancelPeripheralConnection:peripheral];
        // [bluetoothManager scanForPeripheralsWithServices:nil options:nil];
    }
    
}


#pragma mark - CBCharacteristic helpers





@end
