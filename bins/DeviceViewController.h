//
//  DeviceViewController.h
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>

@import CoreBluetooth;
@import QuartzCore;
#import "BinsDataViewController.h"
#import "NotificationControllerView.h"
#import "PulseControllerView.h"
#import  <AVFoundation/AVFoundation.h>
#import "activity_tracking_service.h"
@import CoreLocation;

#define BINS_STATE_SYNC_FINISHED  0
#define BINS_STATE_DISCONNECTED   1
#define BINS_STATE_CONNECTED      2

#define BINS_FINDER_STATUS_NORMAL 0
#define BINS_FINDER_STATUS_ALERT 1
#define BINS_FINDER_STATUS_NOT_ACTIVE 2


#define BINS_DEVICE_NAME_STRING @"BINS"

#define POLARH7_HRM_DEVICE_INFO_SERVICE_UUID    @"180A"
#define POLARH7_HRM_HEART_RATE_SERVICE_UUID     @"180D"

#define POLARH7_HRM_MEASUREMENT_CHARACTERISTIC_UUID @"2A37"
#define POLARH7_HRM_BODY_LOCATION_CHARACTERISTIC_UUID @"2A38"
#define POLARH7_HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID @"2A29"



#define SERVICE_HEART_RATE_MONITOR              @"00002A37-0000-1000-8000-00805f9b34fb"
#define SERVICE_ACTIVITY_TRACKING               @"12345678-1234-1234-1234-123456789abc"
#define SERVICE_DEVICE_INFORMATION              @"0000180a-0000-1000-8000-00805f9b34fb"
#define SERVICE_BATTERY                         @"0000180f-0000-1000-8000-00805f9b34fb"

#define CHARACTERISTIC_ACTIVITY_VALUE           @"20000002-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_TYPE            @"20000001-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_TIME            @"20000003-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_CONFIGURATION            @"20000004-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_TIME_VALUE  	@"20000006-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_LOCAL_CLOCK  	@"20000010-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_TIMER        	@"20000011-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_GOAL         	@"20000012-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_UI_SENSE     	@"20000013-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_ACTIVITY_LINK_LOSS       @"20000014-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_HEARTRATE_STREAM  		@"20000015-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_HEARTRATE_HOST_UPDATE 	@"20000016-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_SLEEP_RECORDS            @"20000017-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_SLEEP_START_TIME  		@"20000018-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_IBEACON_UUID  			@"20000019-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_DEBUG_OUTPUT  			@"2000001A-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_UI_GESTURE_ANGLE  		@"2000001B-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_UI_GESTURE_WTIME  		@"2000001C-1234-1234-1234-123456789abc"
#define CHARACTERISTIC_CLOCK_ALARM  			@"2000001D-1234-1234-1234-123456789abc"

#define CHARACTERISTIC_DI_SERIAL_NUMBER         @"00002a25-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_MODEL_NAME            @"00002a24-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_SYSTEM_ID             @"00002a23-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_HW_REVISION           @"00002a27-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_FW_REVISION           @"00002a26-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_SW_REVISION           @"00002a28-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_MANUFACTURER          @"00002a29-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_DI_PNP_ID                @"00002a50-0000-1000-8000-00805f9b34fb"

#define CHARACTERISTIC_BATTERY                  @"00002a19-0000-1000-8000-00805f9b34fb"


#define TRY_CONNECT_WITH_SYNC  0
#define TRY_CONNECT_ONLY       1

@class DeviceViewController;
@class PulseControllerView;

@protocol DeviceControllerDelegate
- (void)deviceController:(DeviceViewController*)deviceViewController stateChanged:(int)state;
- (void)deviceController:(DeviceViewController*)deviceViewController stepsNotified:(NSInteger)time withNewSteps: (int)steps;

- (void)deviceController:(DeviceViewController*)deviceViewController newActivityRead:(int)readCountSession;

- (void)deviceController:(DeviceViewController*)deviceViewController pulseNotified:(NSUUID*)deviceIdentifier withPulse: (int) pulse;

- (void)deviceController:(DeviceViewController*)deviceViewController finderStatusChanged:(int)status;

-(void)deviceController:(DeviceViewController *)deviceViewController errorFound:(NSString*)errMsg;

-(void)deviceController:(DeviceViewController *)deviceViewController sleepRecordRead: (long)number of: (long)total;

-(void)deviceController:(DeviceViewController *)deviceViewController tryConnect:(int)option;

@end


@interface DeviceViewController : UIViewController <CBCentralManagerDelegate, CBPeripheralDelegate, CLLocationManagerDelegate>


@property (assign) id <DeviceControllerDelegate> delegate;
typedef void (^CompletionBlock)(); // id, NSError*);  // 12.12 for background support

@property (strong, nonatomic) CompletionBlock backgroundHandlerBlock;


-(void)startSync;
-(BOOL)startSync: (int) deviceIdentifier;
-(void)stopSync: (int)deviceID;

-(void)startFinder;
-(void)stopFinder;
-(bool)finderIsRunning;
-(void)checkFinder;
-(void)updateGoal ;
-(void)updateAlarm ;
-(void)updateShakeUiThreshold;

-(bool)isDeviceConnected: (int)deviceID;
-(bool)isActiveDeviceConnected;             //-- 20160121
+(bool)isActiveDeviceConnectedStatic;

-(BOOL)detectBluetooth;

-(void)pulseUpdate:(int)pulse;

-(void) startHeartrateDetection;
-(void) setAlertMessage;
-(void) setHeartRateEnable;
-(void) setAutoHeartrateDetection;
-(void) setBinsX2SensorBlink;
-(void) setStepCheckWhenHRDEnable;
-(void) setHeartRateSleepEnable;
-(void) setBinsDailyStepReset;
-(void) setBinsLock;
-(void) setBinsX2DisplayBrightness;
-(void) setPhoneNotice;
-(void) toggleSleepTrackEnable;
-(void) updateSleepTrackEnable;
-(void) timerTaskUpdate;
-(void) backgroundSyncAndDisconnect;
-(void) backgroundSyncAndDisconnect: (CompletionBlock)handlerBlockFrom;

-(void) stopSleepRecordUpload;
-(void) setDeviceBeaconRegion;
-(BOOL) connectOnly;

-(void) resetStatus;

-(NSInteger)localeTimeIntervalInMinute;

- (void)updatePeriodWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

@property (nonatomic, strong) UIWindow *window;

@property (nonatomic, unsafe_unretained) UIBackgroundTaskIdentifier backgroundTaskIdentifier;

@property  (strong, nonatomic) CBPeripheral     *binsPeripheral;

@property  (strong, nonatomic) CBPeripheral     *testPeripheral;

@property  (strong, nonatomic) CBCentralManager *bluetoothManager;


@property (strong, nonatomic) CLBeaconRegion    *beaconRegion;
@property (strong, nonatomic) CLLocationManager *locationManager;


@end

@interface ActivityRecord : NSObject

@property (nonatomic, strong) NSNumber *activityType;
@property (nonatomic, strong) NSNumber *activityValue;

@end
