//
//  DeviceViewController.m
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "DeviceViewController.h"
#import "AppDelegate.h"
#import "UIView+Toast.h"

#define DEVICE_COMMAND_IDLE 0
#define DEVICE_COMMAND_SYNC_ACTIVE 1
#define DEVICE_COMMAND_FINDER_ACTIVE 2
#define DEVICE_COMMAND_SYNC_BACKGROUND 3
#define DEVICE_COMMAND_CONNECT_ONLY 4

#define BINS_FINDER_RSSI_MIN     (-105.0)
#define BINS_FINDER_RSSI_MAX     (-88.0)
#define BINS_FINDER_RSSI_STEP    ((float)(BINS_FINDER_RSSI_MAX-BINS_FINDER_RSSI_MIN)/BINS_FINDER_LEVEL_MAX)

#define BINS_FINDER_TRACK_MAX   (5)

#define BEACON_MAJOR_NORMAL 0
#define BEACON_MAJOR_BATTERY_LOW 1
#define BEACON_MAJOR_HEARTRATE 2
#define BEACON_MAJOR_FREEDROP 3
#define BEACON_MAJOR_BUTTON_SOS 4
#define BEACON_MAJOR_CONNECT_REQUEST 10
#define BEACON_MAJOR_HEARTRATE_CYCLE 11 // only try connect , no sync data


@implementation DeviceViewController


@synthesize bluetoothManager;
@synthesize binsPeripheral;
@synthesize beaconRegion;
@synthesize locationManager;
@synthesize backgroundTaskIdentifier;

static BinsDataViewController* binsDataController;

static NotificationControllerView *notificationController;
static PulseControllerView *pulseController;

static DeviceViewController *mainDeviceController=nil;

static int deviceCommand=DEVICE_COMMAND_IDLE;

static bool isInitiated=NO;

static bool activeDeviceConnected=NO;

static NSTimer *finderTimer=nil;
static int finderStatus=BINS_FINDER_STATUS_NOT_ACTIVE;

#define IDLE_TIME_DISCONNECT (10*60)
#define CHECK_IDLE_INTERVAL (5) // seconds

#define CHECK_PERIOD_INTERVAL (90) // 90 seconds

#define BINS_CONNECTED_SYNCING 0
#define BINS_CONNECTED_NOTIFYING 1
#define BINS_CONNECTED_PREPARING 2

static int binsReadPhase=BINS_CONNECTED_SYNCING;
static int  binsTotalReadCountsSession=0;

static CBCharacteristic *binsCharacteristicConfiguration=nil;
static CBCharacteristic *binsCharacteristicLocalClock=nil;
static CBCharacteristic *binsCharacteristicGoal=nil;
static CBCharacteristic *binsCharacteristicActivityTimeValue=nil;
static CBCharacteristic *binsCharacteristicPulseStream=nil;
static CBCharacteristic *binsCharacteristicPulseUpdate=nil;
static CBCharacteristic *binsCharacteristicBattery=nil;
static CBCharacteristic *binsCharacteristicSoftwareRevision=nil;
static CBCharacteristic *binsCharacteristicLinkLoss=nil;
static CBCharacteristic *binsCharacteristicSleepStartTime=nil;
static CBCharacteristic *binsCharacteristicSleepRecords=nil;
static CBCharacteristic *binsCharacteristicBeaconId=nil;
static CBCharacteristic *binsCharacteristicAlarmClock=nil;

static bool mIsWroteLocalClock=false;
static bool mIsWroteGoal=false;
static bool mIsWroteConfiguration=false;
static bool mIsWroteAlarmClock=NO;


static int mTotalSteps=0;

static NSTimer *mCheckNotificationTimer=nil;
static NSTimer *syncTimeoutTimer=nil;

static NSTimer *mCheckPeriodicTimer=nil;

static bool hasActivityNotified=NO;

static int mActivityCheckCounter=0;

static int mLinklossInformed=0;
static int rssiRecord[BINS_FINDER_TRACK_MAX];
int rssiRecordHead=0;

int mCountFromLastSync=0;

static NSInteger mSleepRecordConnectTime=0;
static long mSleepRecordSessionSaved=0;
static long mSleepRecordSessionTotal=0;
static long mSleepRecordNextTime=0;
static bool isSleepRecordReadBypass=true;

static long mLastBeaconCheckForBatteryLow=0;

static NSMutableArray *mActivityStack=nil;

-(void)viewDidLoad {
    // [super viewDidLoad];
 /*
    // Do any additional setup after loading the view, typically from a nib.
    self.polarH7DeviceData = nil;
    [self.view setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [self.heartImage setImage:[UIImage imageNamed:@"HeartImage"]];
    
    // Clear out textView
    [self.deviceInfo setText:@""];
    [self.deviceInfo setTextColor:[UIColor blueColor]];
    [self.deviceInfo setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [self.deviceInfo setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:25]];
    [self.deviceInfo setUserInteractionEnabled:NO];
    
    // Create our Heart Rate BPM Label
    self.heartRateBPM = [[UILabel alloc] initWithFrame:CGRectMake(55, 30, 75, 50)];
    [self.heartRateBPM setTextColor:[UIColor whiteColor]];
    [self.heartRateBPM setText:[NSString stringWithFormat:@"%i", 0]];
    [self.heartRateBPM setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:28]];
    [self.heartImage addSubview:self.heartRateBPM];
*/
    // Scan for all available CoreBluetooth LE devices

    
}


-(DeviceViewController*) init {
    
    
    if(isInitiated==YES) return mainDeviceController;

    self=[super init];
    
    mainDeviceController=self;


    isInitiated=YES;
    
    CBCentralManager *centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:@{ CBCentralManagerOptionRestoreIdentifierKey:@"binsCentralManagerIdentifier" }];
    bluetoothManager = centralManager;
    
    binsDataController=[[BinsDataViewController alloc] init];
    
    notificationController=[[NotificationControllerView alloc] init];
    
    pulseController=[[PulseControllerView alloc] init];
    
    [self timerTaskUpdate];
    
    // Initialize location manager and set ourselves as the delegate
    locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    
    if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];

    mActivityStack=[NSMutableArray new];    //-- 20151129 only used as buffer for special case handling
    
    return self;
}

#pragma mark - CBCentralManagerDelegate

- (void)centralManager:(CBCentralManager *)central
      willRestoreState:(NSDictionary *)state {
    
    binsPeripheral = [state[CBCentralManagerRestoredStatePeripheralsKey] firstItem];
    binsPeripheral.delegate = self;
    
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"failed to connect");
    
    activeDeviceConnected=NO;
    
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error {
    NSLog(@"didDisconnectPeripheral: %@", error);
    
    
    activeDeviceConnected=NO;
    
    
    [self.delegate deviceController:self stateChanged:BINS_STATE_DISCONNECTED];
    
    //-- should not need on here,
    //-- 20160121 new chance to upload after disconnected
    [binsDataController uploadTask];
    
}


// method called whenever you have successfully connected to the BLE peripheral
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSLog(@"bins connected");
    
    if([self finderIsRunning]==YES && finderStatus==BINS_FINDER_STATUS_ALERT){
        [self resetFinderStatus];
    }
    
    
    /* NSArray *services= @[ [CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING], [CBUUID UUIDWithString:SERVICE_DEVICE_INFORMATION],
        [CBUUID UUIDWithString:SERVICE_BATTERY]
                        ];
     */
    
     binsPeripheral.delegate=self;
    
     if(peripheral.state == CBPeripheralStateConnected){
         
         
        activeDeviceConnected=YES;
        
         
        [peripheral setDelegate:self];
        
        [peripheral discoverServices:nil];

        
        [self.delegate deviceController:self stateChanged:BINS_STATE_CONNECTED];
        
    }
    
}

// CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    
    NSUUID *deviceAddress=[peripheral identifier];
    
    NSUUID *activeDeviceAddress=[binsDataController getActiveDeviceAddress];
    
    //-- NSLog(@"didDiscoverPeripheral %@ -> %@", deviceAddress, activeDeviceAddress);
    

    //NSString *currentAddress=[deviceAddress UUIDString] ;
    //NSString *activeAddress=[activeDeviceAddress UUIDString];
    //if([[deviceAddress UUIDString] isEqualToString:[activeDeviceAddress UUIDString]]==YES){
    
    if([deviceAddress isEqual:activeDeviceAddress]){
 
        NSLog(@"found active device: %@, %d",deviceAddress, [RSSI intValue]);
        
        binsPeripheral=peripheral; // retain is necessary !
                
        [central stopScan];
        
        [central connectPeripheral:peripheral options:nil];
    }
    
/*
    if ([localName length] > 0) {
        if(localName isEqualToString:<#(NSString *)#>)
        [bluetoothManager stopScan];
        binsPeripheral = peripheral;
        peripheral.delegate = self;
        [bluetoothManager connectPeripheral:peripheral options:nil];
    }
*/
    
     
    
}

- (BOOL)detectBluetooth
{
    if([bluetoothManager state]==CBCentralManagerStatePoweredOn){
        return YES;
    }else{
        [self centralManagerDidUpdateState:bluetoothManager]; // Show initial state
    }
    
    return NO;
}

// method called whenever the device state changes.
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    NSString *stateString;
    bool isReady=NO;
    
    
    // Determine the state of the peripheral
    if ([central state] == CBCentralManagerStatePoweredOff) {
        stateString=(NSLocalizedString(@"Please turn on Bluetooth from Settings.",nil)); // @"CoreBluetooth BLE hardware is powered off");
    }
    else if ([central state] == CBCentralManagerStatePoweredOn) {
        stateString=(@"CoreBluetooth BLE hardware is powered on and ready");
        isReady=YES;
    }
    else if ([central state] == CBCentralManagerStateUnauthorized) {
        stateString=(NSLocalizedString(@"Please turn on Bluetooth from Settings.",nil)); // @"CoreBluetooth BLE state is unauthorized");
    }
    else if ([central state] == CBCentralManagerStateUnknown) {
        stateString=(@"CoreBluetooth BLE state is unknown");
        isReady=YES;
    }
    else if ([central state] == CBCentralManagerStateUnsupported) {
        stateString=(NSLocalizedString(@"Bluetooth is not supported.",nil)); //@"CoreBluetooth BLE hardware is unsupported on this platform");
    }
    
    if(isReady==NO){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ble_not_ready",nil) message:stateString delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok",nil)otherButtonTitles:nil];
        
        [alert show];
        NSLog(@"Bluetooth status:%@", stateString);
        
    }

}


#pragma mark - CBPeripheralDelegate

// CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    NSLog(@"Entered services : %@", error);
    
    for (CBService *service in peripheral.services) {
        bool isActiveService=YES;
        
        NSLog(@"Discovered service: %@", service.UUID);
        
        if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING]]){
            
        }else if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_DEVICE_INFORMATION]]){
            
        }else if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_BATTERY]]){
        }
        else{
            isActiveService=NO;
        }
        
        if(isActiveService==YES){
            [peripheral discoverCharacteristics:nil forService:service];
        }
    }
}

// Invoked when you discover the characteristics of a specified service.
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    
    if([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING]]){
        
        NSLog(@"Found activity characteristics");
        
        [self setSyncWaitTimer:10];
        
        for (CBCharacteristic *characteristic in service.characteristics)
        {
            if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_ACTIVITY_TIME_VALUE]]){
                NSLog(@"start read session");
                
                binsCharacteristicActivityTimeValue=characteristic;
                
                if(deviceCommand!=DEVICE_COMMAND_CONNECT_ONLY){
                    [binsPeripheral  readValueForCharacteristic:characteristic];
                }

            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_CONFIGURATION]]){
            
                binsCharacteristicConfiguration=characteristic;
            
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_ACTIVITY_LOCAL_CLOCK]]){
            
                binsCharacteristicLocalClock=characteristic;
                
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_HEARTRATE_STREAM]]){
                
                binsCharacteristicPulseStream=characteristic;
                
                [binsDataController setActiveDeviceType: BINS_MODEL_BINSX2];    // 12.25
                
                //-- 20151129 early set up notify, no need to wait for the finish of sync
                [binsPeripheral setNotifyValue:YES forCharacteristic:binsCharacteristicPulseStream];

                if(deviceCommand==DEVICE_COMMAND_CONNECT_ONLY){
                    // 150716 to make sure this characteristic can be found.
                    [self endSyncNotify];
                }
                
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_HEARTRATE_HOST_UPDATE]]){
                
                binsCharacteristicPulseUpdate=characteristic;
                
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_ACTIVITY_GOAL]]){
            
                binsCharacteristicGoal=characteristic;
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_ACTIVITY_LINK_LOSS]]){
            
                binsCharacteristicLinkLoss=characteristic;
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_SLEEP_START_TIME]]){
                
                binsCharacteristicSleepStartTime=characteristic;
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_SLEEP_RECORDS]]){
                
                binsCharacteristicSleepRecords=characteristic;
                
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_IBEACON_UUID]]){
            
                binsCharacteristicBeaconId=characteristic;
        
            } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_CLOCK_ALARM]]){
            
                binsCharacteristicAlarmClock=characteristic;
            }

        }
    
    }
    if ([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_HEART_RATE_MONITOR]])  {  // 1
        for (CBCharacteristic *aChar in service.characteristics)
        {
            // Request heart rate notifications
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:POLARH7_HRM_MEASUREMENT_CHARACTERISTIC_UUID]]) { // 2
                [binsPeripheral setNotifyValue:YES forCharacteristic:aChar];
                NSLog(@"Found heart rate measurement characteristic");
            }
            // Request body sensor location
            else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:POLARH7_HRM_BODY_LOCATION_CHARACTERISTIC_UUID]]) { // 3
                [binsPeripheral readValueForCharacteristic:aChar];
                NSLog(@"Found body sensor location characteristic");
            }
        }
    }
    // Retrieve Device Information Services for the Manufacturer Name
    if ([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_DEVICE_INFORMATION]])  {
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if ([aChar.UUID isEqual:[CBUUID UUIDWithString:POLARH7_HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID]]) {
                [binsPeripheral readValueForCharacteristic:aChar];
                NSLog(@"Found a device manufacturer name characteristic");
            } else if ([aChar.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_DI_FW_REVISION]]) {
                
                binsCharacteristicSoftwareRevision=aChar;
                
          //      [binsPeripheral readValueForCharacteristic:aChar];
            }

            
            
        }
    }
    // Retrieve Device Information Services for the Manufacturer Name
    if ([service.UUID isEqual:[CBUUID UUIDWithString:SERVICE_BATTERY]])  {
        
        NSLog(@"Found battery characteristics");
        
 
        for (CBCharacteristic *aChar in service.characteristics)
        {
            if([aChar.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_BATTERY]]){
                
                binsCharacteristicBattery=aChar;

                [binsPeripheral  readValueForCharacteristic:aChar];
            
            }
        }
    
    }
    
}

- (void)peripheralDidUpdateRSSI:(CBPeripheral *)peripheral error:(NSError *)error {
    
    
    NSLog(@"Got RSSI update: %4.1f", [peripheral.RSSI doubleValue]);
    
    
}

- (void)peripheral:(CBPeripheral *)peripheral didReadRSSI:(NSNumber *)RSSI error:(NSError *)error {
    
    if(error){
        NSLog(@"RSSI Error: %@", error.localizedDescription);
        return;
    }
   
    [self finderProcess: [RSSI intValue]];
    
}

-(int)parserInt16: (const uint8_t*) bytes {
    
    return (bytes[0] & 0xFF) | ((bytes[1] & 0xFF) << 8);
}

-(void)stopSleepRecordUpload {
    isSleepRecordReadBypass=true;
}

-(void)insertToStack: (int)activityType withValue: (int)activityValue {
    ActivityRecord *theActivity=[ActivityRecord new];

    theActivity.activityType=[NSNumber numberWithInt: activityType];
    theActivity.activityValue=[NSNumber numberWithInt: activityValue];
    
    [mActivityStack addObject:theActivity];
    
}

-(BOOL)popFromStack: (int*)activityType withValue: (int*)activityValue {
    
    NSUInteger count=[mActivityStack count];
    
    if(count==0) return NO;
    
    ActivityRecord *theActivity;
    theActivity=[mActivityStack lastObject];
    [mActivityStack removeObjectAtIndex:count-1];
    
    *activityType=[theActivity.activityType intValue];
    *activityValue=[theActivity.activityValue intValue];
    
    return YES;
}

-(NSUInteger)countOfStack {
    return [mActivityStack count];
}

-(void)resetStack {
    return [mActivityStack removeAllObjects];
}

// Invoked when you retrieve a specified characteristic's value, or when the peripheral device notifies your app that the characteristic's value has changed.
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    
    // Updated value for heart rate measurement received
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:POLARH7_HRM_MEASUREMENT_CHARACTERISTIC_UUID]]) { // 1
        // Get the Heart Rate Monitor BPM
        [self getHeartBPMData:characteristic error:error];
    }
    // Retrieve the characteristic value for manufacturer name received
    if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:POLARH7_HRM_MANUFACTURER_NAME_CHARACTERISTIC_UUID]]) {  // 2
        [self getManufacturerName:characteristic];
    }
    // Retrieve the characteristic value for the body sensor location received
    else if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:POLARH7_HRM_BODY_LOCATION_CHARACTERISTIC_UUID]]) {  // 3
        [self getBodyLocation:characteristic];
    }
    
    else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_ACTIVITY_TIME_VALUE]]){

        NSLog(@"new time value");
        
        [self setSyncWaitTimer:10];
        
        if(binsReadPhase==BINS_CONNECTED_SYNCING){
            [self getActivityTimeValue:characteristic];
            
        } else if(binsReadPhase== BINS_CONNECTED_NOTIFYING){
            [self getNotifiedActivityTimeValue:characteristic];
            
        } else if(binsReadPhase== BINS_CONNECTED_PREPARING){
            
        }
    }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_SLEEP_START_TIME]]){
        int validBit;
        const uint8_t *bytes=[[characteristic value] bytes];
        
        int valueReceived=[self parserInt16: bytes];
        NSInteger currentTimeInMinute=[self localeTimeIntervalInMinute];
        mSleepRecordConnectTime=currentTimeInMinute;
        
        
        validBit= valueReceived & 0x8000;
        
        long sleepRecordTimeShift= ((~0x8000) & valueReceived)-1;
        NSLog(@"Sleep record shift time read:%ld", sleepRecordTimeShift);
        
        //-- 20151021 FIX BUG,
        //-- startTimeShift returned didn't consider the notifications occurred before.
        long lastSyncTime=[binsDataController getActiveDeviceLastSyncTime ];
        
        //-- 20151129
        //-- mSleepRecordStartTimeShift is not always correctly returned by wearable
        //-- we newly use device last sync time to keep track the status
        //-- when a device is newly paired to the user, we don't know its actual last sync time,
        //-- for this reason, reset the sync time when new pairing happens.
        //-- mSleepRecordStartTimeShift will keep track the actual time to save for the sleep record
        if(lastSyncTime==0){
            mSleepRecordNextTime=0;
            [self resetStack];  //-- 20151130
        }else{
            mSleepRecordNextTime=lastSyncTime;
        }
        
        if((mSleepRecordConnectTime-lastSyncTime)<sleepRecordTimeShift){    //-- 20151130
            mSleepRecordSessionTotal=(mSleepRecordConnectTime-lastSyncTime);
        }else{
            mSleepRecordSessionTotal=sleepRecordTimeShift;
        }
        mSleepRecordSessionSaved=0;
        
        isSleepRecordReadBypass=NO;
        
        NSLog(@"Sleep record valid:time=%d:%ld:%ld",validBit,mSleepRecordNextTime,(long)mSleepRecordConnectTime);
        
        
        //-- 150610 refresh sync view for sleep record
        //-- TODO mBinsFragment.syncViewSleepRecordCounter=0;
        
        if(validBit!=0){  // valid bit
            //-- 150610 intermediate update, to avoid not updated after cancel
            //-- 151103 will confuse sync [binsDataController setActiveDeviceLastSyncTime: [self localeTimeIntervalInMinute]];
            
            [binsPeripheral readValueForCharacteristic:binsCharacteristicSleepRecords];
            
        }else{
            NSLog(@"Read end, start write data ...");
            [binsDataController setActiveDeviceLastSyncTime: mSleepRecordConnectTime]; //-- 20151130
            [self writeSession];
        }
    

    }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_SLEEP_RECORDS]]){
      
        int i;
        int sleepRecord;
        
        const uint8_t *bytes=[[characteristic value] bytes];
        
        int activityType;
        NSInteger activityTime;
        int activityValue;
  
        if(binsReadPhase==BINS_CONNECTED_SYNCING)
        {
            unsigned long sleepRecordArraySize=[[characteristic value] length]-2;	// the first byte is the valid byte
        
            // 0412 bytes[0] is valid indicator byte
            for(i=0; i<sleepRecordArraySize; i=i+2){
                sleepRecord=(bytes[i+2] & 0xFF) + ((bytes[i+3] & 0xFF) << 8);
                
            
                if((sleepRecord & 0x8000)!=0){	// means ambient light record
                    activityType=BINS_ACTIVITY_TYPE_LIGHT;
                    activityTime=mSleepRecordNextTime;  //-- 20151129 reuse current time slot
                }else{
                    activityType=BINS_ACTIVITY_TYPE_SLEEP;
                    
                    //-- 20151129 find the next available time, and update pointer at the same time.
                    //-- to consider there may have some already notified to occupy time slots.
                    activityTime=[binsDataController nextAvailableSleepTime: (&mSleepRecordNextTime)];
                    
                    
                    //-- NSLog(@"Sleep record time: %ld, %ld",(long)activityTime, (long)mSleepRecordConnectTime);
                    
                    if(activityTime>0){
                        [binsDataController setActiveDeviceLastSyncTime: mSleepRecordNextTime]; //-- 20151129
                    }
                    
                    mSleepRecordSessionSaved++;
                    [self.delegate deviceController:self sleepRecordRead: mSleepRecordSessionSaved of: mSleepRecordSessionTotal];

                }
                
             
                activityValue = sleepRecord & 0x7FFF;
            
                //-- NSLog(@"Sleep record:%c:%c:%lu:%d", bytes[0],bytes[1],sleepRecordArraySize,mSleepRecordStartTimeShift);
            
                if(activityTime>0){
                    [binsDataController insertOrUpdateActivity:activityTime withType:activityType withValue:activityValue];
                }else{
                    //-- 20151129 put to stack, in the end, will store to database in reverse order along
                    //-- time
                    [self insertToStack: activityType withValue: activityValue];
                }
                    
            }
        
        
            if(bytes[0]!=0 && isSleepRecordReadBypass==false)
            {   // continue read from wearable
                [binsPeripheral readValueForCharacteristic:characteristic];
                
            }else{
                if(mSleepRecordNextTime==0){
                    long sleepRecordTime;
                    mSleepRecordNextTime=mSleepRecordConnectTime;
                    
                    mSleepRecordSessionTotal=[self countOfStack];
                    mSleepRecordSessionSaved=mSleepRecordSessionTotal;
                    
                    while([self popFromStack: &activityType withValue: &activityValue]==YES){
                        if(activityType==BINS_ACTIVITY_TYPE_SLEEP){
                            sleepRecordTime=[binsDataController previousAvailableSleepTime:(&mSleepRecordNextTime)];
                        }else{
                            sleepRecordTime=mSleepRecordNextTime;
                        }
                        [binsDataController insertOrUpdateActivity:sleepRecordTime withType:activityType withValue: activityValue];

                        
                        [self.delegate deviceController:self sleepRecordRead: mSleepRecordSessionSaved-- of: mSleepRecordSessionTotal];

                    }
                    

                }
                [binsDataController setActiveDeviceLastSyncTime: mSleepRecordConnectTime]; //-- 20151129

                NSLog(@"Sleep Record Ended and start write data");
                [self writeSession];
            }

        } else if(binsReadPhase== BINS_CONNECTED_NOTIFYING){
          
            sleepRecord=(bytes[0] & 0xFF) + ((bytes[1] & 0xFF) << 8);

            long mConnectedTime= [self localeTimeIntervalInMinute];
        
            if((sleepRecord & 0x8000)!=0)	// means ambient light record
                activityType=BINS_ACTIVITY_TYPE_LIGHT;
            else{
                activityType=BINS_ACTIVITY_TYPE_SLEEP;
            }
        
            activityTime= mConnectedTime;
        
            activityValue = sleepRecord & 0x7FFF;

        
            NSLog(@"Sleep record notified: ti:%ld ty:%d va:%d", activityTime, activityType, activityValue);
        
            [binsDataController insertOrUpdateActivity:activityTime withType:activityType withValue:activityValue];
            
            if(isSleepRecordReadBypass==NO){    //-- 20151130 change to update user sync time, 20151104
                 [binsDataController setActiveUserLastSyncTime: mConnectedTime];
            }

        }
        
        
    }else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_IBEACON_UUID]]){
        
        NSLog(@"Ibeacon uuid: %lu", (unsigned long)[[characteristic value] length]);
        
        const uint8_t *bytes=[[characteristic value] bytes];
        
        NSMutableString *strProxiID = [NSMutableString stringWithCapacity:16];

        int i;
        for(i=0; i<16; i++){
            //-- Log.i(TAG, "Ibeacon:"+i+":"+bytes[i]);
            switch (i)
            {
                case 3:
                case 5:
                case 7:
                case 9:[strProxiID appendFormat:@"%02x-", bytes[i]]; break;
                default:[strProxiID appendFormat:@"%02x", bytes[i]];
            }

        }
        NSLog(@"Ibeacon uuid string:%@",strProxiID); // 150502s
        
        [binsDataController setIbeaconUuid: strProxiID];
        
        [self setDeviceBeaconRegion];
        
    }else if([characteristic.UUID isEqual:[CBUUID  UUIDWithString:CHARACTERISTIC_CONFIGURATION]]){
        
        [self getConfiguration: characteristic];
        
        // [binsPeripheral readValueForCharacteristic:binsCharacteristicActivityTimeValue];
        
        
    }else if([characteristic.UUID isEqual:[CBUUID  UUIDWithString:CHARACTERISTIC_HEARTRATE_STREAM]]){
        
        [self getPulseStream:characteristic];
        
    } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_BATTERY]]){
        
        [self getBatteryLevel:characteristic];
        
    } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_DI_FW_REVISION]]){
        
        [self getFirmwareRevision: characteristic];
        

    } else if([characteristic.UUID isEqual:[CBUUID UUIDWithString:CHARACTERISTIC_DI_SW_REVISION]]){
        
    }
    
    
    /*
    // Add your constructed device information to your UITextView
    self.deviceInfo.text = [NSString stringWithFormat:@"%@\n%@\n%@\n", self.connected, self.bodyData, self.manufacturer];  // 4
     */
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    
    if (error) {
        NSLog(@"%@ : Error changing notification state: %@", characteristic, error.localizedDescription);
    }
    
    //[self getNotifiedActivityTimeValue:characteristic];
    NSLog(@"Notified happens");
    
    
    // Notification has started
    if (characteristic.isNotifying) {
        NSLog(@"Notification began on %@", characteristic);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error

{
    
    if(binsReadPhase!=BINS_CONNECTED_SYNCING){
        NSLog(@"wrote:");
        return;
    }
    
    if(characteristic==binsCharacteristicLocalClock){
        NSLog(@"wrote LOCAL CLOCK");
        mIsWroteLocalClock=true;
        
        // int dailyGoal=0x1230;


        // DataSyncFormat.bleSet16(bytes, mDailyGoal);
        uint16_t goal=[binsDataController getDailyGoal];
        NSData* goalData = [NSData dataWithBytes:(void*)&goal length:sizeof(goal)];
        
        [binsPeripheral writeValue:goalData forCharacteristic:binsCharacteristicGoal type:CBCharacteristicWriteWithResponse];
        
    }else if(characteristic==binsCharacteristicGoal){
        
        NSLog(@"wrote GOAL");
        mIsWroteGoal=true;
        
        uint32_t config=[binsDataController getActiveDeviceConfiguration];
        
        int shakeUiThresh=[binsDataController getActiveShakeUiThreshold];
       
        config |= BINS_CONFIG_ACTIVITY_NOTIFY_ENABLE_MASK; // 11.19, it must be set

        //-- 11.13
        config |= BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK;
        
        config=[self setShakeThresholdToConfig: shakeUiThresh fromConfig: config];
        
        // DataSyncFormat.bleSet16(bytes, deviceConfig);
        NSData* configData = [NSData dataWithBytes:(void*)&config length:sizeof(config)];
        
        [binsPeripheral writeValue:configData forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
        

    }else if(characteristic==binsCharacteristicConfiguration){
        NSLog(@"wrote CONFIGURATION");
        mIsWroteConfiguration=true;
        
        [self updateAlarm];
        
    }else if(characteristic==binsCharacteristicAlarmClock){
        NSLog(@"wrote Alarm Clock");
        mIsWroteAlarmClock=true;
        
    }
    
    
    if(mIsWroteLocalClock==true &&
       mIsWroteGoal==true &&
       mIsWroteConfiguration==true && mIsWroteAlarmClock)
    {
        NSLog(@"end of writes");
        [self endSyncNotify];
    }
}

#pragma mark - CBCharacteristic helpers

-(int)parserConfiguration: (const uint8_t*) bytes {
    
    return (bytes[0] & 0xFF) | ((bytes[1] & 0xFF) << 8) |
    ((bytes[2] & 0xFF) << 16)	| ((bytes[3] & 0xFF) << 24);
}


-(int)parserActivityTimeValue: (const uint8_t*) bytes {
    
    return (bytes[0] & 0xFF) | ((bytes[1] & 0xFF) << 8) |
        ((bytes[2] & 0xFF) << 16)	| ((bytes[3] & 0xFF) << 24);
}


-(NSInteger)localeTimeIntervalInMinute
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate * now = [NSDate date];
    
    NSDateComponents *dayComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now];
    
    dayComponents.second=0; // minute boundary
    
    now=[calendar dateFromComponents:dayComponents];
    
    NSInteger interval = [now timeIntervalSince1970];
    
    NSInteger intervalMinute = interval / 60;
    
    return intervalMinute;
}

-(void)getNotifiedActivityTimeValue: (CBCharacteristic*)characteristic {
    
    //-- NSLog(@"Notified activity");
    
    const uint8_t *bytes=[[characteristic value] bytes];
    
    int readValue = [self parserActivityTimeValue: bytes];
    
    int stepCount=(int)(readValue & 0x0000FFFF);
    
    int versionMajor, versionMinor;
    [binsDataController getActiveDeviceVersion:&versionMajor withMinor: &versionMinor];
    
    int activityType;
    NSInteger activityTime;
    int activityValue;
    
    NSInteger currentTimeInMinute=[self localeTimeIntervalInMinute];
    
    int stepMinute;
    
    if(versionMajor<=2){
        stepMinute=((readValue & 0xFFFF0000) >> 16);
        activityType=BINS_ACTIVITY_TYPE_STEPS;
        activityTime=currentTimeInMinute - stepMinute;
    }else{
        stepMinute=((readValue & 0x7FFF0000) >> 16);
        if((readValue & 0x80000000)!=0)
            activityType=BINS_ACTIVITY_TYPE_SLEEP;
        else
            activityType=BINS_ACTIVITY_TYPE_STEPS;
        activityTime=currentTimeInMinute - stepMinute;
    }
    
    //-- NSLog(@"Notified Time Activity- ty:%d ti:%ld ac:%d",activityType,(long)activityTime, activityValue);

    if(activityType==BINS_ACTIVITY_TYPE_STEPS){
        int previousActivityValue;
        [binsDataController getActivityValue: activityTime withType: activityType withValue:(&previousActivityValue)];
        
        NSLog(@"Notified ti:%ld ac:%d, pre:%d",(long)activityTime, activityValue, previousActivityValue);
        
        
        if([binsDataController getActiveDeviceType]==BINS_MODEL_BINSX1 &&
            (versionMajor<=2 && versionMinor<=3))
        {
            //-- 07.28
            activityValue = previousActivityValue+1;
                //-- that should be upgraded later from 0409 version
        } else {
            activityValue = previousActivityValue+stepCount;
        }
        
    } else { // not type 1
        activityValue = stepCount;	//-- 09.13 just overwrite
    }
    
    [binsDataController insertOrUpdateActivity:activityTime withType:activityType withValue:activityValue];

    [self.delegate deviceController:self stepsNotified: activityTime withNewSteps:stepCount];
    
}

-(void)getFirmwareRevision: (CBCharacteristic*)characteristic {

    const uint8_t *bytes=[[characteristic value] bytes];
    
    NSString *strRevision=[[NSString alloc] initWithBytes:bytes length:[[characteristic value] length] encoding:NSUTF8StringEncoding];
    
    int major=[[strRevision substringWithRange:NSMakeRange(16, 1)] intValue];
    int minor=[[strRevision substringWithRange:NSMakeRange(18, 1)] intValue];
    
    [binsDataController setActiveDeviceVersion: major withMinor: minor];

    
    // substringToIndex: 6
}
-(void)getConfiguration: (CBCharacteristic*)characteristic {
    
    const uint8_t *bytes=[[characteristic value] bytes];
    
    uint32_t config = [self parserConfiguration: bytes];
    
    int shakeUiThreshold=[self getShakeThresholdFromConfig:config];
    
    // NSLog(@"new shake UI:%d",shakeUiThreshold);
    
    [binsDataController setActiveDeviceConfiguration:config];
    
    if([binsDataController isDeviceFirstTimeSync]==NO){
        [binsDataController setActiveShakeUiThreshold:shakeUiThreshold];
    }
    
}

-(void)getActivityTimeValue: (CBCharacteristic*)characteristic {
    
    const uint8_t *bytes=[[characteristic value] bytes];
    
    int readValue = [self parserActivityTimeValue: bytes];
    int stepCount=(int)(readValue & 0x0000FFFF);
    
    int versionMajor, versionMinor;
    [binsDataController getActiveDeviceVersion:&versionMajor withMinor: &versionMinor];
    
    int activityType;
    NSInteger activityTime;
    int activityValue;
    
    NSInteger currentTimeInMinute=[self localeTimeIntervalInMinute];
    
    int stepMinute;
    
    if(versionMajor<=2){
        stepMinute=((readValue & 0xFFFF0000) >> 16);
        activityType=BINS_ACTIVITY_TYPE_STEPS;
        activityTime=currentTimeInMinute - stepMinute;
    }else{
        stepMinute=((readValue & 0x7FFF0000) >> 16);

        if((readValue & 0x80000000)!=0)
            activityType=BINS_ACTIVITY_TYPE_SLEEP;
        else
            activityType=BINS_ACTIVITY_TYPE_STEPS;
        
        activityTime=currentTimeInMinute - stepMinute ;
    }
    
    int previousActivityValue;
    [binsDataController getActivityValue: activityTime withType: activityType withValue:(&previousActivityValue)];

    
    if(activityType==BINS_ACTIVITY_TYPE_STEPS){
        activityValue = previousActivityValue+stepCount;
        
    } else { // not type 1
        activityValue = stepCount;	//-- 09.13 just overwrite
    }
    
    NSLog(@"Read Time Activity- ty:%d ti:%ld ac:%d ex:%d",activityType,(long)activityTime, activityValue, previousActivityValue);
   
    if (stepCount != 0 || activityType!=BINS_ACTIVITY_TYPE_STEPS ) {	//-- 09.13
        
        [binsDataController insertOrUpdateActivity:activityTime withType:activityType withValue:activityValue];
        
        if(activityType==1){  //-- 09.17
            mTotalSteps += stepCount;
        }
        
        binsTotalReadCountsSession++;
        
        [self.delegate deviceController:self newActivityRead:binsTotalReadCountsSession];

        // continue read
        [binsPeripheral readValueForCharacteristic:characteristic];
        
    } else {
        /* 20151108
        if(deviceCommand==DEVICE_COMMAND_SYNC_BACKGROUND){
            [self endSyncNotify];
        }
        else
        */
        {
            //-- 150629
            if(binsCharacteristicSleepStartTime!=nil){
                [binsPeripheral readValueForCharacteristic:binsCharacteristicSleepStartTime];
            }else{  // if it is an old version wearable
                NSLog(@"Read end, start write data ...");
                [self writeSession];
            }
        }
    }
    
  
}
-(void)writeSession {
    
    NSLog(@"writeConfiguration ...");
    
    mIsWroteLocalClock=false;
    mIsWroteGoal=false;
    mIsWroteConfiguration=false;
    
    if(binsCharacteristicAlarmClock==nil) mIsWroteAlarmClock=YES;
    else mIsWroteAlarmClock=NO;
    
    
    NSDate *currentDate=[NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *currentDateComponents = [calendar components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:currentDate];
    
    long hour=currentDateComponents.hour;
    long minute=currentDateComponents.minute;
    
    long highV= (hour <<8);
    long localClock= (highV + minute);
    
    NSLog(@"calendar time:%ld:%ld=%ld",hour,minute,localClock);

    uint32_t val = (uint32_t)localClock;
    NSData* localClockData = [NSData dataWithBytes:(void*)&val length:sizeof(val)];

    [binsPeripheral writeValue:localClockData forCharacteristic:binsCharacteristicLocalClock type:CBCharacteristicWriteWithResponse];
}

-(void)deviceConnectionClose {
    
    
    [self timerTaskSwitchFromConnectToDisconnect]; // 12.06
   
    [self.delegate deviceController: self stateChanged:BINS_STATE_DISCONNECTED];

    
    deviceCommand=DEVICE_COMMAND_IDLE;

    if(finderTimer!=nil){
        [self stopFinder];
        // return;
    }
    
    if([self isActiveDeviceConnected]){
        [bluetoothManager cancelPeripheralConnection:binsPeripheral];
    }
    
}

-(void)endSyncNotify {
    
    NSLog(@"endSyncNotify");
    
    [self stopSyncWaitTimer];
    
    
    if(deviceCommand!=DEVICE_COMMAND_SYNC_BACKGROUND){ // 12.04
        //-- chance to set finder;
        [self performSelector:@selector(resetFinderStatus) withObject:nil afterDelay:3];
        [self setFinderTimer];
    }

    NSInteger lastSyncTime=0;
    if(deviceCommand!=DEVICE_COMMAND_CONNECT_ONLY){ // 150713
    
        //-- always reset once after sync completed
        // mCountFromLastSync=0; // complete background sync successful, reset the counter
    
        lastSyncTime= [binsDataController getActiveDeviceLastSyncTime];
    
        //-- 20151103 no need
        //-- 20151129 set user's sync time, different to device sync time.
        [binsDataController setActiveUserLastSyncTime: [self localeTimeIntervalInMinute]];
    }
    
    [self.delegate deviceController: self stateChanged:BINS_STATE_SYNC_FINISHED];
    
    [binsDataController uploadTask];
    
    if(deviceCommand==DEVICE_COMMAND_SYNC_BACKGROUND){
        
        //if([[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground)
        
        if(_backgroundHandlerBlock!=nil){
            _backgroundHandlerBlock();  // 11.25
        }
        
        [self deviceConnectionClose];

        return;
    }

    binsReadPhase=BINS_CONNECTED_NOTIFYING;

    
    [binsPeripheral setNotifyValue:YES forCharacteristic:binsCharacteristicActivityTimeValue];
    
    if(binsCharacteristicSleepRecords!=nil){
        [binsPeripheral setNotifyValue:YES forCharacteristic:binsCharacteristicSleepRecords];
    }

    //-- heart rate
    // 09.04
    if(binsCharacteristicPulseStream!=nil){
        [binsPeripheral setNotifyValue:YES forCharacteristic:binsCharacteristicPulseStream];

        [binsDataController setActiveDeviceType: BINS_MODEL_BINSX2];
    }
    else{
        [binsDataController setActiveDeviceType: BINS_MODEL_BINSX1];
    }
    
    if(deviceCommand!=DEVICE_COMMAND_CONNECT_ONLY){
        [binsPeripheral setNotifyValue:YES forCharacteristic:binsCharacteristicBattery];
        
        [binsPeripheral readValueForCharacteristic:binsCharacteristicBattery];

        if(lastSyncTime!=0){
            NSLog(@"Read configuration");
            [binsPeripheral readValueForCharacteristic:binsCharacteristicConfiguration];
        }
    
        [binsPeripheral readValueForCharacteristic:binsCharacteristicSoftwareRevision];

        // 150710
        if(binsCharacteristicBeaconId!=nil){
            NSLog(@"Trying to read beacon ID");
            
            [binsPeripheral readValueForCharacteristic:binsCharacteristicBeaconId];
        }else{
            NSLog(@"IbeaconUUID characteristic is not found.");
        }
    }
    
    [self setSleepCycleTime];  // TODO 150716 need to confirm
    
    if(deviceCommand!=DEVICE_COMMAND_SYNC_ACTIVE){
        deviceCommand=DEVICE_COMMAND_IDLE;
        return;
    }
    
    // openHeartrate();
    
    hasActivityNotified=false;
    
    //-- 150712 no need ? [self timerTaskSwitchFromDisconnectToConnect];   // 12.06
    
    deviceCommand=DEVICE_COMMAND_IDLE;
    return;

 }


-(void)timerTaskStop {
    if(mCheckNotificationTimer!=nil){
        [mCheckNotificationTimer invalidate];
        mCheckNotificationTimer=nil;
    }
    if(mCheckPeriodicTimer!=nil){
        [mCheckPeriodicTimer invalidate];
        mCheckPeriodicTimer=nil;
    }
}

-(void)timerTaskUpdate {
    
    if(mCheckNotificationTimer!=nil) return;
    
    /* 150713 no need anymore
    if([binsDataController getBackgroundSync]==1 && mCheckPeriodicTimer==nil){
        mCountFromLastSync=0;
        mCheckPeriodicTimer= [NSTimer scheduledTimerWithTimeInterval:CHECK_PERIOD_INTERVAL target:self selector:@selector(mCheckPeriodicTimerRoutine) userInfo:nil repeats:YES];
        
    } else if([binsDataController getBackgroundSync]==0 && mCheckPeriodicTimer!=nil){
        [mCheckPeriodicTimer invalidate];
        mCheckPeriodicTimer=nil;
    }
     */
}

-(void)timerTaskSwitchFromConnectToDisconnect {
    if(mCheckNotificationTimer!=nil){
        [mCheckNotificationTimer invalidate];
        mCheckNotificationTimer=nil;
    }

    /* 150713 no need
    if([binsDataController getBackgroundSync]==1){
        mCountFromLastSync=0;
        if(mCheckPeriodicTimer==nil){
            mCheckPeriodicTimer= [NSTimer scheduledTimerWithTimeInterval:CHECK_PERIOD_INTERVAL target:self selector:@selector(mCheckPeriodicTimerRoutine) userInfo:nil repeats:YES];
        }
    } else {
        if(mCheckPeriodicTimer!=nil){
            [mCheckPeriodicTimer invalidate];
            mCheckPeriodicTimer=nil;
        }
    }
     */
}


-(void)timerTaskSwitchFromDisconnectToConnect {
    
    if(mCheckPeriodicTimer!=nil){
        [mCheckPeriodicTimer invalidate];
        mCheckPeriodicTimer=nil;
    }
    
    if(mCheckNotificationTimer!=nil){
        NSLog(@"Something wrong the noti timer is not nil");
    }
    mCheckNotificationTimer= [NSTimer scheduledTimerWithTimeInterval:CHECK_IDLE_INTERVAL target:self selector:@selector(mCheckNotificationTimerRoutine) userInfo:nil repeats:YES];
}



-(void) mCheckPeriodicTimerRoutine {
    NSLog(@"Sync Timer");
        
    if(binsPeripheral.state==CBPeripheralStateConnected){
        mCountFromLastSync=0;	//-- reset it
        return;
    }
        
    //-- Log.i(TAG, "Sync Timer count :"+mCountFromLastSync+":"+mIsPeriodicScanning+":"+mConnectionState);
        
    if(mCountFromLastSync>=5)
    {       // means 5 * 90 seconds = 7-8 minutes
        NSLog(@"start periodic sync");
        [self backgroundSyncAndDisconnect];
        mCountFromLastSync=0;
    }
    // else
    {
        mCountFromLastSync++;
    }
}


-(void) mCheckNotificationTimerRoutine {

    mActivityCheckCounter++;
    
    if(hasActivityNotified==YES){
        NSLog(@"Noti Timer");

        hasActivityNotified=NO; // reset and go next
        mActivityCheckCounter=0;
        
    }else{
        NSLog(@"Noti Timer counting:%d",mActivityCheckCounter);
    }
    
    if(mActivityCheckCounter>=(IDLE_TIME_DISCONNECT/CHECK_IDLE_INTERVAL)){  //--10,  5 is enough, 10 minutes
        
        // stop timer as early as possible
        [mCheckNotificationTimer invalidate];
        mCheckNotificationTimer=nil;
                
        [binsPeripheral setNotifyValue:NO forCharacteristic:binsCharacteristicActivityTimeValue];
        
        if(binsCharacteristicPulseStream!=nil){
            [binsPeripheral setNotifyValue:NO forCharacteristic:binsCharacteristicPulseStream];
        }
        
        [self deviceConnectionClose];

    }
    else{
        NSLog(@"Status %ld", (long)binsPeripheral.state);
        
        if(binsPeripheral.state==CBPeripheralStateDisconnected){
            if(bluetoothManager!=nil){
                [bluetoothManager connectPeripheral:binsPeripheral options:nil];
            }
        }
    }
}

-(void) backgroundSyncAndDisconnect: (CompletionBlock)handler {
    _backgroundHandlerBlock = [handler copy];
    [self backgroundSyncAndDisconnectInside];
}

-(void) backgroundSyncAndDisconnect {
    _backgroundHandlerBlock=nil;
    [self backgroundSyncAndDisconnectInside];
}

-(void) backgroundSyncAndDisconnectInside {
    
    if(deviceCommand!=DEVICE_COMMAND_IDLE){
        NSLog(@"Sync function is busy, abort background");
        return;
    }
    
    if(binsPeripheral.state==CBPeripheralStateDisconnected)
    {
        
        deviceCommand=DEVICE_COMMAND_SYNC_BACKGROUND;
        
        [self deviceConnectSyncStart];
    }
}


-(void)getPulseStream: (CBCharacteristic*)characteristic {
  
    // NSLog(@"get Pulse Notified");
    
    static uint8_t pulseRawDataStream[25];
    
    const uint8_t *byte=[[characteristic value] bytes];
    NSInteger bytes_count=[[characteristic value] length];
    
    if(bytes_count>20){
        NSLog(@"Wrong with heart rate stream data length");
        
    }
    for(NSInteger i=0; i<bytes_count; i++){
        pulseRawDataStream[i]=byte[i];
    }
    
    [pulseController newDataStream: pulseRawDataStream];
    
}

-(void)getBatteryLevel: (CBCharacteristic*)characteristic {
    const uint8_t *byte=[[characteristic value] bytes];
    
    int batteryLevel=byte[0];
    
    [binsDataController setActiveDeviceBatteryLevel: batteryLevel];
    
}

// Instance method to get the heart rate BPM information
- (void) getHeartBPMData:(CBCharacteristic *)characteristic error:(NSError *)error
{
    // Get the Heart Rate Monitor BPM
    NSData *data = [characteristic value];      // 1
    const uint8_t *reportData = [data bytes];
    uint16_t bpm = 0;
    
    if ((reportData[0] & 0x01) == 0) {          // 2
        // Retrieve the BPM value for the Heart Rate Monitor
        bpm = reportData[1];
    }
    else {
        bpm = CFSwapInt16LittleToHost(*(uint16_t *)(&reportData[1]));  // 3
    }
    // Display the heart rate value to the UI if no error occurred
    if( (characteristic.value)  || !error ) {   // 4
 /*
        self.heartRate = bpm;
        self.heartRateBPM.text = [NSString stringWithFormat:@"%i bpm", bpm];
        self.heartRateBPM.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:28];
        [self doHeartBeat];
        self.pulseTimer = [NSTimer scheduledTimerWithTimeInterval:(60. / self.heartRate) target:self selector:@selector(doHeartBeat) userInfo:nil repeats:NO];
  */
    }
    return;
    
}

// Instance method to get the manufacturer name of the device
- (void) getManufacturerName:(CBCharacteristic *)characteristic
{
}
// Instance method to get the body location of the device
- (void) getBodyLocation:(CBCharacteristic *)characteristic
{
}
// Helper method to perform a heartbeat animation
- (void)doHeartBeat {
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reminder"
                                                        message:notification.alertBody
                                                       delegate:self cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}

-(void)startSync {
    int deviceID=[binsDataController getActiveDeviceID];
    if(deviceID!=0){
        [self startSync:deviceID];
    }
}


-(BOOL)startSync: (int) deviceIdentifier {
    
    if(deviceCommand!=DEVICE_COMMAND_IDLE){
        NSLog(@"Sync: command resource not available");
        return NO;
    }
    deviceCommand=DEVICE_COMMAND_SYNC_ACTIVE;

    [self deviceConnectSyncStart];
    
    return YES;
}

-(BOOL)connectOnly {    // 150712

    if(deviceCommand!=DEVICE_COMMAND_IDLE){
        return NO;
    }
    
    deviceCommand=DEVICE_COMMAND_CONNECT_ONLY;

    [self deviceConnectSyncStart];
    
    return YES;
}

+(bool)isActiveDeviceConnectedStatic {
    
    return activeDeviceConnected;
    
    //-- 20170121
}

-(bool)isActiveDeviceConnected {
    
    return [self isDeviceConnected:[binsDataController getActiveDeviceID]];
            
}

-(bool)isDeviceConnected: (int)deviceID {
    
    if(binsPeripheral.state!=CBPeripheralStateConnected)
        return NO;
    
    return YES;
}

-(void) rssiRecordReset
{
    int i;
    for(i=0; i<BINS_FINDER_TRACK_MAX; i++)
        rssiRecord[i]=0;
    rssiRecordHead=0;
}

-(void)finderProcess:(int)rssi {
    bool isOn;
    int thresholdLevel;
    
    [binsDataController getFinderThreshold:&isOn hasThreshold:&thresholdLevel];

    int finderRssiThresh= (int)((float)BINS_FINDER_RSSI_MAX-(thresholdLevel)*BINS_FINDER_RSSI_STEP);
    

    //-- UIApplication *app = [UIApplication sharedApplication];
    
    NSLog(@"finderProcess:%d:%d",rssi, finderRssiThresh); //-- , app.backgroundTimeRemaining);

    
    rssiRecord[rssiRecordHead]=rssi;
    
    rssiRecordHead++;
    if(rssiRecordHead>=BINS_FINDER_TRACK_MAX) rssiRecordHead=0;
    
    
    Boolean isTimeToClear=true; // flag to say, time to clear the alert status and back to normal, because we observed strong signal in the window
    int signalWeakCount=0;  // number of times in last window, for the weak signals
    for(int i=0; i<BINS_FINDER_TRACK_MAX; i++){
        if(rssiRecord[i]<finderRssiThresh ){
            signalWeakCount++;
            isTimeToClear=false;
        }
    }
    
    if(rssi<finderRssiThresh  && signalWeakCount>2)
    {
        if(finderStatus!=BINS_FINDER_STATUS_ALERT){
            finderStatus=BINS_FINDER_STATUS_ALERT;
        }

        [notificationController startAlertSound];
        
        [self writeAlert:BINS_ALERT_FINDER_MASK withCommand:BINS_ALERT_COMMAND_SET];
        
        mLinklossInformed++;
        

    }
    /* 12.19 too sensitive
    else if(rssi<(finderRssiThresh+8) &&
            rssi>=finderRssiThresh )
    {
        //-- only flash LED
        
        
        [self writeAlert:BINS_ALERT_FINDER_MASK withCommand:BINS_ALERT_COMMAND_SET];

        mLinklossInformed++;
    }
    */
    
    else if(rssi>=103) 	// almost disconnected then, alert LED anyway
    {
         //-- only flash LED
     
         [self writeAlert:BINS_ALERT_FINDER_MASK withCommand:BINS_ALERT_COMMAND_SET];
     
         mLinklossInformed++;
    }
    else {	//-- 09.23 series of good signal level, then
        
        
        [notificationController stopAlertSound];

        
        if(mLinklossInformed>0 && isTimeToClear==true)
        {
            mLinklossInformed--;
            
            [self writeAlert:BINS_ALERT_FINDER_MASK withCommand:BINS_ALERT_COMMAND_RESET];
            
            if(mLinklossInformed==0 && finderStatus!=BINS_FINDER_STATUS_NORMAL){
                finderStatus=BINS_FINDER_STATUS_NORMAL;
            }
        }

    }

    
    
    [self.delegate deviceController:self finderStatusChanged: finderStatus];
    
}

-(void)resetFinderStatus {
    
    finderStatus=BINS_FINDER_STATUS_NORMAL;

    [notificationController stopAlertSound];

    [self writeAlert:BINS_ALERT_FINDER_MASK withCommand:BINS_ALERT_COMMAND_RESET];
}
    
-(void)writeAlert:(int)alertType withCommand:(int)command {

    if([self isActiveDeviceConnected]==NO) return;
    
    uint8_t alert=alertType | command;
    
    NSData* data = [NSData dataWithBytes:(void*)&alert length:sizeof(alert)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicLinkLoss type:CBCharacteristicWriteWithResponse];
    
}

static int finderCountFromLastStart=0;

-(void)setFinderTimer {
    bool isOn;
    int threshold;
    
    [binsDataController getFinderThreshold:&isOn hasThreshold:&threshold];
    
    if(isOn==NO){
        if(finderTimer!=nil){
            [finderTimer invalidate];
            finderTimer=nil;
        }
        return;
    }
    
    
    
    // YES

    if(finderTimer!=nil) return;

    finderTimer= [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(finderTimerRoutine) userInfo:nil repeats:YES];
    
    finderCountFromLastStart=0;
    finderStatus=BINS_FINDER_STATUS_NORMAL;
    [self.delegate deviceController:self finderStatusChanged:finderStatus];
    
    
}


-(void)finderTimerRoutine {
    
    finderCountFromLastStart++;
    if(finderCountFromLastStart%30==0){
        //-- 12.12 in order to maintain background running
        [binsPeripheral readValueForCharacteristic:binsCharacteristicBattery];
    }
    
    if([self isActiveDeviceConnected]){

        binsPeripheral.delegate=self;
        [binsPeripheral readRSSI];  // receive from callback
        
        //-- [notificationController startAlertSound]; // test
    }
    else{
        finderStatus=BINS_FINDER_STATUS_ALERT;
        
        [self.delegate deviceController:self finderStatusChanged:finderStatus];  // disconnected, must alert first
        
        //-- 12.11 alert first
        [notificationController startAlertSound]; // test

    }
    
    hasActivityNotified=YES; // avoid disconnect
}

-(void)startFinder {
    
    if(deviceCommand!=DEVICE_COMMAND_IDLE){
     
        NSLog(@"Finder: command resource not available");
        return;
    }
    
    finderStatus=BINS_FINDER_STATUS_NOT_ACTIVE;
    
    int currentDevice=[binsDataController getActiveDeviceID];
    
    if([self isDeviceConnected:currentDevice]){
        [self resetFinderStatus];

        [self setFinderTimer];
        deviceCommand=DEVICE_COMMAND_IDLE;
        return;
    }

    // deviceCommand=DEVICE_COMMAND_FINDER_ACTIVE;
    // not activate [self deviceConnectSyncStart];
    
}
    
-(void)deviceConnectSyncStart    {
    
    /*
     NSArray *services = @[[CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING],[CBUUID UUIDWithString:SERVICE_DEVICE_INFORMATION],[CBUUID UUIDWithString:SERVICE_BATTERY]];
     */

    NSLog(@"deviceConnectSyncStart");
    
    [self setSyncWaitTimer:15];  // 150712
    binsReadPhase=BINS_CONNECTED_SYNCING;
    
    binsTotalReadCountsSession=0;
    mTotalSteps=0;
    
    binsCharacteristicPulseStream=nil; // reset to detect the machine model later in scan.
    
    //-- services need to be advertised, in order to make services assigned filtered works.
    
    //-- [bluetoothManager scanForPeripheralsWithServices:nil options:nil];

    [bluetoothManager scanForPeripheralsWithServices:[NSArray arrayWithObjects:
                        [CBUUID UUIDWithString:@"1809"],
                        [CBUUID UUIDWithString:SERVICE_ACTIVITY_TRACKING],
                        [CBUUID UUIDWithString:@"180F"],
                        [CBUUID UUIDWithString:@"180A"],
                        [CBUUID UUIDWithString:@"00001016-D102-11E1-9B23-00025B00A5A5"], nil]
                                options:@{ CBCentralManagerScanOptionAllowDuplicatesKey: @YES }];
    
}

-(void)stopSync: (int)deviceID {
    
    [self deviceConnectionClose];
}


-(void)stopFinder {
    
    
    if(finderTimer!=nil){
        [finderTimer invalidate];
        finderTimer=nil;
    }
    
    [self resetFinderStatus];
    
    finderStatus=BINS_FINDER_STATUS_NOT_ACTIVE;
    [self.delegate deviceController:self finderStatusChanged:finderStatus];
    

    deviceCommand=DEVICE_COMMAND_IDLE;
    
    if(mCheckNotificationTimer!=nil) return;
    
/* NOT activate
    if([self isActiveDeviceConnected]){
        [bluetoothManager cancelPeripheralConnection:binsPeripheral];
    }

    */
}

-(void)checkFinder {
    bool enable;
    int threshold;
    [binsDataController getFinderThreshold:&enable hasThreshold:&threshold ];
    if(enable==YES){
        [self startFinder];
    }
    else{
        [self stopFinder];
    }
}

-(bool)finderIsRunning {
    
    if(finderTimer!=nil) return YES;
    
    return NO;
}

-(void)setSyncWaitTimer: (int)seconds {
    
    [self stopSyncWaitTimer];
    
    syncTimeoutTimer= [NSTimer scheduledTimerWithTimeInterval:seconds target:self selector:@selector(syncTimeoutTimerRoutine) userInfo:nil repeats:NO];

}

-(void)stopSyncWaitTimer {
    if(syncTimeoutTimer!=nil){
        [syncTimeoutTimer invalidate];
        syncTimeoutTimer=nil;
    }
}

-(void)syncTimeoutTimerRoutine {
     [self syncReportError: @"Timeout error"];
}
-(void) syncReportError:(NSString*)err {
    NSLog(@"Error:%@",err);
    
    if(bluetoothManager!=nil){
        [bluetoothManager stopScan];
    }

    deviceCommand=DEVICE_COMMAND_IDLE;
    [self.delegate deviceController:self errorFound:err];
    
}


-(void)pulseUpdate:(int)pulse {
    
    if(pulse>0xFF) pulse=0xFF;
    
    unsigned char val=pulse;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicPulseUpdate type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;

}

-(void)noticeForConnection {
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    UIView *topView = window.rootViewController.view;
    
    [topView makeToast:NSLocalizedString(@"suggest_connect",nil)
                duration:2.0
                position:CSToastPositionCenter];
}

-(void)updateAlarm {
    if([self isActiveDeviceConnected]==NO){
        
        [self noticeForConnection];
        
        [self backgroundSyncAndDisconnect: ^() {
            uint16_t alarm=[binsDataController getAlarmTime];
            NSData* alarmData = [NSData dataWithBytes:(void*)&alarm length:sizeof(alarm)];
            
            [binsPeripheral writeValue:alarmData forCharacteristic:binsCharacteristicAlarmClock type:CBCharacteristicWriteWithResponse];
            
        }]; // 150716
        
    }
    
    uint16_t alarm=[binsDataController getAlarmTime];
    NSData* alarmData = [NSData dataWithBytes:(void*)&alarm length:sizeof(alarm)];
    
    [binsPeripheral writeValue:alarmData forCharacteristic:binsCharacteristicAlarmClock type:CBCharacteristicWriteWithResponse];
    
}

-(void)updateGoal {
    if([self isActiveDeviceConnected]==NO){
        [self noticeForConnection];
        [self backgroundSyncAndDisconnect: ^() {
            uint16_t goal=[binsDataController getDailyGoal];
            NSData* goalData = [NSData dataWithBytes:(void*)&goal length:sizeof(goal)];
            
            [binsPeripheral writeValue:goalData forCharacteristic:binsCharacteristicGoal type:CBCharacteristicWriteWithResponse];

        }]; // 12.25
        
    }
    
    uint16_t goal=[binsDataController getDailyGoal];
    NSData* goalData = [NSData dataWithBytes:(void*)&goal length:sizeof(goal)];
    
    [binsPeripheral writeValue:goalData forCharacteristic:binsCharacteristicGoal type:CBCharacteristicWriteWithResponse];

}

-(void)updateShakeUiThreshold {
    
    if([self isActiveDeviceConnected]==NO){
        [self backgroundSyncAndDisconnect: ^() {
            int uiThresh=[binsDataController getActiveShakeUiThreshold];
            int config=[binsDataController getActiveDeviceConfiguration];
            int outConfig=[self setShakeThresholdToConfig:uiThresh fromConfig:config];
            
            // DataSyncFormat.bleSet16(bytes, localClock);
            uint32_t val = outConfig;
            NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
            
            [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
        }]; // 12.25
    }
    
    
    int uiThresh=[binsDataController getActiveShakeUiThreshold];
    int config=[binsDataController getActiveDeviceConfiguration];
    
    int outConfig=[self setShakeThresholdToConfig:uiThresh fromConfig:config];
    
    // DataSyncFormat.bleSet16(bytes, localClock);
    uint32_t val = outConfig;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
}

// 10.11
-(int) getShakeThresholdFromConfig:(int) config
{
    int threshValue=0;
    
    if(config & 0x8000) threshValue |= 0x4;
    if(config & 0x0800) threshValue |= 0x2;
    if(config & 0x0010) threshValue |= 0x1;
    
    return threshValue;
}

// BITS 15, 11, 4

-(int)setShakeThresholdToConfig:(int) threshValue fromConfig: (int)configOrigin
{
    int config=configOrigin;
    
    if(threshValue & 0x04) config |= 0x8000; else config &= (~0x8000);
    if(threshValue & 0x02) config |= 0x0800; else config &= (~0x0800);
    if(threshValue & 0x01) config |= 0x0010; else config &= (~0x0010);
    
    return config;
}


//-----------------------  device control routines

-(void) startHeartrateDetection {

    if([self isActiveDeviceConnected]==NO) return;
    
    uint16_t goal=[binsDataController getDailyGoal];
    NSData* goalData = [NSData dataWithBytes:(void*)&goal length:sizeof(goal)];
    
    [binsPeripheral writeValue:goalData forCharacteristic:binsCharacteristicGoal type:CBCharacteristicWriteWithResponse];

    
    int tmpConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    tmpConfiguration &= 0xFFF0;
    
    tmpConfiguration|= BINS_CONFIG_COMMAND_HRD_START_MASK; // mandatory function

    uint16_t val = tmpConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];

    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
}

BOOL hasAlertMessage=NO;

-(void) setAlertMessage
{
    if([self isActiveDeviceConnected]==NO){	// connect-set-and disconnect
        hasAlertMessage=true;				// set flag first
        [self backgroundSyncAndDisconnect];   	// and connect background to set
        return;
    }
    
    unsigned char val=(BINS_ALERT_MESSAGE_MASK | BINS_ALERT_COMMAND_SET); //-- 09.22 enable (0x01);
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicLinkLoss type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
}


-(void) setBinsDailyStepReset
{
    if([self isActiveDeviceConnected]==NO) return;
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    gConfiguration |= (BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK);
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
}

-(void) setBinsLock
{
    if([self isActiveDeviceConnected]==NO) return;

    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    gConfiguration |= (BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK);
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
}
-(void) setBinsX2DisplayBrightness {
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK )==0){
        gConfiguration |= BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK;
    }else{
        gConfiguration &= (~(BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    // Only continue if connected already
    if([self isActiveDeviceConnected]==NO){
        [self noticeForConnection]; // 20151107
        return;
    }
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;

}

-(void) setPhoneNotice {
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK)==0){
        gConfiguration |= BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK;
    }else{
        gConfiguration &= (~(BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    // Only continue if connected already
    if([self isActiveDeviceConnected]==NO){
       [self noticeForConnection]; // 20151107
       return;
    }
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
    
}

-(void) updateSleepTrackEnable {
    
    if([self isActiveDeviceConnected]==NO){
        
        [self noticeForConnection]; // 20151107
        
        [self backgroundSyncAndDisconnect: ^() {
            int gConfiguration=[binsDataController getActiveDeviceConfiguration];
            gConfiguration &= 0xFFF0;
            uint16_t val = gConfiguration;
            NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
            
            [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
            NSLog(@"sleep setting done");
        }]; // 12.25
        
    }
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
    
}


-(void) toggleSleepTrackEnable {
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK )==0){
        gConfiguration |= BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK;
    }else{
        gConfiguration &= (~(BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    // Only continue if connected already
    if([self isActiveDeviceConnected]==NO) return;
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
    
}



-(void) setHeartRateEnable
{
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_HEARTRATE_ENABLE_MASK )==0){
        gConfiguration |= BINS_CONFIG_HEARTRATE_ENABLE_MASK;
    }else{
        gConfiguration &= (~(BINS_CONFIG_HEARTRATE_ENABLE_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    // Only continue if connected already
    if([self isActiveDeviceConnected]==NO){
        [self noticeForConnection]; // 20151107
        return;
    }
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
    
}

/* This bit means when pulse data cannot be detected over a period (30 seconds),
 * BiNS will continue to wait for it, otherwise will stop waiting
 * and touch sensor will go down idle.
 */

-(void)  setAutoHeartrateDetection
{
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_HRD_ALWAYS_ON_MASK )==0){
        gConfiguration |= (BINS_CONFIG_HRD_ALWAYS_ON_MASK);
    }else{
        gConfiguration &= (~(BINS_CONFIG_HRD_ALWAYS_ON_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    //-- not continue if not connected yet
    if([self isActiveDeviceConnected]==NO) return;
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
    
}

-(void) setBinsX2SensorBlink {
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK )==0){
        gConfiguration |= (BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK);
    }else{
        gConfiguration &= (~(BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    //-- not continue if not connected yet
    if([self isActiveDeviceConnected]==NO){
        [self noticeForConnection]; // 20151107
        return;
    }
    
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
    
    hasActivityNotified=true;
}



-(void) setStepCheckWhenHRDEnable
{
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK)==0){
        gConfiguration |= (BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK);
    }else{
        gConfiguration &= (~(BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    //-- not continue if not connected yet
    //-- not continue if not connected yet
    if([self isActiveDeviceConnected]==NO) return;
    
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];

    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
				
    hasActivityNotified=true;
    
}

-(void) setHeartRateSleepEnable
{
    
    int gConfiguration=[binsDataController getActiveDeviceConfiguration];
    
    gConfiguration &= 0xFFF0;
    
    if((gConfiguration & BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK)==0){
        gConfiguration |= (BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK);
    }else{
        gConfiguration &= (~(BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK));
    }
    
    [binsDataController setActiveDeviceConfiguration:gConfiguration];
    
    //-- not continue if not connected yet
    if([self isActiveDeviceConnected]==NO) return;
    
    
    uint16_t val = gConfiguration;
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicConfiguration type:CBCharacteristicWriteWithResponse];
				
    hasActivityNotified=true;
}

-(void) setSleepCycleTime   {
    
    if(binsCharacteristicSleepStartTime==nil) return;
    
    // for non HBP devices, may need to set this to adjust while connecting

    uint16_t val = 0x0001;  //-- set 1 minute as the cycle time to keep record
    NSData* data = [NSData dataWithBytes:(void*)&val length:sizeof(val)];
    
    [binsPeripheral writeValue:data forCharacteristic:binsCharacteristicSleepStartTime type:CBCharacteristicWriteWithResponse];
}

- (void)updatePeriodWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    
    NSLog(@"No new data found.");
}

-(void) setDeviceBeaconRegion {
    
    NSLog(@"setDeviceBeaconRegion");
    
    [locationManager stopMonitoringForRegion:beaconRegion];
    
    NSString *ibeaconUuid=[binsDataController getIbeaconUuid];
    if(ibeaconUuid==Nil) return;
    if(ibeaconUuid.length<=0) return;
    
    NSLog(@"new beacon id=%@",ibeaconUuid);
    
    // Create a NSUUID with the same UUID as the broadcasting beacon
    NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:ibeaconUuid];
    
    // Setup a new region with that UUID and same identifier as the broadcasting beacon
    beaconRegion = [[CLBeaconRegion alloc] initWithProximityUUID:uuid major:1
                                                    identifier:@"com.hanson.bins.bregion" ];
    
    // Tell location manager to start monitoring for the beacon region
    [locationManager startMonitoringForRegion:beaconRegion];
    [locationManager startRangingBeaconsInRegion:beaconRegion];
   
    /*
    NSSet *setOfRegions = [self.locationManager monitoredRegions];
    for (CLRegion *region in setOfRegions) {
        NSLog (@"region info: %@", region);
    }
    */
}



- (void)locationManager:(CLLocationManager*)manager didEnterRegion:(CLRegion*)region
{
    NSLog(@"beacon didEnterRegion");
    [locationManager startRangingBeaconsInRegion:self.beaconRegion];
}


-(void)locationManager:(CLLocationManager*)manager didExitRegion:(CLRegion*)region
{
    [self.locationManager stopRangingBeaconsInRegion:self.beaconRegion];
    //-- self.beaconFoundLabel.text = @"No";
    
}

- (void) endBackgroundTask{
    
    NSLog(@"device endBackgroundTask");
    
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    
    dispatch_async(mainQueue, ^(void) {
        
        
            [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskIdentifier];
            
            backgroundTaskIdentifier = UIBackgroundTaskInvalid;
        
    });
    
}


-(void)locationManager:(CLLocationManager*)manager
       didRangeBeacons:(NSArray*)beacons
              inRegion:(CLBeaconRegion*)region
{
    //-- NSLog(@"beacon didRangeBeacons");

    if ([beacons count] <=0) return;
    
    CLBeacon *beacon=[beacons firstObject];
    NSLog(@"1st beacon found on %f meters away. major=%d, minor=%d",beacon.accuracy, [beacon.major intValue], [beacon.minor intValue]);
    
    if([beacon.minor intValue]==BEACON_MAJOR_CONNECT_REQUEST){
        
        if(deviceCommand==DEVICE_COMMAND_IDLE){
            
            backgroundTaskIdentifier =
            [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"MyTask" expirationHandler:^{
                // Clean up any unfinished task business by marking where you
                // stopped or ending the task outright.
                
                
                
                //[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskIdentifier];
                
                backgroundTaskIdentifier = UIBackgroundTaskInvalid;

            }];
            
            // Start the long-running task and return immediately.
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                // Do the work associated with the task, preferably in chunks.
                
                NSLog(@"Time Remaining: %f", [[UIApplication sharedApplication] backgroundTimeRemaining]);

                // if(activeDeviceConnected==NO &&  bluetoothManager!=nil && binsPeripheral!=nil)
                //    [bluetoothManager connectPeripheral:binsPeripheral options:nil];

                [self.delegate deviceController:self tryConnect:TRY_CONNECT_WITH_SYNC];
                
                if(/* DISABLES CODE */ (1)==2){
                    [[UIApplication sharedApplication] endBackgroundTask:backgroundTaskIdentifier];
                    backgroundTaskIdentifier = UIBackgroundTaskInvalid;
                }
            });
 
            
            //-- [self.delegate deviceController:self tryConnect:TRY_CONNECT_WITH_SYNC];
        }
        
    }else if ([beacon.minor intValue]==BEACON_MAJOR_HEARTRATE_CYCLE){
        if(deviceCommand==DEVICE_COMMAND_IDLE){
            [self.delegate deviceController:self tryConnect:TRY_CONNECT_ONLY];
        }
    }else if ([beacon.minor intValue]==BEACON_MAJOR_BATTERY_LOW){
        
        NSInteger currentTimeInMinute=[self localeTimeIntervalInMinute];

        if(currentTimeInMinute<(mLastBeaconCheckForBatteryLow+30)){
            // if less than 30 minutes, then just skip
            
            mLastBeaconCheckForBatteryLow=currentTimeInMinute;
            
            [notificationController addNotification:
             [NSString localizedStringWithFormat:NSLocalizedString(@"battery_low",nil), 10 ] withAction: [NSString localizedStringWithFormat:NSLocalizedString(@"charge_device",nil) ]];
        }
        

    }
    
}

-(void)resetStatus {
    mLastBeaconCheckForBatteryLow=0;
}

@end

@implementation  ActivityRecord

@end
