//
//  FinderStatusView.h
//  bins
//
//  Created by Dennis Kung on 11/8/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceViewController.h"

@interface FinderStatusView : UIView

@property DeviceViewController *deviceController;

-(void)setFinderStatus:(bool)isON;

@end
