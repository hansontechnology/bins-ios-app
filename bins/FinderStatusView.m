//
//  FinderStatusView.m
//  bins
//
//  Created by Dennis Kung on 11/8/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "FinderStatusView.h"

@implementation FinderStatusView

@synthesize deviceController;

static bool finderRunning=NO;


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    
    deviceController=[[DeviceViewController alloc] init];
    
    
    
    return self;
}


-(void)setFinderStatus:(bool)isON {
    
    finderRunning=isON;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    
    // if([deviceController finderIsRunning]==NO){
    if(finderRunning==NO){
        return;
    }
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGGradientRef gradient;
    CGColorSpaceRef colorspace;
    CGFloat locations[2] = { 0.0, 1.0};
    
    NSArray *colors = @[(id)[UIColor whiteColor].CGColor,
                        (id)[UIColor greenColor].CGColor];
    
    colorspace = CGColorSpaceCreateDeviceRGB();
    
    gradient = CGGradientCreateWithColors(colorspace,
                                          (CFArrayRef)colors, locations);
    
    CGPoint startPoint, endPoint;
    CGFloat startRadius, endRadius;
    startPoint.x = 7;
    startPoint.y = 7;
    endPoint.x = 10;
    endPoint.y = 10;
    startRadius = 0;
    endRadius = 10;
    
    CGContextDrawRadialGradient (context, gradient, startPoint,
                                 startRadius, endPoint, endRadius,
                                 0);
}


@end
