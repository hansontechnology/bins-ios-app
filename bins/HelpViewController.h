//
//  HelpViewController.h
//  bins
//
//  Created by Dennis Kung on 12/8/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
