//
//  HelpViewController.m
//  bins
//
//  Created by Dennis Kung on 12/8/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "HelpViewController.h"

@implementation HelpViewController

@synthesize webView;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    NSLog(@"Help viewDidLoad");
    
    
    [[self.navigationController navigationBar] setTintColor: [UIColor whiteColor]];
    
    NSString *path = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], NSLocalizedString(@"index-en.html",nil)];
    NSURL *htmlFile = [NSURL fileURLWithPath:path];
    
    /*
    NSURL *htmlFile = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"index-en" ofType:@"html"] isDirectory:NO];
    */
    
    /*
    UIFont *font = [UIFont fontWithName:@"GothamRounded-Bold" size:14];
    NSString *htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: %i\">%@</span>",
                  font.fontName,
                  (int) font.pointSize,
                  htmlString];

    */
    
    [webView loadRequest:[NSURLRequest requestWithURL:htmlFile]];
    
}

@end
