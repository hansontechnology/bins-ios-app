//
//  LevelPeriodBuilder.h
//  bins
//
//  Created by Dennis Kung on 7/7/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BinsDataViewController.h"

@interface LevelPeriodBuilder : NSObject

@property BinsDataViewController *binsDataController;


@property (readonly) int *mActivityMovements;
@property (readonly) int *mLightLevels;
@property (readonly) long mLightNumbers;


-(id) initWithSleepPeriodFrom: (long) timePeriodStart to: (long) timePeriodEnd ;

-(long) getCount ;
-(int) getTimeWindow;

-(int) getLightNumbers;
-(int*) getLightLevels;

-(void) storeToDB ;

-(int)getThreshIdFromMovements: (int) movements;

-(float)getDataAvailability;


@end
