//
//  LevelPeriodBuilder.m
//  bins
//
//  Created by Dennis Kung on 7/7/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import "LevelPeriodBuilder.h"
#import "SleepTableViewController.h"

@implementation LevelPeriodBuilder

@synthesize binsDataController;


int     *mActivityMovements;
int     *mLightLevels;

#define MAX_THRESHOLDS  20
#define MIN_TIME_WINDOW 5   //-- the minimum time unit to keep record
                            //-- the larger the number, the saving time and space during processing.

int     mThresholds[MAX_THRESHOLDS];
int     mTimeWindow;

long    mTimePeriodStart;
long    mTimePeriodEnd;
// TODO SleepView mSleepViewToDisplay;
    
int mSamplePeriod;
long mActivityNumbers=0;
long mLightNumbers=0;

int mNumberActiveData=0;
int mActualSamplePeriod=0;

-(id) initWithSleepPeriodFrom: (long) timePeriodStart to: (long) timePeriodEnd {
    
    self = [super init];

    if(timePeriodStart==0 || timePeriodEnd==0) return self;
    
    binsDataController=[[BinsDataViewController alloc] init];
    
    mTimeWindow=MIN_TIME_WINDOW;
    
    mActivityNumbers=(timePeriodEnd-timePeriodStart+1)/mTimeWindow; //-- +(mTimeWindow-1);
    
    mLightNumbers=(int)((timePeriodEnd-timePeriodStart)+1)/mTimeWindow;

    NSLog(@"mLightNumbers is set to %ld", mLightNumbers);
    
    mActivityMovements=calloc(mActivityNumbers, sizeof(int)); // init to 0 first
    mLightLevels=calloc(mLightNumbers, sizeof(int));
    
    for(int i=0; i<mActivityNumbers; i++){
        mActivityMovements[i]=-1;
    }
    for(int i=0; i<mLightNumbers; i++){
        mLightLevels[i]=-1;
    }
    
    mTimePeriodStart=timePeriodStart;
    mTimePeriodEnd=timePeriodEnd;
        
    int samplePeriod=30; // max. from 30 minutes
    long lastSampleTime=0;
    int movements ;
    int type;
    long sampleTime ;

    mNumberActiveData=0;    //-- 20151126 BUG FIX, reset it first
    int maxIndex=0;
    while ([binsDataController getNextSleepRecord: &movements ofType: &type atTime: &sampleTime])
    {
        if(sampleTime>timePeriodEnd) continue;
        int timeIndex=(int)((sampleTime-mTimePeriodStart)/mTimeWindow);
        
        
        if(type==BINS_ACTIVITY_TYPE_LIGHT){
            
            int timeIndex=(int)((sampleTime-mTimePeriodStart)/mTimeWindow);
            if(mLightLevels[timeIndex]==-1){
                mLightLevels[timeIndex]=movements;
            } else {
                mLightLevels[timeIndex]+=movements;
            }
            
        }else if(type==BINS_ACTIVITY_TYPE_SLEEP){
        
            if(sampleTime>timePeriodEnd) continue;
            
            mNumberActiveData++; 	//-- 20151023
            
            if(timeIndex>maxIndex) maxIndex=timeIndex;
            if(mActivityMovements[timeIndex]==-1){
                mActivityMovements[timeIndex]=movements;
            } else {
                mActivityMovements[timeIndex]+=movements;
            }
            //-- NSLog(@"sleep movements=%d:%d",timeIndex,movements);
            if((sampleTime-lastSampleTime)<samplePeriod){
                NSLog(@"sleep sample period=%d:%ld:%ld",timeIndex,sampleTime,lastSampleTime);
                samplePeriod=(int)(sampleTime-lastSampleTime);
            }
            lastSampleTime=sampleTime;
        }
    }
    
    //-- mActivityNumbers=maxIndex+1;
    
    mSamplePeriod=samplePeriod;
    mActualSamplePeriod=samplePeriod;
    if(mSamplePeriod<mTimeWindow) mSamplePeriod=mTimeWindow;
        
    [self normalizeMovements];
        
    [self getThresholds];
    
    if([binsDataController getFilterUnused]!=0)
    {
        NSLog(@"run filter");
        [self removeDataForTimeUnused:SLEEP_STAGE_LIGHT_WAKE];
    }
    
    return self;
        
}

-(float) getDataAvailability {
    
    if(mActualSamplePeriod==0) return 0;
    long dataShouldHave=((mTimePeriodEnd-mTimePeriodStart)+1)/mActualSamplePeriod;
    NSLog(@"movement avail total actual:should:period=%d:%ld:%d",mNumberActiveData,dataShouldHave,mActualSamplePeriod);
    return ((float)mNumberActiveData/dataShouldHave);
    
}


-(long) getCount {
    return mActivityNumbers;
}

-(int) getTimeWindow {
    return mTimeWindow;
}
-(void)getThresholds {
    

    //-- 150805 use this instead temporarily if(queryResult==NO)
#if 0
    {
        // 150623 for test, to get a reasonable values
        /* 20150829 to improve accuracy
        mThresholds[1] =  30;			// deep sleep
        mThresholds[2] =  90;
        mThresholds[3] = 150;			// light sleep
        mThresholds[4] = 350;
        mThresholds[5] = 500;			// light wake
        mThresholds[6] = 700;
        mThresholds[7] = 900;			// normal
        mThresholds[8] =1000;
        mThresholds[9] =1200;			// active
        mThresholds[10]=2200;
        mThresholds[11]=-1;
        */
        
        mThresholds[1] =   6;			// deep sleep
        mThresholds[2] =  16;			// light sleep
        mThresholds[3] =  75;			// light wake
        mThresholds[4] = 175;			// normal
        mThresholds[5] = 250;
        mThresholds[6] = 350;
        mThresholds[7] = 450;			// active
        mThresholds[8] = 500;
        mThresholds[9] = 900;
        mThresholds[10]=1200;
        mThresholds[11]=-1;
        
        
        return;
    }
#endif

    BOOL queryResult=[binsDataController getThresholds];

    if(queryResult==NO){
        NSLog(@"cannot load threshold table, wrong..");
    }
    
    int maxThresh=0;

    int threshId;
    int movements;
    //-- int totalCount=[binsDataController getThresholdsCount];
    while([binsDataController getNextThresholdFromQuery:&threshId movements: &movements]==YES){
        
        if(threshId>maxThresh) maxThresh=threshId;
        
        mThresholds[threshId]=movements;
        //-- NSLog(@"threshold[%d]=%d", threshId, movements);
        
    }
    mThresholds[maxThresh+1]=-1;

    
}

//-- function to spread the activities to per time window unit, from sampled

-(void) normalizeMovements {
    int sampleTime;
    int lastSampleTime=-1;
    
    if(mSamplePeriod<=mTimeWindow) return; //-- no need to normalize
    
    float sampleFactor=mSamplePeriod/mTimeWindow;
                    // mSamplePeriod should be multiple of mTimeWindow;
    
    for(int i=0; i<mActivityNumbers; i++){
        if(mActivityMovements[i]>=0){
            int sampledMovements=mActivityMovements[i];
                
            sampleTime=i;   // actually an array index;
            
            //- boundary condition to improve efficiency
            // maximum granularity is 10 minutes
            if(((sampleTime-lastSampleTime)*mTimeWindow)>10)
                                        lastSampleTime=sampleTime-(10/mTimeWindow);
            
            if(lastSampleTime<=-1) lastSampleTime=sampleTime-(int)sampleFactor;
                
            int movementsPerTimeWindow;
            for(int j=i; j>=(lastSampleTime+1) && j>=0; j--){
                
                movementsPerTimeWindow=(int)((float)sampledMovements/sampleFactor);
                
                mActivityMovements[j]=movementsPerTimeWindow;
                
                //-- Log.i(TAG, "sleep normalize="+j+":"+mActivityMovements[j]+":"+mSamplePeriod);
            }
            lastSampleTime=sampleTime;
                
        }
    }
        
}

-(int*)mActivityMovements {
    return mActivityMovements;
}


/* TODO
 
-(void) display: SleepView sleepView {
        
    mSleepViewToDisplay=sleepView;
        
    mSleepViewToDisplay.displayUpdate(mSamplePeriod, mThresholds, mActivityMovements);
        
}
 */

-(int)getThreshIdFromMovements: (int) movements {
    
    int movePerMinute=(int)((float)movements/mTimeWindow);	// the input is for a total of the minutes per bar
    
    if(movements<0) return -1;
    
    int idFound=-1;
    int i;
    for(i=1; i<MAX_THRESHOLDS; i++){
        if(mThresholds[i]>movePerMinute) break;
        if(mThresholds[i]==-1) break;
    }
    if(mThresholds[i]==-1) i--;
    idFound=i;
    
    //-- NSLog(@"sleep thresh=%d:%d",movPerMinute,idFound);
    return idFound;
}

-(void) storeToDB {
    int i;
    long currentLevelStartIndex;
    long currentLevelEndIndex;
    long timePeriodStartIndex=mTimePeriodStart/mTimeWindow;
    
    for(i=0; i<mActivityNumbers;) {
        currentLevelStartIndex=timePeriodStartIndex+i;
        currentLevelEndIndex=currentLevelStartIndex; // initial to end at the same slot
        
        int movements=mActivityMovements[i];
        
        if(movements<0){ i++; continue;}
                
        int threshIdStart=[self getThreshIdFromMovements: movements];
        
        if(threshIdStart<1){
            i++;
            continue; // empty slot
        }
        
        int j;
        for(j=i+1; j<mActivityNumbers ;j++){
            
            int movements=mActivityMovements[j];
            
            if(movements<0){ break; }
            
            int threshIdNext=[self getThreshIdFromMovements: movements];
            
            if(threshIdNext==threshIdStart){
                currentLevelEndIndex=timePeriodStartIndex+j;
            }else{
                break;
            }
        }
        long currentLevelStartTime=(currentLevelStartIndex*mTimeWindow);
        long currentLevelEndTime=(currentLevelEndIndex*mTimeWindow)+(mTimeWindow-1);
        
        //-- NSLog(@"level periods:%ld, %ld(%ld)=%d", currentLevelStartTime, currentLevelEndTime, (currentLevelEndTime-currentLevelStartTime+1),threshIdStart);
        
        [binsDataController insertCubeLevelPeriodFrom:
         currentLevelStartTime to: currentLevelEndTime atThreshold: threshIdStart];
        
        i=j;
            
    } //-- end of for loop
}

-(void) removeDataForTimeUnused:(enum SleepStage) sleepStage {
    int threshIdToCheck=(int)sleepStage;
    
    int i;
    for(i=0; i<mActivityNumbers;) {
        int movements=mActivityMovements[i];
        

        if(movements<0){ i++; continue;}
        
        int threshIdStart=[self getThreshIdFromMovements: movements];
        
        //-- NSLog(@"movement start: %d,%d, %d", i, movements, threshIdStart);

        if(threshIdStart<1){
            i++;
            continue; // empty slot
        }
        
        int j;
        Boolean isMovementZero=true;
        Boolean isWithoutUse=false;
        
        if(threshIdStart>2) isMovementZero=false;
        int threshIdNext=0;
        for(j=i+1; j<mActivityNumbers ;j++){
            int moveCount=mActivityMovements[j];
            if(moveCount<0){
                //-- NSLog(@"movement empty: %d,%d, %d", j, moveCount, threshIdNext);
                break;
            }
            
            threshIdNext=[self getThreshIdFromMovements: moveCount];
            
            //-- NSLog(@"movement next: %d,%d, %d", j, moveCount, threshIdNext);
            if(threshIdNext!=threshIdStart){
                break;
            }
        }
        
        if(isMovementZero==true){
            //-- NSLog(@"movement check:%d,%ld", j, mActivityNumbers);
            if(threshIdNext>=threshIdToCheck || j>=mActivityNumbers){
                long timePeriod=(j-i)*mTimeWindow;
                //-- NSLog(@"movement check2:%ld", timePeriod);
                if(timePeriod>90){ // more than 90 minutes then
                    isWithoutUse=true;
                }
            }
        }
        
        if(isWithoutUse==true){
            for(long k=i; k<j; k++){
                mActivityMovements[(int)(k)]=-1;
                //-- NSLog(@"movement unused:%ld", k);
            }
        }
        i=j;
    }  // for loop
}

-(int)getLightNumbers {
    return (int)mLightNumbers;
}

-(int*)getLightLevels {
    return (int*)mLightLevels;
}

@end
