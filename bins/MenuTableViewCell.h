//
//  MenuTableViewCell.h
//  bins
//
//  Created by Dennis Kung on 11/30/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *menuTextLabel;

@property (weak, nonatomic) IBOutlet UIImageView *menuIconImage;

@end
