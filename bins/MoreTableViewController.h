//
//  MoreTableViewController.h
//  bins
//
//  Created by Dennis Kung on 10/27/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceViewController.h"
#import "DevicePairView.h"
#import "BinsDataViewController.h"
#import "DoneCancelNumberPadToolbar.h"
#import "FinderStatusView.h"
#import "SWRevealViewController.h"
#import "NotificationControllerView.h"

@interface MoreTableViewController : UITableViewController <UITableViewDataSource,UITableViewDelegate, DeviceControllerDelegate, DevicePairDelegate, DoneCancelNumberPadToolbarDelegate, SWRevealViewControllerDelegate>

@property NotificationControllerView *notificationController;

@property DeviceViewController *deviceController;

@property DevicePairView *devicePair;

@property BinsDataViewController *binsDataController;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *pairRunningIndicator;

@property (weak, nonatomic) IBOutlet UILabel *pairedBinsInfoLabel;
@property (weak, nonatomic) IBOutlet UITextField *goalTextField;

@property (weak, nonatomic) IBOutlet UILabel *selectedDeviceLabel;


@property (weak, nonatomic) IBOutlet UISlider *shakeUiSlider;
@property (weak, nonatomic) IBOutlet UISlider *finderSlider;
@property (weak, nonatomic) IBOutlet UISwitch *finderSwitch;

@property (weak, nonatomic) IBOutlet UIImageView *batteryLevelImage;
@property (weak, nonatomic) IBOutlet UILabel *batteryLevelLabel;
@property (weak, nonatomic) IBOutlet UILabel *batteryPercentLabel;
- (IBAction)finderSliderValueChanged:(id)sender;
- (IBAction)shakeUiSliderValueChanged:(id)sender;

- (IBAction)finderSwitchValueChanged:(id)sender;

@property (weak, nonatomic) IBOutlet FinderStatusView *finderStatusView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *binsStatusViewCell;
@property (weak, nonatomic) IBOutlet UILabel *binsSetupMessageLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *userSelectButton;


@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizerRight;

@property (weak, nonatomic) IBOutlet UITableViewCell *pairBinsTableCell;

@property (weak, nonatomic) IBOutlet UITableViewCell *userProfileTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *findMeTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *shakeUiTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *helpTableCell;

@property (weak, nonatomic) IBOutlet UIDatePicker *alarmDatePicker;
- (IBAction)alarmPickerValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *alarmSetSwitch;
- (IBAction)alarmSetSwitchValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UIProgressView *pairDeviceSignalProgress;

@end
