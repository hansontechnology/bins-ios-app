//
//  MoreTableViewController.m
//  bins
//
//  Created by Dennis Kung on 10/27/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "MoreTableViewController.h"
#import "binsHelper.h"
#import "TimeControllerView.h"

@interface MoreTableViewController ()

@end

@implementation MoreTableViewController

@synthesize deviceController;
@synthesize pairRunningIndicator;
@synthesize binsDataController;
@synthesize finderStatusView;
@synthesize notificationController;
@synthesize devicePair;
@synthesize pairDeviceSignalProgress;

#define HEIGHT_IPHONE5 568 // 12.22

static int heightOfPhone;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    notificationController=[[NotificationControllerView alloc] init];
    
    deviceController=[[DeviceViewController alloc] init];
    
    //-- no need 12.02 deviceController.delegate=self;
    
    devicePair=[[DevicePairView alloc] init];
    
    devicePair.delegate=self;
    
    
    binsDataController=[[BinsDataViewController alloc] init];
    
    //self.goalTextField.delegate=self;
    DoneCancelNumberPadToolbar *goalDoneCancelToolbar = [[DoneCancelNumberPadToolbar alloc] initWithTextField:self.goalTextField];
    goalDoneCancelToolbar.delegate = self;
    self.goalTextField.inputAccessoryView = goalDoneCancelToolbar;
    
    
    finderStatusView.backgroundColor=[UIColor clearColor];
    
    self.finderSwitch.onTintColor= Rgb2UIColor(99, 184, 255, 1 );
    
    // self.finderSwitch.backgroundColor=[UIColor lightBlueColor];
  
    [self.finderSwitch setTransform:CGAffineTransformMakeScale(1, 0.9)];
    
    // Change button color
    _sidebarButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    // Set the gesture
    //-- [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
    // Change button color
    _userSelectButton.tintColor = [UIColor colorWithWhite:0.8f alpha:0.9f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _userSelectButton.target = self.revealViewController;
    _userSelectButton.action = @selector(rightRevealToggle:);    

    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(revealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer.enabled = NO;
    
    self.tapGestureRecognizerRight = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(rightRevealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
    self.tapGestureRecognizerRight.enabled = NO;
    
    heightOfPhone=self.view.frame.size.height;
    
    
    UILongPressGestureRecognizer* longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongPress:)];
    [self.tableView addGestureRecognizer:longPressRecognizer];
    
    
    [self.selectedDeviceLabel setHidden:YES];
    
    [pairDeviceSignalProgress setHidden:YES];
    CGAffineTransform transform = CGAffineTransformMakeScale(1.0f, 3.0f);
    
    pairDeviceSignalProgress.transform=transform;
    pairDeviceSignalProgress.progressTintColor=[UIColor redColor];
    

}

-(void)onLongPress:(UILongPressGestureRecognizer *)pGesture
{
    NSLog(@"Longpress");
    
    if (pGesture.state == UIGestureRecognizerStateRecognized)
    {
        //Do something to tell the user!
    }
    if (pGesture.state == UIGestureRecognizerStateEnded)
    {
        UITableView* tableView = (UITableView*)self.view;
        CGPoint touchPoint = [pGesture locationInView:self.view];
        NSIndexPath* indexPath = [tableView indexPathForRowAtPoint:touchPoint];
        if (indexPath.row ==1) {  // device de-select
            //Handle the long press on row
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice",nil) message:NSLocalizedString(@"will_release_device", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"confirm_cancel",nil) otherButtonTitles:NSLocalizedString(@"confirm_ok",nil), nil];
            
            [alert show];

        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:NSLocalizedString(@"confirm_ok",nil)])
    {
        NSLog(@"Release device");
        
        int currentUser=[binsDataController getActiveUserID];
        [binsDataController releaseUserDevice:currentUser];
        
    }
    
}


-(void)doneCancelNumberPadToolbarDelegate:(DoneCancelNumberPadToolbar *)controller didClickCancel:(UITextField *)textField
{
    NSLog(@" cancel cancel %@", textField.text);
    
    textField.text=[[NSString alloc] initWithFormat:@"%d", [binsDataController getDailyGoal] ];
}

-(void)doneCancelNumberPadToolbarDelegate:(DoneCancelNumberPadToolbar *)controller didClickDone:(UITextField *)textField
{
    NSLog(@" done cancel %@", textField.text);
    
    if([textField.text intValue]>99999){
        textField.text=[[NSString alloc] initWithFormat:@"%d", [binsDataController getDailyGoal] ];
    }else{
        [binsDataController setDailyGoal: [textField.text intValue]];
        [deviceController updateGoal];
    }
    
}


-(void)viewWillAppear:(BOOL) animated {
    
    [super viewWillAppear:animated];
    
    self.revealViewController.delegate=self;
    
    if([binsDataController getActiveDeviceID]!=0){
        self.binsStatusViewCell.hidden=NO;
    }
    else{
        self.binsStatusViewCell.hidden=YES;
    }
    
    [self updateViews];
    
    //-- no need 12.02 deviceController.delegate=self;
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    int activeHeightTableCell=(int)(80.0*(heightOfPhone-100)/(HEIGHT_IPHONE5-100));

    if(activeHeightTableCell<80) activeHeightTableCell=80;

    if(indexPath.row==0){
        int device=[binsDataController getActiveDeviceID];
        if(device==0 ){
            return 2;
        }
        else{
            //-- NSLog(@"device id=%d",device);
            return activeHeightTableCell;
        }
    }
    else if(indexPath.row==3){
        return 233;
    }
    else if(indexPath.row==4){
        //-- 150415 hide Finder function return activeHeightTableCell+10;
        return 0;
    }
    else if(indexPath.row==6){
        return 0;  //-- 150415 hide help
    }
    
    return activeHeightTableCell; // tableView.rowHeight;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.

    return 7;
    
 }
-(void)tableView: (UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.section!=0) return;
    
    int currentUser=[binsDataController getActiveUserID];
    int currentDevice=[binsDataController getActiveDeviceID];
    
    if(indexPath.row==0){
        if(currentDevice==0)
            [cell setHidden:YES];
        else{
            [cell setHidden:NO];
        }
    }
    else if(indexPath.row==2){ // finder
        if(currentUser==0){
            self.goalTextField.enabled=NO;
        }
        else{
            self.goalTextField.enabled=YES;
        }
    }
    else if(indexPath.row==3){ // alarm
        if(currentDevice==0){
            cell.userInteractionEnabled=NO;
        } else {
            cell.userInteractionEnabled=YES;
        }
    }
    else if(indexPath.row==4){ // finder
        if(currentDevice==0){
            cell.userInteractionEnabled=NO;
        } else {
            cell.userInteractionEnabled=YES;
        }
        [cell setHidden:YES]; // 150415 hide finder function
    }
    else if(indexPath.row==5){ // shake UI
        if(currentDevice==0){
            cell.userInteractionEnabled=NO;
        } else {
            cell.userInteractionEnabled=YES;
        }
        [cell setHidden:YES]; // 150713 shake UI function

    }
    else if(indexPath.row==6){ // finder
        [cell setHidden:YES]; // 150415 hide help function
    } else {
        cell.userInteractionEnabled=YES;
        [cell setHidden:NO];
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    if (indexPath.section == 0 && indexPath.row == 1) {
        // do some action
        [tableView deselectRowAtIndexPath:indexPath animated: YES];
        
        if([binsDataController activeUserIsRemote]==NO){
        
            self.binsSetupMessageLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"searching",nil) ];

            [pairRunningIndicator startAnimating];
        
            [devicePair  autoPair];
        }

    }
}

-(void)deviceController:(DeviceViewController *)deviceViewController stateChanged:(int)state {
    
    NSLog(@"state changed");
    
    [self updateViews];
}

-(void)deviceController:(DeviceViewController *)deviceViewController newActivityRead:(int)numberSync {
    
}

- (void)deviceController:(DeviceViewController*)deviceViewController finderStatusChanged:(int)status {
    
    NSLog(@"finder state changed:%d", status);
    /*
    if(status==BINS_FINDER_STATUS_NOT_ACTIVE){
        [finderStatusView setFinderStatus: NO];
    }
    else{
        [finderStatusView setFinderStatus: YES];
    }
    */
}

- (void)deviceController:(DeviceViewController*)deviceViewController errorFound:(NSString *)errMsg {
    
    NSLog(@"device error:%@", errMsg);
    
}

-(void)devicePair:(DevicePairView *)devicePair foundDevice: (NSUUID*)deviceIdentifier withStrength:(int)rssi {

    float SIGNAL_MAX=-20;
    float SIGNAL_MIN=-90;
    float SIGNAL_PERIOD=SIGNAL_MAX-SIGNAL_MIN;
    
    if(rssi>SIGNAL_MAX) rssi=SIGNAL_MAX;
    if(rssi<SIGNAL_MIN) rssi=SIGNAL_MIN;
    
    float strengthRatio= (rssi-SIGNAL_MIN)/SIGNAL_PERIOD;
    if(strengthRatio<0.1) strengthRatio=0.1;
    
    NSLog(@"pairing signal stregnth ratio=%d, %f", rssi, strengthRatio);
    
    NSUUID* activeDeviceAddress=[binsDataController getActiveDeviceAddress];
    
    if([deviceIdentifier isEqual:activeDeviceAddress]){
        pairDeviceSignalProgress.progressTintColor=[UIColor blueColor];
    }else{
        pairDeviceSignalProgress.progressTintColor=[UIColor redColor];
    }

    [pairDeviceSignalProgress setHidden:NO];
    [pairDeviceSignalProgress setProgress: strengthRatio];
    
}

-(void)devicePair:(DevicePairView *)devicePair selectingDevice: (NSString*)ibeaconUuid withId: (int)deviceId {
    
    self.selectedDeviceLabel.text=[NSString stringWithFormat:@"[%d]",deviceId ];
    //-- [self.selectedDeviceLabel setHidden:NO];
    
    [binsDataController setIbeaconUuid: ibeaconUuid];

    self.pairedBinsInfoLabel.text=[[NSString alloc] initWithFormat:@"[ %5d ]", deviceId];
    
    [self.tableView reloadData];

}

-(void)devicePair:(DevicePairView *)devicePair pairToDevice:(NSUUID *)deviceIdentifier {
    NSLog(@" more paired to device:%@",deviceIdentifier);
    
    [pairRunningIndicator stopAnimating];
    [self.selectedDeviceLabel setHidden:YES];
    
    self.binsSetupMessageLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"new_device",nil) ];
    
    
    if(deviceIdentifier==nil){
        return;
    }
    
    [binsDataController setActiveDeviceAddress: deviceIdentifier];

    [pairDeviceSignalProgress setHidden:YES];

    [self updateViews];
}

-(void)updateViews {
    
    int deviceID=[binsDataController getActiveDeviceID];
    
    NSString *strDeviceID;
    
    if(deviceID>0){
        
        NSString *ibeaconUuid=[binsDataController getIbeaconUuid];
                              
        if(ibeaconUuid.length<=0){
            
            strDeviceID=[[binsDataController getActiveDeviceAddress] UUIDString];
            
            strDeviceID=[strDeviceID substringToIndex:6];
            
        } else {
            
            // Create a NSUUID with the same UUID as the broadcasting beacon
            NSUUID *uuid = [[NSUUID alloc] initWithUUIDString:ibeaconUuid];
            unsigned char uuidBytes[16];
            
            [uuid getUUIDBytes:uuidBytes];
            
            int systemId=0;
            
            for(int i=0; i<15; i++){
                //-- NSLog(@"uuid %d=%d", i, uuidBytes[i]);
                if(i==0) systemId+= (uuidBytes[i]);
                if(i==1) systemId+= (uuidBytes[i]<<8);
                if(i==2)systemId+=  (uuidBytes[i]<<16);
            }
            systemId %=1000000;
            strDeviceID=[NSString stringWithFormat:@"%d",systemId];
        }
                               
    }else{
        strDeviceID=@"";
    }

    self.pairedBinsInfoLabel.text=[[NSString alloc] initWithFormat:@"[ %@ ]", strDeviceID];
    
    bool isEnabled;
    int threshold;
    [binsDataController getFinderThreshold: &isEnabled hasThreshold: &threshold];
    
    self.finderSlider.value=((float)threshold/BINS_FINDER_LEVEL_MAX);
    
    self.finderSwitch.on= isEnabled;

    self.shakeUiSlider.value=((float)[binsDataController getShakeUiThreshold]/BINS_SHAKE_LEVEL_MAX);
    
    self.goalTextField.text=[[NSString alloc] initWithFormat:@"%d", [binsDataController getDailyGoal] ];
    
    self.alarmSetSwitch.on=[binsDataController getAlarmEnable];
    int alarmTime=[binsDataController getAlarmTime] & 0x7FFF;
    
    NSDate *date=[NSDate date];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];

    dateComponents.hour=(alarmTime&0x7F00)>>8;
    dateComponents.minute=(alarmTime&0x00FF);
    
    NSDate *newDate = [calendar dateFromComponents:dateComponents];

    self.alarmDatePicker.date=newDate;
    
    [self showBatteryLevel: [binsDataController getActiveDeviceBatteryLevel]];
    
    [self.tableView reloadData];
}


UIImage *battImage=nil;


-(void)showBatteryLevel: (int)batteryLevel {
    
    if(batteryLevel==0){
        self.batteryLevelLabel.hidden=YES;
        self.batteryPercentLabel.hidden=YES;
        self.batteryLevelImage.hidden=YES;
        return;
    }

    self.batteryLevelLabel.hidden=NO;
    self.batteryPercentLabel.hidden=NO;
    self.batteryLevelImage.hidden=NO;
    
    if(batteryLevel==100){
        battImage=[UIImage imageNamed:@"stat_sys_battery_100"];
    } else if(batteryLevel>=85){
        battImage=[UIImage imageNamed:@"stat_sys_battery_85"];
    } else if(batteryLevel>=71){
        battImage=[UIImage imageNamed:@"stat_sys_battery_71"];
    } else if(batteryLevel>=57){
        battImage=[UIImage imageNamed:@"stat_sys_battery_57"];
    } else if(batteryLevel>=43){
        battImage=[UIImage imageNamed:@"stat_sys_battery_43"];
    } else if(batteryLevel>=28){
        battImage=[UIImage imageNamed:@"stat_sys_battery_28"];
    } else if(batteryLevel>=15){
        battImage=[UIImage imageNamed:@"stat_sys_battery_15"];
    } else if(batteryLevel>=0){
        battImage=[UIImage imageNamed:@"stat_sys_battery_0"];
    }
    
    self.batteryLevelImage.image=battImage;
    
    self.batteryLevelLabel.text=[[NSString alloc] initWithFormat:@"%d",batteryLevel];
    
    
    
}

- (void)deviceController:(DeviceViewController*)deviceViewController sleepRecordRead: (int)left of: (int)total {
}

- (void)deviceController:(DeviceViewController*)deviceViewController pulseNotified:(NSUUID*)deviceIdentifier withPulse: (int) pulse {
    
}

- (void)deviceController:(DeviceViewController*)deviceViewController stepsNotified:(NSInteger)deviceIdentifier withNewSteps: (int) steps {

}

-(void)deviceController:(DeviceViewController *)deviceViewController tryConnect:(int)option {
    
}

-(BOOL)tableView: (UITableView*)tableView shouldHighlightRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.row==1) return YES;
    if(indexPath.row==2) return YES;
    if(indexPath.row==6) return YES;
    
    return NO;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 1.0; //seconds
    [cell addGestureRecognizer:lpgr];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
*/


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)finderSliderValueChanged:(id)sender {
    
    int newFinderLevel=self.finderSlider.value*BINS_FINDER_LEVEL_MAX;
    
    // NSLog(@"finder :%d",newFinderLevel);
    
    [binsDataController setFinderThreshold:self.finderSwitch.isOn withFinderThreshold:newFinderLevel];
    
    if(self.finderSwitch.isOn==NO){
        [notificationController stopNotification];
    }
}

- (IBAction)shakeUiSliderValueChanged:(id)sender {

    int newShakeLevel=self.shakeUiSlider.value*BINS_SHAKE_LEVEL_MAX;
    
    
    if([binsDataController getActiveShakeUiThreshold]==newShakeLevel) return;

    NSLog(@"shakeUiSlider :%d",newShakeLevel);

    [binsDataController setShakUiThreshold:newShakeLevel];
    [deviceController updateShakeUiThreshold];

}

- (IBAction)finderSwitchValueChanged:(id)sender {
    
    [self finderSliderValueChanged:sender];
    
    
    // chance to trigger or stop finder setting
    [deviceController checkFinder];
    
   /*
    if(self.finderSwitch.isOn==YES)
        [deviceController startFinder];
    else{
        [deviceController stopFinder];
    }
    */
    
}

-(void) userInteractionEnable: (BOOL)enable {
    self.pairBinsTableCell.userInteractionEnabled=enable;
    self.userProfileTableCell.userInteractionEnabled=enable;
    self.findMeTableCell.userInteractionEnabled=enable;
    self.shakeUiTableCell.userInteractionEnabled=enable;
    self.helpTableCell.userInteractionEnabled=enable;
    self.tabBarController.tabBar.userInteractionEnabled = enable;
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeftSide) {
        // NSLog(@"FrontViewPositionLeftSide");
        // jump to right side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
        self.tapGestureRecognizerRight.enabled = YES;
        [self userInteractionEnable:NO];
    }
    else if (position == FrontViewPositionLeft){
        // NSLog(@"FrontViewPositionLeft");
        // back to main screen
        self.tapGestureRecognizer.enabled = NO;
        self.tapGestureRecognizerRight.enabled = NO;
        [self userInteractionEnable:YES];
    }
    else if (position == FrontViewPositionRight){
        // NSLog(@"FrontViewPositionRight");
        // jump to left side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.enabled = YES;
        [self userInteractionEnable:NO];
        
    }
}



- (IBAction)alarmPickerValueChanged:(id)sender {
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

    NSDateComponents *dateComponents = [calendar
            components:NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitHour |NSCalendarUnitMinute |NSCalendarUnitSecond
            fromDate:self.alarmDatePicker.date];
    
    int alarmTime=(int)((dateComponents.hour << 8) | dateComponents.minute);
 
    [binsDataController setAlarmTime: alarmTime withEnable: self.alarmSetSwitch.isOn];
    
    
    self.alarmSetSwitch.on=NO;  // once changed, disable the alarm
    
}
- (IBAction)alarmSetSwitchValueChanged:(id)sender {

    [binsDataController setAlarmEnable: self.alarmSetSwitch.isOn];
    [deviceController updateAlarm];
    
}
@end
