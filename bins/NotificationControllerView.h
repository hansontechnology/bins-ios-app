//
//  NotificationControllerView.h
//  bins
//
//  Created by Dennis Kung on 11/2/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationControllerView : UIView

-(void) addNotification: (NSString*)noticeBody withAction: (NSString*) noticeAction;
-(void) stopNotification;

-(void)startAlertSound;
-(void)stopAlertSound;

@end
