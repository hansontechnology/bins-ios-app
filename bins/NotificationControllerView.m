//
//  NotificationControllerView.m
//  bins
//
//  Created by Dennis Kung on 11/2/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "NotificationControllerView.h"
#import <AVFoundation/AVFoundation.h>

@implementation NotificationControllerView

static AVAudioPlayer *audioPlayer;

-(NotificationControllerView*) init {
    
    [self prepareAudioSound];
    
    return [super init];
}


-(void) addNotification: (NSString*)noticeBody withAction: (NSString*) noticeAction {


    UILocalNotification *localNotif = [[UILocalNotification alloc] init];
    
    NSDate *eventDate=[NSDate dateWithTimeIntervalSinceNow:10];
    
    localNotif.fireDate = [eventDate dateByAddingTimeInterval:5];
    localNotif.timeZone = [NSTimeZone defaultTimeZone];
    
    
    localNotif.alertBody = noticeBody;
    
    localNotif.alertAction = noticeAction;
    
    localNotif.soundName = UILocalNotificationDefaultSoundName;
    // localNotif.applicationIconBadgeNumber = 0;
    
    localNotif.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] + 1;
    
    [[UIApplication sharedApplication] scheduleLocalNotification: localNotif];
    
    // Request to reload table view data
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    
}

-(void) stopNotification {
    [self stopAlertSound];
}

static NSTimer *soundAlertTimer=nil;

-(void)prepareAudioSound {
    NSString *path = [NSString stringWithFormat:@"%@/burglar_alarm.aif", [[NSBundle mainBundle] resourcePath]];
    NSURL *soundUrl = [NSURL fileURLWithPath:path];
    
    NSError *err1;
    
    
    NSLog(@"startAlertSound");
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    NSError *setCategoryError = nil;
    BOOL success = [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    if (!success) { NSLog(@"Still error from audio session"); }
    
    NSError *activationError = nil;
    success = [audioSession setActive:YES error:&activationError];
    if (!success) { /* handle the error condition */ }
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents]; // 12.8 very important otherwise wont work on background mode.
    
    // Create audio player object and initialize with URL to sound
    audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:soundUrl error:&err1];
    
    [[AVPlayerLayer layer] setPlayer: nil];
    
}

-(void) startAlertSound {
    
    //-- NSLog(@"audio player error: %@",[err1 description]);
    
    [audioPlayer setNumberOfLoops:100]; // repeat more times
    
    //-- [audioPlayer play];
    
    if([audioPlayer prepareToPlay])
    {
        //-- NSLog(@"audio play start");

        [audioPlayer play];
    } else {
        NSLog(@"audio play failed");
        
    }    
    
    /*
    if(soundAlertTimer!=nil){
        [soundAlertTimer invalidate];
        soundAlertTimer=nil;
    }
    soundAlertTimer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(soundAlertTimerRoutine) userInfo:services repeats:YES];
    */
}

-(void)stopAlertSound {
    if(soundAlertTimer!=nil){
        [soundAlertTimer invalidate];
        soundAlertTimer=nil;
    }
    [audioPlayer stop];
}

-(void)soundAlertTimerRoutine {
    
    if([audioPlayer prepareToPlay]){
        [audioPlayer play];
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
