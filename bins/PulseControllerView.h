//
//  PulseControllerView.h
//  bins
//
//  Created by Dennis Kung on 11/2/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATT3GFTT.h"
#import "DeviceViewController.h"
// #import "BinsTableViewController.h"


#define PULSE_STATUS_READING_STREAM 0
#define PULSE_STATUS_STOP_STREAMING 1
#define PULSE_STATUS_PREPARING_STREAM 2

@class PulseControllerView;
@class DeviceViewController;
@class BinsTableViewController;


@protocol PulseControllerDelegate

-(void)PulseControllerView: (PulseControllerView *)pulseController hasStatus: (int)status withMessage: (NSString*)stateMessage;

-(void)PulseControllerView: (PulseControllerView *)pulseController pulseResult: (int)pulse isReady:(BOOL)ready;


@end

@interface PulseControllerView : UIView


-(PulseControllerView*)init: (BinsTableViewController*)hostBinsView;
-(void)openNewSession;

-(void)newDataStream: (uint8_t*)rawDataStream;


@property (assign) id <PulseControllerDelegate> delegate;


@end
