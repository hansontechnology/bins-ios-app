//
//  PulseControllerView.m
//  bins
//
//  Created by Dennis Kung on 11/2/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "PulseControllerView.h"
#import "BinsTableViewController.h"
#import "BinsDataViewController.h"

@implementation PulseControllerView


static BinsTableViewController *binsTableViewController=nil;

static BinsDataViewController *binsDataController=nil;
static DeviceViewController* deviceController=nil;

@synthesize delegate = _delegate;

static bool isInitiated=NO;

static int hrdViewUpdateCyclicCounter=0;
static bool gHadPulseResultLastTime=NO;

static int iHR=0;
static int iReadyFlag = 0;
static int iMotionFlag = 0;
static int iTouchFlag = 0;

static NSTimer *pulseStreamTimeoutTimer=nil;

-(PulseControllerView*)init{
    
    self=[super init];
    
    if(isInitiated==YES) return self;
    isInitiated=YES;
    
    binsDataController=[[BinsDataViewController alloc] init];
    deviceController=[[DeviceViewController alloc] init];
    
    return self;
}

-(PulseControllerView*)init: (BinsTableViewController*)hostBinsView {
    binsTableViewController=hostBinsView;
    return [self init];
}


-(void) openNewSession {
    
    /*
    const int TYPE_SPO2 = 90;
    const int TYPE_HRD = 91;
    const int TYPE_HRD_GS = 0xB8 ;
    

    int odr=20;
    PXIALGMOTION.Close();
    PXIALGMOTION.Open(odr);
     
    */
    
    PxiAlg_EnableFastOutput(true);
    // PxiAlg_SetSigGradeThrd(50);

    PxiAlg_Close();
    PxiAlg_Open();
    
    // PxiAlg_SetAge();
    
    
    [self pulseStreamReadingTimerRoutine];
    
    if(pulseStreamReadingTimer!=nil){
        [pulseStreamReadingTimer invalidate];
        pulseStreamReadingTimer=nil;
    }
    if(pulseDeviceUpdateTimer!=nil){
        [pulseDeviceUpdateTimer invalidate];
        pulseDeviceUpdateTimer=nil;
    }
    pulseStreamReadingTimer= [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(pulseStreamReadingTimerRoutine) userInfo:nil repeats:YES];
    pulseDeviceUpdateTimer= [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(pulseDeviceUpdateTimerRoutine) userInfo:nil repeats:YES];

}

NSTimer *pulseStreamReadingTimer;
NSTimer *pulseDeviceUpdateTimer;
BOOL hasNewPulseStream=NO;

-(void)pulseDeviceUpdateTimerRoutine {
    //-- once every X 5 second
    if(iHR>0){ // && iReadyFlag==1){
        [deviceController pulseUpdate: iHR];
        NSLog(@"HRD writeback");
    }
}
-(void)pulseStreamReadingTimerRoutine {
    
    NSLog(@"HRD stable:ready:motion:iHR=%d:%d:%d",iReadyFlag,iTouchFlag,iHR);

    if(hrdViewUpdateCyclicCounter>250){	//-- 20151108
        if( (iHR>55 && iHR<110) ||
            (iHR>45 && iHR<180 && iReadyFlag==1))
        {
            [self insertPulseRecord: iHR];
        }
    }
    
    if(iHR>45 && iHR<180){
        [self.delegate PulseControllerView:self pulseResult:iHR isReady:gHadPulseResultLastTime];
    }
    
    
    if(iHR==0){
        NSString *statusMessage=[[NSString alloc] initWithFormat:@"Pulse calculating : %d",hrdViewUpdateCyclicCounter/20];

        [self.delegate PulseControllerView:self hasStatus:PULSE_STATUS_READING_STREAM withMessage:statusMessage ];
    }
    
    if(hasNewPulseStream==YES){
        if(pulseStreamTimeoutTimer!=nil){
            [pulseStreamTimeoutTimer invalidate];
            pulseStreamTimeoutTimer=nil;
        }
        pulseStreamTimeoutTimer= [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(pulseStreamTimeoutTimerRoutine) userInfo:nil repeats:NO];
    }
    
    hasNewPulseStream=NO;
}

-(void)newDataStream: (uint8_t*)rawData {

    self.delegate=binsTableViewController;  // 11.27
    
    float mems_data[] = {0, 0, 0};
    float lastMemsData[] = {0, 0, 0};
    float pulseNumber=0;

    uint8_t ppg_data [20];
                    
    
    for(int i=0; i<(13); i++)
        ppg_data[i] = rawData[i];
    
    for(int i=0; i<(3); i++){
        short gdata;
        gdata=(short)(rawData[13+i*2] + (rawData[13+i*2+1]<<8));
        gdata=(short)(gdata);
        lastMemsData[i]=gdata;
    }
                    
    //-- 09.08
    if(hrdViewUpdateCyclicCounter==0){
        [self.delegate PulseControllerView:self hasStatus:PULSE_STATUS_PREPARING_STREAM withMessage:@"New session" ];
 
        [self openNewSession];
        
    }
    hrdViewUpdateCyclicCounter++;
    hasNewPulseStream=YES;
    
    //-- 08.20
    //-- observed that, without mems data, it is easier a lot to fix to the result and sooner.
    //-- if fixed already, it is easy to keep the pulse result continuously
    //-- solution is not to use MEMS until
                    
    if(gHadPulseResultLastTime==false){
        PxiAlg_Process(ppg_data, mems_data);
    }else{
        PxiAlg_Process(ppg_data, lastMemsData);
    }
    PxiAlg_HrGet(&pulseNumber);
    iReadyFlag = PxiAlg_GetReadyFlag();
    iMotionFlag = PxiAlg_GetMotionFlag();
    // iTouchFlag = PxiAlg_GetTouchFlag();
    iHR=(int)pulseNumber;

    /*
    if(hrdViewUpdateCyclicCounter%100==0){
        if(pulseNumber>0){
            [deviceController pulseUpdate:(int)pulseNumber];
        }
    }
    */
    
   // 08.20
    if(pulseNumber>0 && iReadyFlag==1){
        gHadPulseResultLastTime=true;
    }
    else{
        gHadPulseResultLastTime=false;
    }
    
}


-(void) pulseStreamTimeoutTimerRoutine {

    NSLog(@"HRD View Timeout");
    
    [pulseStreamTimeoutTimer invalidate];
    pulseStreamTimeoutTimer=nil;
    
    
    if(pulseStreamReadingTimer!=nil){
        [pulseStreamReadingTimer invalidate];
        pulseStreamReadingTimer=nil;
    }
    if(pulseDeviceUpdateTimer!=nil){
        [pulseDeviceUpdateTimer invalidate];
        pulseDeviceUpdateTimer=nil;
    }

    
    // TODO stop view
    //--** PxiAlg_Close();
                
                
    hrdViewUpdateCyclicCounter=0;

    [self.delegate PulseControllerView:self hasStatus:PULSE_STATUS_STOP_STREAMING withMessage:@""];
    
}


-(void)insertPulseRecord: (int) iHR {
    NSInteger currentTimeInMinute=[deviceController localeTimeIntervalInMinute];

    NSLog(@"save pulse record:%ld : %d",(long)currentTimeInMinute, iHR);
    
    [binsDataController insertNewActivity:currentTimeInMinute withType:BINS_ACTIVITY_TYPE_PULSE withValue:iHR];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
