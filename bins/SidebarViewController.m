//
//  SidebarViewController.m
//  SidebarDemo
//
//  Created by Simon on 29/6/13.
//  Copyright (c) 2013 Appcoda. All rights reserved.
//

#import <ShareSDK/ShareSDK.h>

#import "SidebarViewController.h"
#import "SWRevealViewController.h"
// #import "PhotoViewController.h"
#import "DeviceViewController.h"
#import "BinsDataViewController.h"
#import "MenuTableViewCell.h"
#import "binsTabBarViewController.h"

@interface SidebarViewController ()

@property DeviceViewController *deviceController;
@property BinsDataViewController *binsDataController;


@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;

@end

@implementation SidebarViewController {
    NSArray *menuItems;
    NSArray *menuBinsX1Items;
    NSArray *menuActiveItems;
    
    NSDictionary *configurationMaskOfMenu;
    NSDictionary *menuTextForEnable;
    NSDictionary *menuTextForDisable;
    
    UIViewController *currentFrontViewController;

}

@synthesize deviceController;
@synthesize binsDataController;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}
/*
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
*/
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    /* 20150825
    menuItems = @[@"title", @"start_pulse_detection", @"bins_pulse_enable", @"auto_pulse_enable", @"pulse_sleep_enable", @"track_step_pulse", @"step_counter_reset", @"sleep_track_enable", @"display_brightness", @"bins_lock", @"optical_sensor_blink"];
    */
    menuItems = @[@"title", @"start_pulse_detection", @"bins_pulse_enable", @"track_step_pulse", @"step_counter_reset", @"display_brightness", @"filter_unused", @"share_screen"];
    
    // @"phone_notice_enable",@"background_sync_enable"

    menuBinsX1Items = @[@"title", @"step_counter_reset",  @"bins_lock", @"filter_unused", @"share_screen"];
    
    
    //-- @"phone_notice_enable", @"background_sync_enable",
    
    configurationMaskOfMenu=@{
                        @"start_pulse_detection" : [NSNumber numberWithInt:BINS_CONFIG_COMMAND_HRD_START_MASK],
                        @"bins_pulse_enable" : [NSNumber numberWithInt:BINS_CONFIG_HEARTRATE_ENABLE_MASK],
                        @"auto_pulse_enable" : [NSNumber numberWithInt:BINS_CONFIG_HRD_ALWAYS_ON_MASK],
                        @"pulse_sleep_enable" : [NSNumber numberWithInt:BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK],
                        @"track_step_pulse" : [NSNumber numberWithInt:BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK],
                        @"step_counter_reset" : [NSNumber numberWithInt:BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK],
                        @"sleep_track_enable" : [NSNumber numberWithInt:BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK],
                        @"phone_notice_enable" : [NSNumber numberWithInt:BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK],
                        @"display_brightness" : [NSNumber numberWithInt:BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK],
                        @"bins_lock" : [NSNumber numberWithInt:BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK],
                        @"optical_sensor_blink" : [NSNumber numberWithInt:BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK],
            };
    
    
    menuTextForEnable=@{          @"title" :                  NSLocalizedString(@"menu_control_title",nil),
                                  @"start_pulse_detection" :  NSLocalizedString(@"menu_start_hrd_title",nil),
                                  @"bins_pulse_enable" :      NSLocalizedString(@"menu_bins_heartrate_disable_title",nil),
                                  @"auto_pulse_enable" :      NSLocalizedString(@"menu_reset_auto_hrd_title",nil),
                                  @"pulse_sleep_enable" :     NSLocalizedString(@"menu_bins_heartrate_sleep_disable_title",nil),
                                  @"track_step_pulse" :       NSLocalizedString(@"menu_bins_heartrate_step_disable_title",nil),
                                  @"step_counter_reset" :     NSLocalizedString(@"menu_bins_step_counter_wholeday_title",nil),
                                  @"sleep_track_enable" :     NSLocalizedString(@"menu_bins_sleep_track_disable_title",nil),
                                  @"phone_notice_enable" :    NSLocalizedString(@"menu_bins_phone_notice_disable_title",nil),
                                  @"background_sync_enable":  NSLocalizedString(@"menu_bins_background_sync_disable_tite",nil),
                                  @"display_brightness" :     NSLocalizedString(@"menu_bins_display_bright_normal_title",nil),
                                  @"bins_lock" :              NSLocalizedString(@"menu_bins_lock_bins_title",nil),
                                  @"optical_sensor_blink" :   NSLocalizedString(@"menu_bins_oled_do_blink_title",nil),
                                  @"filter_unused" :           NSLocalizedString(@"menu_filter_unused_disable_title",nil),
                                  @"share_screen" :           NSLocalizedString(@"menu_share_screen",nil)
                                  };

    
    menuTextForDisable=@{         @"title" :                  NSLocalizedString(@"menu_control_title",nil),
                                  @"start_pulse_detection" :  NSLocalizedString(@"menu_start_hrd_title",nil),
                                  @"bins_pulse_enable" :      NSLocalizedString(@"menu_bins_heartrate_enable_title",nil),
                                  @"auto_pulse_enable" :      NSLocalizedString(@"menu_set_auto_hrd_title",nil),
                                  @"pulse_sleep_enable" :     NSLocalizedString(@"menu_bins_heartrate_sleep_enable_title",nil),
                                  @"track_step_pulse" :       NSLocalizedString(@"menu_bins_heartrate_step_enable_title",nil),
                                  @"step_counter_reset" :     NSLocalizedString(@"menu_bins_step_counter_reset_title",nil),
                                  @"sleep_track_enable" :     NSLocalizedString(@"menu_bins_sleep_track_enable_title",nil),
                                  @"phone_notice_enable" :    NSLocalizedString(@"menu_bins_phone_notice_enable_title",nil),
                                  @"background_sync_enable":  NSLocalizedString(@"menu_bins_background_sync_enable_tite",nil),
                                  @"display_brightness" :     NSLocalizedString(@"menu_bins_display_bright_high_title",nil),
                                  @"bins_lock" :              NSLocalizedString(@"menu_bins_lock_bins_title",nil),
                                  @"optical_sensor_blink" :   NSLocalizedString(@"menu_bins_oled_not_blink_title",nil),
                                  @"filter_unused" :           NSLocalizedString(@"menu_filter_unused_enable_title",nil),
                                  @"share_screen" :           NSLocalizedString(@"menu_share_screen",nil)
                                  };

    deviceController=[[DeviceViewController alloc] init];
    binsDataController=[[BinsDataViewController alloc] init];
    
    
    
    // Set the gesture
    // [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
   // self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    // self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:self.leftSwipeGestureRecognizer];
    // [self.view addGestureRecognizer:self.rightSwipeGestureRecognizer];
    
  
    self.tableView.alwaysBounceVertical = NO;
}

-(void) viewWillAppear:(BOOL)animated {
    
    binsTabBarViewController *tabBar=(binsTabBarViewController*)(self.revealViewController.frontViewController);
    
    UINavigationController *naviCon=(UINavigationController*)tabBar.selectedViewController;
    
    currentFrontViewController=naviCon.topViewController;
    
        
    if([binsDataController getActiveDeviceType]==BINS_MODEL_BINSX1){
        menuActiveItems=menuBinsX1Items;
    }
    else{
        menuActiveItems=menuItems;
    }

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    
    
    if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        

    }
    else if (sender.direction == UISwipeGestureRecognizerDirectionLeft){
        [self.revealViewController revealToggleAnimated: YES];
    }
    
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setDuration:0.25];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
                                  kCAMediaTimingFunctionEaseIn]];
    [self.view.window.layer addAnimation:animation forKey:@"fadeTransition"];
    
    
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [menuActiveItems count];
}

-(void)setMenuText: (UITableViewCell*)cell {
    
}

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
    if(indexPath.section==0 && indexPath.row==0){
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.backgroundColor=[UIColor colorWithRed:0x00/255.0 green:0x4B/255.0 blue:0x97/255.0 alpha:0.85];
 
    }
    
    /*
    if([binsDataController getActiveDeviceType]==BINS_MODEL_BINSX1){
        if([menuBinsX1Items containsObject:[menuItems objectAtIndex:indexPath.row]]){
            [cell setHidden:NO];
        } else {
            [cell setHidden:YES];
        }
    }
    else{
        [cell setHidden:NO];        
    }
    */
    
    
    if([cell isHidden]==NO){
        int menuBitMask=[[configurationMaskOfMenu objectForKey: [menuActiveItems objectAtIndex:indexPath.row]] intValue];
        int currentConfig=[binsDataController getActiveDeviceConfiguration];
        
        //-- NSLog(@"bintmask:%d:%d", currentConfig, menuBitMask);
        
        if((currentConfig & menuBitMask) !=0){
            
            cell.menuTextLabel.text=[menuTextForEnable objectForKey:[menuActiveItems objectAtIndex:indexPath.row]];
        
        } else{
            cell.menuTextLabel.text=[menuTextForDisable objectForKey:[menuActiveItems objectAtIndex:indexPath.row]];
            
        }
    }
    
    if(indexPath.row==[menuActiveItems indexOfObject:@"background_sync_enable"]){
        
        if([binsDataController getBackgroundSync] !=0){
            
            cell.menuTextLabel.text=[menuTextForEnable objectForKey:[menuActiveItems objectAtIndex:indexPath.row]];
            
        } else{
            cell.menuTextLabel.text=[menuTextForDisable objectForKey:[menuActiveItems objectAtIndex:indexPath.row]];
            
        }
    } else if(indexPath.row==[menuActiveItems indexOfObject:@"filter_unused"]){
        
        if([binsDataController getFilterUnused] !=0){
            
            cell.menuTextLabel.text=[menuTextForEnable objectForKey:[menuActiveItems objectAtIndex:indexPath.row]];
            
        } else{
            cell.menuTextLabel.text=[menuTextForDisable objectForKey:[menuActiveItems objectAtIndex:indexPath.row]];
            
        }
    }

    
    [cell setSelectionStyle:UITableViewCellSelectionStyleBlue];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section!=0) return 0;
    
    /*
    if([binsDataController getActiveDeviceType]==BINS_MODEL_BINSX1){
        if([menuBinsX1Items containsObject:[menuItems objectAtIndex:indexPath.row]]){
            return tableView.rowHeight;
        } else {
            return 1;
        }
    }
    */
    
    return tableView.rowHeight;
}
-(BOOL)tableView: (UITableView*)tableView shouldHighlightRowAtIndexPath:(NSIndexPath*)indexPath {
    
    // if(indexPath.section==0 && indexPath.row==0) return NO;
    
    return YES;
}

- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // Set the title of navigation bar by using the menu items
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    UINavigationController *destViewController = (UINavigationController*)segue.destinationViewController;
    destViewController.title = [[menuItems objectAtIndex:indexPath.row] capitalizedString];
    /*
    // Set the photo if it navigates to the PhotoView
    if ([segue.identifier isEqualToString:@"showPhoto"]) {
        PhotoViewController *photoController = (PhotoViewController*)segue.destinationViewController;
        NSString *photoFilename = [NSString stringWithFormat:@"%@_photo.jpg", [menuItems objectAtIndex:indexPath.row]];
        photoController.photoFilename = photoFilename;
    }
    */
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section > 0) return;
    /*
        menuItems = @[@"title", @"start_pulse_detection", @"bins_pulse_enable", @"auto_pulse_enable", @"pulse_sleep_enable", @"track_step_pulse", @"step_counter_reset", @"sleep_track_enable", @"phone_notice_enable", @"display_brightness", @"bins_lock", @"optical_sensor_blink"];
    */
    
    
    if(indexPath.row==[menuItems indexOfObject:@"start_pulse_detection"]){
        [deviceController startHeartrateDetection];
    } else if(indexPath.row==[menuItems indexOfObject:@"bins_pulse_enable"]){
        [deviceController setHeartRateEnable];
    } else if(indexPath.row==[menuItems indexOfObject:@"auto_pulse_enable"]){
        [deviceController setAutoHeartrateDetection];
    } else if(indexPath.row==[menuItems indexOfObject:@"pulse_sleep_enable"]){
        [deviceController setHeartRateSleepEnable];
    } else if(indexPath.row==[menuItems indexOfObject:@"track_step_pulse"]){
        [deviceController setStepCheckWhenHRDEnable];
    } else if(indexPath.row==[menuItems indexOfObject:@"step_counter_reset"]){
        [deviceController setBinsDailyStepReset];
    } else if(indexPath.row==[menuItems indexOfObject:@"sleep_track_enable"]){
        [deviceController toggleSleepTrackEnable];
    } else if(indexPath.row==[menuItems indexOfObject:@"phone_notice_enable"]){
        [deviceController setPhoneNotice];
    } else if(indexPath.row==[menuItems indexOfObject:@"background_sync_enable"]){
        [binsDataController toggleBackgroundSync];
        [deviceController timerTaskUpdate];
    } else if(indexPath.row==[menuItems indexOfObject:@"display_brightness"]){
        [deviceController setBinsX2DisplayBrightness];
    } else if(indexPath.row==[menuItems indexOfObject:@"bins_lock"]){
        [deviceController setBinsLock];
    } else if(indexPath.row==[menuItems indexOfObject:@"optical_sensor_blink"]){
        [deviceController setBinsX2SensorBlink];
    } else if(indexPath.row==[menuItems indexOfObject:@"filter_unused"]){
        [binsDataController toggleFilterUnused];
    } else if(indexPath.row==[menuItems indexOfObject:@"share_screen"]){
        NSLog(@"share screen");
        
        
        //-- UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        UIGraphicsBeginImageContext(screenRect.size);

        [currentFrontViewController.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *theImage=UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        
        /*
        // create graphics context with screen size
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        UIGraphicsBeginImageContext(screenRect.size);
        CGContextRef ctx = UIGraphicsGetCurrentContext();
        [[UIColor blackColor] set];
        CGContextFillRect(ctx, screenRect);
        
        // grab reference to our window
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        
        // transfer content into our context
        [window.layer renderInContext:ctx];
        UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        */
        
        
        
        NSData*theImageData=UIImagePNGRepresentation(theImage); //you can use PNG too
        
        // Get the documents directory
        NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(
                                                       NSDocumentDirectory, NSUserDomainMask, YES);

        NSString *fileDir = [dirPaths objectAtIndex:0];
        
        // Build the path to the database file
        NSString *filePath = [[NSString alloc]
                                  initWithString: [fileDir stringByAppendingPathComponent:
                                                   @"binsScreenCaptured.png"]];

        NSLog(@"share screen image on dir %@", fileDir);
        
        [theImageData writeToFile:filePath atomically:YES];
        

        
        //-- 20150831 ShareSDK part
        
        //NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"ShareSDK"  ofType:@"jpg"];  //make sure you actually have the picture in your project
        
        //Create a share content object
        id<ISSContent> publishContent = [ShareSDK content:@"Share Content"
                                           defaultContent:@" The default share content. No content displayed"
                                                    image:[ShareSDK imageWithPath:filePath]
                                                    title:@"ShareSDK"
                                                      url:@"http://www.sharesdk.cn"
                                              description:@"BiNS App to share screen"
                                                mediaType:SSPublishContentMediaTypeNews];
        
        //show share content view
        [ShareSDK showShareActionSheet:nil
                             shareList:nil
                               content:publishContent
                         statusBarTips:YES
                           authOptions:nil
                          shareOptions: nil
                                result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end)
                                        {
                                            if (state == SSResponseStateSuccess)
                                            {
                                                NSLog(@"Share Success!");
                                            }
                                            else if (state == SSResponseStateFail)
                                            {
                                                NSLog(@"Share Fail,Error code:%ld,Error description:%@", (long)[error
                                                                         errorCode], [error errorDescription]);
                                            }
                                        }
         ];
        

        
    } else if(indexPath.row==[menuItems indexOfObject:@"title"]){
        //-- [self.revealViewController revealToggleAnimated: YES];  // 12.02
    }
        
    
    [tableView deselectRowAtIndexPath:indexPath animated: YES];
    
    [tableView reloadData];
    
    [currentFrontViewController viewWillAppear:YES];
    
    [self.revealViewController revealToggleAnimated: YES];  // 12.02

}


@end
