//
//  SleepTableViewController.h
//  bins
//
//  Created by Dennis Kung on 10/25/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeControllerView.h"
#import "MIRadioButtonGroup.h"
#import "BinsDataViewController.h"
#import "SWRevealViewController.h"
#import "CorePlot-CocoaTouch.h"
#import "DeviceViewController.h"



@interface SleepTableViewController : UITableViewController <TimeControllerViewDelegate,RadioButtonGroupDelegate,CPTBarPlotDataSource,CPTPlotSpaceDelegate, SWRevealViewControllerDelegate, BinsDataControllerDelegate>

@property (weak, nonatomic) IBOutlet TimeControllerView *timeControllerView;

- (IBAction)leftButtonClick:(id)sender;
- (IBAction)rightButtonClick:(id)sender;

@property (weak, nonatomic) IBOutlet UITableViewCell *sleepSetViewCell;

@property BinsDataViewController *binsDataController;
@property DeviceViewController *deviceController;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UITableViewCell *activityDisplayView;

@property (weak, nonatomic) IBOutlet UIButton *leftButton;

@property (weak, nonatomic) IBOutlet UIButton *rightButton;


@property (weak, nonatomic) IBOutlet UIView *sleepSetView;

@property (weak, nonatomic) IBOutlet UIView *sleepDisplayView;

@property (weak, nonatomic) IBOutlet UIView *lightDisplayView;

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UITableViewCell *timeSetViewCell;
- (IBAction)dateSelectPickerValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lightSleepLabel;
@property (weak, nonatomic) IBOutlet UILabel *deepSleepLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalSleepLabel;
@property (weak, nonatomic) IBOutlet UITableViewCell *totalSleepTimeCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *totalSleepTimeResultCell;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *userSelectButton;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizerRight;

@property (weak, nonatomic) IBOutlet UITableViewCell *timeTableCell;
@property (weak, nonatomic) IBOutlet UITableViewCell *datePickerTableCell;
@property (strong, nonatomic) IBOutlet UITableView *sleepTableView;


enum SleepStage {
    SLEEP_STAGE_DEEP_SLEEP=1,
    SLEEP_STAGE_LIGHT_SLEEP=3,  //-- 20160117 from 2 to 3
    SLEEP_STAGE_LIGHT_WAKE=4,
    SLEEP_STAGE_NORMAL_WAKE=5,
    SLEEP_STAGE_ACTIVE=7,
    SLEEP_STAGE_INVALID=100
};

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *workingIndicator;

@end
