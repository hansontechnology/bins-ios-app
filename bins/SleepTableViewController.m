//
//  SleepTableViewController.m
//  bins
//
//  Created by Dennis Kung on 10/25/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "SleepTableViewController.h"
#import "LevelPeriodBuilder.h"


@interface SleepTableViewController ()

@property (nonatomic, strong) CPTGraphHostingView *hostView;

@property (nonatomic, strong) CPTGraphHostingView *lightHostView;

@property (nonatomic, strong) CPTTheme *selectedTheme;

@property (assign) int mMaxValue ;

@property (assign) int mLightMaxValue ;


@end

@implementation SleepTableViewController

@synthesize hostView = hostView_;
@synthesize lightHostView = lightHostView_;
@synthesize selectedTheme = selectedTheme_;
@synthesize timeControllerView;
@synthesize binsDataController;
@synthesize mMaxValue ;
@synthesize mLightMaxValue ;
@synthesize deviceController;
@synthesize workingIndicator;

#define INTEGER_MAX 0x7fffffff

#define AXIS_COUNT_X 100
#define AXIS_COUNT_Y 10


static int mLightSleepTime;
static int mDeepSleepTime;
static int mTotalSleepTime;


static int sleepMeasuringTimeUnitInMinutes;
static int axisCountX[] = { 48, 72, 96}; // must follow the order

static int mValues[AXIS_COUNT_X];
static int mMinutesPerBar=1;

static int *mLightStages;
static int mLightNumberOfBars=0;
static CPTBarPlot *mLightPlot=nil;

static int mSleepStages[AXIS_COUNT_X];
#define NUMBER_SLEEP_QUALITY_RECORD_DAY 24*60/5
static int mSleepQualityRecords[NUMBER_SLEEP_QUALITY_RECORD_DAY];

static int mSleepStartBarIndex= -1;
static int mSleepEndBarIndex= -1;
static int mNumberOfBars=0;

static int mViewMode=VIEW_MODE_UNIT_DAY;

static NSArray *dayLabels;
static NSArray *monthLabels;

static NSDictionary *data;
NSDictionary *sets;

NSArray *dates;

CPTColor *endColor;

MIRadioButtonGroup *sleepEnableRadio;

#define HEIGHT_IPHONE5 568 // 12.22

static NSString *PLOT_LIGHT_GRAPH_IDENTIFIER=@"light graph plot";

static int heightOfPhone;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    [self generateData];
    
    [self configureHost];
    
    [self configureLightViewHost];
    
    
    mViewMode=[timeControllerView getBrowseUnit];
 
  /*
    [UIView appearanceWhenContainedIn:[UITableView class], [UIDatePicker class], nil].backgroundColor = [UIColor colorWithWhite:1 alpha:1];
   */
    self.datePicker.backgroundColor=COLOR_TIME_PICKER_NEW3;

    timeControllerView.delegate = self;
    
    [timeControllerView setUserInteractionEnabled:YES];
    
    binsDataController=[[BinsDataViewController alloc] init];
    deviceController=[[DeviceViewController alloc] init];
    
    self.timeSetViewCell.hidden=YES;
    
    
    NSArray *options =[[NSArray alloc]initWithObjects:[NSString localizedStringWithFormat:NSLocalizedString(@"setting_on",nil) ],[NSString localizedStringWithFormat:NSLocalizedString(@"setting_off",nil) ],nil];
    CGRect radioButtonRect=self.sleepSetView.bounds;
    
    // MIRadioButtonGroup *group =[[MIRadioButtonGroup alloc]initWithFrame:CGRectMake(0, 0, 150, 30) andOptions:options andColumns:2];
    MIRadioButtonGroup *group =[[MIRadioButtonGroup alloc]initWithFrame:radioButtonRect andOptions:options andColumns:2];
    [self.sleepSetView addSubview:group];
    self.sleepSetView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];

    
    
    mViewMode=VIEW_MODE_UNIT_DAY;
    
    [group setSelected:0];
    
    group.delegate=self;
    sleepEnableRadio=group;
    
    
    // Change button color
    _sidebarButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
    
    
    // Set the gesture
    //-- [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    // Change button color
    _userSelectButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _userSelectButton.target = self.revealViewController;
    _userSelectButton.action = @selector(rightRevealToggle:);

    
    NSString *leftArrow =@"\U000025C0\U0000FE0E";
    
    NSString *rightArrow =@"\U000025B6\U0000FE0E";
    
    
    [self.leftButton setTitle:leftArrow forState:UIControlStateNormal];
    
    [self.rightButton setTitle:rightArrow forState:UIControlStateNormal];
    
    self.leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;

    endColor=[CPTColor colorWithComponentRed:0x00 green:0x00 blue:0xCD alpha:1];

    self.lightSleepLabel.textColor=[UIColor whiteColor];
    self.deepSleepLabel.textColor=[UIColor whiteColor];

    self.totalSleepTimeCell.layer.shadowOffset = CGSizeMake(5, 5);
    self.totalSleepTimeCell.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.totalSleepTimeCell.layer.shadowRadius = 15;
    self.totalSleepTimeCell.layer.shadowOpacity = .5;

    
    self.totalSleepTimeResultCell.layer.shadowOffset = CGSizeMake(5, 5);
    self.totalSleepTimeResultCell.layer.shadowColor = [[UIColor blackColor] CGColor];
    self.totalSleepTimeResultCell.layer.shadowRadius = 15;
    self.totalSleepTimeResultCell.layer.shadowOpacity = .5;

    /*
    CGRect shadowFrame = self.totalSleepTimeCell.layer.bounds;
    CGPathRef shadowPath = [UIBezierPath bezierPathWithRect:shadowFrame].CGPath;
    self.totalSleepTimeCell.layer.shadowPath = shadowPath;
     */
    
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(revealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer.enabled = NO;
    
    self.tapGestureRecognizerRight = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(rightRevealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
    self.tapGestureRecognizerRight.enabled = NO;
    
    self.revealViewController.delegate=self;
    

    heightOfPhone=self.sleepTableView.frame.size.height;
}

- (void)radioButtonGroup:(MIRadioButtonGroup*)radioButtonGroup clicked:(int)index {
    
}
- (void)radioButtonGroup:(MIRadioButtonGroup*)radioButtonGroup buttonClicked:(UIButton*)button{
    
    NSLog(@"sleep button %@", button.titleLabel.text);
    
    NSString *radioButtonStr=button.titleLabel.text;
    
    if([radioButtonStr isEqual:NSLocalizedString(@"setting_on",nil)]){
        NSLog(@"sleep activated");
        [binsDataController setSleepEnable:YES];
    }
    else{
        [binsDataController setSleepEnable:NO];

    }

    [deviceController updateSleepTrackEnable]; // 12.25
}


-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    BOOL sleepEnable=[binsDataController getSleepEnable];
    
    if(sleepEnable==YES){
        NSLog(@" sleep YES");
        [sleepEnableRadio setSelected:0];
    } else { // off
        NSLog(@" sleep NO");
        
        [sleepEnableRadio setSelected:1];
    }
    
    self.revealViewController.delegate=self;
    
    [timeControllerView refresh];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)leftButtonClick:(id)sender {
    [self.timeControllerView changeDate: -1];
}

- (IBAction)rightButtonClick:(id)sender {
    [self.timeControllerView changeDate: 1];
}


#pragma mark - retrive data from database

NSString* stringTimeFormatFromMinute(int totalMinutes)
{
    int hour=totalMinutes /60;
    int minute=totalMinutes-hour*60;
    NSString* message=@"";
    
    if(hour!=0){
        message=[NSString stringWithFormat:@"%d %@", hour, NSLocalizedString(@"hour",nil)];
    }
    if(minute!=0){
        message=[NSString stringWithFormat:@"%@ %d %@", message, minute, NSLocalizedString(@"minute",nil)];
    }
    
    if(totalMinutes==0){
        message =@" 0 ";
    }
    
    return message;
}

-(void) queryData: (NSDate*)currentDate {
    
    NSLog(@"queryData ...");
    
    int numberOfRecords=0;
    
    NSInteger currentTime=[binsDataController timeIntervalInMinute: currentDate withUnitMinute:YES];
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate * now = [NSDate date];
    
    NSDateComponents *dayComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond fromDate:now];
    
    if(dayComponents.hour>=12){
        currentTime=currentTime+(12*60); // 12 hours
    }
    else{
        currentTime=currentTime-(12*60);
    }
    
    NSInteger showingStartTime, showingEndTime;
    
    //-- if(mViewMode==VIEW_MODE_UNIT_DAY)
    {
        showingStartTime=currentTime;
        showingEndTime=currentTime+(DAY_MINUTES);
        numberOfRecords=48;
    }
    
    NSLog(@"sleep queryData: show:%ld,%ld", (long)showingStartTime,(long)showingEndTime);
    
    int user=[binsDataController getActiveUserID];
    
    binsDataController.delegate=self;
    int queryResult;
    if([workingIndicator isAnimating]==YES){
        dispatch_async(dispatch_get_main_queue(), ^{
            [workingIndicator stopAnimating];
        });
        queryResult=[binsDataController querySleepRecords: user timeFrom: showingStartTime timeTo: showingEndTime];
    }else{
        queryResult=[binsDataController querySleepRecordsWithCloudLoad: user timeFrom: showingStartTime timeTo: showingEndTime];
    }
    
    
    if(queryResult==QUERY_STOPPED) return;
    
    if(queryResult==QUERY_WORKING){
        dispatch_async(dispatch_get_main_queue(), ^{
            [workingIndicator startAnimating];
        });
        
        return;
    }
    
    // this object will continue to use the queried data
    
    LevelPeriodBuilder *levelPeriod=[[LevelPeriodBuilder alloc] initWithSleepPeriodFrom:showingStartTime to:showingEndTime];
    
    
    if([levelPeriod getCount]>0){
        [self displayUpdate: levelPeriod];
    }
    
    [self updateSleepQuality];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.lightSleepLabel.text=stringTimeFormatFromMinute(mLightSleepTime);
        self.deepSleepLabel.text=stringTimeFormatFromMinute(mDeepSleepTime);
        self.totalSleepLabel.text=stringTimeFormatFromMinute(mTotalSleepTime);
    });
    
}


-(void)displayUpdate: (LevelPeriodBuilder*)levelPeriod {
    /* 20150723
     [self resetSleep: mViewMode unitOf:5];
     
     int sleepQualityValue;
     NSInteger sleepTime;
     while([binsDataController getNextActivityFromQuery:&sleepQualityValue atTime:&sleepTime]==YES){
     
     [self setSleep: (sleepTime-showingStartTime) withQuality: sleepQualityValue];
     
     }
     */
    
    mViewMode=2;	// force to set to 0
	   
    mNumberOfBars=axisCountX[mViewMode];
    
    mSleepStartBarIndex=-1;
    mSleepEndBarIndex=-1;
    
    mLightSleepTime=mDeepSleepTime=mTotalSleepTime=0;
    
    long samplesPerPeriod=[levelPeriod getCount];
    int sampleTimeWindow=[levelPeriod getTimeWindow];
    
    mMinutesPerBar= (int)((samplesPerPeriod*sampleTimeWindow) / mNumberOfBars);
    
    
    for(int i=0; i<NUMBER_SLEEP_QUALITY_RECORD_DAY; i++) mSleepQualityRecords[i]=-1;
    
    for(int i=0; i<AXIS_COUNT_X; i++) mSleepStages[i]=SLEEP_STAGE_INVALID;
    
    int *activityMovements=[levelPeriod mActivityMovements];
    mLightStages=[levelPeriod getLightLevels];
    mLightNumberOfBars=(int)[levelPeriod getLightNumbers];
	   
    for(int i=0; i<samplesPerPeriod;i++){
        
        int index = (int)((float)(i*sampleTimeWindow) / mMinutesPerBar);
        
        
        if(mSleepQualityRecords[index]<0){
            mSleepQualityRecords[index]=activityMovements[i];
        }
        else{
            mSleepQualityRecords[index]+=activityMovements[i];
        }
        
        //-- NSLog(@"sleep quality=%d,%d,%d,%d",
        //      i,index,mSleepQualityRecords[index],activityMovements[i]);
        
    }
    
    for(int i=0; i<mNumberOfBars;i++){
        if(mSleepQualityRecords[i]>=0){
            int normalizedMovements= (int)((float)(mSleepQualityRecords[i]/mMinutesPerBar)*sampleTimeWindow);
            
            mSleepStages[i]=[levelPeriod getThreshIdFromMovements:normalizedMovements];
            
            //-- NSLog(@"sleep stages=%d(%d):%d (%d)",i,normalizedMovements, mSleepStages[i], mSleepQualityRecords[i]);
        }
    }
    
    //-- 20160117 redundant no need, [self updateSleepQuality]; 

}

-(void) updateSleepQuality {
    
    /* respect to every analysis */
    //-- normalize if only short period of time
    
    for (int i = 0; i < mNumberOfBars; i++) {
        if(i<(mNumberOfBars-2) &&
           mSleepStages[i]==mSleepStages[i+2] &&
           mSleepStages[i+1]!=mSleepStages[i] ){
            
           	mSleepStages[i+1]=mSleepStages[i];
            
        }
    }
    
    
    for (int i = 0; i < mNumberOfBars; i++) {
        if(i>=2 && i<mNumberOfBars-2 &&
           /* 20150829
            mSleepStages[i]<=SLEEP_STAGE_LIGHT_SLEEP &&
            mSleepStages[i-2]>=SLEEP_STAGE_LIGHT_WAKE &&
            mSleepStages[i-1]>=SLEEP_STAGE_LIGHT_WAKE &&
            mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE &&
            mSleepStages[i+2]>=SLEEP_STAGE_LIGHT_WAKE
            */
           mSleepStages[i]<=SLEEP_STAGE_LIGHT_SLEEP &&
           mSleepStages[i-1]>=SLEEP_STAGE_LIGHT_WAKE &&
           mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE
           )
        {
            mSleepStages[i]=mSleepStages[i-1];
        }
    }
    
    mSleepStartBarIndex=-1;
    mSleepEndBarIndex=-1;
    
    //-- if not in a sleep time, then it only can be light wake-up, no sleep stages assigned
    for (int i = 0; i < mNumberOfBars; i++) {
        if(mSleepStartBarIndex<0 &&
           mSleepStages[i]>=SLEEP_STAGE_LIGHT_WAKE &&
           (
            ((i<(mNumberOfBars-1) && mSleepStages[i+1]==SLEEP_STAGE_DEEP_SLEEP) ||
             (i<(mNumberOfBars-2) &&
        				  mSleepStages[i+1]==SLEEP_STAGE_LIGHT_SLEEP &&
        				  (mSleepStages[i+2]<=SLEEP_STAGE_LIGHT_SLEEP	  ))
        			  )
            )
        	  )
        {
        	   mSleepStartBarIndex=i+1;
        }
        
    }

    /* For early sleep situation, double confirm if it is true
     *
     */
    int barsPerHour=mNumberOfBars/24; // how many bars in one hour
    if(mSleepStartBarIndex<=(barsPerHour*9)){ // if before 9PM, early sleep, re-confirming
    	   for (int i = mSleepStartBarIndex+1; i < (barsPerHour*12); i++) {
               if(	mSleepStages[i]>=SLEEP_STAGE_LIGHT_WAKE &&
                  mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE &&
                  mSleepStages[i+2]>=SLEEP_STAGE_LIGHT_WAKE &&
                  mSleepStages[i+3]<=SLEEP_STAGE_LIGHT_SLEEP &&
                  mSleepStages[i+3]<=SLEEP_STAGE_LIGHT_SLEEP 		)
               {
                   mSleepStartBarIndex=i+3;
               }
           }
    }
    
    if(mSleepStartBarIndex<0){
    	   mSleepStartBarIndex=0;
    }
    
    for (int i = mSleepStartBarIndex; i < mNumberOfBars; i++) {
        
        //-- 150618
        if(
           mSleepStages[i]>=SLEEP_STAGE_LIGHT_WAKE &&
           mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE	&&
           i>=(barsPerHour*19) )		//-- morning time
        {
        	   break;
        }
        
        if(!(mSleepStages[i]==SLEEP_STAGE_DEEP_SLEEP ||
             (i>=1 &&
              mSleepStages[i]<=SLEEP_STAGE_LIGHT_SLEEP &&
              mSleepStages[i-1]<=SLEEP_STAGE_LIGHT_WAKE	 &&
              mSleepStages[i]!=0))
           )
        {
            continue;
        }
        
        //-- Log.i(TAG, "sleep-end 2:"+i+":"+mSleepStages[i]+":"+mSleepStages[i+1]+":"+mSleepStages[i+2]);
        
        BOOL foundSleepEnd=false;
        
        if(	(i<(mNumberOfBars-2) && (mSleepStages[i+2]>=SLEEP_STAGE_LIGHT_WAKE || mSleepStages[i+2]==0))
           &&
           (i<(mNumberOfBars-1) && (mSleepStages[i+1]>=SLEEP_STAGE_LIGHT_WAKE || mSleepStages[i+1]==0))
           )
        {
        	   foundSleepEnd=true;
        }
        
        if(i<(mNumberOfBars-3) && checkSleepEnd(i+1)==true){
        	   foundSleepEnd=true;
        }
        
        if(i<(mNumberOfBars-2) && mSleepStages[i+1]==0 && mSleepStages[i+2]==0){
        	   foundSleepEnd=true;
        }
        
        if(foundSleepEnd==true)
        {
        	   mSleepEndBarIndex=i;
        }
        
        
    }
    
    //-- if not in a sleep time, then it only can be light wake-up, no sleep stages assigned
    mLightSleepTime=mDeepSleepTime=mTotalSleepTime=0;
    
    
    NSLog(@"sleep period start, end: %d, %d",mSleepStartBarIndex,mSleepEndBarIndex);
    
    for (int i = 0; i < mNumberOfBars; i++) {
        
        if(i<mSleepStartBarIndex) continue;
        if(i>mSleepEndBarIndex && mSleepEndBarIndex>0) continue;
    	   
        int threshId=mSleepStages[i];
    	   
        if(threshId==0) continue;
    	   
        if(threshId<=SLEEP_STAGE_DEEP_SLEEP){
            mDeepSleepTime++;
        }
        else if(threshId<=SLEEP_STAGE_LIGHT_SLEEP){
            mLightSleepTime++;
        }

    }
    
    mDeepSleepTime *= mMinutesPerBar;
    mLightSleepTime *= mMinutesPerBar;
    mTotalSleepTime = mDeepSleepTime + mLightSleepTime;

}

#pragma mark - preparing graph data


-(void)resetSleep: (int)vm unitOf: (int)measuringTimeUnit {
    
    NSLog(@"resetSleep ...");

    if(measuringTimeUnit<=0){
        return;
    }
    
    sleepMeasuringTimeUnitInMinutes=measuringTimeUnit;
    
    for(int i=0; i<AXIS_COUNT_X; i++) mValues[i]=-1;
    for(int i=0; i<NUMBER_SLEEP_QUALITY_RECORD_DAY; i++) mSleepQualityRecords[i]=-1;
    
    
    mViewMode=vm;
    
    mNumberOfBars=axisCountX[mViewMode];
    
    //-- NSLog(@"resetSleep number bars %d",mNumberOfBars);
    
}

-(int)barIndexFrom: (NSInteger)timeShift
{
    int timePerBar;
    
    if(mViewMode==VIEW_MODE_UNIT_DAY) {
        
        timePerBar= DAY_MINUTES /mNumberOfBars;
    }else if(mViewMode==VIEW_MODE_UNIT_MONTH){
        timePerBar= DAY_MINUTES/2;
    }else{
        timePerBar= 60/mNumberOfBars;
        // 60 minutes in one hour
    }
    long index = timeShift / timePerBar;
    
    if(index>=mNumberOfBars){
        index=(mNumberOfBars-1);
        NSLog(@"Something wrong with index");
    }
    if(index<0){ //-- 08.14
        index=0;
        //-- NSLog(@"setSteps: wrong time assigned, time was:%d", timeShift);
    }
    
    return (int)index;
}

BOOL checkSleepEnd(int index){
	   int wakeSum=0;
	   int sleepSum=0;
	   
	   for(int i=0; i<3; i++){
           if(mSleepStages[i+index]<=SLEEP_STAGE_LIGHT_SLEEP) sleepSum++;
           if(mSleepStages[i+index]>=SLEEP_STAGE_LIGHT_WAKE &&
              mSleepStages[i+index]!=SLEEP_STAGE_INVALID) wakeSum++;
       }
	   
	   if(wakeSum>=2) return true;
	   
	   return false;
}


-(void)setSleep: (NSInteger)time withQuality: (int)sleepQuality {
    
    int stepWidth=1;
    if(mViewMode==VIEW_MODE_UNIT_DAY){
        stepWidth= DAY_MINUTES / axisCountX[mViewMode];
    } else {
        NSLog(@"sleep mode not implemented");
        return;
    }
    int index=[self barIndexFrom:time];
    
    if(index>=axisCountX[mViewMode]) index=(axisCountX[mViewMode]-1);
    if(index<0){ //-- 08.14
        index=0;
        NSLog(@"setSleep: wrong time assigned");
    }

    //-- NSLog(@"setSleep index time value:%d %d, %d", index, time, sleepQuality);
    
    if(mValues[index]==-1){
        mValues[index]=sleepQuality;
    }
    else{
        mValues[index] += sleepQuality;
    }

    mSleepQualityRecords[time/(sleepMeasuringTimeUnitInMinutes*60)]=sleepQuality;;

}


- (void)generateData
{
    NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
    
    //Array containing all the dates that will be displayed on the X axis
    dates = [NSArray arrayWithObjects:@"2012-05-01", @"2012-05-02", @"2012-05-03",
             @"2012-05-04", @"2012-05-05", @"2012-05-06", @"2012-05-07", nil];
    
    //Array containing all the dates that will be displayed on the X axis
    dayLabels = [NSArray arrayWithObjects:@"", @"4", @"8", @"12", @"16", @"20", @"", nil];
    monthLabels = [NSArray arrayWithObjects:@"", @"5", @"10", @"15", @"20", @"25", @"", nil];
    
    
    //Dictionary containing the name of the two sets and their associated color
    //used for the demo
    sets = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blueColor], @"Plot 1",
            [UIColor redColor], @"Plot 2",
            [UIColor greenColor], @"Plot 3", nil];
    
    //Generate random data for each set of data that will be displayed for each day
    //Numbers between 1 and 10
    for (NSString *date in dates) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        for (NSString *set in sets) {
            NSNumber *num = [NSNumber numberWithInt:arc4random_uniform(10)+1];
            [dict setObject:num forKey:set];
        }
        [dataTemp setObject:dict forKey:date];
    }
    
    data = [dataTemp copy];
    
    // NSLog(@"%@", data);
    
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 8;
}

-(BOOL)tableView: (UITableView*)tableView shouldHighlightRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return NO;
}

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section!=0) return 0;

    float activeHeightFactor=[self getHeightFactor];
    
    if(indexPath.row==0){
        return 50;
    } else if(indexPath.row==1){
        if(self.timeSetViewCell.hidden==YES){
            return 0;
        }
        else{
            return 160;
        }

    } else if(indexPath.row==2){
        return 40;
    } else if(indexPath.row==3){  // sleep graph part
        float h=[self getGraphCellHeight];
        return  h; // 250; // 12.20 200;
    } else if(indexPath.row==4){  // different sleep stage labels
        return (60.0*activeHeightFactor);
    } else if(indexPath.row==5){  // label Time Slept
        float h = (30.0*activeHeightFactor);
        // if(h>40) h=40;
        return h;
    } else if(indexPath.row==6){  // sleep result
        float h=(40.0*activeHeightFactor);
        // if(h>50) h=50;
        return h;
    } else if(indexPath.row==7){  // light display
        float h=(40.0);
        return h;
    }

    return 80; // tableView.rowHeight;
}

-(void)tableView: (UITableView*)tableView willDisplayCell:(UITableViewCell*)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.section!=0) return;
    
    if(indexPath.row==0){
        
        [cell setBackgroundColor:COLOR_TIME_CONTROLLER_BAR];
        
    }else if(indexPath.row==2){
        UIColor *bcolor=[endColor uiColor];
        
        [cell setBackgroundColor:[bcolor colorWithAlphaComponent:0.2]];
        
        // [cell setBackgroundColor:[UIColor lightGrayColor]];

        
    } else if(indexPath.row==3){
        // UIColor *bcolor=[endColor uiColor];
        
        // [cell setBackgroundColor:[bcolor colorWithAlphaComponent:0.8]];
        // [cell setBackgroundColor:[UIColor greenColor]];

        
    } else if(indexPath.row==4){
        UIColor *bcolor=[endColor uiColor];
        
        [cell setBackgroundColor:[bcolor colorWithAlphaComponent:1]];
        // [cell setBackgroundColor:[UIColor yellowColor]];
        
    }
    
}

- (void)timeControllerView:(TimeControllerView *)timeController browseModeChanged:(int)mode {
    
    mViewMode=mode;
    
    [self queryData: [timeControllerView getCurrentDate]];
    
    // [self reloadPlot];
    
    
}

- (void)timeControllerView:(TimeControllerView *)timeControllerView timeDidChange:(NSDate*)dateChanged {
    
    NSLog(@"delegate called");
    
    
    /*
     NSMutableDictionary *dataTemp = [[NSMutableDictionary alloc] init];
     
     //Generate random data for each set of data that will be displayed for each day
     //Numbers between 1 and 10
     for (NSString *date in dates) {
     NSMutableDictionary *dict = [NSMutableDictionary dictionary];
     for (NSString *set in sets) {
     NSNumber *num = [NSNumber numberWithInt:arc4random_uniform(10)+1];
     [dict setObject:num forKey:set];
     }
     [dataTemp setObject:dict forKey:date];
     }
     
     data = [dataTemp copy];
     */
    
    [self queryData: dateChanged];
    
    [self reloadPlot];
    // [self.hostView.hostedGraph reloadData];
    
}

- (void)timeControllerView:(TimeControllerView *)timeController touchUpInside:(NSDate*)dateNow {
    
    NSLog(@"delegate touch called");
 
    if(self.timeSetViewCell.hidden==YES){
        self.timeSetViewCell.hidden=NO;
        self.datePicker.maximumDate=[NSDate date];
        [self.datePicker setDate:[timeController getCurrentDate]];
        
        
    }else{
        self.timeSetViewCell.hidden=YES;
    }

    [self.tableView reloadData];
    
}

#define HEIGHT_TABLEVIEW_IPHONE_5 568
#define HEIGHT_GRAPH_IPHONE_5 175

-(float)getGraphCellHeight {
    CGRect parentRectRef =self.sleepTableView.frame;
    
    return HEIGHT_GRAPH_IPHONE_5*((parentRectRef.size.height-160)/(HEIGHT_TABLEVIEW_IPHONE_5-160))+40;
    
}

-(float)getHeightFactor {
    CGRect parentRectRef =self.sleepTableView.frame;
    
    return (parentRectRef.size.height-160)/(HEIGHT_TABLEVIEW_IPHONE_5-160);
    
}
#pragma mark - Chart behavior
-(void)reloadPlot {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self generateLayout];
        [self generateLightLayout];
    });
}

-(void)configureHost {
    // 1 - Set up view frame
    CGRect parentRect = self.sleepDisplayView.bounds;
    CGRect parentRectRef =self.sleepTableView.frame;
    
    
    // CGRect parentRect = self.view.bounds;
    parentRect = CGRectMake(parentRect.origin.x,
                            parentRect.origin.y,
                            parentRectRef.size.width,
                            [self getGraphCellHeight]);
    
    // 2 - Create host view
    self.hostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:parentRect];
    self.hostView.allowPinchScaling = NO;
    [self.sleepDisplayView  addSubview:self.hostView];
    
    mMaxValue=parentRect.size.height;   //-- 11.29
    
    //-- 11.29 test self.sleepDisplayView.backgroundColor=[UIColor redColor];
    
}


-(void)configureLightViewHost {
    // 1 - Set up view frame
    CGRect parentRect = self.lightDisplayView.bounds;
    CGRect parentRectRef =self.sleepTableView.frame;
    
    
    // CGRect parentRect = self.view.bounds;
    parentRect = CGRectMake(parentRect.origin.x,
                            parentRect.origin.y,
                            parentRectRef.size.width-20, //- 20151114 20 for sunlingt icon
                            40);  // fixed height
    
    // 2 - Create host view
    self.lightHostView = [(CPTGraphHostingView *) [CPTGraphHostingView alloc] initWithFrame:parentRect];
    self.lightHostView.allowPinchScaling = NO;
    [self.lightDisplayView  addSubview:self.lightHostView];
    
    mLightMaxValue=parentRect.size.height;   //-- 11.29
    

}


-(void)configureGraph {
    // 1 - Create and initialise graph
    
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    
    self.hostView.hostedGraph = graph;
    graph.paddingLeft = 0.0f;
    graph.paddingTop = 0.0f;
    
    graph.paddingRight = 0.0f;
    graph.paddingBottom = 0.0f;
    graph.axisSet = nil;
    // 2 - Set up text style
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color = [CPTColor grayColor];
    textStyle.fontName = @"Helvetica-Bold";
    textStyle.fontSize = 16.0f;
    /*
     // 3 - Configure title
     NSString *title = @"Portfolio Prices: May 1, 2012";
     graph.title = title;
     graph.titleTextStyle = textStyle;
     graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
     graph.titleDisplacement = CGPointMake(0.0f, -12.0f);
     */
    // 4 - Set theme
    // self.selectedTheme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    // [graph applyTheme:self.selectedTheme];
}


-(void)configureChart {
    // 1 - Get reference to graph
    CPTGraph *graph = self.hostView.hostedGraph;
    // 2 - Create chart
    
    CPTBarPlot *barChart = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
    barChart.identifier = graph.title;
    /*
     // 3 - Create gradient
     CPTGradient *overlayGradient = [[CPTGradient alloc] init];
     overlayGradient.gradientType = CPTGradientTypeRadial;
     overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.0] atPosition:0.9];
     overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.4] atPosition:1.0];
     */
    barChart.barBasesVary   = NO;
    barChart.barWidth           = CPTDecimalFromFloat(0.8f);
    barChart.barsAreHorizontal  = NO;
    barChart.dataSource         = self;
    barChart.delegate = self;
    
    // 4 - Add chart to graph
    [graph addPlot:barChart];
}

//-- this is the main redraw function while update is required.

- (void)generateLayout
{
    
    // 1 - Get graph instance
    // CPTGraph *graph = self.hostView.hostedGraph;
    
    //Create graph from theme
    CPTGraph *graph                               = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    // [graph applyTheme:[CPTTheme themeNamed:kCPTStocksTheme]];
    self.hostView.hostedGraph                    = graph;
    graph.plotAreaFrame.masksToBorder   = NO;
    graph.paddingLeft                   = 0.0f;
    graph.paddingTop                    = 0.0f;
    graph.paddingRight                  = 0.0f;
    graph.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor               = [CPTColor whiteColor];
    borderLineStyle.lineWidth               = 0.0f;
    graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
    graph.plotAreaFrame.paddingTop          = 10.0;
    graph.plotAreaFrame.paddingRight        = 10.0;
    graph.plotAreaFrame.paddingBottom       = 40.0; //12.20 0,  80.0;
    graph.plotAreaFrame.paddingLeft         = 25; // 25.0; // 70.0;
    
    // 2 - Set up text style
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color = [CPTColor lightGrayColor];
    // textStyle.fontName = @"Helvetica-Bold";
    // textStyle.fontSize = 16.0f;
    
    
    // 3 - Create gradient
    CPTGradient *overlayGradient = [[CPTGradient alloc] init];
    CPTColor *endColor=[CPTColor colorWithComponentRed:0x00 green:0x00 blue:0xCD alpha:1];
    //-- CPTColor *startColor=[CPTColor colorWithComponentRed:0xF5 green:0xFF blue:0xFA alpha:1];
    
    overlayGradient.gradientType = CPTGradientTypeAxial;   //-- CPTGradientTypeRadial;
    /*
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.0] atPosition:0.3];
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.4] atPosition:0.6];
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.8] atPosition:1.0];
     */
    
    overlayGradient = [overlayGradient addColorStop:[endColor colorWithAlphaComponent:0.2]  atPosition:0.0];
    overlayGradient = [overlayGradient addColorStop:[endColor colorWithAlphaComponent:0.6]  atPosition:0.6];
    overlayGradient = [overlayGradient addColorStop:[endColor colorWithAlphaComponent:1.0]  atPosition:1.0];

    overlayGradient.angle= 270;
    
    // overlayGradient = [overlayGradient gradientWithBlendingMode:<#(CPTGradientBlendingMode)#>
    
    graph.fill = [CPTFill fillWithGradient:overlayGradient];

    
    //Add plot space
    CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.delegate              = self;
    plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                                   length:CPTDecimalFromInt(mMaxValue)]; //-- 11.29
    // 10 * sets.count)];
    plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromInt(0)
                                                                   length:CPTDecimalFromInt(axisCountX[mViewMode])];
    
    //Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth            = 0.75;
    majorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth            = 0.25;
    minorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    
    //Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(-1);
    x.majorIntervalLength           = CPTDecimalFromInt(1);
    x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:-1.0];
   
    x.labelTextStyle= textStyle;

    
    //X labels
    int labelLocations = 0;
    NSMutableArray *customXLabels = [NSMutableArray array];
    
    
    int labelNumbers[] = { 14, 18, 22, 2, 6, 10};
    int shift=mNumberOfBars/6;
    for (int i=0, j=0; j<6; i=i+3, j++) {
        
        NSString *strLabel=[[NSString alloc] initWithFormat:@"%d",labelNumbers[j]];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:strLabel textStyle:x.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:shift*j+shift/2] decimalValue]; // [[NSNumber numberWithInt:labelLocations] decimalValue];
        newLabel.offset         = x.labelOffset; //-- 11.29 x.labelOffset + x.majorTickLength;
        //-- 11.29 newLabel.rotation       = M_PI / 4;
        [customXLabels addObject:newLabel];
        labelLocations++;
    }
    x.axisLabels                    = [NSSet setWithArray:customXLabels];
    
    

    //Y axis
    CPTXYAxis *y            = axisSet.yAxis;
    //y.title                 = @"Value";
    //y.titleOffset           = 10.0f; // 50.0f;
    // y.labelingPolicy        = CPTAxisLabelingPolicyEqualDivisions;      // CPTAxisLabelingPolicyAutomatic;
    // y.labelingPolicy        = CPTAxisLabelingPolicyEqualDivisions;
    // y.labelingPolicy     = CPTAxisLabelingPolicyAutomatic;
    y.labelingPolicy                = CPTAxisLabelingPolicyNone;
    
    CPTMutableTextStyle *textStyleY = [CPTMutableTextStyle textStyle];
    textStyleY.color = [CPTColor whiteColor];
    
    y.labelTextStyle= textStyleY;

    
    [customXLabels removeAllObjects];
    
    int step=mMaxValue/4;
    for (int i=0, j=0; i<=mMaxValue; i=i+step, j++) {
        
        NSString *strLabel;
        if(j==0){
            strLabel=@"";
        } else if(j==1){
            strLabel=NSLocalizedString(@"sleep_wake",nil);
        } else if(j==3){
            strLabel=NSLocalizedString(@"sleep_light",nil);
            textStyleY.color = [CPTColor whiteColor];
            y.labelTextStyle= textStyleY;

        } else if(j==4){
            strLabel=NSLocalizedString(@"sleep_deep",nil);
            textStyleY.color = [CPTColor darkGrayColor];
            y.labelTextStyle= textStyleY;


        }
        
        //-- NSString *strLabel=[[NSString alloc] initWithFormat:@"%d",i];
        
        CPTAxisLabel *newLabel = [[CPTAxisLabel alloc] initWithText:strLabel textStyle:y.labelTextStyle];
        newLabel.tickLocation   = [[NSNumber numberWithInt:i] decimalValue];
        newLabel.offset = y.labelOffset;
        //-- newLabel.offset         = x.labelOffset + x.majorTickLength;
        newLabel.rotation       = M_PI / 3;
        [customXLabels addObject:newLabel];
        labelLocations++;
    }
    y.axisLabels                    = [NSSet setWithArray:customXLabels];
    
    
    
    y.majorIntervalLength = [[ NSDecimalNumber decimalNumberWithString : @"50" ] decimalValue ];
    //y.majorGridLineStyle    = majorGridLineStyle;
    //y.minorGridLineStyle    = minorGridLineStyle;
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
    whiteTextStyle.color                = [CPTColor whiteColor];
    
    
  
    
    BOOL firstPlot = YES;
    for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
            //-- set the datasource to itself for delegate functions
        plot.identifier         = set;
        
        
        //-- NSLog(@"generateLayout plot=%lx", (long)plot);
        [graph addPlot:plot toPlotSpace:plotSpace];
        
    }
/*
    //Plot
    BOOL firstPlot = YES;
    {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        plot.lineStyle          = barLineStyle;
        CGColorRef color        = [UIColor purpleColor].CGColor;
        plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        if (firstPlot) {
            plot.barBasesVary   = NO;
            firstPlot           = NO;
        } else {
            plot.barBasesVary   = YES;
        }
        plot.barWidth           = CPTDecimalFromFloat(0.8f);
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        plot.identifier         = @"Test";
        [graph addPlot:plot toPlotSpace:plotSpace];
    }
*/
    
    /* 11.29
    NSString *title = [[NSString alloc] initWithFormat:@"total : %d steps",mTotalSteps];
    
    graph.title = title;
    // 3 - Create and set text style
    CPTMutableTextStyle *titleStyle = [CPTMutableTextStyle textStyle];
    titleStyle.color = [CPTColor blackColor];
    titleStyle.fontName = @"Helvetica-Bold";
    titleStyle.fontSize = 16.0f;
    graph.titleTextStyle = titleStyle;
    // graph.titlePlotAreaFrameAnchor+= CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(50.0f, -10.0f);
    */
    
    /*
     //Add legend
     CPTLegend *theLegend      = [CPTLegend legendWithGraph:graph];
     theLegend.numberOfRows	  = 1; //-- sets.count;
     theLegend.fill			  = [CPTFill fillWithColor:[CPTColor colorWithGenericGray:0.15]];
     theLegend.borderLineStyle = barLineStyle;
     theLegend.cornerRadius	  = 10.0;
     theLegend.swatchSize	  = CGSizeMake(15.0, 15.0);
     whiteTextStyle.fontSize	  = 13.0;
     theLegend.textStyle		  = whiteTextStyle;
     theLegend.rowMargin		  = 5.0;
     theLegend.paddingLeft	  = 10.0;
     theLegend.paddingTop	  = 10.0;
     theLegend.paddingRight	  = 10.0;
     theLegend.paddingBottom	  = 10.0;
     graph.legend              = theLegend;
     graph.legendAnchor        = CPTRectAnchorTopRight;
     graph.legendDisplacement  = CGPointMake(-40.0, -10.0);
     */
}



- (void)generateLightLayout
{
    
    //Create graph from theme
    CPTGraph *graph  = [[CPTXYGraph alloc] initWithFrame:CGRectZero];
    self.lightHostView.hostedGraph           = graph;
    graph.plotAreaFrame.masksToBorder   = NO;
    graph.paddingLeft                   = 0.0f;
    graph.paddingTop                    = 0.0f;
    graph.paddingRight                  = 0.0f;
    graph.paddingBottom                 = 0.0f;
    
    CPTMutableLineStyle *borderLineStyle    = [CPTMutableLineStyle lineStyle];
    borderLineStyle.lineColor               = [CPTColor whiteColor];
    borderLineStyle.lineWidth               = 0.0f;
    graph.plotAreaFrame.borderLineStyle     = borderLineStyle;
    graph.plotAreaFrame.paddingTop          = 0.0;
    graph.plotAreaFrame.paddingRight        = 10.0;
    graph.plotAreaFrame.paddingBottom       = 5.0; //20151114 //12.20 0,  80.0;
    graph.plotAreaFrame.paddingLeft         = 0; // 20151114  5+20 // 25.0; // 70.0;
    
    // 2 - Set up text style
    CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color = [CPTColor lightGrayColor];
    
    // 3 - Create gradient
    /*
    CPTGradient *overlayGradient = [[CPTGradient alloc] init];
    CPTColor *endColor=[CPTColor colorWithComponentRed:0x00 green:0x00 blue:0xCD alpha:1];
    
    overlayGradient.gradientType = CPTGradientTypeAxial;   //-- CPTGradientTypeRadial;
    
    overlayGradient = [overlayGradient addColorStop:[endColor colorWithAlphaComponent:0.2]  atPosition:0.0];
    overlayGradient = [overlayGradient addColorStop:[endColor colorWithAlphaComponent:0.6]  atPosition:0.6];
    overlayGradient = [overlayGradient addColorStop:[endColor colorWithAlphaComponent:1.0]  atPosition:1.0];
    
    overlayGradient.angle= 270;
    
    
    graph.fill = [CPTFill fillWithGradient:overlayGradient];
    */
    
    //Add plot space
    CPTXYPlotSpace *plotSpace       = (CPTXYPlotSpace *)graph.defaultPlotSpace;
    plotSpace.delegate              = self;
    plotSpace.yRange                = [CPTPlotRange plotRangeWithLocation:
                                        CPTDecimalFromInt(0)
                                        length:
                                        CPTDecimalFromInt(mLightMaxValue)];
    
    plotSpace.xRange                = [CPTPlotRange plotRangeWithLocation:
                                        CPTDecimalFromInt(0)
                                        length:
                                        CPTDecimalFromInt(mLightNumberOfBars)];
    
    //Grid line styles
    CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
    majorGridLineStyle.lineWidth            = 0.75;
    majorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
    minorGridLineStyle.lineWidth            = 0.25;
    minorGridLineStyle.lineColor            = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
    
    //Axes
    CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
    
    //X axis
    CPTXYAxis *x                    = axisSet.xAxis;
    x.hidden=YES;
    x.orthogonalCoordinateDecimal   = CPTDecimalFromInt(-1);
    x.majorIntervalLength           = CPTDecimalFromInt(1);
    x.minorTicksPerInterval         = 0;
    x.labelingPolicy                = CPTAxisLabelingPolicyNone;
    x.majorGridLineStyle            = majorGridLineStyle;
    x.axisConstraints               = [CPTConstraints constraintWithLowerOffset:-1.0];
    
    x.labelTextStyle= textStyle;
    
    //Y axis
    CPTXYAxis *y            = axisSet.yAxis;
    
    y.hidden=YES;
    
    y.labelingPolicy                = CPTAxisLabelingPolicyNone;
    
    CPTMutableTextStyle *textStyleY = [CPTMutableTextStyle textStyle];
    textStyleY.color = [CPTColor whiteColor];
    
    y.labelTextStyle= textStyleY;
    
    y.majorIntervalLength = [[ NSDecimalNumber decimalNumberWithString : @"1" ] decimalValue ];
    y.axisConstraints       = [CPTConstraints constraintWithLowerOffset:0.0];
    
    //Create a bar line style
    CPTMutableLineStyle *barLineStyle   = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineWidth              = 0.0;
    barLineStyle.lineColor              = [CPTColor whiteColor];
    CPTMutableTextStyle *whiteTextStyle = [CPTMutableTextStyle textStyle];
    whiteTextStyle.color                = [CPTColor whiteColor];
    
    {
        CPTBarPlot *plot        = [CPTBarPlot tubularBarPlotWithColor:[CPTColor blueColor] horizontalBars:NO];
        
        mLightPlot=plot;    //-- 150910
        
        plot.lineStyle          = barLineStyle;
        // CGColorRef color        = ((UIColor *)[sets objectForKey:set]).CGColor;
        // plot.fill               = [CPTFill fillWithColor:[CPTColor colorWithCGColor:color]];
        // plot.barBasesVary   = NO;
        plot.barWidth           = CPTDecimalFromFloat(0.5f); //-- 0.8f
        plot.barsAreHorizontal  = NO;
        plot.dataSource         = self;
        //-- set the datasource to itself for delegate functions
        plot.identifier         = PLOT_LIGHT_GRAPH_IDENTIFIER;
        [graph addPlot:plot toPlotSpace:plotSpace];
        
    }

}   // end of generateLightLayout




#pragma mark - CPTPlotDataSource methods

//-- link number of bars to mNumberOfBars size number

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    
    if([plot.identifier isEqual:PLOT_LIGHT_GRAPH_IDENTIFIER]==YES){
        //-- NSLog(@"numberOfRecords mLightNumberOfBars=%d", mLightNumberOfBars );

        return mLightNumberOfBars;
    }
    
    // NSLog(@"number bars %d", mNumberOfBars);
    
    return mNumberOfBars;
}

struct LevelToBarAttr {
    int barColor;
    float barHeight;
};

static struct LevelToBarAttr levelToBarAttr[] = {
    
    {0xff0066CC, 1},	// 0, not a valid value
    {0xff0066CC, 1},	// 1, deep sleep
    {0xff00bfff, 8.5f/10.0f},	// 2, Restless Sleep
    {0xff7ac5cd, 7.0f/10.0f},	// 3, Resting
    {0xffcdc673, 5.5f/10.0f},	// 4, Couch
    {0xffcdc673, 4.5f/10.0f},	// 5, Desk Work
    {0xffffe485, 3.5f/10.0f},	// 6, Walking
    {0xffffe485, 3.3f/10.0f},	// 7, Jogging
    {0xff00fa9a, 2.8f/10.0f},	// 8, Running
    {0xff00fa9a, 2.0f/10.0f},	// 9, Spiriting
    {0xff00fa9a, 1.0f/10.0f},	// 10, Error condition
    {0xff00fa9a, 0}			// 11, not a valid value
 
};

#define LIGHT_STAGE_INVALID  (-1)

-(float)heightOfLightBar: (int)index {
    
    float height=mLightPlot.frame.size.height; //-- 20151114 -10 // plus the padding parts

    int lightLevel=mLightStages[index];
    
    if(lightLevel==LIGHT_STAGE_INVALID) return 0;
    
    if(lightLevel>0x7FFF) lightLevel=0x7FFF;
    
    float lightStrength=sqrt((float)(lightLevel));
    
    float lightDivider=sqrt(0x7FFF);
    
    //-- NSLog(@"heightOfLightBar=%d,%f, %f", lightLevel, lightStrength, height);
    
    float heightFactor=(lightStrength/lightDivider);
    
    float heightOfBar=(float)(height)*heightFactor*1.5; //-- 20151114 * 1.5 to amplify
    
    if(heightOfBar<2) heightOfBar=2;
    
    return heightOfBar;
}


//-- Define the height of each index of the bar graph
- (double)doubleForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index
{
    //-- NSLog(@"doubleForPlot %d, %lx", (int)index, (long)plot);
    
    double num = NAN;
    
    if([plot.identifier isEqual:PLOT_LIGHT_GRAPH_IDENTIFIER]==YES){

        if (fieldEnum == 0) {
            num = index;
        }else{
            float heightBar=[self heightOfLightBar:(int)index];

            num=heightBar;
        }

        return num; //-- ((float)index/mLightNumberOfBars)*mLightMaxValue;
        
     }

    
    float barHeightFactor;
    float height=plot.frame.size.height+40; // plus the padding parts
    
    if(mSleepStages[index]!=SLEEP_STAGE_INVALID){
        int threshId=mSleepStages[index];
        barHeightFactor=levelToBarAttr[threshId].barHeight;
    } else{
        barHeightFactor=0;
    }

    //-- NSLog(@"sleep plot %ld,%d=%f",index,mSleepStages[index], barHeightFactor);
    
    //X Value
    if (fieldEnum == 0) {
        num = index;
    }
    else {
        double offset = 0;
        if (((CPTBarPlot *)plot).barBasesVary) {
            /*
             for (NSString *set in [[sets allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]) {
             if ([plot.identifier isEqual:set]) {
             break;
             }
             offset += [[[data objectForKey:[dates objectAtIndex:index]] objectForKey:set] doubleValue];
             }
             */
            
        }
        
        //Y Value
        if (fieldEnum == 1) {
            
            // num = [[[data objectForKey:[dates objectAtIndex:index]] objectForKey:plot.identifier] doubleValue] + offset;
            
            
            num=offset+ (height*barHeightFactor);
            // NSLog(@"index=%d, %f",index, num);
        }
        //Offset for stacked bar
        else {
            num = offset;
        }
    }
    
    //NSLog(@"%@ - %d - %d - %f", plot.identifier, index, fieldEnum, num);
    
    return num;
}

//-- Let the bar graph index relate to the mSleepStages data
//-- define the color of each index number

-(CPTFill *)barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index {
    
    
   
    if([barPlot.identifier isEqual:PLOT_LIGHT_GRAPH_IDENTIFIER]==YES){
        float height=[self heightOfLightBar:(int)index];
        
        int unitColor=0;
        
        if(height<=2){
            unitColor=levelToBarAttr[(int)height].barColor;
        }else{
            unitColor=0xffff4500; // 6347;
        }
        
        unitColor=(unitColor & 0x00FFFFFF);
        
        float redColorFactor=    ((float)(((unitColor)>>16)& 0xFF))/0xFF;
        float greenColorFactor=  ((float)(((unitColor)>>8) & 0xFF))/0xFF;
        float blueColorFactor=   ((float)( (unitColor)     & 0xFF))/0xFF;

        CPTColor *lightStageColor=[CPTColor colorWithComponentRed:redColorFactor green:greenColorFactor blue:blueColorFactor alpha:1];
        
        return [CPTFill fillWithColor:lightStageColor];
        
    }
    
    CPTColor *sleepStageColor;    
    
    int threshId=mSleepStages[index];
    int unitColor=(levelToBarAttr[threshId].barColor & 0x00FFFFFF);
    
    float redColorFactor=    ((float)(((unitColor)>>16)& 0xFF))/0xFF;
    float greenColorFactor=  ((float)(((unitColor)>>8) & 0xFF))/0xFF;
    float blueColorFactor=   ((float)( (unitColor)     & 0xFF))/0xFF;
    
    sleepStageColor=[CPTColor colorWithComponentRed:redColorFactor green:greenColorFactor blue:blueColorFactor alpha:1];
    
    return [CPTFill fillWithColor:sleepStageColor];
}

- (IBAction)dateSelectPickerValueChanged:(id)sender {
    
    [timeControllerView setDate: self.datePicker.date];
}

-(void) userInteractionEnable: (BOOL)enable {
    self.timeTableCell.userInteractionEnabled = enable;
    self.datePickerTableCell.userInteractionEnabled = enable;
    self.sleepSetView.userInteractionEnabled = enable;
    self.tabBarController.tabBar.userInteractionEnabled = enable;
    
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeftSide) {
        // NSLog(@"FrontViewPositionLeftSide");
        // jump to right side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
        self.tapGestureRecognizerRight.enabled = YES;
        [self userInteractionEnable:NO];
    }
    else if (position == FrontViewPositionLeft){
        // NSLog(@"FrontViewPositionLeft");
        // back to main screen
        self.tapGestureRecognizer.enabled = NO;
        self.tapGestureRecognizerRight.enabled = NO;
        [self userInteractionEnable:YES];
    }
    else if (position == FrontViewPositionRight){
        // NSLog(@"FrontViewPositionRight");
        // jump to left side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.enabled = YES;
        [self userInteractionEnable:NO];
        
    }

}

//-- delegates of BinsDataViewController
//-- 20151119
- (void)binsDataChanged:(BinsDataViewController*)binsDataController {
    
    NSLog(@"sleep table, binsDataChanged");
    
    [self queryData: [timeControllerView getCurrentDate]];
    [self reloadPlot];
}

@end
