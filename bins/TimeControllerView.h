//
//  TimeControllerView.h
//  CustomView
//
//  Created by Ray Wenderlich on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "binsHelper.h"

#define VIEW_MODE_UNIT_DAY      0
#define VIEW_MODE_UNIT_MONTH    1
#define VIEW_MODE_UNIT_HOUR     2

#define COLOR_TIME_CONTROLLER_BAR Rgb2UIColor(245, 245, 245, 1)
//Rgb2UIColor(240, 255, 255, 0.8)

#define COLOR_TIME_PICKER_BG Rgb2UIColor(105, 105, 105, 1)
//Rgb2UIColor(240, 255, 255, 0.8)

// #define COLOR_TIME_PICKER_NEW Rgb2UIColor(0x84, 0xC1, 0xFF, 1)

//-- #define COLOR_TIME_PICKER_NEW2 Rgb2UIColor(0xE0, 0xE0, 0xE0, 1)
#define COLOR_TIME_PICKER_NEW Rgb2UIColor(0x00, 0x80, 0xFF, 1)
#define COLOR_TIME_PICKER_NEW3 Rgb2UIColor(0xCE, 0xCE, 0xFF, 1)
#define COLOR_TIME_PICKER_NEW2 Rgb2UIColor(0, 0, 0, 0)


#define DAY_MINUTES (24*60)



@class TimeControllerView;

@protocol TimeControllerViewDelegate
- (void)timeControllerView:(TimeControllerView *)timeControllerView timeDidChange:(NSDate*)newDate;

- (void)timeControllerView:(TimeControllerView *)timeControllerView touchUpInside:(NSDate*)currentDate;

- (void)timeControllerView:(TimeControllerView *)timeControllerView browseModeChanged:(int)mode;


@end

@interface TimeControllerView : UILabel <UIGestureRecognizerDelegate>

@property (strong, nonatomic) UIImage *buttonLeftImage;
@property (strong, nonatomic) UIImage *buttonRightImage;
@property (assign, nonatomic) int currentTime;
@property (assign) BOOL editable;
@property (strong) NSMutableArray * imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <TimeControllerViewDelegate> delegate;

@property (assign) int browseUnitMode;


- (void)changeDate: (int) dayShift;

- (NSDate*)getCurrentDate;

- (NSDate*)getToday;

-(void)setDate: (NSDate*)date;

-(void)setBrowseUnit: (int)viewMode;
-(int)getBrowseUnit;
-(void) moveToToday;

- (void)refresh;

-(void)getMonthPeriod: date startOfMonth: (NSDate**)startDay endOfMonth: (NSDate**)endDay;


@end
