//
//  TimeControllerView.m
//  CustomView
//
//  Created by Ray Wenderlich on 2/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TimeControllerView.h"

@implementation TimeControllerView

@synthesize buttonLeftImage = _buttonLeftImage;
@synthesize buttonRightImage = _buttonRightImage;
@synthesize delegate = _delegate;
@synthesize browseUnitMode;

static NSDate *currentDate;        // current date on view
static NSDate *todayDate;
static NSDate *tomorrowDate;
static NSDate *nextMonthDate;
static bool isInitiated=false;


- (void)baseInit {
    
    if(isInitiated==true) return;
    
    isInitiated=true;
    
    _editable = NO;
    
    [self setToday];
    
    currentDate=[NSDate date];

    browseUnitMode=VIEW_MODE_UNIT_DAY;
    

    _delegate = nil;
 /*
    UITapGestureRecognizer *labelTap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap)];
    
    labelTap.delegate = self;
    
    labelTap.numberOfTapsRequired=1;
    [self addGestureRecognizer:labelTap];
   */ 
}

//The event handling method
- (void)handleSingleTap{
    // if(recognizer.view.tag == 1){}
    //Do stuff here...
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self baseInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if ((self = [super initWithCoder:aDecoder])) {
        [self baseInit];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    float desiredImageWidth = (self.frame.size.width - (self.leftMargin*2) - (self.midMargin*self.imageViews.count)) / self.imageViews.count;
    float imageWidth = MAX(self.minImageSize.width, desiredImageWidth);
    float imageHeight = MAX(self.minImageSize.height, self.frame.size.height);
    
    for (int i = 0; i < self.imageViews.count; ++i) {
        
        UIImageView *imageView = [self.imageViews objectAtIndex:i];
        CGRect imageFrame = CGRectMake(self.leftMargin + i*(self.midMargin+imageWidth), 0, imageWidth, imageHeight);
        imageView.frame = imageFrame;
        
    }    
    
}

- (void)setMaxRating:(int)maxRating {
    _maxRating = maxRating;
    
    // Remove old image views
    for(int i = 0; i < self.imageViews.count; ++i) {
        UIImageView *imageView = (UIImageView *) [self.imageViews objectAtIndex:i];
        [imageView removeFromSuperview];
    }
    [self.imageViews removeAllObjects];
    
    // Add new image views
    for(int i = 0; i < maxRating; ++i) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.imageViews addObject:imageView];
        [self addSubview:imageView];
    }
    
    // Relayout and refresh
    [self setNeedsLayout];
    [self refresh];
}


- (void)setCurrentTime:(int)time {
    _currentTime = time;
    [self refresh];
}

- (void)handleTouchAtLocation:(CGPoint)touchLocation {
    // if (!self.editable) return;
    // touchLocation.x; //
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    UITouch *touch = [touches anyObject];
    CGPoint touchLocation = [touch locationInView:self];
    [self handleTouchAtLocation:touchLocation];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.delegate timeControllerView:self touchUpInside:currentDate];
}

- (void)setToday {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDate *now = [NSDate date];
    
    NSDateComponents *dayComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:now];
    
    dayComponents.hour=0;
    dayComponents.minute=0;
    dayComponents.second=0;
    

    todayDate= [calendar dateFromComponents:dayComponents];
    
    NSDateComponents *todayComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:todayDate];
    
    todayComponents.day+=1;
 
    tomorrowDate = [calendar dateFromComponents:todayComponents];
    
    todayComponents.day=1;
    todayComponents.month+=1;
    nextMonthDate = [calendar dateFromComponents:todayComponents];
    
}

- (void)setDate {
    
    [self setToday];
    
    currentDate = todayDate;
 
}

-(void)setDate: (NSDate*)date {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitYear|NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitHour |NSCalendarUnitMinute |NSCalendarUnitSecond
                                                   fromDate:date];
    
    dateComponents.hour=0;
    dateComponents.minute=0;
    dateComponents.second=0;
    
    NSDate *newDate = [calendar dateFromComponents:dateComponents];
    
    currentDate=newDate;
   
    if(browseUnitMode==VIEW_MODE_UNIT_DAY){
        self.text=[NSDateFormatter localizedStringFromDate:currentDate  dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle];
        
    } else if(browseUnitMode==VIEW_MODE_UNIT_MONTH){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMMM   yyyy"];
        
        self.text=[formatter stringFromDate:currentDate];
    }

   [self.delegate timeControllerView:self timeDidChange:currentDate];
}

-(NSDate *)createNewDate: (NSDate*)date shiftUnit: (int)period withUnitOf:(int)unit{
    
    NSDate* confirmedDate=date;
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *currentDateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];

    
    if(unit==VIEW_MODE_UNIT_DAY){
        
        currentDateComponents.day+=period;
        
        NSDate *newDate = [calendar dateFromComponents:currentDateComponents];
        
        if([newDate compare:tomorrowDate]==NSOrderedAscending){
            confirmedDate=newDate;
         }
    }
    else if(unit==VIEW_MODE_UNIT_MONTH){
        
        currentDateComponents.month+=period;
        
        NSDate *newDate = [calendar dateFromComponents:currentDateComponents];
        
        if([newDate compare:nextMonthDate]==NSOrderedAscending){
            confirmedDate=newDate;
        }
    }
    
    return confirmedDate;
}
-(void)moveToToday {
    [self setToday];
    [self setDate: todayDate];
}
- (void)changeDate: (int) periodShift {
    
    [self setToday];
    
    currentDate=[self createNewDate:currentDate shiftUnit:periodShift withUnitOf:browseUnitMode];
    
    if(browseUnitMode==VIEW_MODE_UNIT_DAY){
        self.text=[NSDateFormatter localizedStringFromDate:currentDate  dateStyle:NSDateFormatterLongStyle timeStyle:NSDateFormatterNoStyle];

    } else if(browseUnitMode==VIEW_MODE_UNIT_MONTH){
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MMMM   yyyy"];
        
        self.text=[formatter stringFromDate:currentDate];
    }
    
    [self.delegate timeControllerView:self timeDidChange:currentDate];
}

-(void)getMonthPeriod: (NSDate*)date startOfMonth: (NSDate**)startDay endOfMonth: (NSDate**)endDay {
   
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *currentDateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];
    

    currentDateComponents.day=1;    //-- 12.01
    *startDay = [calendar dateFromComponents:currentDateComponents];
    
    currentDateComponents.month++;
    *endDay=[calendar dateFromComponents:currentDateComponents];
}

-(void) refresh {
    [self changeDate:0];
}

-(NSDate*)getCurrentDate {
    return currentDate;
}

-(NSDate*)getToday {
    return todayDate;
}

-(void)setBrowseUnit:(int)mode {
 
    if(browseUnitMode==mode) return;
    
    browseUnitMode=mode;

    [self.delegate timeControllerView:self browseModeChanged:browseUnitMode];
    
    [self refresh];
    
    
}

-(int)getBrowseUnit {
    return browseUnitMode;
}

@end
