//
//  UnlockingLifeService.h
//  bins
//
//  Created by Dennis Kung on 6/29/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSVParser.h"

@class UnlockingLifeService;

@protocol UnlockingLifeServiceDelegate

@optional

-(void)unlockingLifeService: (UnlockingLifeService*)ulockingService hasUser: (BOOL)status;

@optional
-(void)unlockingLifeService: (UnlockingLifeService*)ulockingService addedNewUser: (BOOL)status userName: (NSString*)userName userId: (int)userId;

-(void)unlockingLifeService:(UnlockingLifeService*)ulockingService addedNewData: (BOOL) isSuccess user: (int)userID ;

@end


@interface UnlockingLifeService : NSObject <NSURLConnectionDelegate>


@property (weak) id <UnlockingLifeServiceDelegate> delegate;

-(BOOL) checkInternet;

-(BOOL) checkUserExist: (NSString*) userName ;
-(BOOL) addNewUser: (NSString *)userName firstNameIs: (NSString*)firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender ;

-(id)initWithTableDownload;


-(void)createUploadSession: (int)user ofType:(int)type;
-(void)addUploadActivity:(int)actValue ofType: (int)actType ofTime:(long)time;
-(BOOL)startUploadSession;


@end
