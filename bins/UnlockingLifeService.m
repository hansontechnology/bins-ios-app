//
//  UnlockingLifeService.m
//  bins
//
//  Created by Dennis Kung on 6/29/15.
//  Copyright (c) 2015 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UnlockingLifeService.h"
#import "Reachability.h"
#import "BinsDataViewController.h"

@interface UnlockingLifeService ()

@property BinsDataViewController *binsDataController;

@end

@implementation UnlockingLifeService

@synthesize binsDataController;

NSTimer *mConnectTimeoutTimer=nil;
typedef void (^CompletionBlock)(); // id, NSError*);

static const NSString *URL_CIRCADIAN_TABLE = @"https://dev.unlockinglife.com/lookups/circadian.csv";
static const NSString *URL_THRESH_TABLE = @"https://dev.unlockinglife.com/lookups/thresholds.csv";
static const NSString *URL_TIME_EFFECT_TABLE = @"https://dev.unlockinglife.com/lookups/timeeffect.csv";

NSString *URL_USER_SEARCH = @"https://dev.unlockinglife.com/apiv1/users/search/{\"keyword\":\"%@\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";

NSString *URL_USER_ADD_NEW = @"https://dev.unlockinglife.com/apiv1/users/new/{\"fname\":\"%@\",\"lname\":\"%@\",\"email\":\"%@\",\"dob\":\"%@\",\"gender\":\"%d\",\"password\":\"%@\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";

NSString *URL_USER_ADD_DATA = @"https://dev.unlockinglife.com/apiv1/add_bin_data/{\"sleepdata\":\"[%@]\",\"stepdata\":\"[%@]\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";


NSURLConnection *connectCircadian=nil;
NSURLConnection *connectTimeEffect=nil;
NSURLConnection *connectThreshold=nil;

NSURLConnection *connectUserSearch=nil;
NSURLConnection *connectUserAddNew=nil;
NSURLConnection *mConnectUserAddData=nil;
int mActiveConnectUser=0;

NSMutableString *mUploadDataSleep=nil;
NSMutableString *mUploadDataSteps=nil;



-(id)init
{
    self= [super init];
    
    NSLog(@"Unlocking init");
    
    binsDataController=[[BinsDataViewController alloc] init];
    
    mUploadDataSleep=[[NSMutableString alloc] initWithFormat:@""];
    mUploadDataSteps=[[NSMutableString alloc] initWithFormat:@""];
    
    return self;
}



-(id)initWithTableDownload
{
    self= [self init];
    
    NSLog(@"Unlocking init and table download");
    
    connectCircadian=[self downloadCsvFromUrl: URL_CIRCADIAN_TABLE ];
    
    connectThreshold=[self downloadCsvFromUrl: URL_THRESH_TABLE ];
    connectTimeEffect=[self downloadCsvFromUrl: URL_TIME_EFFECT_TABLE ];
    
    // We want this service to continue running until it is explicitly
    // stopped, so return sticky
    
    return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    // _responseData = [[NSMutableData alloc] init];
    // NSLog(@"didReceiveResponse");
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)rawData {

    NSLog(@"didReceiveData");
    
    if(mConnectTimeoutTimer!=nil){
        [mConnectTimeoutTimer invalidate];
        mConnectTimeoutTimer=nil;
    }
    
    NSString *rawString = [[NSString alloc] initWithData:rawData encoding:NSUTF8StringEncoding];

    
    if(connection==mConnectUserAddData){
        BOOL isSuccess=YES;
        //-- TODO check for complete or not
        //-- NSLog(@"connectUserAddData:%@",rawString);
        
        [self.delegate unlockingLifeService:self addedNewData:isSuccess user:mActiveConnectUser];
        mActiveConnectUser=0;
        
        return;
        
    }else if(connection==connectUserSearch){
        BOOL foundUser;
        NSLog(@"user search returns : %@", rawString);
        connectUserSearch=nil;
 
        foundUser=([rawString isEqualToString:@"null"]!=YES);
        
        [self.delegate unlockingLifeService:self hasUser: foundUser];
        return;
    }else if(connection==connectUserAddNew){
        
        connectUserAddNew=nil;
        
        NSError *e = nil;
        
        NSDictionary* json=[NSJSONSerialization JSONObjectWithData: rawData options: NSJSONReadingMutableContainers error: &e];
        
        if (!json) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            NSString *strUserId=[json objectForKey:@"userid"];
            
            if(strUserId!=nil){
                NSLog(@"new user ID=%@", strUserId);
                
                [self.delegate unlockingLifeService:self addedNewUser: YES userName: gConnectUserName userId: [strUserId intValue] ];
            }
            
        }

        return;
    }else{
        NSLog(@"Work for table updates");
    }
    
     NSString *csvString = [[NSString alloc] initWithData:rawData encoding:NSUTF8StringEncoding];
    
     //-- NSLog(@"csvString=%@", csvString);
    
     CSVParser *parser = [[CSVParser alloc]
     initWithString:csvString
     separator:@","
     hasHeader:YES
     fieldNames: nil];
     
     NSArray *records=[parser arrayOfParsedRows];

    
    if(connection==connectCircadian){
        [binsDataController updateCircadianTable:records];
        connectCircadian=nil;
    }
    else if(connection==connectThreshold){
        [binsDataController updateSleepThresholdTable:records];
        connectThreshold=nil;
    }
    else if(connection==connectTimeEffect){
        [binsDataController updateSleepTimeEffectTable:records];
        connectTimeEffect=nil;
    }
    else
        NSLog(@"Wrong URL from csv download");

    
    /*
     if([url isEqual:URL_CIRCADIAN_TABLE]==YES){
     [binsDataController updateCircadianTable:records];
     }else if([url isEqual:URL_THRESH_TABLE]==YES){
     [binsDataController updateSleepThresholdTable:records];
     }else if([url isEqual:URL_TIME_EFFECT_TABLE]==YES){
     [binsDataController updateSleepTimeEffectTable:records];
     }else {
     NSLog(@"Wrong URL from csv download");
     
     */    

}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    NSURLProtectionSpace *protectionSpace = [challenge protectionSpace];
    
    id<NSURLAuthenticationChallengeSender> sender = [challenge sender];
    
    if ([[protectionSpace authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust])
    {
        SecTrustRef trust = [[challenge protectionSpace] serverTrust];
        
        NSURLCredential *credential = [[NSURLCredential alloc] initWithTrust:trust];
        
        [sender useCredential:credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [sender performDefaultHandlingForAuthenticationChallenge:challenge];
    }
}

-(NSURLConnection*) downloadCsvFromUrl: (const NSString*)  strUrl
{
    
    //-- Log.i(TAG,"CSV:"+url);
    NSLog(@"downloadCsvFromUrl");
    
    
    
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
    

    return connection;
}

-(void) connectTimeoutHandler: (NSTimer*)timer {
    CompletionBlock runBlock;
    
    runBlock=(CompletionBlock)timer.userInfo;
    runBlock();
              
}

-(void) connectTimeoutCheck: (CompletionBlock)handler {
    
    mConnectTimeoutTimer= [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(connectTimeoutHandler:) userInfo:handler repeats:NO];
}

-(BOOL) checkInternet {
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    
    return YES;
}


-(BOOL) checkUserExist: (NSString*) userName {
    
    if([self checkInternet]==NO) return NO;

    
    NSString *strUrl=[NSString stringWithFormat:URL_USER_SEARCH, userName];
    
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];

    
    connectUserSearch=connection;
    
    [self connectTimeoutCheck: ^() {
        connectUserSearch=nil;
        [self.delegate unlockingLifeService:self hasUser: YES];
    }];

    return YES;
    
}

// NSString *URL_USER_ADD_NEW = @"https://dev.unlockinglife.com/apiv1/users/new/{\"fname\":\"%@\",\"lname\":\"%@\",\"email\":\"%@\",\"dob\":\"%@\",\"gender\":\"u\",\"password\":\"%@\",\"apik\":\"8346gh2\",\"apip\":\"a9s6d43llg9\"}";

NSString *gConnectUserName=nil;


-(BOOL) addNewUser: (NSString *)userName firstNameIs: (NSString*)firstName lastNameIs: (NSString*)lastName dobIs: (NSString*)dob weightIs: (int)weight heightIs: (int)height genderIs: (int)gender {
    
    if([self checkInternet]==NO){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_no_internet",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
        
        [alert show];
       
        return NO;
    }

    
    NSString *strGenderName=@"F";
    if(gender==1) strGenderName=@"M";
    
    NSString *strUrl=[NSString stringWithFormat:URL_USER_ADD_NEW, firstName, lastName, userName, dob, strGenderName, userName];
    
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    gConnectUserName=userName;
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];

    

    
    connectUserAddNew=connection;
    
    [self connectTimeoutCheck: ^() {
        connectUserAddNew=nil;
        [self.delegate unlockingLifeService:self addedNewUser: NO userName: userName userId: 0];
    }];
    
    
    // 150801 test [self.delegate unlockingLifeService:self addedNewUser: YES userName: gConnectUserName userId: 1234 ];

    
    return YES;
    
}

/*
 mUploadDataSleep=@"1||23||1423204200";
 mUploadDataSteps=@"1||23||1423204200";
 
 user, value, time
*/


-(void)addUploadActivity:(int)actValue ofType: (int)type ofTime:(long)time {
    
    if(type==BINS_ACTIVITY_TYPE_SLEEP){
        [mUploadDataSleep appendFormat:@"\"%d||%d||%ld\";",mActiveConnectUser, actValue, time];

        //-- NSLog(@"addUploadActivity:%d,%ld=%@",actValue, time,mUploadDataSleep);
    }else if(type==BINS_ACTIVITY_TYPE_STEPS){
        [mUploadDataSteps appendFormat:@"\"%d||%d||%ld\";",mActiveConnectUser, actValue, time];
    }
    
}


-(void)createUploadSession:(int)userUID ofType:(int)type {
    
    mActiveConnectUser=userUID;

    if(type==BINS_ACTIVITY_TYPE_SLEEP){
        [mUploadDataSleep setString:@""];
    }else if(type==BINS_ACTIVITY_TYPE_STEPS){
        [mUploadDataSteps setString:@""];
    }
    
}


-(BOOL)startUploadSession{
    if([self checkInternet]==NO) return NO;
    
    //-- NSLog(@"startUploadSession internet checked ok");
    

    if([mUploadDataSleep isEqual:@""] &&
       [mUploadDataSteps isEqual:@""]       ){
        
        NSLog(@"startUploadSession: no more data to upload");
        return NO;
    }
    
    
    NSString *strUrl=[NSString stringWithFormat:URL_USER_ADD_DATA,
                      mUploadDataSleep,
                      mUploadDataSteps
                      ];
    
    NSURL *url = [NSURL URLWithString:[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    
    NSURLConnection *connection = [NSURLConnection connectionWithRequest:urlRequest delegate:self];
    
    if(connection==nil){
        NSLog(@"startUploadSession connection=nil");
        return NO;
    }
    
    NSLog(@"startUploadSession");
    mConnectUserAddData=connection;
    
    [self connectTimeoutCheck: ^() {
        mConnectUserAddData=nil;
        [self.delegate unlockingLifeService:self addedNewData: NO user:mActiveConnectUser];
    }];
    
    
    // 150801 test [self.delegate unlockingLifeService:self addedNewUser: YES userName: gConnectUserName userId: 1234 ];
    
    
    return YES;
    

}



@end
