//
//  UserSelectViewController.h
//  bins
//
//  Created by Dennis Kung on 12/2/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BinsDataViewController.h"

@interface UserSelectViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource>

@property BinsDataViewController *binsDataController;


@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeGestureRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeGestureRecognizer;

@end
