//
//  UserSelectViewController.m
//  bins
//
//  Created by Dennis Kung on 12/2/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "UserSelectViewController.h"
#import "SWRevealViewController.h"
#import "UserTableViewController.h"
#import "binsTabBarViewController.h"
#import "DeviceViewController.h"

@implementation UserSelectViewController 
{
    
    NSMutableArray *userList;
    
    UserTableViewController *userProfileController;
    
    UIViewController *currentFrontViewController;
    
    DeviceViewController *deviceController;
    
}

@synthesize binsDataController;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    binsDataController=[[BinsDataViewController alloc] init];
    userList = [[NSMutableArray alloc] init];
    
    deviceController=[[DeviceViewController alloc] init];
    
    // Set the gesture
    // [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    self.leftSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    self.rightSwipeGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipes:)];
    
    self.leftSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    self.rightSwipeGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:self.leftSwipeGestureRecognizer];
    [self.view addGestureRecognizer:self.rightSwipeGestureRecognizer];
    
    self.tableView.alwaysBounceVertical = NO;
    
}

-(void)updateUserList {
    [userList removeAllObjects];
    
    BOOL result=[binsDataController queryUsers];
    
    if(result==YES){
        int user;
        NSString *name;
        while([binsDataController getNextUserFromQuery:&user withName:&name]){
            
            [userList addObject:name];
            
        }
    }
}
-(void)viewWillAppear:(BOOL) animated {
    NSLog(@"user select view appear");
    
    
    binsTabBarViewController *tabBar=(binsTabBarViewController*)(self.revealViewController.frontViewController);
    
    UINavigationController *naviCon=(UINavigationController*)tabBar.selectedViewController;

    currentFrontViewController=naviCon.topViewController;
    
    [self updateUserList];
    [self.tableView reloadData];
    
}

- (void)handleSwipes:(UISwipeGestureRecognizer *)sender
{
    
    
    if (sender.direction == UISwipeGestureRecognizerDirectionRight)
    {
        [self.revealViewController rightRevealToggleAnimated: YES];
    }
    else if (sender.direction == UISwipeGestureRecognizerDirectionLeft){
    }
    
    CATransition *animation = [CATransition animation];
    [animation setType:kCATransitionFade];
    [animation setDuration:0.25];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:
                                  kCAMediaTimingFunctionEaseIn]];
    [self.view.window.layer addAnimation:animation forKey:@"fadeTransition"];
    
    
    
}

-(BOOL)isFromUserProfile {
    return [currentFrontViewController isKindOfClass:[UserTableViewController class]];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView  {
    
    if([self isFromUserProfile]==YES) return 3;
    
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section==1) return [userList count];
    return 1;
}

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //-- NSLog(@"cell for %d, %d", indexPath.section, indexPath.row);
    
    static NSString *simpleTableIdentifier = @"UserSelectTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    
    if(indexPath.section==0){
        
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.backgroundColor=[UIColor colorWithRed:0x00/255.0 green:0x4B/255. blue:0x97/255. alpha:0.85f];


        cell.textLabel.font=[UIFont fontWithName:@"Helvetica Light" size:15.0];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"menu_user_title",nil) ];
    } else if(indexPath.section==1){
        cell.textLabel.font=[UIFont fontWithName:@"Helvetica Light" size:15.0];
        cell.textLabel.textColor=[UIColor blackColor];
        cell.textLabel.text = userList[indexPath.row];   //-- objectAtIndex:indexPath.row];
        if([binsDataController getUserIdByName:userList[indexPath.row]]==[binsDataController getActiveUserID]){
            cell.backgroundColor=UIColorFromRGB(0xD2E9FF);
        } else {
            cell.backgroundColor=[UIColor whiteColor];
        }
    } else{
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.backgroundColor=[UIColor colorWithRed:0x00/255.0 green:0x4B/255. blue:0x97/255. alpha:0.7f];

        
        cell.textLabel.font=[UIFont fontWithName:@"Helvetica Light" size:15.0];
        // cell.textLabel.numberOfLines=2;
        // cell.textLabel.lineBreakMode=UILineBreakModeMiddleTruncation;
        cell.textLabel.backgroundColor=[UIColor clearColor];
        
        cell.textLabel.text=[NSString localizedStringWithFormat:NSLocalizedString(@"menu_add_new_user",nil) ];
        //-- cell.backgroundColor=[UIColor whiteColor];

    }
    
    cell.textLabel.textAlignment=NSTextAlignmentLeft;
    
    return cell;
}


- (void) prepareForSegue: (UIStoryboardSegue *) segue sender: (id) sender
{
    // Set the title of navigation bar by using the menu items
    // NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    /*
     // Set the photo if it navigates to the PhotoView
     if ([segue.identifier isEqualToString:@"showPhoto"]) {
     PhotoViewController *photoController = (PhotoViewController*)segue.destinationViewController;
     NSString *photoFilename = [NSString stringWithFormat:@"%@_photo.jpg", [menuItems objectAtIndex:indexPath.row]];
     photoController.photoFilename = photoFilename;
     }
     */
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] )
    // if ( [segue.identifier isEqualToString:@"showUserProfile"] )
    {

        
    }
    
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.section==0){
        [self.revealViewController rightRevealToggleAnimated: YES];  // 12.02
    } else if(indexPath.section==2){
        [binsDataController insertNewUser];
        
        [currentFrontViewController viewWillAppear:YES];
        
        [self.revealViewController rightRevealToggleAnimated: YES];  // 12.02
    
    } else if(indexPath.section==1){
        
        int lastActiveUser=[binsDataController getActiveUserID];
    
        int user=[binsDataController getUserIdByName:userList[indexPath.row]];
        if(lastActiveUser!=user){
            int device=[binsDataController getActiveDeviceID];
            if(device>0){
                [deviceController stopSync:device];
            }
        }
        [binsDataController setActiveUserID:user];
        [deviceController resetStatus]; //-- 20150831
        
        [currentFrontViewController viewWillAppear:YES];

        
    
        [self.revealViewController rightRevealToggleAnimated: YES];  // 12.02
        

        /*
         
         binsTabBarViewController *tabBar=(binsTabBarViewController*)(self.revealViewController.frontViewController);
         
         UIView * fromView = tabBar.selectedViewController.view;
         
         [tabBar.selectedViewController.view reloadInputViews];
         
         UIView * toView = [[tabBar.viewControllers objectAtIndex:3] view];
         
         // Transition using a page curl.
         [UIView transitionFromView:fromView
         toView:toView
         duration:0.5
         options:(2 > tabBar.selectedIndex ? UIViewAnimationOptionTransitionCurlUp : UIViewAnimationOptionTransitionCurlDown)
         completion:^(BOOL finished) {
         if (finished) {
         tabBar.selectedIndex = 3;
         }
         }];
         
       // UIViewController *vcNew = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"userProfile"];
 
        UIViewController *vcNew = [[UIStoryboard storyboardWithName:@"Main" bundle:NULL] instantiateViewControllerWithIdentifier:@"moreNavigationController"];

        // Swap out the Front view controller and display
        [self.revealViewController setFrontViewController:vcNew];
        [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        // [self.revealViewController rightRevealToggleAnimated: YES];  // 12.02
        
        */

    }
    
    
    [tableView deselectRowAtIndexPath:indexPath animated: YES];
    
    [self updateUserList];
    
    [tableView reloadData];
}



@end
