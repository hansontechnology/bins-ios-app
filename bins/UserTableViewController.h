//
//  UserTableViewController.h
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DoneCancelNumberPadToolbar.h"
#import "MIRadioButtonGroup.h"
#import "SWRevealViewController.h"
#import "binsHelper.h"
#import "DevicePairView.h"
#import "UnlockingLifeService.h"
#import "BinsCloudService.h"


@interface UserTableViewController : UITableViewController <UITextFieldDelegate, UIActionSheetDelegate, DoneCancelNumberPadToolbarDelegate, UIPickerViewDelegate, RadioButtonGroupDelegate, UIActionSheetDelegate, DevicePairDelegate, SWRevealViewControllerDelegate,UnlockingLifeServiceDelegate, BinsCloudServiceDelegate, BinsDataControllerDelegate>


- (IBAction)weightTextEditingDidEnd:(id)sender;

- (IBAction)weightTextTouchUpInside:(id)sender;

- (IBAction)heightTextEditingDidEnd:(id)sender;
- (IBAction)heightTextTouchUpInside:(id)sender;

- (IBAction)nameTextEditingDidEnd:(id)sender;

- (IBAction)dobTextTouchUpInside:(id)sender;
- (IBAction)dobTextTouchDown:(id)sender;
- (IBAction)dobTextEditingDidBegin:(id)sender;
- (IBAction)saveButtonTouchUpInside:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *dobTextField;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIView *genderView;


-(void)doneButtonPressed;

//-- -(IBAction)showUnitActionSheet:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIView *unitView;

@property (weak, nonatomic) IBOutlet UIView *unitSetView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;


@property DevicePairView *devicePair;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *pairRunningIndicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *userSelectButton;
- (IBAction)userDeleteTouchUpInside:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *userDeleteBUtton;
@property (weak, nonatomic) IBOutlet UIButton *userDeleteButton;


@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizerRight;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
- (IBAction)emailTextFieldEditingDidEnd:(id)sender;
- (IBAction)emailTextFieldEditingBegin:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
- (IBAction)passwordTextEditDidEnd:(id)sender;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *cloudAccessIndicator;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *cloudLoadIndicator;

@end
