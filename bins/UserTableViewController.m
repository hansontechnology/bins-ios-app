//
//  UserTableViewController.m
//  bins
//
//  Created by Dennis Kung on 10/28/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import "UserTableViewController.h"
#import "BinsDataViewController.h"
#import "UIView+Toast.h"



@interface UserTableViewController ()

@end

@implementation UserTableViewController

@synthesize nameTextField;
@synthesize weightTextField;
@synthesize heightTextField;
@synthesize dobTextField;
@synthesize emailTextField;
@synthesize lastNameTextField;
@synthesize saveButton;
@synthesize devicePair;
@synthesize pairRunningIndicator;
@synthesize userDeleteButton;
@synthesize passwordTextField;
@synthesize cloudAccessIndicator;
@synthesize cloudLoadIndicator;


BinsDataViewController *binsData;
UnlockingLifeService *unlockingLifeServer;
static BinsCloudService *binsCloudServer;

int genderBuffer;
int weightBuffer;
int heightBuffer;
NSString *dobBuffer;
NSString *userNameBuffer;
NSString *passwordBuffer;

MIRadioButtonGroup *genderRadioGroup;

int currentUserID=INVALID_USER_ID;

int userGender=0; // means man;

BOOL mIsNetworkUser=NO;
NSString *mUserUID=nil;
long mUserLastSyncTime=0;

BOOL mCloudUserExist=NO;

#define STATUS_DONTCARE 2
#define STATUS_YES 1
#define STATUS_NO 0
int mSaveToCloudCompleted=STATUS_DONTCARE;
int mDevicePairCompleted=STATUS_DONTCARE;


#define HEIGHT_IPHONE5 568 // 12.22

static int heightOfPhone;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    devicePair=[[DevicePairView alloc] init];
    devicePair.delegate=self;
    
    binsData=[[BinsDataViewController alloc] init];
    binsData.delegate=self;

#ifdef CONFIG_BINS_HBP
    unlockingLifeServer=[[UnlockingLifeService alloc] init];
    unlockingLifeServer.delegate=self;
#else
    binsCloudServer=[[BinsCloudService alloc] init];
    binsCloudServer.delegate=self;
#endif
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
   
    weightTextField.delegate=self;
    heightTextField.delegate=self;
    nameTextField.delegate=self;
    dobTextField.delegate=self;
    emailTextField.delegate=self;
    lastNameTextField.delegate=self;
    passwordTextField.delegate=self;
    
    DoneCancelNumberPadToolbar *weightDoneCancelToolbar = [[DoneCancelNumberPadToolbar alloc] initWithTextField:weightTextField];
    weightDoneCancelToolbar.delegate = self;
    weightTextField.inputAccessoryView = weightDoneCancelToolbar;

    DoneCancelNumberPadToolbar *heightDoneCancelToolbar = [[DoneCancelNumberPadToolbar alloc] initWithTextField:heightTextField];
    heightDoneCancelToolbar.delegate = self;
    heightTextField.inputAccessoryView = heightDoneCancelToolbar;

    DoneCancelNumberPadToolbar *passwordDoneCancelToolbar = [[DoneCancelNumberPadToolbar alloc] initWithTextField:passwordTextField];
    passwordDoneCancelToolbar.delegate = self;
    passwordTextField.inputAccessoryView = passwordDoneCancelToolbar;

    
    
    // Change button color
    _sidebarButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _sidebarButton.target = self.revealViewController;
    _sidebarButton.action = @selector(revealToggle:);
   
    
    // Change button color
    _userSelectButton.tintColor = [UIColor colorWithWhite:0.9f alpha:0.8f];
    
    // Set the side bar button action. When it's tapped, it'll show up the sidebar.
    _userSelectButton.target = self; //-- self.revealViewController;
    _userSelectButton.action = @selector(userSelectMenu); //-- )(rightRevealToggle:);
    
    
    // Set the gesture
    //-- [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    
    // saveButton.frame = CGRectMake(100, 200, 44, 44);
    saveButton.layer.cornerRadius = 10.0;
    
    saveButton.layer.borderWidth = 3.0;

    // saveButton.layer.borderColor = [UIColor blueColor].CGColor;
    
    saveButton.layer.borderColor = [UIColor grayColor].CGColor;
    // Rgb2UIColor(99, 184, 255, 1 ).CGColor;
    saveButton.clipsToBounds = YES;
    saveButton.backgroundColor=Rgb2UIColor(240, 255, 255, 0.7 ); // 12.23 Azure, [UIColor lightGrayColor];

    userDeleteButton.layer.cornerRadius = 10.0;
    
    userDeleteButton.layer.borderWidth = 3.0;
    
    // saveButton.layer.borderColor = [UIColor blueColor].CGColor;
    
    userDeleteButton.layer.borderColor = [UIColor grayColor].CGColor;
    // Rgb2UIColor(99, 184, 255, 1 ).CGColor;
    userDeleteButton.clipsToBounds = YES;
    userDeleteButton.backgroundColor=Rgb2UIColor(240, 255, 255, 0.7);

    
    
    NSArray *options =[[NSArray alloc]initWithObjects:[NSString localizedStringWithFormat:NSLocalizedString(@"setting_female",nil) ],[NSString localizedStringWithFormat:NSLocalizedString(@"setting_male",nil) ],nil];
    MIRadioButtonGroup *group =[[MIRadioButtonGroup alloc]initWithFrame:CGRectMake(0, 0, 250, 70) andOptions:options andColumns:4];
    [self.genderView addSubview:group];
    group.delegate=self;
    
    genderRadioGroup=group;
    
    
    
    self.tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(revealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizer];
    self.tapGestureRecognizer.enabled = NO;
    
    self.tapGestureRecognizerRight = [[UITapGestureRecognizer alloc] initWithTarget:self.revealViewController action:@selector(rightRevealToggle:)];
    [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
    self.tapGestureRecognizerRight.enabled = NO;
    
    self.revealViewController.delegate=self;
    
    heightOfPhone=self.view.frame.size.height;
    
}

-(void)userSelectMenu{
    //-- 20151120 should not responsible to save here, [self saveUserData];
    //-- a comparison and notice for a save may be required ?
    [self.revealViewController rightRevealToggle:self];
}

-(void)loadUserData{
    
    int gender;
    int weight;
    int height;
    NSString *dob;
    NSString *userName;
    NSString *lastName;
    NSString *firstName;
    NSString *password=@"123456";
    long lastSyncTime;
    
    NSString *userUID;
    
    currentUserID=[binsData getActiveUserID];
    
    NSLog(@"LoadUserData user id=%d", currentUserID);
    
    
    [binsData getUserProfile: currentUserID userNameIs: &userName firstNameIs: &firstName lastNameIs: &lastName dobIs: (&dob) weightIs: (&weight) heightIs: (&height) genderIs: (&gender) withPassword: (&password) isRemoteUser: (&mIsNetworkUser)  uidIs: (&userUID) lastSyncTimeIs: (&lastSyncTime)];
    
    
    if(currentUserID!=INVALID_USER_ID){
        [self.saveButton setEnabled:YES];
        mCloudUserExist=YES; // locally available means the cloud is existing
    }else{
        [self.saveButton setEnabled:NO];
        mCloudUserExist=NO;
    }
    
    mUserUID=userUID;
    mUserLastSyncTime=lastSyncTime;
    
    nameTextField.text=firstName;
    lastNameTextField.text=lastName;
    emailTextField.text=userName;
    if([self isValidEmail:emailTextField.text]==YES){
        emailTextField.enabled=NO;
        emailTextField.textColor=[UIColor grayColor];
    }else{
        emailTextField.enabled=YES;
    }
    
    dobTextField.text=dob;
    weightTextField.text=[NSString stringWithFormat:@"%d",weight];
    heightTextField.text=[NSString stringWithFormat:@"%d",height];
    
    userGender=gender;
    
    [self saveToBuffer];
    
    [genderRadioGroup setSelected:userGender];
    
    //-- 20151119 TODO conditions required
    
    mIsNetworkUser=[binsData userIsRemoteUser: currentUserID];
    passwordTextField.text=password;
    
    
    [self setEditFieldMode];
    
    /*
    if(mIsNetworkUser==YES){
        [self.passwordTextField setHidden: NO];
    }else{
        [self.passwordTextField setHidden: YES];
    }\*/
    
}

-(void)viewWillAppear:(BOOL) animated {
    
    [super viewWillAppear:animated];
    
    [self loadUserData];
    
}


- (void)radioButtonGroup:(MIRadioButtonGroup*)radioButtonGroup buttonClicked:(UIButton*)button {
    
    NSLog(@"radio button %@", button.titleLabel.text);
    
    NSString *radioButtonStr=button.titleLabel.text;
    
    if([radioButtonStr isEqual:NSLocalizedString(@"setting_man",nil)]){
        userGender=1;
    }
    else{
        userGender=0;
    }
    [self saveToBuffer];
    
}

-(void)radioButtonGroup:(MIRadioButtonGroup *)radioButtonGroup clicked:(int)index {
    userGender=index;
    [self saveToBuffer];
    
}

-(void)saveToBuffer {
    genderBuffer=userGender;
    weightBuffer=[weightTextField.text intValue];
    heightBuffer=[heightTextField.text intValue];
    dobBuffer=[[NSString alloc] initWithString:dobTextField.text];
    userNameBuffer=[[NSString alloc] initWithString:nameTextField.text];
    passwordBuffer=[[NSString alloc] initWithString:passwordTextField.text];
}

-(void)loadFromBuffer {
    userGender=genderBuffer;
    heightTextField.text=[NSString stringWithFormat:@"%d",heightBuffer];
    weightTextField.text=[NSString stringWithFormat:@"%d",weightBuffer];
    dobTextField.text=[[NSString alloc] initWithString:dobBuffer];
    nameTextField.text=[[NSString alloc] initWithString:userNameBuffer];
    passwordTextField.text=[[NSString alloc] initWithString:passwordBuffer];
}


-(void)doneButtonPressed: (UITextField*)tfield {
    
}
-(void)cancelButtonPressed: (UITextField*)tfield {
    
}

#pragma mark - number pad delegate

-(void)doneCancelNumberPadToolbarDelegate:(DoneCancelNumberPadToolbar *)controller didClickDone:(UITextField *)textField
{
    NSLog(@"%@", textField.text);
    [self saveToBuffer];
   
}

-(void)doneCancelNumberPadToolbarDelegate:(DoneCancelNumberPadToolbar *)controller didClickCancel:(UITextField *)textField
{
    NSLog(@"Canceled: %@", [textField description]);
    
    [self loadFromBuffer];
}


-(void)updateTextFieldFromDatePicker:(id)sender {
    NSLog(@"updateTextField");
    
    if([dobTextField isFirstResponder]){
        UIDatePicker *picker = (UIDatePicker*)dobTextField.inputView;
        
        NSDateFormatter * dateFormats= [[NSDateFormatter alloc] init];
        
        [dateFormats    setDateFormat:@"YYYY'/'MM'/'dd"]; //ex @"MM/DD/yyyy hh:mm:ss"
       dobTextField.text=[dateFormats stringFromDate:picker.date];
   /*
        dobTextField.text = [NSDateFormatter
                             
                             
                             
                             localizedStringFromDate:picker.date
                                       dateStyle:NSDateFormatterShortStyle
                                       timeStyle:NSDateFormatterNoStyle];
    
    */
        dobTextField.enabled=YES;
        
        [dobTextField becomeFirstResponder];
    }
    
}


-(void)doneButtonPressed {
    
}


-(void)cancelButtonPressed {
    
}

UIActionSheet *pickerViewPopup;

-(void)textFieldDidBeginEditing:(UITextField *)textField {
 
    
    if ([nameTextField isFirstResponder]){
        NSLog(@"name edit end");
        
    }
    
//    if ([dobTextField isFirstResponder]){
//
//    }
   
    
}

-(void)textFieldEditingDidEnd:(UITextField *)textField {
    
    
    if ([nameTextField isFirstResponder]){
        NSLog(@"name edit end");
        
    }
    
    //    if ([dobTextField isFirstResponder]){
    //
    //    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

-(BOOL)tableView: (UITableView*)tableView shouldHighlightRowAtIndexPath:(NSIndexPath*)indexPath {
    
    if(indexPath.row==1) return YES;
    return NO;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    int activeHeightSection=(int)(40.0*(heightOfPhone-100)/(HEIGHT_IPHONE5-100));

    if(activeHeightSection<40) activeHeightSection=40;
    
    if(section==4) activeHeightSection=5;
    
    return activeHeightSection;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int activeHeightTableCell=(int)(60.0*(heightOfPhone-100)/(HEIGHT_IPHONE5-100));
    
    if(activeHeightTableCell<60) activeHeightTableCell=60;

    return activeHeightTableCell;
    
}

UIFont *gHeaderViewFont=nil;

-(void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    // Background color
    // view.tintColor = [UIColor colorWithRed:0.329 green:0.557 blue:0.827 alpha:1.000];
    
    // Text Color & Alignment
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    // [header.textLabel setTextColor:[UIColor whiteColor]];
    // [header.textLabel setTextAlignment:NSTextAlignmentCenter];
    // Text Font
    // UIFont *saveFont = header.textLabel.font;
    
    UIFont *saveFont = [UIFont systemFontOfSize:15.0f];
    [header.textLabel setFont:[UIFont fontWithName:saveFont.fontName size:15.0]];
    gHeaderViewFont=saveFont;
    
    
    // Another way to set the background color
    // Note: does not preserve gradient effect of original heade!r
    // header.contentView.backgroundColor = [UIColor blackColor];
}

-(NSString*)titleForHeadersinSection: (NSString*)part1 with: (NSString*)part2 {
    
    NSMutableString *strWhole=[[NSMutableString alloc] initWithString:part1];
    
    float displayWidth=self.view.frame.size.width;
    
    //-- NSLog(@"table section witdth:%f", displayWidth);
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:gHeaderViewFont, NSFontAttributeName, nil];
    
    float strPart1Width=[[[NSAttributedString alloc] initWithString:part1 attributes:attributes] size].width;
    
    // float strPart2Width=[[[NSAttributedString alloc] initWithString:part2 attributes:attributes] size].width;
    
    float strSpaceWidth=[[[NSAttributedString alloc] initWithString:@" " attributes:attributes] size].width;
    
    int noAdditionalSpace=((displayWidth/2)-strPart1Width)/strSpaceWidth+1;
    
    
    //-- NSLog(@"table section string witdth:%f, %f",strPart1Width, strPart2Width);
    
    for(int i=0; i<noAdditionalSpace; i++){
        [strWhole appendString:@" "];
    }
    
    
    //int noAdditionalSpace=10+(tableView.frame.size.width-320)/16*4;
    
    // for(int i=0; i<noAdditionalSpace; i++){
    //    [part1 appendString:@" "];
    // }
    
    [strWhole appendString:part2];

    return strWhole;
}

- (NSString *) tableView:(UITableView *)tableView
 titleForHeaderInSection:(NSInteger)section{
    
    NSString *result = nil;
    
    if ( section == 0) result=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_name",nil) ];
    else if(section==1){
        NSString *part1=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_dayofbirth",nil) ];
        NSString *part2=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_gender",nil) ];
        result=[self titleForHeadersinSection:part1 with:part2];

    
    } else if(section==2){
        NSString *part1=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_height",nil) ];
        NSString *part2=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_weight",nil) ];
        result=[self titleForHeadersinSection:part1 with:part2];
        
    } else if(section==3){
        NSString *part1=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_email",nil) ];
        NSString *part2=[NSString localizedStringWithFormat:NSLocalizedString(@"setting_password",nil) ];
        result=[self titleForHeadersinSection:part1 with:part2];
    }

    return result;
    
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - text field delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if([dobTextField isFirstResponder]) return NO;
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    
    return NO;
/*
    if (action == @selector(paste:))
        return NO;
    return [super canPerformAction:action withSender:sender];
*/
}



#pragma mark

- (IBAction)weightTextEditingDidEnd:(id)sender {
    
    NSLog(@"weightTextEditingEnd");
    
    int value=[weightTextField.text intValue];
    if(value==0 || value>300){
        if(weightBuffer==0) weightBuffer=70;
        
        [self loadFromBuffer];
        return;
    }
    
}

- (IBAction)weightTextTouchUpInside:(id)sender {
    
    NSLog(@"weightTextTouchUpInside");
    
 
    
}

- (IBAction)heightTextEditingDidEnd:(id)sender {
    
        NSLog(@"heightTextEditingEnd");
    
    int value=[heightTextField.text intValue];
    if(value==0 || value>300){
        if(heightBuffer==0) heightBuffer=170;

        [self loadFromBuffer];
        return;
    }
}

- (IBAction)heightTextTouchUpInside:(id)sender {
    
           NSLog(@"heightTextTouchUpInside");
}

- (IBAction)nameTextEditingDidEnd:(id)sender {
    if([nameTextField.text isEqual:@""]){
        [self loadFromBuffer];
    }
}


- (IBAction)dobTextTouchUpInside:(id)sender {

    NSLog(@"dobTextTouchUpInside");
    
    //[dobTextField resignFirstResponder];
    [dobTextField becomeFirstResponder];
    [dobTextField.inputView setHidden:YES];
}

- (IBAction)dobTextTouchDown:(id)sender {
    
    NSLog(@"dobTextTouchDown");
    [dobTextField resignFirstResponder];

}

-(void)cancelPicker {
    [self loadFromBuffer];
    
    [dobTextField resignFirstResponder];
}
-(void)donePicker {
    NSLog(@"dob picker done");
    [dobTextField resignFirstResponder];

}



- (IBAction)dobTextEditingDidBegin:(id)sender {
    
 
    UIDatePicker *datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 100, 320, 116)];
    
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setDate:[NSDate date]];
    [datePicker addTarget:self action:@selector(updateTextFieldFromDatePicker:) forControlEvents:UIControlEventValueChanged];
    
    [UIView appearanceWhenContainedIn:[UITableView class], [UIDatePicker class], nil].backgroundColor = [UIColor colorWithWhite:1 alpha:1];
    
    datePicker.backgroundColor= [UIColor darkGrayColor];  // ]darkGrayColor];
    
    
    NSDateFormatter * dateFormats= [[NSDateFormatter alloc] init];
    
    [dateFormats    setDateFormat:@"YYYY'/'MM'/'dd"]; //ex @"MM/DD/yyyy hh:mm:ss"
    NSDate *mDate=[dateFormats dateFromString:dobTextField.text];
    NSLog(@"edit %@",mDate);
    
    datePicker.date = mDate;
    

    
    //-- tool bar
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    
    UIBarButtonItem *cancel = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel
                                                                          target:self
                                                                          action:@selector(cancelPicker)];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                           target:nil
                                                                           action:nil];
    
    UIBarButtonItem *done = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                          target:self
                                                                         action:@selector(donePicker)];

    toolBar.items = [NSArray arrayWithObjects:cancel,space,done,nil];
    toolBar.backgroundColor=[UIColor yellowColor];
    dobTextField.inputAccessoryView = toolBar;
    
    
    
    [dobTextField setInputView:datePicker];
  
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)saveUserData {
    
    //if([nameTextField.text isEqualToString:@""] ||
    //   [nameTextField.text isEqualToString:@"Me"]){
    //    return NO;
    //}
        
    if([self isValidEmail:emailTextField.text]==NO){
        NSLog(@"save user data email edit invalid address");
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_invalid_username",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
        
        [alert show];

        return NO;
    }

    
    if([binsData hasUserExistingName:emailTextField.text]){
        
       if([binsData getUserIdByName:emailTextField.text]!=currentUserID){
        
           UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_username_used",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
           
           [alert show];

           return NO;
       }
    }

    
    BOOL isSuccessCallCloud=NO;
    binsData.delegate=self;
    if(currentUserID==INVALID_USER_ID){
        
        isSuccessCallCloud=[binsData insertNewUser:emailTextField.text
                    firstNameIs: nameTextField.text
                    lastNameIs: lastNameTextField.text
                    dobIs:dobTextField.text
                    weightIs:[weightTextField.text intValue]
                    heightIs:[heightTextField.text intValue]
                    genderIs:userGender
                    withPassword:passwordTextField.text    //-- TODO 20151119
                    isRemoteUser:mIsNetworkUser
                           uidIs:mUserUID
                  lastSyncTimeIs:mUserLastSyncTime
                    ];
    } else{
        isSuccessCallCloud=[binsData setUserProfile: currentUserID userNameIs: emailTextField.text firstNameIs: nameTextField.text
                      lastNameIs: lastNameTextField.text
                           dobIs: dobTextField.text
                        weightIs: [weightTextField.text intValue]
                        heightIs: [heightTextField.text intValue]
                        genderIs: userGender
                    withPassword: passwordTextField.text
                    isRemoteUser: mIsNetworkUser
                  lastSyncTimeIs: mUserLastSyncTime

                    ];
        
    }
    if(isSuccessCallCloud==YES) mSaveToCloudCompleted=STATUS_NO;
        
    return YES;
        
}

//-- 20151119
- (IBAction)passwordTextEditDidEnd:(id)sender {
    NSLog(@"passwordTextEditDidEnd");
    
    if(mCloudUserExist==NO) return;
    
    BOOL isSuccess=NO;
    
    binsCloudServer.delegate=self;
    isSuccess=[binsCloudServer loadUserProfile:emailTextField.text withPassword:passwordTextField.text];
    
    if(isSuccess==YES){
        [cloudLoadIndicator startAnimating];
    }

}

- (IBAction)saveButtonTouchUpInside:(id)sender {
 
    if([self saveUserData]==NO){
        
        return; // not a successful save
    }

#if 1
    
    if([binsData getActiveDeviceID]==0 && mIsNetworkUser==NO){
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        UIView *topView = window.rootViewController.view;
        
        [topView makeToast:NSLocalizedString(@"button_press",nil)
                  duration:3.5
                  position:CSToastPositionCenter];
        
        pairRunningIndicator.color = [UIColor grayColor];
        [pairRunningIndicator startAnimating];
        
        [saveButton setTitle:NSLocalizedString(@"message_pairing",nil) forState:UIControlStateNormal];

        [saveButton setEnabled:NO];
        
        mDevicePairCompleted=STATUS_NO;
        [devicePair autoPair];
        
        [self.tableView reloadData];
    }
    else{
        [self chanceToExit];
    }

#else
    
#endif
    

}

-(void)chanceToExit {
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if((mSaveToCloudCompleted==STATUS_YES || mSaveToCloudCompleted==STATUS_DONTCARE)
           &&
           (mDevicePairCompleted==STATUS_YES || mDevicePairCompleted==STATUS_DONTCARE) ){
            
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            //-- reuse this indicator to show until finish
            [pairRunningIndicator startAnimating];
        }
    });
}

-(void)devicePair:(DevicePairView *)devicePair pairToDevice:(NSUUID *)deviceIdentifier {
    
    NSLog(@" main paired to device:%@",deviceIdentifier);    
    
    if(deviceIdentifier!=nil){
        [binsData setActiveDeviceAddress: deviceIdentifier];
        
    }else{
        
        //-- no need to stop until exit [pairRunningIndicator stopAnimating];

        mDevicePairCompleted=STATUS_YES;
        [self chanceToExit];
    }
    
}


-(void)devicePair:(DevicePairView *)devicePair foundDevice: (NSUUID*)deviceIdentifier withStrength:(int)rssi {
    
    pairRunningIndicator.color = [UIColor blueColor];
    
}

-(void)devicePair:(DevicePairView *)devicePair selectingDevice: (NSString*)ibeaconUuid withId: (int)deviceId {

    if(ibeaconUuid!=nil) [binsData setIbeaconUuid: ibeaconUuid];

    //-- [pairRunningIndicator stopAnimating];
    
    mDevicePairCompleted=STATUS_YES;
    [self chanceToExit];

}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:NSLocalizedString(@"user_edit_delete",nil)])
    {
        NSLog(@"Delete user");
        
        [binsData deleteUserIfPossible:currentUserID];
        
        [self loadUserData];

    }else if([title isEqualToString:NSLocalizedString(@"use_remote",nil)]){
        NSLog(@"Use network user");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"enter_password",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles: nil];
        
        [alert show];
        
        mIsNetworkUser=YES;
        
        //-- 20151119 TODO Temporarilly read user profile from network start here
        [passwordTextField becomeFirstResponder];
        
    }else if([title isEqualToString:NSLocalizedString(@"use_local",nil)]){
        NSLog(@"Use as local user");
        
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"enter_password",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles: nil];
        
        [alert show];
        */
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        UIView *topView = window.rootViewController.view;
        
        [topView makeToast:NSLocalizedString(@"enter_password",nil)
                  duration:3.5
                  position:CSToastPositionCenter];

        mIsNetworkUser=NO;

        [passwordTextField becomeFirstResponder];
        
        
    }else if([title isEqualToString:NSLocalizedString(@"confirm_cancel",nil)]){
        
        emailTextField.text=strLastUserName;
        mIsNetworkUser=NO;
    }

    
}

- (IBAction)userDeleteTouchUpInside:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice",nil) message:NSLocalizedString(@"data_will_be_deleted", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"confirm_cancel",nil) otherButtonTitles:NSLocalizedString(@"user_edit_delete",nil), nil];
    
    [alert show];
    
    
}

-(void)setEditFieldMode {
    if(mIsNetworkUser==YES){
        [self setEditFields: NO];
    }else{
        [self setEditFields: YES];
    }
}

-(void)setEditFields: (BOOL)newSetting {
    nameTextField.enabled=newSetting;   //-- it is First Name field
    lastNameTextField.enabled=newSetting;
    weightTextField.enabled=newSetting;
    heightTextField.enabled=newSetting;
    dobTextField.enabled=newSetting;
    genderRadioGroup.userInteractionEnabled=newSetting;
}



-(void) userInteractionEnable: (BOOL)enable {
    self.nameTextField.userInteractionEnabled = enable;
    self.weightTextField.userInteractionEnabled = enable;
    self.heightTextField.userInteractionEnabled = enable;
    self.dobTextField.userInteractionEnabled = enable;
    self.saveButton.userInteractionEnabled = enable;
    self.userDeleteButton.userInteractionEnabled = enable;
    genderRadioGroup.userInteractionEnabled = enable;
    self.passwordTextField.userInteractionEnabled= enable;
    self.emailTextField.userInteractionEnabled= enable;
    
    self.tabBarController.tabBar.userInteractionEnabled = enable;
    
}

- (void)revealController:(SWRevealViewController *)revealController willMoveToPosition:(FrontViewPosition)position
{
    if (position == FrontViewPositionLeftSide) {
        // NSLog(@"FrontViewPositionLeftSide");
        // jump to right side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizerRight];
        self.tapGestureRecognizerRight.enabled = YES;
        [self userInteractionEnable:NO];
    }
    else if (position == FrontViewPositionLeft){
        // NSLog(@"FrontViewPositionLeft");
        // back to main screen
        self.tapGestureRecognizer.enabled = NO;
        self.tapGestureRecognizerRight.enabled = NO;
        [self userInteractionEnable:YES];
        [self setEditFieldMode];
    }
    else if (position == FrontViewPositionRight){
        // NSLog(@"FrontViewPositionRight");
        // jump to left side menu
        [self.view addGestureRecognizer:self.tapGestureRecognizer];
        self.tapGestureRecognizer.enabled = YES;
        [self userInteractionEnable:NO];
        
    }
}

NSString *strLastUserName=nil;

- (IBAction)emailTextFieldEditingDidEnd:(id)sender {
    
    NSLog(@"email edit end");

    if([self isValidEmail:emailTextField.text]==NO){
        [self.saveButton setEnabled:YES];
        NSLog(@"email edit invalid address");
        emailTextField.text=strLastUserName;
        return;
    }
 
    int activeUserId=[binsData getActiveUserID];
    
    if([binsData hasUserExistingName:emailTextField.text] &&
       [binsData getUserIdByName:emailTextField.text]!=activeUserId){
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        UIView *topView = window.rootViewController.view;
        
        [topView makeToast:NSLocalizedString(@"notice_username_used",nil)
                  duration:3
                  position:CSToastPositionCenter];
        
        emailTextField.text=strLastUserName;
        
        return;
        
    }
        

#ifdef CONFIG_BINS_HBP  //-- 20151112
    
    [self.saveButton setEnabled:NO];

    unlockingLifeServer.delegate=self;
    
    if([unlockingLifeServer checkInternet]==NO){
        // just return, no notice needed, already notice before
        return;
        
    } else if([unlockingLifeServer checkUserExist:emailTextField.text]==NO){
        [self.saveButton setEnabled:YES];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_username_used",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
        
        [alert show];

        emailTextField.text=strLastUserName;
        
    };
    
#else
    //-- 20151119

    [self.saveButton setEnabled:NO];
    
    binsCloudServer.delegate=self;
    
    mCloudUserExist=NO;
    
    if([binsCloudServer checkInternet]==NO){
        // just return, no notice needed, already notice before
        return;
        
    }
#if 1
    else if([binsCloudServer checkUserExist:emailTextField.text]==NO){
        [self.saveButton setEnabled:YES];
        NSLog(@"Impossible to be here!");
        /*
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_username_used",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
        
        [alert show];
        
        emailTextField.text=strLastUserName;
        */
    }
#endif

    [cloudAccessIndicator startAnimating];
    
    //-- [self.saveButton setEnabled:YES];
    
#endif
    
    
}

- (IBAction)emailTextFieldEditingBegin:(id)sender {
    strLastUserName=[[NSString alloc] initWithFormat:@"%@", emailTextField.text];
    
    [self.saveButton setEnabled:NO];
 
}

-(BOOL)isValidEmail: (NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

//-- delegates of UnlockingLifeService

- (void)unlockingLifeService:(UnlockingLifeService *)ulockingService hasUser: (BOOL)isUserExist {

    NSLog(@"email checked returned: %d", isUserExist);

    mIsNetworkUser=NO;
    
    if(isUserExist){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice", nil) message:NSLocalizedString(@"notice_username_used",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
        
        [alert show];

        emailTextField.text=strLastUserName;
    }
    
    [self.saveButton setEnabled:YES];
}

- (void)unlockingLifeService:(UnlockingLifeService *)ulockingService addedNewUser: (BOOL)result userName: (NSString*)userName userId: (int)userId {
    
    NSLog(@"User Table unlocking addedUser called");
    

}

//-- delegates of BinsCloudService

- (void)binsCloudService:(BinsCloudService *)binsCloudService hasUser: (BOOL)isUserExist {
    
    NSLog(@"email checked returned: %d", isUserExist);

    mCloudUserExist=isUserExist;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [cloudAccessIndicator stopAnimating];

        if(isUserExist){
            //-- Can login to remote user data
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice_username_used", nil) message:NSLocalizedString(@"want_network_user",nil) delegate:self cancelButtonTitle:NSLocalizedString(@"use_remote", nil) otherButtonTitles:NSLocalizedString(@"use_local", nil), nil];

            [alert show];  // handled by clickedButtonAtIndex
        }
    
        //-- New user from local
    
        mIsNetworkUser=NO;
    
        [self.saveButton setEnabled:YES];
        
    });

  
}


- (void)binsCloudService:(BinsCloudService *)binsCloudService addedNewUser: (BOOL)result userName: (NSString*)userName userId: (NSString*)userUID {
    
    NSLog(@"User Table binsCloud addedUser called");
    
    
}


-(void)binsCloudService:(BinsCloudService*)binsCloudService loadedUserProfile:(NSString *)userName withUID:(NSString *)userUID firstNameIs:(NSString *)firstName lastNameIs:(NSString *)lastName dobIs:(NSString *)dob weightIs:(int)weight heightIs:(int)height genderIs:(int)gender lastSyncTimeIs:(long)lastSyncTime {
    
    NSLog(@"loadedUserProfile called successfully");
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [cloudLoadIndicator stopAnimating];
        nameTextField.text=firstName;
        lastNameTextField.text=lastName;
        emailTextField.text=userName;
        if([self isValidEmail:emailTextField.text]==YES){
            emailTextField.enabled=NO;
            emailTextField.textColor=[UIColor grayColor];
        }else{
            emailTextField.enabled=YES;
        }
    
        dobTextField.text=dob;
        weightTextField.text=[NSString stringWithFormat:@"%d",weight];
        heightTextField.text=[NSString stringWithFormat:@"%d",height];
    
        userGender=gender;
    
        [self saveToBuffer];
    
        [genderRadioGroup setSelected:userGender];
    
        mUserUID=userUID;
        mUserLastSyncTime=lastSyncTime;
    
        [self setEditFieldMode];
        
        [self.saveButton setEnabled:YES];
    });
}

-(void)binsCloudService:(BinsCloudService*)binsCloudService loadUserProfileFailed: (NSString*)userName {
    
    //-- 20151119 TODO clear password field
    
    dispatch_async(dispatch_get_main_queue(), ^{

        [cloudLoadIndicator stopAnimating];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"notice_login_failed", nil) message:NSLocalizedString(@"notice_wrong_password",nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"confirm_ok", nil) otherButtonTitles:nil];
    
        [alert show];
    
        [self.saveButton setEnabled:NO];
    });
}

//-- binsDataViewController delegates
//-- 20151119
- (void)binsDataChanged:(BinsDataViewController*)binsDataController {
    mSaveToCloudCompleted=STATUS_YES;
    [self chanceToExit];
}

@end
