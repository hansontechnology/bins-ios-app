/******************************************************************************
 *  Copyright (C) Cambridge Silicon Radio Limited 2012-2013
 *  Part of CSR uEnergy SDK 2.2.2
 *  Application version 2.2.2.0
 *
 *  FILE
 *      health_thermo_service.h
 *
 *  DESCRIPTION
 *      Header definitions for Health Thermometer service
 *
 *****************************************************************************/

#ifndef __ACTIVITY_TRACKING_SERVICE_H__
#define __ACTIVITY_TRACKING_SERVICE_H__

/*============================================================================*
 *  SDK Header Files
 *===========================================================================*/


// 08.19 disable #define BINS_CONFIG_COMMAND_SYS_RESET_BIT           0
#define BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_BIT          0
#define BINS_CONFIG_COMMAND_HRD_START_BIT                   1
#define BINS_CONFIG_COMMAND_TOUCH_LOCK_BIT                  2   //-- 08.20


#define BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_MASK (1<< BINS_CONFIG_COMMAND_STEP_COUNTER_RESET_BIT)
#define BINS_CONFIG_COMMAND_HRD_START_MASK      (1<< BINS_CONFIG_COMMAND_HRD_START_BIT)
#define BINS_CONFIG_COMMAND_TOUCH_LOCK_MASK     (1<< BINS_CONFIG_COMMAND_TOUCH_LOCK_BIT)
#define BINS_CONFIG_HRD_ALWAYS_ON_BIT                       7
#define BINS_CONFIG_HRD_ALWAYS_ON_MASK          (1<< BINS_CONFIG_HRD_ALWAYS_ON_BIT)



#define BINS_CONFIG_HEARTRATE_ENABLE_BIT                    6
#define BINS_CONFIG_HEARTRATE_ENABLE_MASK       (1<< BINS_CONFIG_HEARTRATE_ENABLE_BIT)

#define BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_BIT              5
#define BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_MASK (1<< BINS_CONFIG_HEARTRATE2SLEEP_ENABLE_BIT)

#define BINS_CONFIG_STEP_WITH_HRD_ENABLE_BIT                4
#define BINS_CONFIG_STEP_WITH_HRD_ENABLE_MASK   (1<< BINS_CONFIG_STEP_WITH_HRD_ENABLE_BIT)

#define BINS_CONFIG_COMMAND_UI_ONETIME_BIT                  3
#define BINS_CONFIG_COMMAND_UI_ONETIME_BIT_MASK (1<< BINS_CONFIG_COMMAND_UI_ONETIME_BIT)

#define BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT              9
#define BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT_MASK   (1<< BINS_CONFIG_BINSX2_OLED_BRIGHTNESS_BIT)

#define BINS_CONFIG_TOUCH_NOBLINK_BIT                       10
#define BINS_CONFIG_TOUCH_NOBLINK_BIT_MASK   (1<< BINS_CONFIG_TOUCH_NOBLINK_BIT)

#define BINS_CONFIG_SLEEP_MONITORING_ENABLE_BIT             12 //-- 09.26
#define BINS_CONFIG_SLEEP_MONITORING_ENABLE_MASK   (1<< BINS_CONFIG_SLEEP_MONITORING_ENABLE_BIT)

#define BINS_CONFIG_MESSAGE_ALERT_ENABLE_BIT                13 //-- 10.08
#define BINS_CONFIG_MESSAGE_ALERT_ENABLE_MASK   (1<< BINS_CONFIG_MESSAGE_ALERT_ENABLE_BIT)

#define BINS_CONFIG_ACTIVITY_NOTIFY_ENABLE_BIT              14
#define BINS_CONFIG_ACTIVITY_NOTIFY_ENABLE_MASK        (1<<BINS_CONFIG_ACTIVITY_NOTIFY_ENABLE_BIT)


//-- Shake UI threshold spreads on bits [15, 11, 4]

//  09.18
#define BINS_ALERT_FINDER_MASK  (0x80)
#define BINS_ALERT_MESSAGE_MASK (0x40)
#define BINS_ALERT_COMMAND_SET   (0x01)
#define BINS_ALERT_COMMAND_RESET (0x00)



#if 0

#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_BIT      15 //-- 10.10
#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_MASK   (1<< BINS_CONFIG_UI_WAKEUP_THRESHOLD_INCREASE_BIT)

#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_BIT      4 //-- 10.10
#define BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_MASK   (1<< BINS_CONFIG_UI_WAKEUP_THRESHOLD_DECREASE_BIT)

#endif




#define BINS_CONFIG_SETTING_ALL_MASK            (0xFFF0)    //-- 10.10 0xFFF0 to 0x7FE0

// extern int getShakeThresholdFromConfig(uint8 config);  // 10.11
// extern void setShakeThresholdToConfig(int8 threshValue);

#endif /* __HEALTH_THERMO_SERVICE_H__ */
