//
//  binsHelper.h
//  bins
//
//  Created by Dennis Kung on 11/9/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#ifndef bins_binsHelper_h
#define bins_binsHelper_h

#define Rgb2UIColor(r, g, b, a)  [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:(a)]


#endif
