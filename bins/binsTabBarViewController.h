//
//  binsTabBarViewController.h
//  bins
//
//  Created by Dennis Kung on 11/9/14.
//  Copyright (c) 2014 Dennis Kung. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"


@interface binsTabBarViewController : UITabBarController <UITabBarControllerDelegate>

@end
